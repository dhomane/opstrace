# Error Tracking

Error Tracking allows developers to discover and view the errors that their application may be generating.
This guide is meant to provide you with basics of setting up error tracking for your project using different languages as an example.
Error Tracking provided by Gitlab Observability Platform is based on [Sentry SDK](https://docs.sentry.io/).
Check Sentry SDK documentation for more thorough examples of how you can use the Sentry SDK in your application.

## Limitations

Currently only basic support is provided with `capture_exception` as the holding method.
Additional features requests (see this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/340178)) will be added on case by case basis.

## Debugging issues

Majority of languages supported by Sentry expose `debug` option as part of initialization.
This can be helpful when debugging issues with sending errors.
Apart from that, there are options that could allow outputting JSON before it is send to the API.
See the example for Golang to see how it can be done.

## Enabling Error Tracking

### Enabling error tracking itself

Independent of which programming language you use, one needs to enable error tracking for the given Project.
We will use `gitlab.com` instance in this guide.
Please follow the steps below:

* this guide assumes that you already have a project for which you want to enable error tracking.
Please refer to Gitlab's documentation in case you need to create a new one.

* in the given project, go to Settings->Monitor. Expand `Error Tracking` tab:

![MonitorTabPreEnable](../../assets/Monitor_tab-pre-enable.png)

* enable Error Tracking with Gitlab as backend:

![MonitorTabPostEnable](../../assets/Monitor_tab-post-enable.png)

Click on `Save Changes` button.
Copy the DSN string, we will need it later on.

## Listing captured errors

Once your application has emitted errors to the Error Tracking API via the Sentry SDK, they should be available under Monitor > Error Tracking tab/section.

![MonitorListErrors](../../assets/Monitor-list_errors.png)

For more detailed documentation please refer to [GitLab's ErrorTracking documentation](https://gitlab.com/help/operations/error_tracking#error-tracking-list).

Be aware that the above process assumes that GDK feature flag `integrated_error_tracking` is enabled. In case you are running GDK locally and you do not see the option for an error tracking Gitlab backend ensure that you enable it by running the following commands:

```linux
cd <PATH_TO_GDK>
gdk rails console
Feature.enable(:integrated_error_tracking)
```

## Emiting Errors

### Supported Sentry Types

According to the [data model](https://develop.sentry.dev/sdk/envelopes/#data-model) the available item types are:

* [Event](https://develop.sentry.dev/sdk/event-payloads/)
* [Transactions](https://develop.sentry.dev/sdk/event-payloads/transaction/)
* Attachment
* [Session](https://develop.sentry.dev/sdk/sessions/)
* [Sessions](https://develop.sentry.dev/sdk/sessions/)
* [User feedback](https://develop.sentry.dev/sdk/envelopes/#user-feedback) (also known as user report)
* [Client report](https://develop.sentry.dev/sdk/client-reports/)

Items of various types can be sent to the error tracking app either using the Store endpoint or envelope endpoint or both. In the table below you can see a list of all event types available via Sentry SDK. It also mentions which endpoint can be used for ingestion and if it is currently supported by Gitlab Observability Backend.

Notice that items of type events can contain various interfaces like exception, message, stacktrace and template. You can read more about the core data interfaces in [Sentry documentation](https://develop.sentry.dev/sdk/event-payloads/#core-interfaces). Please keep in mind that the table below refers to data items which can be ingested by the Gitlab Observability Backend. It doesn't refer to [Gitlab Observability UI](https://gitlab.com/gitlab-org/opstrace/opstrace-ui#gitlab-observability-ui) features related to these data items.

| Item type | Interface  | Can be sent through the Store endpoint | Can be sent through the Envelope endpoint | Currently Supported |
|---------------|--------------|----------------|----------------|---------------------|
| event         | exception    |  :white_check_mark:      |  :white_check_mark:       | :white_check_mark:           |
| event         | message      |  :white_check_mark:       |  :white_check_mark:       | :white_check_mark:          |
| event         | stacktrace   |  :white_check_mark:       |  :white_check_mark:       | :white_check_mark:          |
| event         | template     |  :white_check_mark:       |  :white_check_mark:       | :x:          |
| transaction   |     NA       |  :x:      |  :white_check_mark:       | :x:           |
| attachment    |     NA       |  :x:      |  :white_check_mark:       | :x:           |
| session       |     NA       |  :x:      |  :white_check_mark:       | :white_check_mark:           |
| sessions      |     NA       |  :x:      |  :white_check_mark:       | :x:           |
| user_report   |     NA       |  :x:      |  :white_check_mark:       | :x:           |
| client_report |     NA       |  :x:      |  :white_check_mark:       | :x:           |

\* NA = Not Applicable

Below you will find examples for different languages.
Each program shows a basic example of how to capture exception with the respective sdk.
For more in-depth documentation please refer to [Sentry SDK's documentation](https://docs.sentry.io/) where you can have information also for different programming languages.

### Supported languages

Currently only a subset of languages is supported.
Table below lists them.

| Sentry SDK  | Supported?  |
| ----------- | ----------- |
| Ruby        | Yes |
| Go          | Yes |
| Javascript  | Yes |
| Java        | Yes |
| Python      | Yes |
| PHP         | Yes |
| .NET        | Not tested |
| Android     | Not tested |
| Apple       | Not tested |
| Perl        | Not tested |

A more up to date version of this matrix can be found [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1737).

### Golang

* chdir into folder `docs/guides/user/error_tracking_examples/go/`
* install dependencies:

```bash
go mod tidy
```

* issue following command:

```bash
export SENTRY_DSN="<DSN string>"
go run main.go <DSN string>
```

Once you run this program, there should be an error visible in the Error tracking tab from `Listing captured errors` section of this document.

### Ruby

* chdir into folder `docs/guides/user/error_tracking_examples/ruby/`
* install dependencies

```bash
gem install bundler
bundle install
```

* execute the example

```bash
export SENTRY_DSN="<DSN string>"
ruby app.rb
```

Once you run this program, there should be an error visible in the Error tracking tab from `Listing captured errors` section of this document.

### PHP

* chdir into folder `docs/guides/user/error_tracking_examples/php/`

* build and run docker container

```bash
export SENTRY_DSN="<DSN string>"
docker build -t sentry-php .
docker run -e SENTRY_DSN --rm sentry-php
```

Once you run this program, there should be an error visible in the Error tracking tab from `Listing captured errors` section of this document.

### Python

* chdir into folder `docs/guides/user/error_tracking_examples/python/`

* install dependencies

```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

* execute the following commands

```bash
export SENTRY_DSN="<DSN string>"
python send_exception.py
```

Once you run this program, there should be an error visible in the Error tracking tab from `Listing captured errors` section of this document.

### Java

* chdir into folder `docs/guides/user/error_tracking_examples/python/`

* execute the following command

```bash
export SENTRY_DSN="<DSN string>"
./gradlew run
```

### NodeJS

* chdir into folder `docs/guides/user/error_tracking_examples/nodejs/`

* install dependencies

```bash
npm install --save @sentry/node @sentry/tracing
```

* execute the following command

```bash
export SENTRY_DSN="<DSN string>"
node ./test.js
```

Once you run this program, there should be an error visible in the Error tracking tab from `Listing captured errors` section of this document.


## Rotating sentry DSN

Sentry DSN (client key) is a secret and it should not be exposed to the public. In case of a leak you can rotate the Sentry DSN by following these steps:

* [Create an access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) by clicking your profile picture in Gitlab.com. Then choose Preferences and then Access Token. Make sure you add api scope.
* Using the [error tracking api](https://docs.gitlab.com/ee/api/error_tracking.html), create a new Sentry DSN:

```bash
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys"
```

* Get the avaialble client keys (Sentry DSNs). Ensure that the newly created Sentry DSN is in place. Then note down the key id of the old client key:

```bash
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys"
```

* Delete the old client key.

```bash
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<your_project_number>/error_tracking/client_keys/<key_id>"
```


