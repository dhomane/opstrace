#!/usr/bin/env python
import sentry_sdk, os, sys

def init_sentry(dsn: str) -> None:

    sentry_sdk.init(
        dsn=dsn,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
        debug=True,
        attach_stacktrace=True,
    )


def failing_function() -> None:
    raise Exception("An exception")

if __name__ == "__main__":
    dsn = os.getenv("SENTRY_DSN")
    if dsn is None:
        print("empty dsn; exiting")
        sys.exit(1)

    init_sentry(dsn)
    try:
        failing_function()
    except Exception as e:
        sentry_sdk.capture_exception(e)

