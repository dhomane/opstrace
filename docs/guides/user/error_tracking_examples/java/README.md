## Test project that sends exceptions using sentry SDK

Related [issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1737)


## Instructions

```bash

DSN=http://sample_dsn ./gradlew run

```
