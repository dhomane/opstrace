# Generating and Sending Traces using OpenTelemetry

This [docker-compose.yaml](../../../test/tracing/docker-compose.yaml) can be used to generate traces and send them to a GitLab namespace.

## Prerequisites

You must have a GitLab Observability instance running and have a namespace provisioned for Observability. See the [QuickStart](../../quickstart.md)
for more details.

This document assumes that GitLab Observability is running locally on your machine. If you're using a remote instance,
replace `http://localhost` with the URL to your instance.

Navigate to your namespace in GitLab Observability and create an API Key.

![api_key](../../assets/create-token.png)

Copy the created Key:

![key](../../assets/key.png)

## Getting started

First install a small tool to help us workaround an issue of the OTLP exporter trying to send data to the
loopback interface.

```bash
go install github.com/axetroy/forward-cli/cmd/forward@latest
```

or alternatively (you will need admin access rights)

```bash
curl -fsSL https://github.com/release-lab/install/raw/v1/install.sh | bash -s -- -r=axetroy/forward-cli -e=forward
```

Start the reverse proxy with any port of your choosing.

```bash
forward --port=4000 http://localhost:80
```

You should get output like the following:

```bash
forward --port=4000 http://localhost:80
2022/05/19 12:13:06 Proxy 'http://192.168.1.47:4000' to 'http://localhost:80'
```

Copy the address of the forwarder ([http://192.168.1.47:4000](http://192.168.1.47:4000) in the example above).


Take the key and the namespace which the key was created in and let's start the docker services that will
generate and send traces to this namespace. Make sure you have both docker-compose.yaml and otel-config.yaml files before running the following command:

```bash
GITLAB_O11Y_ADDRESS=http://192.168.1.47:4000 \
NAMESPACE=13490748 \
NAMESPACE_TOKEN=eyJrIjoiTTdqekNOV0JIOHpvMjRsRFlXb1MxcVVaNHNCZFVucmMiLCJuIjoiZGVtbyIsImlkIjoxMzQ5MDc0OH0= \
docker compose up
```

Open [http://localhost:8080](http://localhost:8080) and click some buttons. This will create traces and they will be sent
to your namespace.


Navigate to the explore view in your namespace and select Jaeger as the datasource. You should see
traces which you can select and view.

![traces](../../assets/traces.png)

