# Administrator Guide

This document is meant to show how to perform the most typical maintenance and debugging.

## Running Terraform locally

Normally, Terraform should be run on staging and production environments by CI, after a MR was merged to one of infrastructure repos.
The instructions here outline steps to use in exceptional cases for these two environments.
In case of dev environment, there is no such requirement.

### Obtaining GCP credentials

The first step is to log into your Google Cloud Account using Google Cloud SDK.
The description of different Gitlab Observability Stack (GOP) environments can be found [here](../../architecture/infrastructure.md).

After [installing Google Cloud SDK](https://cloud.google.com/sdk/docs/install), issue:

```bash
gcloud auth login
gcloud config set project opstrace-...
```

you will be redirected to your browser.
Follow the instructions on the screen.

Then, one needs to create a service account used by Terraform or add a new set of credentials to an existing one.
Please follow steps outlined [here](https://ops.gitlab.net/opstrace-realm/environments/gcp/opstrace-stg-a8655950/opstrace-staging/-/wikis/set-up-credentials).

Now we need to obtain the kubeconfig of the cluster Git is running (see [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1854) why).
Detailed instructions can be found [here](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#view_kubeconfig).
TLDR:

```bash
gcloud container clusters list
export KUBECONFIG=kubeconfig
gcloud container clusters get-credentials opstrace-... --region ...
```

### Obtaining API token from the Gitlab instance

Next step is obtaining an API token from the Gitlab instance.
Instructions can be found [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
The token should be exported as environment variable.

```bash
export GITLAB_ACCESS_TOKEN=...
```

### Inintializing Terraform's remote state

We are using [Gitlab's remote state] feature for keeping Terraform state.
In order to be able to run terraform locally, one needs to initialize the local state.
Follow the instructions from [here](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#access-the-state-from-your-local-machine) to initialize it.

### Running Terraform

And finally, we can start issuing terraform commands:

```bash
terraform plan -var 'kubeconfig_path=<path to kubeconfig>'
```

## Releasing new version into given environment

In the git repository for the given environment, one needs to find and bump all versions of TF modules from GOP repo that the given environment's TF modules refer to, plus update the scheduler image.

```bash
$ git -P grep gitlab-org/opstrace/opstrace
main.tf:  source = "git::https://gitlab.com/gitlab-org/opstrace/opstrace.git//terraform/modules/gke?ref=0.1.5"
main.tf:  source = "git::https://gitlab.com/gitlab-org/opstrace/opstrace.git//terraform/modules/opstrace?ref=0.1.5"
main.tf:  scheduler_image        = "registry.gitlab.com/gitlab-org/opstrace/opstrace/scheduler:0.1.5"
main.tf:  source = "git::https://gitlab.com/gitlab-org/opstrace/opstrace.git//terraform/modules/opstracecluster?ref=0.1.5"
```

So in this case, if were were releasing version `2.0.0.`, we would need to change occurrences of `0.1.5` to `2.0.0` in the files above.

Once this is done, one can apply the changes manually via `terraform plan` + `terraform apply` if necessary, but these changes will be reverted by CI sooner or later.
The preferred way is to simply create an MR, review it and merge it.
The CI pipeline will take care of applying the changes itself once the MR lands.

## Rotating secrets

The tokens are stored in a secret which in turn is referenced in Gatekeeper's deployment.

Steps are as follows:

* create a change request for Infra team in gl-infra/production. A sample issue for reference can be found [here](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7625).
* edit the CI variables, as the authentication variables are stored there.
* the CI job needs to be run to apply the changes and update the secret on the target k8s cluster.
* rotate the secret for the cookies in order to invalidate all currently issued session cookies and thus force all the users to re-login:

```bash
$ kubectl get secret session-cookie-secret  -o yaml
apiVersion: v1
data:
  COOKIE_SECRET: UEEtRVhXcks4SW8tZFllWW5SZU1TcWRQQlJja3FqNTZSOWFpOEt3eDFpd21VY2VlbFRBMlRWc2xpTHl2Y3hDZkItdz0=
kind: Secret
metadata:
  creationTimestamp: "2022-08-23T07:48:11Z"
  name: session-cookie-secret
  namespace: default
  ownerReferences:
  - apiVersion: opstrace.com/v1alpha1
    blockOwnerDeletion: true
    controller: true
    kind: Cluster
    name: dev-cluster
    uid: 98cb3776-10a1-4b7f-a0ec-f8bd13014010
  resourceVersion: "2264"
  uid: 371d3bdf-97df-48a8-a274-01f5a73e8d64
type: Opaque
```

* until adding Reloader [lands](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1794), the hard restart of Gatekeeper and scheduler deployments is necessary:

```bash
kubectl delete deployment/scheduler
kubectl delete deployment/gatekeeper
terraform apply
```

## Clickhouse debugging 101

Apart from simple checking of clickhouse DB instance logs, one can execute into one of DB containers and connect to Clickhouse DB.
We need to obtain credentials first though:

```bash
$ kubectl get clickhouse cluster -o jsonpath-as-json='{ .spec.adminUsers }'
[
    [
        {
            "name": "opstrace_scheduler",
            "secretKeyRef": {
                "key": "password",
                "name": "opstrace-scheduler-clickhouse-credentials"
            }
        }
    ]
]
```

```bash
$ k get secret opstrace-scheduler-clickhouse-credentials -o jsonpath-as-json='{ .data.password }' | jq -r  '.[0]' | base64 -d
<decoded-secret>
```

This pair of credentials has full access and is used by the scheduler while provisioning tenants.

```bash
$ kubectl exec -ti cluster-0-0-0 /bin/sh
/ $ clickhouse-client -u opstrace_scheduler --password <decoded-secret>
ClickHouse client version 22.6.2.12 (official build).
Connecting to localhost:9000 as user default.
Connected to ClickHouse server version 22.6.2 revision 54455.

Warnings:
 * Available disk space for logs at server startup is too low (1GiB): /var/log/clickhouse-server
 * Effective user of the process (clickhouse) does not match the owner of the data (root).

cluster-0-0-0.cluster-0-0.default.svc.cluster.local :)
```

Some most relevant/useful commands:

* `SHOW DATABASES` - list databases
* `USE errortracking_api` - connect to given database, no need to prefix all table names with DB name
* `describe table error_tracking_errors`
* `show create table error_tracking_errors`
* `SELECT quota_name,duration,query_inserts,max_query_inserts,errors FROM system.quotas_usage` - get information about quotas usage

More information about SQL syntax that Clickhouse is using can be found [here](https://clickhouse.com/docs/en/sql-reference/statements/).

## HTTP requests debugging 101

Although not required, [installing kubetail](https://github.com/johanhaleby/kubetail) greatly helps with debugging any issues on k8s clusters.
This is due to the fact that most of our deployments deploy multiple replicas of a given pod for reliability.
One could override them via `Cluster` CRD's overrides, but apart from being troublesome/slow, in some cases it may even prevent one from finding the root cause.

Once kubetail is installed:

Tailing gatekeeper logs (each pod has a different colour for its logs):

```bash
$ kubetail  -l app=gatekeeper -c gatekeeper -s '15m' | grep -v /readyz
Will tail 3 logs...
gatekeeper-748c8f4588-bcqz8
gatekeeper-748c8f4588-msfvl
gatekeeper-748c8f4588-thvfb
[gatekeeper-748c8f4588-thvfb] [GIN] 2022/08/22 - 18:40:59 | 200 |    1.095404ms |    10.148.0.210 | GET      "/"
[gatekeeper-748c8f4588-thvfb] [GIN] 2022/08/22 - 18:42:26 | 404 |    1.445611ms |     10.148.2.35 | PUT      "/testing-put.txt"
[gatekeeper-748c8f4588-msfvl] [GIN] 2022/08/22 - 18:42:26 | 404 |    1.544795ms |     10.148.2.35 | GET      "/testing-put.txt"
[gatekeeper-748c8f4588-bcqz8] [GIN] 2022/08/22 - 18:50:10 | 200 |     992.912µs |     10.148.2.35 | GET      "/"
...
```

Tailing ingress-nginx logs:

```bash
$ kubetail  -l app=nginx-ingress  -s '1m'
Will tail 3 logs...
nginx-ingress-7cb89d6b98-dm5f6
nginx-ingress-7cb89d6b98-l2p5g
nginx-ingress-7cb89d6b98-z5zn2
[nginx-ingress-7cb89d6b98-l2p5g] I0822 19:06:57.963671       7 event.go:285] Event(v1.ObjectReference{Kind:"Ingress", Namespace:"9555164", Name: "opentelemetry-jaeger-9555164", UID:"030c6569-7913-4994-912f-efe094104af3", APIVersion:"networking.k8s.io/v1", ResourceVersion:"23597836", Field Path:""}): type: 'Normal' reason: 'Sync' Scheduled for sync
[nginx-ingress-7cb89d6b98-l2p5g] I0822 19:07:01.433088       7 event.go:285] Event(v1.ObjectReference{Kind:"Ingress", Namespace:"9555164", Name: "opentelemetry-9555164", UID:"e2300ee3-6229-4b8b-b124-084175d0589e", APIVersion:"networking.k8s.io/v1", ResourceVersion:"23597861", FieldPath:"" }): type: 'Normal' reason: 'Sync' Scheduled for sync
...
```

Usually, Gatekeeper logs helpful messages if the requests are rejected.

## Distroless images, no tooling in containers

Another useful technique is using ephemeral debug containers that were [introduced in k8s 1.23](https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/#ephemeral-container).
Majority of the containers that Gitlab Observability Platform is build with, do not have debug tooling, sometimes there isn't even a shell binary to begin with.
Personally, I use `nicolaka/netshoot` as the debugging container container, as it offers all the required tooling and is pretty lightweight:

```bash
kubectl debug -it -n 6 opentelemetry-6-69d85b5cb4-sfttw  --image=nicolaka/netshoot
```

## Accessing cluster metrics/Prometheus directly

In case when everything else fails, and we still have kubectl access to cluster via k8s, one can access Prometheus UI in order to try to determine what is happening with the cluster:

```bash
kubectl port-forward svc/prometheus 9090
```

Prometheus' UI should become accessible locally at the address `http://localhost:9090`

## Gotchas

GKE storage provisioner seems to provision volumes with size starting with 1Gi.
Everything lower than that is rounded to 1Gi causing Cluster to never transition to `Complete` state as the desired storage size (e.g. 512Mi) and actual size (1Gi) of PVC differ.
