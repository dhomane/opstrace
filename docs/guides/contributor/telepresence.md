# Telepresence

We can use [telepresence](https://www.getambassador.io/docs/telepresence/latest/quick-start) to run our
applications, or any container, as a replacement or intercept for any pod in the cluster.

Telepresence commands are provided in all the project specific `Makefiles` to allow for local development against a GOB cluster.

Use `telepresence connect` to make cluster [service DNS](https://www.getambassador.io/docs/telepresence/latest/reference/dns) available on your machine.
This can be a useful alternative to port-forwards.

Once connected, [`telepresence intercept`](https://www.getambassador.io/docs/telepresence/latest/reference/intercepts) can be used
to route service traffic to your host machine and a specific port.

To verify `telepresence` is working, try the following commands and verify the output is similar to this:

```bash
> telepresence connect
Connected to context kind-opstrace (https://127.0.0.1:43979)
> telepresence status
User Daemon: Running
  Version           : v2.10.3
  Executable        : /home/joe/.asdf/installs/telepresence/2.10.3/bin/telepresence
  Install ID        : 3d20d5f0-a1e2-4fd2-9e95-d0bd600f7dcb
  Status            : Connected
  Kubernetes server : https://127.0.0.1:43979
  Kubernetes context: kind-opstrace
  Intercepts        : 0 total
Root Daemon: Running
  Version    : v2.10.3
  DNS        :
    Local IP        : 192.168.47.59
    Remote IP       : <nil>
    Exclude suffixes: [.com .io .net .org .ru]
    Include suffixes: []
    Timeout         : 8s
  Also Proxy : (0 subnets)
  Never Proxy: (1 subnets)
    - 127.0.0.1/32
Ambassador Cloud:
  Status      : Logged out
Traffic Manager: Connected
  Version : v2.10.3
  Mode    : single-user
```

In particular, it should show `Status: Connected` and the kubernetes cluster details.

## Project Compatibility

> Note: operators don't use `intercept` as only one instance should be running and reconciling resources.
> We may be able to handle this at some point with leader election.

| Project     | Usage example | Notes |
| ----------- | ------------- | ---------------- |
| `scheduler` | `cd scheduler && make deploy-without-manager run` | `telepresence connect` is used required to make service DNS available.
| `tenant-operator` | `cd tenant-operator && make deploy-without-manager run` | As above. Has lots of dependencies so it is difficult to set up.
| `clickhouse-operator` | `cd clickhouse-operator && make run` | Doesn't need telepresence as no other services are used.
| `gatekeeper` | `cd gatekeeper && make intercept-run` | Intercepts but uses local redis container as Sentinel is not `intercept` compatible.
| `tracing` | `cd go && make intercept-tracing` | Runs but not currently using config mount. Needs work.
| `errortracking` | `cd go && make intercept-errortracking` | Works fine

## Environment Variables

It's a good idea to read about [telepresence environment variable handling](https://www.getambassador.io/docs/telepresence/latest/reference/environment)
so you can best understand how to utilise intercepts and env vars.

## Volume Mounts

Telepresence intercepts [support volume mounting](https://www.getambassador.io/docs/telepresence/latest/reference/volume) pod volumes
on the host in a tmp directory.

This requires `sshfs`, so ensure this is installed.

There are [known issues](https://www.getambassador.io/docs/telepresence/latest/troubleshooting#volume-mounts-are-not-working-on-macos)
with this mechanism.

The mounted data *should* available in `$TELEPRESENCE_ROOT` which exists for the duration of the intercept.
