# Testing

Each project should have a sensible unit test coverage, integration tests and end-to-end tests where sensible.

Unit tests are a mix of typical Go tests, [testify](https://github.com/stretchr/testify) assertions/suites
and for the operators we have some [Ginkgo](https://onsi.github.io/ginkgo/) [controller tests](https://book.kubebuilder.io/cronjob-tutorial/writing-tests.html).

End-to-end tests use Ginkgo as it provides some very useful constructs to check for declarative state.
We use Ginkgo V2 with the CLI as it provides more robust [interrupt and timeout handling](https://onsi.github.io/ginkgo/MIGRATING_TO_V2#interrupt-behavior).

Each project `Makefile` has a `unit-tests` target to run project specific unit tests.

## Running E2E tests

Prior to running tests locally, one must follow instructions in [quickstart](../../quickstart.md) to configure the OAuth application against your GitLab instance.

If you cannot set "Trusted" in the GitLab OAuth config, you must follow the auth flow at least once
to authorize the application.

You will need to set up these environment variables locally, ideally just once:

```bash
export TEST_GITLAB_OAUTH_CLIENT_ID=<your_oauth_client_id>
export TEST_GITLAB_OAUTH_CLIENT_SECRET=<your_oauth_client_secret>
```

After this, running end-to-end tests locally via `kind` is as simple as:

```bash
make kind docker-ensure kind-load-docker-images

make deploy

make e2e-test
```

NOTE: On M1 Mac loading images to Kind cluster may not work, see [this](https://github.com/kubernetes-sigs/kind/issues/2772) upstream issue.

These targets use relevant parts of the scheduler `Makefile`.

The local kind cluster is set up to route ingress to localhost.
You can override this value with the env var `TEST_DOMAIN`.

## Test cycle

When updating the various components you can run `make kind-load-docker-images` to build all containers
and load them into your `kind` cluster.

You may then proceed to manually test changes and run/update the e2e tests.
