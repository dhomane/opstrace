# Tracing deep dive

This document provides a detailed explanation of how tracing works.

It is assumed that the infrastructure has already been provisioned and the tenant created.
For details on how this is done, see the other docs in the `architecture` folder.

## Overview

The following diagram provides a brief overview of the components of the Gitlab Observability Platform (GOP) that implement tracing.

![Tracing-overview](../assets/tracing_overview.png)

The previous diagram assumes there are two `Group`s deployed: `1st` and `n-th` in the given `GitlabNamespace`.
Each Group has corresponding OTel collector and Jaeger instance. Both are deployed by the tenant operator.

Traces are submitted through ingress to the OTel collector, based on the ID of the Group encoded in the HTTP's requests path.
Ingress is responsible for routing requests to the correct collector.

For example, for the `Group.Id == 6`, there are two ingress objects:

```bash
$ kubectl get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o wide
NAME                     CLASS    HOSTS       ADDRESS     PORTS   AGE
opentelemetry-6          <none>   localhost   localhost   80      6h40m
opentelemetry-jaeger-6   <none>   localhost   localhost   80      6h40m

$ kubectl get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o jsonpath="{range .items[*].spec.rules[0].http.paths[0]}{.path}{'\n'}{end}"
/v1/traces/6
/v1/jaegertraces/6
```

The OTel collector then submits the traces to the Jaeger instance.

In turn, the Jaeger instance uses the [jaeger-clickhouse](https://github.com/jaegertracing/jaeger-clickhouse) plugin to submit these traces to ClickHouse.

Each collector/Group uses a separate ClickHouse database.

Traces are retrieved from ClickHouse by Jaeger, using the same plugin.

Gitlab Observability UI (GOUI) issues API calls to Jaeger to fetch traces. It does not connect directly to ClickHouse.

GOUI, Jaeger and OTel collectors all use Gatekeeper for authorization and authentication.
The source of truth for authentication and authorization data is the GOUI instance or Gitlab instance itself, depending on whether you use an authentication token or a session cookie.
The Authentication token is used on the ingestion path. The session cookie is used when accessing GOUI/Jaeger through a browser.

Gatekeeper uses Redis for caching so that it does not overwhelm Gitlab instance with API calls.

The instrumented code may use Jaeger SDK to send traces, but this is not recommended.
This is because of the deprecation of Jaeger SDK (see [here](https://www.jaegertracing.io/docs/1.37/client-libraries/)).
The preferred way to send metrics is using OTel SDK for the given language.

## OTel collector

The OTel collector is a customized distribution of [the upstream OTel collector](https://opentelemetry.io/docs/collector/).
Currently, only HTTP ingestion is supported using [OTel](https://github.com/open-telemetry/opentelemetry-specification/blob/main/specification/trace/api.md) and Jaeger protocols.
The metrics are then batched and sent to Jaeger using the Jaeger protocol, where they are also logged.

See [this example tracing app](https://gitlab.com/ankitbhatnagar/opstraceware/-/blob/master/cmd/tracing/main.go) for details of how traces can be sent from the application.
Users may send traces directly to GOP. Alternatively, if there is a need for a single outgoing IP or batching/queueing of metrics, an intermediary OTel collector can be connected to the GOP collector.

Currently, GitLab uses [Labkit](https://gitlab.com/gitlab-org/labkit/), but work is ongoing to either add support for sending metrics in OTel format, or to drop Labkit and use OTel SDK directly (see for example [here](https://gitlab.com/gitlab-org/labkit/-/merge_requests/79#note_437451442)).

## Jaeger

Jaeger is currently used for storing and retrieving traces from ClickHouse.
Jaeger uses the default ClickHouse DB schema, but there is plan to improve it using suggestions from ClickHouse team (see [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1804) and [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1692)).

## Authentication and Authorization

Ingress together with Gatekeeper are responsible for authentication and authorization for tracing.
Ingress objects for metrics in OTel format, metrics in Jaeger format and Jaeger instance itself are annotated with:

```yaml
nginx.ingress.kubernetes.io/auth-response-headers: X-WEBAUTH-EMAIL, X-GRAFANA-ORG-ID
nginx.ingress.kubernetes.io/auth-signin: http://localhost/v1/auth/start?rt=$escaped_request_uri
nginx.ingress.kubernetes.io/auth-url: http://gatekeeper.default.svc.cluster.local:3001/v1/auth/webhook/35?uri=$escaped_request_uri
```

This leverages the `ngx_http_auth_request_module` [Nginx module](https://nginx.org/en/docs/http/ngx_http_auth_request_module.html).
The request, before being forwarded to the backend, is first forwarded to Gatekeeper.

* If the subrequest returns a 2xx response code, access is allowed.
* If the subrequest returns a 401 or 403, access is denied with the corresponding error code.
* Any other response code returned by the subrequest is considered an error.

If the request includes an `Authorization` token, an additional subrequest is made, this time to GOUI instance, which is used as authentication's and authorization's source of truth.
This path is taken usually by ingestion requests.

```go
func GetNamespaceFromArgusForToken(
    ctx *gin.Context, argusURL string, bearerToken string,
) (string, error) {
    group := argusapi.Group{}

    u, err := url.Parse(argusURL)
    if err != nil {
        return "", err
    }
    u.Path = "/api/group/"

    req, err := http.NewRequestWithContext(ctx, "GET", u.String(), nil)
    if err != nil {
        return "", err
    }
    req.Header.Add("Authorization", bearerToken)
    req.Header.Add("Content-Type", "application/json")

    log.Debug(fmt.Sprintf("GetNamespaceFromArgusForToken request: %+v", req))

    client := &http.Client{
        Timeout: time.Second * 5,
    }
    resp, err := client.Do(req)
    if err != nil {
        return "", err
    }
    defer resp.Body.Close()

    bodyContents, err := ioutil.ReadAll(resp.Body)

    if err != nil {
        return "", err
    }
    err = json.Unmarshal(bodyContents, &group)
    if err != nil {
        return "", err
    }

    if group.ID <= 1 {
        return "", fmt.Errorf("current namespace context returned a default value")
    }
    return fmt.Sprint(group.ID), nil
}
```

If this subrequest confirms that the subrequest's namespace is the same as the one returned by GOUI, the main request is permitted.

In the case when there is no `Authorization` token, it is assumed that the request originated from the browser and an attempt is made to authorize basing on a session cookie.

```go
// Check if current user can access the namespace by Id.
func (s *GitLabService) CanAccessNamespace(nid interface{}) (bool, gitlab.AccessLevelValue, error) {
    // A namespace could be a group or a user.
    ns, err := s.GetNamespace(nid)
    if err != nil || ns == nil {
        return false, gitlab.NoPermissions, err
    }
    if ns.Kind == GroupNamespaceType {
        membership, err := s.getGroupMembership(nid)
        if err != nil {
            return false, gitlab.NoPermissions, err
        }
        return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
            membership.AccessLevel,
            nil
    }
    if ns.Kind == UserNamespaceType {
        // User must be the owner of their own user namespace
        return true, gitlab.OwnerPermission, nil
    }
    return false, gitlab.NoPermissions, nil
}
```

For a request to be permitted, the request must come from entity that is either a member of the group, or in case of the user-namespace, the user itself.
The source of truth in this case is the Gitlab Instance.
