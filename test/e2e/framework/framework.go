package framework

import (
	"context"
	"errors"
	"os"
	"strings"
	"sync"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/scheme"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

func init() {
	scheme.AddToScheme(scheme.Scheme)
	v1.AddToScheme(scheme.Scheme)
	appsv1.AddToScheme(scheme.Scheme)
	rbacv1.AddToScheme(scheme.Scheme)
	netv1.AddToScheme(scheme.Scheme)
	certmanager.AddToScheme(scheme.Scheme)
	redis.AddToScheme(scheme.Scheme)
	monitoring.AddToScheme(scheme.Scheme)
	schedulerv1alpha1.AddToScheme(scheme.Scheme)
	tenantv1alpha1.AddToScheme(scheme.Scheme)
	clickhousev1alpha1.AddToScheme(scheme.Scheme)
}

var (
	once           sync.Once
	errUnitialized = errors.New("object not setup yet")
)

type Framework struct {
	testIdentifier     string
	testTarget         string
	testInfrastructure *Infrastructure

	ctx    context.Context
	cancel context.CancelFunc

	k8sClient     client.Client
	defaultClient client.Client
	clientset     *kubernetes.Clientset
}

var f *Framework

func GetInstance() (*Framework, error) {
	var err error
	if f == nil {
		once.Do(func() {
			f = &Framework{}
			f.ctx, f.cancel = context.WithCancel(context.Background())
			if err = f.setTestIdentifier(); err != nil {
				return
			}
			if err = f.setTestTarget(); err != nil {
				return
			}
			if err = f.setTestInfrastructure(); err != nil {
				return
			}
		})
	}
	if err != nil {
		return nil, err
	}
	return f, nil
}

// This setter allows for lazy building clients only a
// corresponding cluster has been built and configured
// accessible
func (f *Framework) BuildClients() error {
	var err error
	if err = f.setK8sClient(); err != nil {
		return err
	}
	if err = f.setDefaultClient(); err != nil {
		return err
	}
	if err = f.setK8sClientSet(); err != nil {
		return err
	}
	return nil
}

func (f *Framework) setTestInfrastructure() error {
	i, err := NewInfrastructure(f.testTarget)
	if err != nil {
		return err
	}
	f.testInfrastructure = i
	return nil
}

func (f *Framework) GetTestInfrastructure() (*Infrastructure, error) {
	if f.testInfrastructure != nil {
		return f.testInfrastructure, nil
	}
	return nil, errUnitialized
}

func (f *Framework) GetContextWithCancel() (context.Context, context.CancelFunc) {
	return f.ctx, f.cancel
}

func (f *Framework) setTestIdentifier() error {
	// use ${CI_COMMIT_SHORT_SHA} as an identifier to help identify this
	// test run and all associated resources/dependencies
	if _, ok := os.LookupEnv("CI_COMMIT_SHORT_SHA"); !ok {
		return errUnitialized
	}
	f.testIdentifier = strings.ToLower(os.Getenv("CI_COMMIT_SHORT_SHA"))
	return nil
}

func (f *Framework) GetTestIdentifier() (string, error) {
	if f.testIdentifier == "" {
		return "", errUnitialized
	}
	return f.testIdentifier, nil
}

func (f *Framework) setTestTarget() error {
	if _, ok := os.LookupEnv("TARGET_PROVIDER"); !ok {
		f.testTarget = string(common.KIND)
	} else {
		f.testTarget = os.Getenv("TARGET_PROVIDER")
	}
	return nil
}

func (f *Framework) GetTestTarget() (string, error) {
	if f.testTarget == "" {
		return string(common.KIND), nil
	}
	return f.testTarget, nil
}

func getRestConfig() (*rest.Config, error) {
	return config.GetConfig()
}

func (f *Framework) setK8sClient() error {
	restConfig, err := getRestConfig()
	if err != nil {
		return err
	}
	k8sClient, err := client.New(restConfig, client.Options{Scheme: scheme.Scheme})
	if err != nil {
		return err
	}
	f.k8sClient = k8sClient
	return nil
}

func (f *Framework) GetK8sClient() (client.Client, error) {
	if f.k8sClient != nil {
		return f.k8sClient, nil
	}
	return nil, errUnitialized
}

func (f *Framework) setDefaultClient() error {
	k8sClient, err := f.GetK8sClient()
	if err != nil {
		return err
	}
	f.defaultClient = client.NewNamespacedClient(k8sClient, "default")
	return nil
}

func (f *Framework) GetDefaultClient() (client.Client, error) {
	if f.defaultClient != nil {
		return f.defaultClient, nil
	}
	return nil, errUnitialized
}

func (f *Framework) setK8sClientSet() error {
	restConfig, err := getRestConfig()
	if err != nil {
		return err
	}
	f.clientset = kubernetes.NewForConfigOrDie(restConfig)
	return nil
}

func (f *Framework) GetK8sClientSet() (*kubernetes.Clientset, error) {
	if f.clientset != nil {
		return f.clientset, nil
	}
	return nil, errUnitialized
}
