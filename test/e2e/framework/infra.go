package framework

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	gcp "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/gcp"
	kind "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/kind"
)

type EnvironmentBuilder interface {
	CreateEnvironment(string) error
	DestroyEnvironment(string) error
}

type K8sClusterBuilder interface {
	CreateK8sCluster(string) (string, error)
	DestroyK8sCluster(string) error
}

type GOBClusterBuilder interface {
	GetGOBCluster(string, *schedulerv1alpha1.Cluster) error
}

type InfrastructureBuilder interface {
	EnvironmentBuilder
	K8sClusterBuilder
	GOBClusterBuilder
}

type Infrastructure struct {
	provider string
	builder  InfrastructureBuilder
}

func NewInfrastructure(provider string) (*Infrastructure, error) {
	var (
		builder InfrastructureBuilder
		err     error
	)
	switch provider {
	case string(common.KIND):
		builder, err = kind.NewKind()
	case string(common.GCP):
		builder, err = gcp.NewGCP()
	}
	if err != nil {
		return nil, err
	}
	return &Infrastructure{
		provider: provider,
		builder:  builder,
	}, nil
}

func (i *Infrastructure) CreateEnvironment(testIdenfier string) error {
	return i.builder.CreateEnvironment(testIdenfier)
}

func (i *Infrastructure) DestroyEnvironment(testIdentifier string) error {
	return i.builder.DestroyEnvironment(testIdentifier)
}

func (i *Infrastructure) CreateK8sCluster(testIdentifier string) (string, error) {
	return i.builder.CreateK8sCluster(testIdentifier)
}

func (i *Infrastructure) DestroyK8sCluster(testIdentifier string) error {
	return i.builder.DestroyK8sCluster(testIdentifier)
}

func (i *Infrastructure) GetGOBCluster(testIdentifier string, out *schedulerv1alpha1.Cluster) error {
	return i.builder.GetGOBCluster(testIdentifier, out)
}
