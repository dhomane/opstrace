package e2e

import (
	"testing"

	_ "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/cluster"
	_ "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/namespace"
)

func TestE2E(t *testing.T) {
	RunE2ETests(t)
}
