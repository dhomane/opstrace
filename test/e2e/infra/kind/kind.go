package kind

import (
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
)

type Kind struct{}

func NewKind() (*Kind, error) {
	return &Kind{}, nil
}

func (g *Kind) CreateEnvironment(_ string) error {
	// do nothing right now
	return nil
}

func (g *Kind) DestroyEnvironment(_ string) error {
	// do nothing right now
	return nil
}

func (g *Kind) CreateK8sCluster(_ string) (string, error) {
	return "", nil
}

func (g *Kind) DestroyK8sCluster(_ string) error {
	// do nothing right now
	return nil
}

func (g *Kind) GetGOBCluster(testIdentifier string, out *schedulerv1alpha1.Cluster) error {
	common.LoadYaml("../../scheduler/config/examples/Cluster.yaml", out)
	return nil
}
