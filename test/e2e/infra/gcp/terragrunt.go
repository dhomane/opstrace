package gcp

import (
	"fmt"
	"os"
	"strings"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/testing"
)

func GetTestEnvVars(testIdentifier string) (map[string]string, error) {
	if _, ok := os.LookupEnv("TEST_ENV_GITLAB_IMAGE"); !ok {
		return nil, fmt.Errorf("TEST_ENV_GITLAB_IMAGE not set")
	}
	gitlabImage := os.Getenv("TEST_ENV_GITLAB_IMAGE")

	// unless overridden, scheduler image used is inferred from the test identifier
	var schedulerImage string
	if _, ok := os.LookupEnv("TEST_ENV_SCHEDULER_IMAGE"); ok {
		schedulerImage = os.Getenv("TEST_ENV_SCHEDULER_IMAGE")
	} else {
		schedulerImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace/scheduler:0.1.44"
	}

	var gouiImage string
	if _, ok := os.LookupEnv("TEST_ENV_GOUI_IMAGE"); ok {
		gouiImage = os.Getenv("TEST_ENV_GOUI_IMAGE")
	} else {
		gouiImage = "registry.gitlab.com/gitlab-org/opstrace/opstrace-ui/gitlab-observability-ui:391b3d14"
	}

	gitlabDomain := fmt.Sprintf(GitlabDomainNameTmpl, testIdentifier)
	cloudDNSZone := strings.ReplaceAll(gitlabDomain, ".", "-")

	return map[string]string{
		"TF_VAR_project_id":               DevRealmProjectName,
		"TF_VAR_instance_name":            fmt.Sprintf("testplatform-%s", testIdentifier),
		"TF_VAR_region":                   "us-west2",
		"TF_VAR_location":                 "us-west2-a",
		"TF_VAR_zone":                     "us-west2-a",
		"TF_VAR_opstrace_domain":          fmt.Sprintf(PlatformDomainNameTmpl, testIdentifier),
		"TF_VAR_gitlab_domain":            gitlabDomain,
		"TF_VAR_cloud_dns_zone":           cloudDNSZone,
		"TF_VAR_acme_server":              "",
		"TF_VAR_acme_email":               "abhatnagar@gitlab.com",
		"TF_VAR_cert_issuer":              "letsencrypt-staging",
		"TF_VAR_domain":                   gitlabDomain,
		"TF_VAR_registry_username":        "ankitbhatnagar",
		"TF_VAR_gitlab_image":             gitlabImage,
		"TF_VAR_cluster_secret_name":      "auth-secret",
		"TF_VAR_cluster_secret_namespace": "default",
		"TF_VAR_scheduler_image":          schedulerImage,
		"TF_VAR_goui_image_path":          gouiImage,
	}, nil
}

func ProvisionViaTerragrunt(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	testEnvVars, err := GetTestEnvVars(testIdentifier)
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         testEnvVars,
		Logger:          logger.TestingT,
	})

	_, err = terraform.RunTerraformCommandE(t, terraformOptions, terraform.FormatArgs(terraformOptions, "run-all", "init")...)
	if err != nil {
		return err
	}
	_, err = terraform.TgApplyAllE(t, terraformOptions)
	if err != nil {
		return err
	}
	return nil
}

func GetProvisionedKubernetesCluster(t testing.TestingT, testIdentifier string) (string, error) {
	if testIdentifier == "" {
		return "", fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	testEnvVars, err := GetTestEnvVars(testIdentifier)
	if err != nil {
		return "", err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration/gke",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         testEnvVars,
		Logger:          logger.TestingT,
	})

	return terraform.OutputE(t, terraformOptions, "kubeconfig_path")
}

func DeprovisionViaTerragrunt(t testing.TestingT, testIdentifier string) error {
	if testIdentifier == "" {
		return fmt.Errorf("testIdentifier not specified, resources cannot be attributed correctly")
	}

	testEnvVars, err := GetTestEnvVars(testIdentifier)
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:    "../../terraform/environments/integration",
		TerraformBinary: "terragrunt",
		LockTimeout:     "1h",
		EnvVars:         testEnvVars,
		Logger:          logger.TestingT,
	})

	_, err = terraform.TgDestroyAllE(t, terraformOptions)
	if err != nil {
		return err
	}
	return nil
}
