package gcp

import (
	"fmt"
	"os"

	. "github.com/onsi/ginkgo/v2"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	GitlabDomainNameTmpl   string = "test-%s-gitlab.opstracegcp.com"
	PlatformDomainNameTmpl string = "test-%s-platform.opstracegcp.com"
)

type GCP struct{}

func NewGCP() (*GCP, error) {
	return &GCP{}, nil
}

func (g *GCP) CreateEnvironment(testIdentifier string) error {
	// check if the credentials have been exported and are available
	if _, ok := os.LookupEnv("DEV_REALM_CREDENTIALS_FILE"); !ok {
		return fmt.Errorf("couldn't find DEV_REALM_CREDENTIALS_FILE from env")
	}
	if _, ok := os.LookupEnv("DEVELOPMENT_CREDENTIALS_FILE"); !ok {
		return fmt.Errorf("couldn't find DEVELOPMENT_CREDENTIALS_FILE from env")
	}
	// setup DNS
	if err := setupDNS(testIdentifier, fmt.Sprintf(GitlabDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	if err := setupDNS(testIdentifier, fmt.Sprintf(PlatformDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	return nil
}

func (g *GCP) DestroyEnvironment(testIdentifier string) error {
	// destroy DNS setup specific to this test run
	if err := destroyDNS(testIdentifier, fmt.Sprintf(GitlabDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	if err := destroyDNS(testIdentifier, fmt.Sprintf(PlatformDomainNameTmpl, testIdentifier)); err != nil {
		return err
	}
	return nil
}

func (g *GCP) CreateK8sCluster(testIdentifier string) (string, error) {
	// setup dev-realm credentials henceforth
	if _, ok := os.LookupEnv("DEV_REALM_CREDENTIALS_FILE"); !ok {
		return "", fmt.Errorf("couldn't find DEV_REALM_CREDENTIALS_FILE from env")
	}
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", os.Getenv("DEV_REALM_CREDENTIALS_FILE")); err != nil {
		return "", err
	}
	// provision the platform specific to this test run
	if err := ProvisionViaTerragrunt(GinkgoT(), testIdentifier); err != nil {
		return "", err
	}
	// setup access to this cluster
	return GetProvisionedKubernetesCluster(GinkgoT(), testIdentifier)
}

func (g *GCP) DestroyK8sCluster(testIdentifier string) error {
	// deprovision the platform provisioned specific to this test run
	if err := DeprovisionViaTerragrunt(GinkgoT(), testIdentifier); err != nil {
		return err
	}
	return nil
}

func (g *GCP) GetGOBCluster(testIdentifier string, out *schedulerv1alpha1.Cluster) error {
	return getGOBCluster(testIdentifier, out)
}
