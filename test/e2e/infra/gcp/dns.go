package gcp

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/pkg/clouddns/gcp"
	"google.golang.org/api/dns/v1"
)

const (
	DevelopmentProjectName string = "vast-pad-240918"
	DevRealmProjectName    string = "opstrace-dev-bee41fca"
	RootManagedZoneName    string = "root-opstracegcp"
)

func setupDNS(testID, expectedDNSName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	// setup managed zone
	devCredentialsFile := os.Getenv("DEV_REALM_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", devCredentialsFile); err != nil {
		return err
	}
	provider, err := gcp.NewDNSProvider(ctx, DevRealmProjectName)
	if err != nil {
		return err
	}

	expectedName := strings.ReplaceAll(expectedDNSName, ".", "-")
	expectedDNSName = fmt.Sprintf("%s.", expectedDNSName) // trailing dot is important!
	expectedManagedZone := dns.ManagedZone{
		Name:        expectedName,
		DnsName:     expectedDNSName,
		Description: fmt.Sprintf("managed zone for GOP integration test: %s", testID),
	}

	// check if the expected zone name already exists
	zones, err := provider.ListManagedZones(ctx, DevRealmProjectName)
	if err != nil {
		return err
	}

	if _, ok := zones[expectedName]; ok {
		fmt.Println("managed zone for GOP integration test already exists")
		return nil // nothing to do
	}

	// create managed zone
	createdManagedZone, err := provider.CreateManagedZone(DevRealmProjectName, &expectedManagedZone)
	if err != nil {
		return err
	}

	// setup NS delegate
	developmentCredentialsFile := os.Getenv("DEVELOPMENT_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", developmentCredentialsFile); err != nil {
		return err
	}

	provider, err = gcp.NewDNSProvider(ctx, DevelopmentProjectName)
	if err != nil {
		return err
	}

	// https://cloud.google.com/dns/docs/records#api
	expectedChange := dns.Change{
		Additions: []*dns.ResourceRecordSet{
			{
				Type:    gcp.RecordTypeNS,
				Name:    expectedDNSName,
				Ttl:     300,
				Rrdatas: createdManagedZone.NameServers,
			},
		},
	}

	_, err = provider.CreateRecords(DevelopmentProjectName, RootManagedZoneName, &expectedChange)
	if err != nil {
		return err
	}
	return nil
}

func destroyDNS(testID, expectedDNSName string) error {
	return nil
}
