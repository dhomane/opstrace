package common

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

// load config/examples namespace
func LoadNamespace(
	ctx context.Context,
	defaultClient client.Client,
	out *schedulerv1alpha1.GitLabNamespace,
) {
	LoadYaml("../../scheduler/config/examples/OpstraceNamespace.yaml", out)
	CreateOrUpdate(ctx, defaultClient, out)
}

// wait for the cluster to have a successful ready condition
func ExpectClusterReady(
	ctx context.Context,
	defaultClient client.Client,
	cluster *schedulerv1alpha1.Cluster,
) {
	By("check Cluster CR has ready condition: True")
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.Cluster{}
		g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(cluster), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}).WithTimeout(time.Minute * 20).Should(Succeed())
}

// wait for GitLabNamespace resource to have successful ready condition
func ExpectGitLabNamespaceReady(
	ctx context.Context,
	defaultClient client.Client,
	ns *schedulerv1alpha1.GitLabNamespace,
) {
	By(fmt.Sprintf("check GitLabNamespace CR: %s has ready condition: True", ns.Namespace()))
	Eventually(func(g Gomega) {
		cr := &schedulerv1alpha1.GitLabNamespace{}
		g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(ns), cr)).To(Succeed())

		g.Expect(
			meta.IsStatusConditionTrue(cr.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}).Should(Succeed())
}

func ExpectNamespacePodsReady(
	ctx context.Context,
	k8sClient client.Client,
	namespace string,
) {
	By("check all Pods are ready in namespace: " + namespace)
	Eventually(func(g Gomega) {
		pods := &corev1.PodList{}
		g.Expect(k8sClient.List(ctx, pods, client.InNamespace(namespace))).To(Succeed())
		for _, p := range pods.Items {
			ready := false
			for _, c := range p.Status.Conditions {
				if c.Type == corev1.PodReady && c.Status == corev1.ConditionTrue {
					ready = true
					break
				}
			}
			g.Expect(ready).To(BeTrue(), "all Pods should be ready")
		}
	}).Should(Succeed())
}

func ExpectObjectExists(
	ctx context.Context,
	k8sClient client.Client,
	key types.NamespacedName,
	object client.Object,
) {
	Eventually(func() bool {
		if err := k8sClient.Get(ctx, key, object); err != nil {
			return false
		}
		return true
	}, 30*time.Second, 3*time.Second).Should(BeTrue())
}

// gather resources owned by the resource and ensure they are all deleted
func DeleteCustomResourceAndVerify(
	ctx context.Context,
	k8sClient client.Client,
	cr client.Object,
	extraNamespaces ...string,
) {
	By("gather all resources that are owned by this resource")

	crUID := cr.GetUID()
	Expect(crUID).NotTo(Equal(""), "CR must have UID set from server")
	expectDeleted := []client.Object{}

	namespaces := append([]string{cr.GetNamespace()}, extraNamespaces...)

	for _, ns := range namespaces {
		for _, r := range potentiallyOwnedResources() {
			Expect(k8sClient.List(ctx, r, client.InNamespace(ns))).To(Succeed())
			v := reflect.ValueOf(r)
			// Note(joe): I can't find a nice way of doing this without reflection or lots of boilerplate for each type.
			// Every list type has the Items field.
			items := reflect.Indirect(v).FieldByName("Items")
			// all cleared up.
			if items.IsNil() {
				continue
			}
			for i := 0; i < items.Len(); i++ {
				item := items.Index(i)
				// list items can either be struct values or pointers
				var meta metav1.Object
				if item.Kind() == reflect.Pointer {
					meta = item.Interface().(metav1.Object)
				} else {
					meta = item.Addr().Interface().(metav1.Object)
				}
				for _, o := range meta.GetOwnerReferences() {
					if o.UID == crUID {
						expectDeleted = append(expectDeleted, meta.(client.Object))
					}
				}
			}
		}
	}

	Expect(expectDeleted).NotTo(BeEmpty(), "at least one owned resource must exist")

	By(fmt.Sprintf("delete the CR: %s/%s", cr.GetNamespace(), cr.GetName()))
	Expect(k8sClient.Delete(ctx, cr)).To(Succeed())

	By(fmt.Sprintf("wait for %d owned resources to be deleted", len(expectDeleted)))
	Eventually(func(g Gomega) {
		for _, u := range expectDeleted {
			obj := u
			err := k8sClient.Get(ctx, client.ObjectKeyFromObject(obj), obj)
			g.Expect(err).To(HaveOccurred(), "want a not found error")
			g.Expect(client.IgnoreNotFound(err)).NotTo(HaveOccurred())
		}
		err := k8sClient.Get(ctx, client.ObjectKeyFromObject(cr), cr)
		g.Expect(err).To(HaveOccurred(), "want a not found error")
		g.Expect(client.IgnoreNotFound(err)).NotTo(HaveOccurred())
	}).Should(Succeed(), "check owned resources and cr are deleted")
}

func DeleteGitLabNamespaceAndVerify(
	ctx context.Context,
	k8sClient client.Client,
	cr *schedulerv1alpha1.GitLabNamespace,
) {
	DeleteCustomResourceAndVerify(ctx, k8sClient, cr, cr.Namespace())

	if !cr.IsTopLevel() {
		return
	}

	By("check top-level namespace is deleted")
	Eventually(func(g Gomega) {
		ns := &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: cr.Namespace(),
			},
		}
		err := k8sClient.Get(ctx, client.ObjectKeyFromObject(ns), ns)
		g.Expect(err).To(HaveOccurred(), "want a not found error")
		g.Expect(client.IgnoreNotFound(err)).NotTo(HaveOccurred())
	}).Should(Succeed())
}

// get potentially owned resource types for our CRs.
// Note(joe): It would be nice if there were a better way of inferring potentially created types.
// I tried using `k8sClient.Scheme().AllKnownTypes()` but the number of potential types is impractical.
func potentiallyOwnedResources() []client.ObjectList {
	return []client.ObjectList{
		&appsv1.DeploymentList{},
		&appsv1.StatefulSetList{},
		&netv1.IngressList{},
		&corev1.ConfigMapList{},
		&corev1.ServiceList{},
		&corev1.SecretList{},
		&corev1.ServiceAccountList{},
		&rbacv1.ClusterRoleList{},
		&rbacv1.ClusterRoleBindingList{},
		&rbacv1.RoleList{},
		&rbacv1.RoleBindingList{},
		&monitoring.ServiceMonitorList{},
		&monitoring.PrometheusRuleList{},
		&monitoring.PrometheusList{},
		&redis.RedisFailoverList{},
		&clickhousev1alpha1.ClickHouseList{},
		&certmanager.CertificateList{},
		&certmanager.IssuerList{},
		&tenantv1alpha1.TenantList{},
		&tenantv1alpha1.GroupList{},
	}
}

// wait for all pods to be in a ready state
func ExpectClusterPodsReady(
	ctx context.Context,
	defaultClient client.Client,
) {
	ExpectNamespacePodsReady(ctx, defaultClient, "default")
}

func LoadYaml(path string, out interface{}) {
	f, err := filepath.Abs(path)
	Expect(err).NotTo(HaveOccurred())
	secretBytes, err := os.ReadFile(f)
	Expect(err).NotTo(HaveOccurred())

	Expect(yaml.Unmarshal(secretBytes, out)).To(Succeed())
}

func CreateOrUpdate(
	ctx context.Context,
	defaultClient client.Client,
	obj client.Object,
) {
	cp, ok := obj.DeepCopyObject().(client.Object)
	Expect(ok).To(BeTrue(), "runtime object should support client.Object")
	err := defaultClient.Get(ctx, client.ObjectKeyFromObject(cp), cp)
	Expect(client.IgnoreNotFound(err)).To(Succeed())
	if err == nil {
		obj.SetResourceVersion(cp.GetResourceVersion())
		Expect(defaultClient.Update(ctx, obj)).To(Succeed())
	} else {
		Expect(defaultClient.Create(ctx, obj)).To(Succeed())
	}
}

func envVarOrDefault(name string, def string) string {
	e := os.Getenv(name)
	if e == "" {
		return def
	}
	return e
}

// get the domain of the deployed Observability endpoint.
// Local testing uses localhost via kind config.
func testDomain() string {
	d := os.Getenv("TEST_DOMAIN")
	if d == "" {
		return "localhost"
	}
	return d
}

// get scheme used for the deployed Observability endpoint.
func testScheme() string {
	s := os.Getenv("TEST_SCHEME")
	if s == "" {
		return "https"
	}
	return s
}
