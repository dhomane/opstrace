# E2E Tests Suite

## Summary

This test-suite aims to provide coverage of all application features provided as part of the Gitlab Observability Component. Most tests have been written through the entrypoint of the `scheduler`/`gitlabnamespace` custom resources.

These tests are written using [ginkgo](https://onsi.github.io/ginkgo/).

## Target Environments

This test-suite can be run against multiple providers, namely:
### kind

Note, when `kind` is used as the target provider, the test-suite does NOT install the CRDs required for the scheduler OR the scheduler manager itself. These must be installed into the target cluster first.

> The reason for this is to allow the user to decide how the cluster is set up, where the manager runs (could be on host machine, or in CI).

When running locally,

At the root of the project, run:

```bash
export CI_COMMIT_SHORT_SHA="$testSHA"
export TARGET_PROVIDER="kind"
make kind deploy e2e-test-suite
```
### GCP

When `GCP` is used as the target provider, the test-suite also provisions
the necessary infrastructure, i.e DNS entries, GCP Cloud instance to run a
self-hosted version of Gitlab, GKE cluster to host platform resources, etc.

To do this, it uses a similar terraform/terragrunt environment as the one
we use for provisioning our other prod/staging environments. This also
helps us regularly test our provisioning machinery, in addition to the tests targeting application correctness.

When running against GCP, the setup needs access to the underlying
credentials as shown below.

At the root of the project, run:

```bash
export DEV_REALM_CREDENTIALS_FILE="$filepath"
export DEVELOPMENT_CREDENTIALS_FILE="$filepath"
export GOOGLE_APPLICATION_CREDENTIALS="$filepath"
export TF_VAR_registry_auth_token="$secret"
export CI_COMMIT_SHORT_SHA="$testSHA"
export TARGET_PROVIDER="gcp"
make e2e-test-suite
```
## Developing tests

In principle, we try to write tests through external interfaces (e.g. ingress) when testing user-facing features.

Use the custom resources of the scheduler (`Cluster` and `GitLabNamespace`) to create necessary cluster/namespace state. This allows us to set up namespace resources without conducting the provisioning step via GitLab OAuth.

As a general rule, we keep a single `Cluster` and dependencies operational for the duration of the tests as it is time consuming to rebuild the cluster for each spec.

There are a few tests that check `Cluster` and `GitLabNamespace` deletion are working as expected.
