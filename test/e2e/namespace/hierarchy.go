package namespace

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/framework"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("namespace hierarchy", Ordered, Serial, func() {
	var (
		ctx            context.Context
		testIdentifier string
		k8sClient      client.Client
		defaultClient  client.Client
	)

	cluster := &schedulerv1alpha1.Cluster{}

	BeforeAll(func() {
		f, err := framework.GetInstance()
		Expect(err).ToNot(HaveOccurred())

		ctx, _ = f.GetContextWithCancel()

		k8sClient, err = f.GetK8sClient()
		Expect(err).ToNot(HaveOccurred())

		defaultClient, err = f.GetDefaultClient()
		Expect(err).ToNot(HaveOccurred())

		testIdentifier, err = f.GetTestIdentifier()
		Expect(err).ToNot(HaveOccurred())

		testInfra, err := f.GetTestInfrastructure()
		Expect(err).ToNot(HaveOccurred())

		err = testInfra.GetGOBCluster(testIdentifier, cluster)
		Expect(err).ToNot(HaveOccurred())

		common.CreateOrUpdate(ctx, defaultClient, cluster)
		common.ExpectClusterReady(ctx, defaultClient, cluster)
		common.ExpectClusterPodsReady(ctx, defaultClient)
	})

	AfterAll(func() {
		// delete the cluster CR and underlying resources
		common.DeleteCustomResourceAndVerify(ctx, k8sClient, cluster)
	})

	Context("with multiple GitLabNamespace(s) without nesting", func() {
		var namespace1 *schedulerv1alpha1.GitLabNamespace
		var namespace2 *schedulerv1alpha1.GitLabNamespace

		BeforeAll(func() {
			namespace1 = &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "1",
					Namespace: "default",
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  1,
					TopLevelNamespaceID: 1,
					Name:                "1",
					Path:                "test1",
					FullPath:            "test1",
				},
			}
			namespace2 = &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "2",
					Namespace: "default",
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  2,
					TopLevelNamespaceID: 2,
					Name:                "2",
					Path:                "test2",
					FullPath:            "test2",
				},
			}
		})

		It("can be created successfully", func() {
			common.CreateOrUpdate(ctx, defaultClient, namespace1)
			common.CreateOrUpdate(ctx, defaultClient, namespace2)

			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace1)
			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace2)

			common.ExpectNamespacePodsReady(ctx, k8sClient, namespace1.Namespace())
			common.ExpectNamespacePodsReady(ctx, k8sClient, namespace2.Namespace())
		})

		It("can be deleted successfully", func() {
			common.DeleteGitLabNamespaceAndVerify(ctx, k8sClient, namespace1)
			common.DeleteGitLabNamespaceAndVerify(ctx, k8sClient, namespace2)
		})
	})

	Context("with multiple GitLabNamespace(s) with nesting", func() {
		var namespace1 *schedulerv1alpha1.GitLabNamespace
		var namespace2 *schedulerv1alpha1.GitLabNamespace
		var namespace3 *schedulerv1alpha1.GitLabNamespace

		BeforeAll(func() {
			namespace1 = &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "1",
					Namespace: "default",
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  1,
					TopLevelNamespaceID: 1,
					Name:                "1",
					Path:                "test1",
					FullPath:            "test1",
				},
			}
			namespace2 = &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "2",
					Namespace: "default",
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  2,
					TopLevelNamespaceID: 1,
					Name:                "2",
					Path:                "test1/test2",
					FullPath:            "test1/test2",
				},
			}
			namespace3 = &schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "3",
					Namespace: "default",
				},
				Spec: schedulerv1alpha1.GitLabNamespaceSpec{
					ID:                  3,
					TopLevelNamespaceID: 1,
					Name:                "3",
					Path:                "test1/test3",
					FullPath:            "test1/test3",
				},
			}
		})

		It("can be created successfully", func() {
			By("creating root namespace")
			common.CreateOrUpdate(ctx, defaultClient, namespace1)
			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace1)

			By("creating nested namespaces")
			common.CreateOrUpdate(ctx, defaultClient, namespace2)
			common.CreateOrUpdate(ctx, defaultClient, namespace3)

			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace2)
			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace3)

			common.ExpectNamespacePodsReady(ctx, k8sClient, namespace1.Namespace())
		})

		It("can be deleted successfully", func() {
			By("deleting child namespaces first")
			common.DeleteGitLabNamespaceAndVerify(ctx, k8sClient, namespace2)
			common.DeleteGitLabNamespaceAndVerify(ctx, k8sClient, namespace3)

			By("checking root namespace still happy")
			common.ExpectGitLabNamespaceReady(ctx, defaultClient, namespace1)
			common.ExpectNamespacePodsReady(ctx, k8sClient, namespace1.Namespace())

			common.DeleteGitLabNamespaceAndVerify(ctx, k8sClient, namespace1)
		})
	})
})
