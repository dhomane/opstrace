package cluster

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	common "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	framework "gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/framework"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("cluster provisioning", Ordered, Serial, func() {
	var (
		ctx            context.Context
		testIdentifier string
		k8sClient      client.Client
		defaultClient  client.Client
	)

	cluster := &schedulerv1alpha1.Cluster{}
	BeforeAll(func() {
		ctx = context.Background()

		f, err := framework.GetInstance()
		Expect(err).ToNot(HaveOccurred())

		k8sClient, err = f.GetK8sClient()
		Expect(err).ToNot(HaveOccurred())

		defaultClient, err = f.GetDefaultClient()
		Expect(err).ToNot(HaveOccurred())

		testIdentifier, err = f.GetTestIdentifier()
		Expect(err).ToNot(HaveOccurred())

		testInfra, err := f.GetTestInfrastructure()
		Expect(err).ToNot(HaveOccurred())

		err = testInfra.GetGOBCluster(testIdentifier, cluster)
		Expect(err).ToNot(HaveOccurred())
	})

	Context("on creating an example cluster", func() {
		It("ensures it can be created successfully", func() {
			common.CreateOrUpdate(ctx, defaultClient, cluster)
			common.ExpectClusterReady(ctx, defaultClient, cluster)
			common.ExpectClusterPodsReady(ctx, defaultClient)
		})
	})

	Context("on deleting the example cluster", func() {
		It("ensures it can be deleted successfully", func() {
			common.DeleteCustomResourceAndVerify(ctx, k8sClient, cluster)
		})
	})
})
