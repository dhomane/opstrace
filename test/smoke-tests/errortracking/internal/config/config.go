package config

import (
	"errors"
	"os"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/applogger"
)

var (
	appConfig Configuration
	appLog    = applogger.Get()
)

type Configuration struct {
	grafanaApiToken                string // API token created in grafana used to authenticate calls
	sentryDSN                      string // Sentry dsn used to write errors for error tracking
	groupErrorTrackingEndpoint     string // ET group endpoint for which we can read an error
	errorTrackingIngestionDelaySec int    // Delay between writting and reading an error to ensure that reading will succeed
	logLevel                       int    // Log level. Values can be found in https://github.com/sirupsen/logrus/blob/master/logrus.go
	testsPeriodInSec               int    // Testing period in seconds
}

func GetGrafanaAPItoken() string {
	return appConfig.grafanaApiToken
}

func GetSentryDSN() string {
	return appConfig.sentryDSN
}

func GetGroupErrorTrackingEndpoint() string {
	return appConfig.groupErrorTrackingEndpoint
}

func GetErrorTrackingIngestionDelaySec() int {
	return appConfig.errorTrackingIngestionDelaySec
}

func GetLogLevel() int {
	return appConfig.logLevel
}

func GetTestsPeriodInSec() int {
	return appConfig.testsPeriodInSec
}

func InitializeConfig() error {

	appConfig.grafanaApiToken = strings.TrimSpace(os.Getenv("GRAFANA_API_TOKEN"))
	if appConfig.grafanaApiToken == "" {
		return errors.New("Grafana API token cannot be empty. Ensure GRAFANA_API_TOKEN is set properly")
	}

	appConfig.sentryDSN = strings.TrimSpace(os.Getenv("SENTRY_DSN"))
	if appConfig.sentryDSN == "" {
		return errors.New("Sentry DSN cannot be empty. Ensure SENTRY_DSN is set properly")
	}

	appConfig.groupErrorTrackingEndpoint = strings.TrimSpace(os.Getenv("GROUP_ERROR_TRACKING_ENDPOINT"))
	if appConfig.groupErrorTrackingEndpoint == "" {
		return errors.New("Group error tracking endpoint cannot be empty. Ensure GROUP_ERROR_TRACKING_ENDPOINT is set properly")
	}

	delaystr := strings.TrimSpace(os.Getenv("ERROR_TRACKING_INGESTION_DELAY_SEC"))
	if delaystr == "" {
		return errors.New("ERROR_TRACKING_INGESTION_DELAY_SEC cannot be empty")
	}
	var err error
	appConfig.errorTrackingIngestionDelaySec, err = strconv.Atoi(delaystr)
	if err != nil {
		return errors.New("ERROR_TRACKING_INGESTION_DELAY_SEC needs to be an integer number")
	}

	testsFrequencyInSecStr := strings.TrimSpace(os.Getenv("TESTS_PERIOD_SEC"))
	if testsFrequencyInSecStr == "" {
		return errors.New("TESTS_PERIOD_SEC cannot be empty")
	}
	appConfig.testsPeriodInSec, err = strconv.Atoi(testsFrequencyInSecStr)
	if err != nil {
		return errors.New("TESTS_PERIOD_SEC needs to be an integer number")
	}

	appConfig.logLevel = applogger.SetLevel(strings.TrimSpace(os.Getenv("LOG_LEVEL")))

	appLog.Infof("Config: Error Tracking ingestion delay in sec = %v", appConfig.errorTrackingIngestionDelaySec)

	return nil
}
