package errortracking

import (
	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/mock"
)

type MockSentry struct {
	mock.Mock
	SentryClient
}

func (m *MockSentry) Init(options sentry.ClientOptions) error {
	args := m.Called(options)
	return args.Error(0)
}

func (m *MockSentry) CaptureException(exception error) *sentry.EventID {
	args := m.Called(exception)
	return args.Get(0).(*sentry.EventID)
}
