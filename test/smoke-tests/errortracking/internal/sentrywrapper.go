package errortracking

import "github.com/getsentry/sentry-go"

type SentryClient interface {
	Init(options sentry.ClientOptions) error
	CaptureException(exception error) *sentry.EventID
}

type SentryClientWrapper struct{}

func (s *SentryClientWrapper) Init(options sentry.ClientOptions) error {
	return sentry.Init(options)
}

func (s *SentryClientWrapper) CaptureException(exception error) *sentry.EventID {
	return sentry.CaptureException(exception)
}
