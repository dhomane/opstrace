package errortracking

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/applogger"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/http_client"
)

const (
	SMOKE_TEST_ERROR_TEXT = "smoke-test-gop fake-error"
	FAILURE               = 0
	SUCCESS               = 1
)

var (
	rpErrorTrackingSuccess = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "smoketest_errortracking_success_read_path",
		Help: "Set to 1 if the error tracking read path succeeded",
	})

	wpErrorTrackingSuccess = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "smoketest_errortracking_success_write_path",
		Help: "Set to 1 if the error tracking write path succeeded",
	})

	appLog = applogger.Get()

	// Variable to be filled in by the makefile.
	// Release is used when sending error events using the Sentry SDK.
	// It refers to this application release.
	// Ignoring the value
	release string
)

// TransportWithHooks is an http.RoundTripper that wraps an existing
// http.RoundTripper adding hooks that run before and after each round trip.
type TransportWithHooks struct {
	http.RoundTripper
	Before func(*http.Request) error
	After  func(*http.Request, *http.Response, error) (*http.Response, error)
}

func (t *TransportWithHooks) RoundTrip(req *http.Request) (*http.Response, error) {
	if err := t.Before(req); err != nil {
		return nil, err
	}
	resp, err := t.RoundTripper.RoundTrip(req)
	return t.After(req, resp, err)
}

type ErrorTrackingTestRunner struct {
	SentryClient
	HttpClient http_client.HttpRequestInterface
	failure    error
}

func (g *ErrorTrackingTestRunner) RunUptimeTest() error {
	// We need to send errors to sentry in a sync way in order
	// to set an error before exiting the function in case something goes wrong
	sentrySyncTransport := sentry.NewHTTPSyncTransport()
	// We chose a number for timeout that give us enough confident that smoke-test
	// will not fail unexpectedly
	sentrySyncTransport.Timeout = time.Second * 6

	err := g.SentryClient.Init(sentry.ClientOptions{
		Debug:     (config.GetLogLevel() == int(logrus.DebugLevel) || config.GetLogLevel() == int(logrus.TraceLevel)),
		Release:   release,
		Dsn:       config.GetSentryDSN(),
		Transport: sentrySyncTransport,
		HTTPTransport: &TransportWithHooks{
			RoundTripper: http.DefaultTransport,
			Before: func(req *http.Request) error {
				return nil
			},
			After: func(req *http.Request, resp *http.Response, err error) (*http.Response, error) {
				if err != nil {
					g.failure = err
					return nil, err
				}
				if resp != nil {
					appLog.Infof("Sending data to Sentry: %v", resp.Status)
					if resp.StatusCode != http.StatusOK {
						g.failure = fmt.Errorf("sending error request to Gitlab Observability Platform failed with %v", resp.Status)
						return nil, g.failure
					}
				} else {
					g.failure = errors.New("sending error request to Gitlab Observability Platform failed due to nil response")
					return nil, g.failure
				}
				return resp, err
			},
		},
	})
	if err != nil {
		wpErrorTrackingSuccess.Set(FAILURE)
		return fmt.Errorf("Failed to initialize Sentry: %v", err)
	}

	// First we smoke test the write path
	fakeError := errors.New("smoke-test-gop fake-error")
	eventID := g.SentryClient.CaptureException(fakeError)
	if g.failure != nil {
		appLog.Errorf("Failed to send error: %v", g.failure)
		wpErrorTrackingSuccess.Set(FAILURE)
		return g.failure
	}
	appLog.Infof("reported to Sentry: %s - %v", fakeError, *eventID)
	wpErrorTrackingSuccess.Set(SUCCESS)

	// Then we smoke test the read path
	time.Sleep(time.Duration(config.GetErrorTrackingIngestionDelaySec()) * time.Second)
	url := config.GetGroupErrorTrackingEndpoint()
	req, err := g.HttpClient.CreateRequest(http.MethodGet, url, nil)
	if err != nil {
		rpErrorTrackingSuccess.Set(FAILURE)
		return err
	}

	// Query the last seen errors
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.GetGrafanaAPItoken()))
	resp, err := g.HttpClient.Do(req)
	defer func() { _ = resp.Body.Close() }()
	if err != nil {
		rpErrorTrackingSuccess.Set(FAILURE)
		return err
	}

	if resp.StatusCode != http.StatusOK {
		rpErrorTrackingSuccess.Set(FAILURE)
		return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", url, resp.StatusCode, resp.Status)
	}

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		rpErrorTrackingSuccess.Set(FAILURE)
		return err
	}

	var errorEvents []ErrorEvent
	err = json.Unmarshal(r, &errorEvents)
	if err != nil {
		rpErrorTrackingSuccess.Set(FAILURE)
		return err
	}

	for _, e := range errorEvents {
		// Find the error we previously wrote
		if strings.Contains(e.Description, SMOKE_TEST_ERROR_TEXT) {
			// Resolve it to avoid Gitlab error pollution
			appLog.Infof("Found %v", SMOKE_TEST_ERROR_TEXT)
			resolveUrl := fmt.Sprintf("%s/%d", url, e.Fingerprint)

			body := UpdateErrorEvent{
				Status: "resolved",
			}
			postBody, err := json.Marshal(body)
			if err != nil {
				rpErrorTrackingSuccess.Set(FAILURE)
				return err
			}
			req, err := g.HttpClient.CreateRequest(http.MethodPut, resolveUrl, bytes.NewReader(postBody))
			if err != nil {
				rpErrorTrackingSuccess.Set(FAILURE)
				return err
			}

			req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", config.GetGrafanaAPItoken()))
			req.Header.Add("Content-Type", "application/json")
			resp, err := g.HttpClient.Do(req)
			defer func() { _ = resp.Body.Close() }()
			if err != nil {
				rpErrorTrackingSuccess.Set(FAILURE)
				return err
			}

			if resp.StatusCode != http.StatusOK {
				rpErrorTrackingSuccess.Set(FAILURE)
				return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", resolveUrl, resp.StatusCode, resp.Status)
			}
			appLog.Infof("Resolved %v", SMOKE_TEST_ERROR_TEXT)
			rpErrorTrackingSuccess.Set(SUCCESS)

			return nil
		}
	}

	rpErrorTrackingSuccess.Set(FAILURE)
	return errors.New("failed to read " + SMOKE_TEST_ERROR_TEXT)

}

type ErrorEvent struct {
	Status      string `json:"status,omitempty"`
	ProjectId   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	Eventcount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

type UpdateErrorEvent struct {
	Status string `json:"status"`
}
