package http_client

import (
	"io"
	"net/http"

	"github.com/stretchr/testify/mock"
)

type MockHttpClient struct {
	mock.Mock
	HttpRequestInterface
}

func (m *MockHttpClient) Do(req *http.Request) (*http.Response, error) {
	args := m.Called(req)
	return args.Get(0).(*http.Response), args.Error(1)
}

func (m *MockHttpClient) CreateRequest(method, url string, body io.Reader) (*http.Request, error) {
	args := m.Called(method, url, body)
	return args.Get(0).(*http.Request), args.Error(1)
}

func (m *MockHttpClient) Get(url string) (*http.Response, error) {
	args := m.Called(url)
	return args.Get(0).(*http.Response), args.Error(1)
}
