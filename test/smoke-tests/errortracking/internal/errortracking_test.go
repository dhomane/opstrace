package errortracking

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/http_client"
)

const (
	GateKeeperHealthEndpoint   = "gatekeeper_endpoint"
	GouiHealthEndpoint         = "goui_endpoint"
	GrafanaApiToken            = "token"
	SentryDSN                  = "sentry_dsn"
	groupErrorTrackingEndpoint = "group_endpoint"
	errorTrackingIngDelaySec   = "0"
)

func InitializeConfig() {
	os.Setenv("GATEKEEPER_HEALTH_CHECK", GateKeeperHealthEndpoint)
	os.Setenv("GOUI_HEALTH_CHECK", GouiHealthEndpoint)
	os.Setenv("GRAFANA_API_TOKEN", GrafanaApiToken)
	os.Setenv("SENTRY_DSN", SentryDSN)
	os.Setenv("GROUP_ERROR_TRACKING_ENDPOINT", groupErrorTrackingEndpoint)
	os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", errorTrackingIngDelaySec)
	_ = config.InitializeConfig()
}

var (
	id              sentry.EventID
	sentryMock      = &MockSentry{}
	httpMock        = &http_client.MockHttpClient{}
	expectedErr     = errors.New("mock-error")
	expectedRequest = http.Request{Method: http.MethodGet, Header: make(map[string][]string, 1)}
	emptyResponse   = &http.Response{Body: io.NopCloser(strings.NewReader(""))}
	fingerprint     = 345
	resp            = []ErrorEvent{{Status: "unapproved", Fingerprint: 123, Description: "some description"},
		{Status: "unapproved", Fingerprint: fingerprint, Description: SMOKE_TEST_ERROR_TEXT}}
)

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

func TestErrorTrackingRunUptimeTest(t *testing.T) {
	assert := assert.New(t)
	et := ErrorTrackingTestRunner{
		SentryClient: sentryMock,
		HttpClient:   httpMock,
	}
	id = "d5b817dd84604ec998fa4287bbcde1b9"
	binResp, err := json.Marshal(resp)
	assert.Nil(err)
	InitializeConfig()
	expectedResolveUrl := fmt.Sprintf("%s/%d", config.GetGroupErrorTrackingEndpoint(), fingerprint)

	t.Run("Fails if sentry init fails", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(expectedErr)
		assert.True(strings.Contains(et.RunUptimeTest().Error(), expectedErr.Error()))
		sentryMock.AssertExpectations(t)
	})

	t.Run("Fails if it fails to create a request to query for getting the test error", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&http.Request{}, expectedErr)
		assert.Equal(expectedErr, et.RunUptimeTest())
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if it fails to perform the query for getting the test error", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(emptyResponse, expectedErr)
		assert.Equal(expectedErr, et.RunUptimeTest())
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if it the query for getting the test error returns a non 200 status code", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 400, Body: nopCloser{bytes.NewBufferString("data")}}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		assert.True(strings.Contains(et.RunUptimeTest().Error(), " status code: 400"))
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if it if fails to unmarshal the response", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)

		r := &http.Response{StatusCode: 200, Body: io.NopCloser(strings.NewReader(""))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		assert.True(strings.Contains(et.RunUptimeTest().Error(), "unexpected end of JSON input"))
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails to find the error in the response", func(t *testing.T) {
		resp := []ErrorEvent{{Status: "unapproved", Fingerprint: 123}, {Status: "unapproved", Fingerprint: 345}}
		binResp, err := json.Marshal(resp)
		assert.Nil(err)
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 200, Body: io.NopCloser(bytes.NewReader(binResp))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		assert.True(strings.Contains(et.RunUptimeTest().Error(), "failed to read "+SMOKE_TEST_ERROR_TEXT))
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Finds the error but fails to create the request to resolve it", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 200, Body: io.NopCloser(bytes.NewReader(binResp))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		httpMock.On("CreateRequest", http.MethodPut, expectedResolveUrl, mock.Anything).
			Times(1).Return(&http.Request{}, expectedErr)

		assert.Equal(expectedErr, et.RunUptimeTest())
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Finds the error but fails to do the request to resolve it", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		r := &http.Response{StatusCode: 200, Body: io.NopCloser(bytes.NewReader(binResp))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(r, nil)
		httpMock.On("CreateRequest", http.MethodPut, expectedResolveUrl, mock.Anything).
			Times(1).Return(&expectedRequest, nil)
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(emptyResponse, expectedErr)

		assert.Equal(expectedErr, et.RunUptimeTest())
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("The request for resolving the error returns a non 200 status code", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		fetchErrorsResp := &http.Response{StatusCode: 200, Body: io.NopCloser(bytes.NewReader(binResp))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(fetchErrorsResp, nil)
		httpMock.On("CreateRequest", http.MethodPut, expectedResolveUrl, mock.Anything).
			Times(1).Return(&expectedRequest, nil)
		resolveResp := &http.Response{StatusCode: 400, Body: nopCloser{bytes.NewBufferString("data")}}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(resolveResp, nil)

		assert.True(strings.Contains(et.RunUptimeTest().Error(), " status code: 400"))
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

	t.Run("Succeeds", func(t *testing.T) {
		sentryMock.On("Init", mock.Anything).Times(1).Return(nil)
		sentryMock.On("CaptureException", errors.New(SMOKE_TEST_ERROR_TEXT)).Times(1).Return(&id)
		httpMock.On("CreateRequest", http.MethodGet, config.GetGroupErrorTrackingEndpoint(), nil).
			Times(1).Return(&expectedRequest, nil)
		fetchErrorsResp := &http.Response{StatusCode: 200, Body: io.NopCloser(bytes.NewReader(binResp))}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(fetchErrorsResp, nil)
		httpMock.On("CreateRequest", http.MethodPut, expectedResolveUrl, mock.Anything).
			Times(1).Return(&expectedRequest, nil)
		resolveResp := &http.Response{StatusCode: 200, Body: nopCloser{bytes.NewBufferString("data")}}
		httpMock.On("Do", mock.AnythingOfType("*http.Request")).Times(1).Return(resolveResp, nil)

		assert.Nil(et.RunUptimeTest())
		sentryMock.AssertExpectations(t)
		httpMock.AssertExpectations(t)
	})

}
