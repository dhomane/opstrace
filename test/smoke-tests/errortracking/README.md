# Error Tracking Smoke tests (ETST)

This code is used to perform smoke tests for the read and write path of the error tracking application.
It constantly tests the error tracking application and generates metrics that are scraped by prometheus.

The gauge metrics generated can be queried in the `/metrics` path. These are:
- smoketest_errortracking_success_write_path: Is set to 1 when the write path of the error tracking smoke test succeeds
- smoketest_errortracking_success_read_path: Is set to 1 when the read path of the error tracking smoke test succeeds

ETST tests the write path by writting an error using the Sentry SDK. Then the read path is tested by reading the same error back.

## Setup
In order to run this application, first you need to generate a grafana API token. Visit the opstrace [GOUI](https://observe.gitlab.com/9970/group/apikeys), press add API key, give a name to the key and press add. Save the generated api key somewhere safe. Finally you need to find the Sentry DSN url in the settings of gitlab.com under the observability tab. Once you have the API key and the Sentry DSN then you can execute the following commands:
```
export GRAFANA_API_TOKEN=<your_api_token>
export SENTRY_DSN=<sentry_dsn>
make docker-build
make docker-run
```

You can run the unit tests of this app by running:
```
make run-tests
```

