include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  # default CI template targetting merge requests, tags or main
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml"
  # ClickHouse operator build jobs
  - local: clickhouse-operator/.gitlab-ci.yml

stages:
  - prepare
  - lint
  - build
  - test
  - images
  - image_scanning
  - e2e
  - cleanup

variables:
  NEXT_RELEASE_TAG: 0.2.0
  DOCKER_VERSION: "20.10.14"
  CHECKOUT_VERSION_STRING: ${CI_COMMIT_SHORT_SHA}-ci
  DOCKER_IMAGE_REGISTRY: registry.gitlab.com/gitlab-org/opstrace/opstrace
  # Bump the CI image version if the image changes or needs refreshing
  CI_IMAGE: $DOCKER_IMAGE_REGISTRY/ci:0.0.7
  ## runner feature flag to try and speed up cache zipping - https://docs.gitlab.com/runner/configuration/feature-flags.html
  FF_USE_FASTZIP: "true"
  # compression levels set to fast (larger cache files)
  ARTIFACT_COMPRESSION_LEVEL: "fast"
  CACHE_COMPRESSION_LEVEL: "fast"
  GOMODCACHE: $CI_PROJECT_DIR/.go-mod-cache
  GO_VERSION: "1.19.3"

# parallel matrix for jobs to cover all our Go modules
.go-module-parallel:
  image: golang:${GO_VERSION}
  cache: &gomod-cache
    key:
      gomodules_${GOMOD_PATH}
      # would be great to use files based cache key based on var but you can't - https://gitlab.com/gitlab-org/gitlab/-/issues/118466
      # files:
      #   - $GOMOD_PATH/go.sum
    paths:
      - $GOMODCACHE
    policy: pull
  parallel:
    matrix:
      - GOMOD_PATH: go
      - GOMOD_PATH: tenant-operator
      - GOMOD_PATH: scheduler
      - GOMOD_PATH: gatekeeper
      - GOMOD_PATH: clickhouse-operator

# default jobs run with the custom CI image
default:
  image: $CI_IMAGE
  tags:
    # cost optimized non-docker runners by default
    - gitlab-org
  retry:
    max: 2
    when:
      - runner_system_failure
  interruptible: true

build CI image:
  stage: prepare
  extends:
    - .docker
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - containers/ci/opstrace-ci.Dockerfile
  script:
    # out Makefile is set up using bash, override this here (Alpine)
    - make BUILD_ARGS="--build-arg DOCKER_VERSION=$DOCKER_VERSION" rebuild-ci-container-image
    - make push-ci-container-image

# use for jobs that can start any time after the CI image is built
# optional - CI image only builds when changed
.needs-ci-image: &needs-ci-image
  - job: build CI image
    optional: true

gomod download:
  stage: prepare
  extends:
    - .go-module-parallel
  script:
    - cd $GOMOD_PATH && go mod download -x
  cache:
    policy: pull-push

lint docs:
  stage: lint
  image:
    name: "ghcr.io/igorshubovych/markdownlint-cli:latest"
    entrypoint: [""]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - .gitlab/ci/**/*
        - .gitlab-ci.yml
        - Makefile
        - "**/Makefile"
        - ci/**/*
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - .markdownlint.json
        - .gitattributes
        - docs/**/*
        - README.md
  cache: []
  script:
    # Override the entrypoint to be able to point at the markdown files.
    - /usr/local/bin/markdownlint "/builds/gitlab-org/opstrace/opstrace/docs/**/*.md" README.md

lint code:
  stage: lint
  image: golangci/golangci-lint:v1.50.1
  extends:
    - .go-module-parallel
  script:
    - make lint-code-${GOMOD_PATH}
  cache: []

lint terraform:
  stage: lint
  image: alpine/terragrunt:1.3.0
  before_script:
    # unset this variable used for e2e testing so we can do strict input validation
    - unset TF_VAR_registry_auth_token
    # use terragrunt env to test variable inputs/dependencies
    - cd terraform/environments/test-terragrunt
  script:
    - terragrunt run-all validate-inputs --terragrunt-strict-validate
    - terragrunt run-all validate
    - terragrunt run-all plan -refresh=false

# This stage verifies that all the changes to manifests were commited to the
# repo.
verify manifests sync:
  stage: lint
  script:
    - make regenerate
    - make -C scheduler api-docs manifests
    - make -C tenant-operator manifests
    - make -C clickhouse-operator manifests
    - git status
    - test -z "$(git status --porcelain)"
  cache: []

build:
  stage: build
  extends:
    - .go-module-parallel
  needs:
    # No point in building anything if we aren't building what is commited to
    # the repo.
    - verify manifests sync
  script:
    - make build-${GOMOD_PATH}

smoke tests build:
  stage: build
  extends:
    - .docker
  script:
    - cd test/smoke-tests/errortracking
    - make docker-build
    - make docker-push

unit test:
  stage: test
  extends:
    # docker for tests that user testcontainers
    - .docker
    - .go-module-parallel
  # we don't need init to login to registry
  before_script: []
  script:
    - make unit-tests-${GOMOD_PATH}
  needs: [build]

smoke tests unit-testing:
  image: golang:1.19.3
  stage: test
  script:
    - cd test/smoke-tests/errortracking
    - make run-tests

images:
  stage: images
  extends:
    - .go-module-parallel
    # docker second, to use the docker specific image here
    - .docker
  script:
    - make docker-${GOMOD_PATH}
  needs: [unit test]
  cache: []

# generate child pipeline content based on controller image tags and docker-images.json
image scan config:
  stage: image_scanning
  needs:
    - job: images
      optional: true
  script:
    - >
      cp -v ${PIPELINE_FILE}.header ${PIPELINE_FILE}
    - >
      cat go/pkg/constants/docker-images.json |
      jq -r 'to_entries | .[] |
      "\(.key) image scan:
        extends: [.scanning_base]
        variables:
          DOCKER_IMAGE: \(.value)"' >> $PIPELINE_FILE
    - >
      for i in $(sed -n -e 's/^\s\+-\s\+GOMOD_PATH:\s\+//p' .gitlab-ci.yml); do
        for IMG in $(make -sC $i print-docker-images); do
          echo -e "$i image scan:\n        extends: [.scanning_base]\n        variables:\n          DOCKER_IMAGE: $IMG\n" >> $PIPELINE_FILE
        done
      done
  artifacts:
    paths:
      - $PIPELINE_FILE
  variables:
    PIPELINE_FILE: .gitlab/ci/container_scanning.yml

image scan pipeline:
  stage: image_scanning
  needs:
    - image scan config
  trigger:
    include:
      - artifact: .gitlab/ci/container_scanning.yml
        job: image scan config

cleanup gke disks:
  stage: cleanup
  image: google/cloud-sdk:latest
  script:
    - gcloud auth activate-service-account --key-file=${GOOGLE_APPLICATION_CREDENTIALS} --project ${OPSTRACE_DEV_GCP_PROJECT_ID}
    # cleanup orphaned gke compute disks older than yesterdays date
    - |
      disks=$(gcloud compute disks list --filter="-users:* labels.goog-gke-volume='' lastAttachTimestamp<$(date +%Y-%m-%d -d yesterday)" --format 'value(uri())')
      [ ! -z "$disks" ] && gcloud compute disks delete $disks || echo "No disks to delete"

# docker defaults
.docker:
  image: docker:${DOCKER_VERSION}-git
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_VERIFY: 1
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_CERT_PATH: "/certs/client"
  tags:
    - gitlab-org-docker
  before_script:
    # Note(joe): alpine docker image doesn't have Make, we might consider baking a custom base image if this is slow,
    - apk add --no-cache make bash
    - echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

# overrides for security scanning - included at the top

# make the tools work on merge request pipelines as they check for CI_COMMIT_BRANCH
.security_tool: &security_tool
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_BRANCH
  needs: []

gemnasium-dependency_scanning:
  <<: *security_tool

kics-iac-sast:
  <<: *security_tool

kubesec-sast:
  <<: *security_tool

semgrep-sast:
  <<: *security_tool

license_scanning:
  <<: *security_tool
  needs:
    - job: gomod download
      optional: true
  variables:
    LICENSE_FINDER_CLI_OPTS: "--enabled-package-managers=gomodules --aggregate_paths= go/ tenant-operator/ clickhouse-operator/ scheduler/ gatekeeper/"
  cache:
    - *gomod-cache
