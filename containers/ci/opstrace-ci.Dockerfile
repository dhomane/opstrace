# The image defined by this Dockerfile here is supposed to provide all dependencies
# required by CI
FROM debian:buster-slim

# - moreutils, for `chronic` and `ts`.
# - make, for running make targets in the container
# - git, for interacting with the current checkout.
# - gettext-base for envsubst
# - uuid-runtime for uuidgen used by ci_events
# - netcat for debugging
# - tree, because we love nature
RUN apt-get update && apt-get install -y -q --no-install-recommends \
    uuid-runtime rsync curl gnupg2 git make jq moreutils netcat-openbsd \
    build-essential gettext-base ca-certificates unzip less tree python3 \
    # Playwright browser automation system dependencies:
    libnss3 libcups2 libnspr4 libatk1.0-0 libatk-bridge2.0-0 \
    libdbus-c++-1-0v5 libdrm2 libxkbcommon0 libxcomposite1 \
    libxdamage1 libxfixes3 libxrandr2 libgbm1 libgtk-3-0 \
    libasound2 libatspi2.0-0 libxshmfence1 \
    # Terraform dependencies
    software-properties-common openssh-server

# gcloud CLI, for managing GCP
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | \
    tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
    apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update && apt-get install -y -q --no-install-recommends google-cloud-sdk
RUN apt-get -y autoclean

RUN gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image

# Install kubectl, discover current stable version dynamically.
RUN KVERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    echo "install kubectl ${KVERSION}" && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/${KVERSION}/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl

# Install terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install terraform
# Check install succeeded
RUN terraform -help

# AWS CLI, for fetching data from S3. Mount this into the container: ~/.aws
# (the home dir has AWS credentials set up for accessing our S3 secrets bucket.)
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip -q awscliv2.zip && \
    ./aws/install && \
    rm awscliv2.zip

# Set up the Docker binaries so that within the container we can manage
# containers running on the host with the `docker` CLI (the host's Docker
# socket is going to be mounted in).
ARG DOCKER_VERSION=20.10.14
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz && \
    tar --strip-components=1 -xvzf docker-${DOCKER_VERSION}.tgz -C /usr/local/bin && \
    rm -f docker-${DOCKER_VERSION}.tgz

# Set up golang. Required to run golanglint-ci in the linter step.
ENV GOLANG_VERSION 1.19
RUN curl -fsSLO https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz && \
    tar -xzf go${GOLANG_VERSION}.linux-amd64.tar.gz -C /usr/local/ && \
    rm -f go${GOLANG_VERSION}.linux-amd64.tar.gz

ENV GOPATH /go

ENV PATH /usr/local/go/bin:$GOPATH/bin:$PATH

# Set up golanglint-ci in the container image.
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | \
    sh -s -- -b /usr/local/bin v1.50.1

# Register build args, set defaults. GID and UID are expected to be overridden
# in CI.
ARG CIUNAME=ciuser
ARG CIUID=1000
ARG CIGID=1000

RUN echo "set up user $CIUNAME / $CIUID in group $CIGID"
RUN groupadd -g $CIGID -o $CIUNAME
RUN useradd -m -u $CIUID -g $CIGID -o -s /bin/bash $CIUNAME
USER $CIUNAME
