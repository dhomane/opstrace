terraform {
    source = "${get_terragrunt_dir()}/../../../..//terraform/modules/sql"
}

include {
    path = find_in_parent_folders()
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        vpc_id = "projects/mock/global/networks/mock"
        vpc_name = "mock"
    }
    mock_outputs_allowed_terraform_commands = ["validate", "validate-inputs", "plan"]
}

inputs = {
    instance_name   = local.common_vars.inputs.instance_name
    project_id      = local.common_vars.inputs.project_id
    region          = local.common_vars.inputs.region
    location        = local.common_vars.inputs.location
    vpc_name        = dependency.gke.outputs.vpc_name
    vpc_id          = dependency.gke.outputs.vpc_id
}