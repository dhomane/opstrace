# Test with Terragrunt

This test project uses [terragrunt](https://terragrunt.gruntwork.io/) to handle the module dependencies.

Use `terragrunt run-all apply` to create the environment.

`env.hcl` contains default values that you must override.

To do this you can provide a `custom.hcl` with an `inputs = {...}` definition with your values in the same dir as `env.hcl`.

You can also override the module inputs using the standard terraform [variable definition precedence](https://www.terraform.io/language/values/variables#variable-definition-precedence).

Most module inputs can be set via terraform vars, with the exception of those used in `cluster.cluster_manifest` which must be set in the terragrunt inputs.
You can also the `cluster_manifest` explicitly as a terraform var.
