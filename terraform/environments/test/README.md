# Test

This terraform environment will create an instance of GitLab Omnibus on GCP, create an instance-wide OAuth Application and then create an Opstrace instance on GCP configured with the OAuth Application.

This environment serves primarily for testing, either in CI or by developers.
