terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gke"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    instance_name   = get_env("TF_VAR_instance_name")
    project_id      = get_env("TF_VAR_project_id")
    region          = get_env("TF_VAR_region")
    location        = get_env("TF_VAR_location")
    zone            = get_env("TF_VAR_zone")
    kubeconfig_path = "${get_terragrunt_dir()}/../.kubeconfig"
}