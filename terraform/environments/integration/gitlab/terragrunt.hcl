terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gitlab/gcp"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    skip_outputs = true
}

dependency "sql" {
    config_path = "../sql"
    skip_outputs = true
}

inputs = {
    instance_name   = get_env("TF_VAR_instance_name")
    project         = get_env("TF_VAR_project_id")
    region          = get_env("TF_VAR_region")
    location        = get_env("TF_VAR_location")
    zone            = get_env("TF_VAR_zone")

    # Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
    opstrace_domain = get_env("TF_VAR_opstrace_domain")
    # Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
    # https://letsencrypt.org/docs/expiration-emails
    acme_email      = get_env("TF_VAR_acme_email")

    # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
    domain = get_env("TF_VAR_domain")
    gitlab_domain   = get_env("TF_VAR_gitlab_domain")
    # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
    # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
    cloud_dns_zone  = get_env("TF_VAR_cloud_dns_zone")

    # GitLab registry username for pulling a specific GitLab image
    # Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
    # test against any work in development
    registry_username = get_env("TF_VAR_registry_username")
    ################ NOTE #########################
    # Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    # Set this as an environment variable to prevent leaking it in SCM so instead of:
    # registry_auth_token = "your_token"
    # -> export TF_VAR_registry_auth_token = "your_token"

    # The image to use when deploying GitLab
    gitlab_image    = get_env("TF_VAR_gitlab_image")
}