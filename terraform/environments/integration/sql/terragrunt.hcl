terraform {
    source = "${get_terragrunt_dir()}/../../../modules//sql"
}

include {
    path = find_in_parent_folders()
}

dependency "gke" {
    config_path = "../gke"
    mock_outputs = {
        kubernetes_cluster_host = "https://mock"
        kubernetes_cluster_certificate = "mock"
        kubeconfig_path = "~/.kube/config"
        vpc_id = "mock"
        vpc_name = "mock"
    }
    mock_outputs_merge_with_state = true
    mock_outputs_merge_strategy_with_state = "shallow"
    mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

inputs = {
    instance_name   = get_env("TF_VAR_instance_name")
    project_id      = get_env("TF_VAR_project_id")
    region          = get_env("TF_VAR_region")
    location        = get_env("TF_VAR_location")
    vpc_id          = dependency.gke.outputs.vpc_id
    vpc_name        = dependency.gke.outputs.vpc_name
}