terraform {
    source = "${get_terragrunt_dir()}/../../../modules//gke"
}

include {
    path = find_in_parent_folders()
}

locals {
    common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

inputs = {
    instance_name   = local.common_vars.inputs.instance_name
    project_id      = local.common_vars.inputs.project_id
    region          = local.common_vars.inputs.region
    location        = local.common_vars.inputs.location
    zone            = local.common_vars.inputs.zone
    kubeconfig_path = "${get_terragrunt_dir()}/../.kubeconfig"
}