terraform {
  source = "${get_terragrunt_dir()}/../../../modules//aws"
}

include {
  path = find_in_parent_folders()
}

locals {
  common_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

dependency "gke" {
  config_path = "../gke"
  mock_outputs = {
    kubernetes_cluster_host = "https://mock"
    kubernetes_cluster_certificate = "mock"
    kubeconfig_path = "~/.kube/config"
    vpc_id = "mock"
    vpc_name = "mock"
  }
  mock_outputs_merge_with_state = true
  mock_outputs_merge_strategy_with_state = "shallow"
  mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

inputs = {
  cluster_endpoint = dependency.gke.outputs.kubernetes_cluster_host
  cluster_ca_certificate = dependency.gke.outputs.kubernetes_cluster_certificate
  kubeconfig_path = dependency.gke.outputs.kubeconfig_path
  clickhouse_s3_bucket = local.common_vars.inputs.clickhouse_s3_bucket
  aws_region = local.common_vars.inputs.aws_region
  s3_secret_namespace = local.common_vars.inputs.s3_secret_namespace
  clickhouse_s3_user_name = local.common_vars.inputs.clickhouse_s3_user_name
}
