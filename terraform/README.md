# Opstrace Terraform

## Requirements

* Tested with terraform version >= 1.1.9
* Google GCP credentials

## What to Expect

The `environments/test` terraform environment will create an instance of GitLab Omnibus on GCP, create an instance-wide OAuth Application and then create an Opstrace instance on GCP configured with the OAuth Application. In other words, it will deploy GitLab and Opstrace together and automatically configure their connection.

This environment serves primarily for testing, either in CI or by developers and could serve as a future reference for automating the connection
between GitLab/Opstrace when Opstrace is packages with GitLab.

## Terraform Outputs

The following terraform outputs are available through `terraform output`:

* `gitlab_url`: The URL of the GitLab instance
* `admin_token`: An Admin Auth Token for the GitLab instance. Can be used to create/manage users/groups for testing
* `root_password`: The Root User Password for the GitLab instance to login as the administrator

These outputs can be used to login as the root user or to automate additional configuration for testing.

## Instructions

[Set up GCP credentials](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials).

Ensure you have two [DNS zones in GCP set up](https://cloud.google.com/dns/docs/zones), one each for Opstrace and GitLab. These will represent
the DNS for Opstrace and GitLab and are required inputs in the terraform variables below.

Then move to the test directory to set up a test instance:

```bash
cd environments/test
```

Initialize terraform:

```bash
terraform init
```

Edit `terraform.tfvars` and pick a unique `instance_name` and set up the `domain`.

```yaml
project_id = "vast-pad-240918"
# Unique name within project_id for naming resources
instance_name = "mat-dev"
# Number of nodes for Opstrace instance
num_nodes     = 3
region        = "us-west2"
location      = "us-west2-a"
zone          = "us-west2-a"

# Defaults to letsencrypt-staging for Opstrace, uncomment to change:
# cert_issuer = "letsencrypt-prod"

# Domain for Opstrace. This requires setting up a DNS zone in the GCP project manually.
domain = "mat-demo.opstrace.io"
# Optionally set an email to opt in for certificate expiry notices from LetsEncrypt
# https://letsencrypt.org/docs/expiration-emails
acme_email = "mat@opstrace.com"

# GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
gitlab_domain  = "mats-gitlab.gcp.opstrace.io"
# GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
# It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
cloud_dns_zone = "mats-gitlab-gcp"

# GitLab registry username for pulling a specific GitLab image
# Any GitLab image can be used that is not public, i.e. images created in merge_requests, so we can
# test against any work in development
registry_username = "mappelman"
################ NOTE #########################
# Personal Auth Token to access the GitLab registry https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
# Set this as an environment variable to prevent leaking it in SCM so instead of:
# registry_auth_token = "your_token"
# -> export TF_VAR_registry_auth_token = "your_token"

# The image to use when deploying GitLab
gitlab_image = "registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:8652627bf86d02d20599b1e0caad1465bb545481"
```

**NOTE: choose a short instance_name to avoid resource names that are too long for the cloud provider**. This instance name is prepended to all resource names. If you see an error that looks like:

```bash
"account_id" ("mat-dev-externaldns") doesn't match regexp "^[a-z](?:[-a-z0-9]{4,28}[a-z0-9])$"
```

Then your instance name is too long (entire name violates the regexp of max 28 chars).

Export sensitive tfvars as environment variables:

```bash
export TF_VAR_registry_auth_token = "your_gitlab_personal_token"
```

Check the plan:

```bash
terraform plan
```

Proceed to install and configure GitLab, Opstrace infrastructure, install the Scheduler and set the Cluster custom resource:

```bash
terraform apply
```

## Cleanup

To destroy, run:

```bash
terraform destroy
```
