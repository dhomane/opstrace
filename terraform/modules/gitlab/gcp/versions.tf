terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.7.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }

  }

  required_version = ">= 1.1.9"
}