
output "oauth_client_id" {
  value = jsondecode(data.local_file.opstrace_config.content).application_id
  description = "OAuth Application client ID"
  sensitive = true

  depends_on = [
    null_resource.gitlab_docker
  ]
}

output "oauth_client_secret" {
  value = jsondecode(data.local_file.opstrace_config.content).secret
  description = "OAuth Application client Secret"
  sensitive = true

  depends_on = [
    null_resource.gitlab_docker
  ]
}

output "gitlab_url" {
  value = jsondecode(data.local_file.opstrace_config.content).gitlabURL
  description = "GitLab URL for Opstrace to connect to"

  depends_on = [
    null_resource.gitlab_docker
  ]
}

output "admin_token" {
  value = jsondecode(data.local_file.opstrace_config.content).adminAuthToken
  description = "Admin auth token for the GitLab instance. Can be used to create/manage users/groups for testing"
  sensitive = true

  depends_on = [
    null_resource.gitlab_docker
  ]
}

output "root_password" {
  value = jsondecode(data.local_file.opstrace_config.content).rootUserPassword
  description = "Root User Password for GitLab"
  sensitive = true

  depends_on = [
    null_resource.gitlab_docker
  ]
}

output "internal_endpoint_token" {
  value = "anystringfornow"
  description = "Shared Token for Error Tracking Authentication"
  sensitive = true

  depends_on = [
    null_resource.gitlab_docker
  ]
}
