# Terraform module to setup AWS resources (currently only S3)

## Prerequisite

* Access to a GCP project via GOOGLE_APPLICATION_CREDENTIALS or via ADC (application default credentials).
* Access to an AWS project via configured  `aws` cli and export your IAM credentials via shell variables
     `AWS_ACCESS_ID` and `AWS_SECRET_ACCESS_KEY`.

  It is expected to have at least admin or greater privileges to the projects in both of the above cases.

In current setup the GCP credentials are used to create a kubernetes secret resource on the cluster endpoint provided.

AWS credentials are used to create S3 buckets that will be used by Clickhouse. It is also used to create corresponding
IAM user and policies that allow access to the bucket.

### Settings specific to AWS components
```terraform
    clickhouse_s3_bucket = "clickhouse-s3-bucket"
    # Currently this only decides the region where the bucket would be created as there is no `aws_s3_bucket` specific configuration
    aws_region = "eu-west-3"
    s3_secret_namespace = "default"
    clickhouse_s3_user_name = "clickhouse-user"
```