---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-nginx-ingress-rules
spec:
  groups:
    - name: nginx
      rules:
        - alert: NGINXAbsent
          annotations:
            title: NGINX Ingress is gone
            description: NGINX ingress has dissapeared from Prometheus service discovery.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
          expr: |
            absent(up{job="ingress-nginx-metrics"} == 1)
          for: 5m
          labels:
            alertname: NGINXAbsent
            type: nginx
            severity: s1
            alert_type: cause

        - alert: NGINXConfigFailed
          annotations:
            title: NGINX Ingress configuration is invalid
            description: NGINX ingress config test failed, configuration is invalid
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
          expr: |
            count(nginx_ingress_controller_config_last_reload_successful == 0) > 0
          for: 5m
          labels:
            alertname: NGINXConfigFailed
            type: nginx
            severity: s3
            alert_type: cause

        - alert: NGINXCertificateExpiry
          annotations:
            title: NGINX Ingress certificate is about to expire
            description: ssl certificate for host {{ $labels.host }} will expire in less then a week
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
          expr: (min(nginx_ingress_controller_ssl_expire_time_seconds) by (host) - time()) < 604800
          for: 5m
          labels:
            alertname: NGINXCertificateExpiry
            type: nginx
            severity: s3
            alert_type: cause

        - alert: NGINXTooMany500s
          annotations:
            title: Number of 5xx is high
            description: More than 3% of all requests returned 5XX
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
          expr: |
            sum(rate(nginx_ingress_controller_requests{status=~"5.*"}[1m])) by (exported_service)
              /
            sum(rate(nginx_ingress_controller_requests[1m])) by (exported_service)
              > .03
          for: 5m
          labels:
            alertname: NGINXTooMany500s
            type: nginx
            severity: s2
            alert_type: cause

# NOTE(prozlach) In production, 50% of our requests are 400s :( Disabling the
# alert for now.
#        # 404s are pretty normal as we do not support lots of sentry APIs yet
#        - alert: NGINXTooMany400s
#          annotations:
#            title: Number of 4xx is high
#            description: More than 5% of all requests returned 4XX that are non-404s
#            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
#            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
#          expr: |
#            sum(rate(nginx_ingress_controller_requests{status=~"4..", status!="404"}[1m])) by (exported_service)
#              /
#            sum(rate(nginx_ingress_controller_requests{status!="404"}[1m])) by (exported_service)
#              > .05
#          for: 5m
#          labels:
#            alertname: NGINXTooMany400s
#            type: nginx
#            severity: s3
#            alert_type: cause

        - alert: NginxLatencyHigh
          annotations:
            title: Nginx latency high
            description: Nginx p99 latency is higher than 3 seconds
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-ingress-nginx/observability-ingress-nginx
          expr: |
            histogram_quantile(0.99, sum(rate(nginx_ingress_controller_request_duration_seconds_bucket[5m])) by (host, path, le)) > 3
          for: 5m
          labels:
            alertname: NginxLatencyHigh
            type: nginx
            severity: s3
            alert_type: cause
