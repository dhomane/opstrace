data "google_client_config" "default" {}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.location
}

provider "kubernetes" {
  host = var.cluster_endpoint
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

provider "kubernetes-alpha" {
  host = "https://${var.cluster_endpoint}"
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

provider "kustomization" {
  kubeconfig_path = var.kubeconfig_path
}
