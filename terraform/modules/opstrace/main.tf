# TODO: create a secret for the docker hub credentials.
# When we do this, we'll also have to plumb all imagePullSecrets in the
# CRs (Cluster, Tenant) through to the container definitions.
# Some are plumbed through, but not all.
# https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1717

resource "kubernetes_secret" "postgres-secret" {
  metadata {
    name      = "postgres-secret"
    namespace = "kube-system"
  }

  data = {
    endpoint = var.postgres_dsn_endpoint
  }

}

# -------------------------------------------------------------------------------------------
# Opstrace - scheduler, tenant-operator, clickhouse-operator, BlackBox-exporter, smoke tests
# -------------------------------------------------------------------------------------------

# BlackBox Exporter secret holding grafana API key used for auth
# for the GOUI probe
resource "kubernetes_secret" "bbe-api-key" {
  metadata {
    name = "bbe-api-key"
  }

  data = {
    apikey = var.blackbox_exporter_api_key
  }
}

resource "kubernetes_secret" "smoketests-errortracking-secret" {
  metadata {
    name = "smoketests-errortracking-secret"
  }

  data = {
    grafanaapitoken = var.blackbox_exporter_api_key
    sentrydsn = var.sentry_dsn
  }
}

resource "kubernetes_config_map" "smoketests-errortracking-config" {
  metadata {
    name = "smoketests-errortracking-config"
  }

  data = {
    group_error_tracking_endpoint  = var.group_error_tracking_endpoint
    error_tracking_ingestion_delay_sec   = "10"
    log_level = "INFO"
    test_period_sec = "10"
  }

}

data "kustomization_build" "scheduler-crds" {
  path = "${path.module}/../../../scheduler/config/crd"
}

resource "kustomization_resource" "scheduler-crds" {
  for_each = data.kustomization_build.scheduler-crds.ids
  manifest = data.kustomization_build.scheduler-crds.manifests[each.value]
}

data "kustomization_build" "alerting" {
  path = "${path.module}/alerting/overlays/${var.environment}"
}

resource "kustomization_resource" "alerting" {
  for_each = data.kustomization_build.alerting.ids
  manifest = data.kustomization_build.alerting.manifests[each.value]

  # We need Prometheus Operators CRDs
  depends_on = [
    kustomization_resource.scheduler-crds,
  ]
}

data "kustomization_build" "tenant-operator-crds" {
  path = "${path.module}/../../../tenant-operator/config/crd"
}

resource "kustomization_resource" "tenant-operator-crds" {
  for_each = data.kustomization_build.tenant-operator-crds.ids
  manifest = data.kustomization_build.tenant-operator-crds.manifests[each.value]
}

data "kustomization_build" "clickhouse-operator-crds" {
  path = "${path.module}/../../../clickhouse-operator/config/crd"
}

resource "kustomization_resource" "clickhouse-operator-crds" {
  for_each = data.kustomization_build.clickhouse-operator-crds.ids
  manifest = data.kustomization_build.clickhouse-operator-crds.manifests[each.value]
}

// Since our upstream kustomize manifests here override the scheduler image name
// using environment variables which TF doesn't really like, we just add a kustomize
// overlay here which patches the image name as was passed to this module.
data "kustomization_overlay" "scheduler" {
  resources = [
    "${path.module}/../../../scheduler/config/deploy",
  ]
  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: ${var.scheduler_image}
    EOF
    target {
      group   = "apps"
      version = "v1"
      kind    = "Deployment"
      name    = "scheduler-controller-manager"
    }
  }
}

resource "kustomization_resource" "scheduler" {
  for_each = data.kustomization_overlay.scheduler.ids
  manifest = data.kustomization_overlay.scheduler.manifests[each.value]

  depends_on = [
    kustomization_resource.scheduler-crds,
    kustomization_resource.tenant-operator-crds,
  ]
}

resource "google_container_node_pool" "clickhouse_nodes" {
  name = "clickhouse-nodes"

  cluster    = var.gke_cluster_name
  node_count = var.ch_nodepool_nodes_number

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = merge(var.global_labels, tomap({
      env = var.project_id
    }))

    taint = [{
      key    = "nodepool"
      value  = "clickhouse-nodes"
      effect = "NO_SCHEDULE"
    }]

    # preemptible  = true
    machine_type = "n1-standard-2"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}


data "kustomization_overlay" "blackbox-exporter-manifest" {
  resources = ["${path.module}/blackbox-exporter"]
  patches {

    target {
      kind = "ServiceMonitor"
      name = "gatekeeper-probe"
    }
    patch  = <<-EOF
      - op: replace
        path: /spec/endpoints/0/params/target/0
        value: "${var.gatekeeper_probe_url}"
    EOF

  }

  patches {

    target {
      kind = "ServiceMonitor"
      name = "goui-probe"
    }
    patch  = <<-EOF
      - op: replace
        path: /spec/endpoints/0/params/target/0
        value: "${var.goui_probe_url}"
    EOF

  }
}

resource "kustomization_resource" "blackbox-exporter-manifest" {
  for_each = data.kustomization_overlay.blackbox-exporter-manifest.ids
  manifest = data.kustomization_overlay.blackbox-exporter-manifest.manifests[each.value]
  depends_on = [
    kubernetes_secret.bbe-api-key,
  ]
}

data "kustomization_build" "smoketests-manifest" {
  path = "${path.module}/../../../terraform/modules/opstrace/smoketests"
}

resource "kustomization_resource" "smoketests-manifest" {
  for_each = data.kustomization_build.smoketests-manifest.ids
  manifest = data.kustomization_build.smoketests-manifest.manifests[each.value]
  depends_on = [
    kubernetes_secret.smoketests-errortracking-secret,
  ]
}

