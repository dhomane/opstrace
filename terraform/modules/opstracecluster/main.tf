resource "kubernetes_secret" "cluster" {
  provider = kubernetes
  metadata {
    name      = var.cluster_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    gitlab_oauth_client_id     = var.gitlab_oauth_client_id
    gitlab_oauth_client_secret = var.gitlab_oauth_client_secret
    internal_endpoint_token    = var.internal_endpoint_token
  }

  immutable = true
}

resource "kubernetes_secret" "cloudflare" {
  provider = kubernetes

  metadata {
    name = var.cloudflare_secret_name
    namespace = var.cloudflare_secret_namespace
  }

  data = {
    "CF_API_TOKEN" = var.cloudflare_api_token
  }

  immutable = true
}

# Bear in mind, setting up `disable_cluster_creation`
# on an already existing resource would actually delete
# it. This flag is only used to consume the module without
# creating a cluster initially.
resource "kubectl_manifest" "cluster" {
  count = var.disable_cluster_creation ? 0 : 1
  yaml_body = var.cluster_manifest
}
