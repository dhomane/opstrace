data "google_client_config" "default" {}

provider "kubernetes" {
  host = var.cluster_endpoint
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

provider "kustomization" {
  kubeconfig_path = var.kubeconfig_path
}

provider "kubectl" {
  host                   = var.cluster_endpoint
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
  load_config_file       = false
}