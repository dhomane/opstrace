variable "project_id" {
  type = string
}

variable "bucket_name" {
  type        = string
  description = "name of our bucket"
}

variable "bucket_location" {
  type        = string
  description = "location of our bucket"
  default     = "us-east-1"
}

variable "storage_class" {
  type = string
}

variable "bucket_root_path" {
  type    = string
  default = "dbdata/"
}

variable "cluster_endpoint" {
  type        = string
  description = "cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "cluster CA certificate"
}

variable "secret_name" {
  type    = string
  default = "clickhouse-gcs-secret"
}

variable "secret_namespace" {
  type    = string
  default = "default"
}

variable "gcs_access_key_id" {
  type      = string
  sensitive = true
}

variable "gcs_access_key_secret" {
  type      = string
  sensitive = true
}
