provider "google" {
  project = var.project_id
}

data "google_client_config" "default" {}
provider "kubernetes" {
  host                   = var.cluster_endpoint
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = var.cluster_ca_certificate
}

# GCS-backed disk creation on CH does not support authenticating
# with service account HMAC keys. For now, we resort to creating
# user account HMAC keys out of band and passing it to this module.

# Create a new service account
# resource "google_service_account" "service_account" {
#   account_id = var.service_account_name
# }

# Create the HMAC key for the associated service account
# resource "google_storage_hmac_key" "key" {
#   service_account_email = google_service_account.service_account.email
# }

# Create the GCS bucket
resource "google_storage_bucket" "clickhouse_data" {
  name          = var.bucket_name
  location      = var.bucket_location
  storage_class = var.storage_class
}

resource "google_storage_bucket_object" "clickhouse_data_root" {
  name          = var.bucket_root_path
  content       = "CH data root path"
  bucket        = google_storage_bucket.clickhouse_data.name
}

# Create corresponding Kubernetes secret and store HMAC key credentials
resource "kubernetes_secret" "clickhouse_gcs_secret" {
  metadata {
    name      = var.secret_name
    namespace = var.secret_namespace
  }
  data = {
    access_key_id     = var.gcs_access_key_id
    access_key_secret = var.gcs_access_key_secret
  }
}
