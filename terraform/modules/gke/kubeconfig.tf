resource "local_file" "kubeconfig" {
  content = templatefile("${path.module}/kubeconfig-template.yaml.tpl", {
    context                = google_container_cluster.primary.name
    cluster_ca_certificate = google_container_cluster.primary.master_auth[0].cluster_ca_certificate
    endpoint               = google_container_cluster.primary.endpoint
    token                  = data.google_client_config.primary.access_token
  })
  filename = var.kubeconfig_path
}
