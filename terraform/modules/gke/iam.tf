resource "google_service_account" "certmanager_service_account" {
  project      = var.project_id
  account_id   = "${var.instance_name}-cm"
  display_name = "GCP SA bound to K8S SA certmanager"
}

resource "google_project_iam_member" "certmanager_iam_binding" {
  project = var.project_id
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.certmanager_service_account.email}"
}

resource "google_service_account_iam_member" "certmanager_gcp_sa_k8s_sa_binding" {
  service_account_id = google_service_account.certmanager_service_account.id
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project_id}.svc.id.goog[ingress/cert-manager]"
}

resource "google_service_account" "externaldns_service_account" {
  project      = var.project_id
  account_id   = "${var.instance_name}-dns"
  display_name = "GCP SA bound to K8S SA externaldns"
}

resource "google_project_iam_member" "externaldns_iam_binding" {
  project = var.project_id
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.externaldns_service_account.email}"
}

resource "google_service_account_iam_member" "externaldns_gcp_sa_k8s_sa_binding" {
  service_account_id = google_service_account.externaldns_service_account.id
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.project_id}.svc.id.goog[ingress/external-dns]"
}
