variable "project_id" {
  type        = string
  description = "Google Cloud project where resources will be created"
}

variable "gke_cluster_region" {
  type        = string
  description = "Region of the GKE cluster"
}

variable "gke_cluster_name" {
  type        = string
  description = "Cluster/Instance name of the GKE cluster"
}