variable "source_ip_ranges_allowed_access" {
  type        = string
  description = "List of IPs that will be allowed access to the ingress resources (thanos-receiver & thanos-store) in comma separated format"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "thanos_receive_store_api_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes the store API of thanos receiver"
}

variable "thanos_store_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes thanos-store"
}

variable "thanos_query_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that exposes thanos-query"
}

variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}