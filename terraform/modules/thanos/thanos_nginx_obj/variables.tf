variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "thanos_receive_remote_write_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that receives remote writes"
}

