terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.11.0"
    }

    kustomization = {
      source  = "kbst/kustomization"
      version = "0.8.2"
    }
  }

  required_version = ">= 1.1.9"
}