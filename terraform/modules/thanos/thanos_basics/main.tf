data "kustomization_build" "thanos-config" {
  path = "${path.module}/config"
}

resource "kustomization_resource" "thanos-manifests" {
  for_each = data.kustomization_build.thanos-config.ids
  manifest = data.kustomization_build.thanos-config.manifests[each.value]
  depends_on = [
    kubernetes_namespace.thanos-namespace,
    kubernetes_secret.storage-creds,
    kubernetes_config_map.thanos-storage-config,
    kubernetes_config_map.prometheus-config,
    kubernetes_config_map.thanos-rule-alerts,
    kubernetes_config_map.thanos-query-config
  ]
}

resource "kubernetes_namespace" "thanos-namespace" {
  provider = kubernetes
  metadata {
    name = var.namespace
  }
}

locals {
  storage_svc_account_name = "${substr("${var.gke_cluster_name}", 0, 12)}-thanos-storage"
}

resource "google_service_account" "storage-service-account" {
  project      = var.project_id
  account_id   = local.storage_svc_account_name
  display_name = "GCP SA used by thanos storage to interact with GCS"
}

resource "google_project_iam_member" "storage-service-account-binding" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.storage-service-account.email}"
}

resource "google_service_account_key" "storage-service-account-key" {
  service_account_id = google_service_account.storage-service-account.name
}

# Apparently this method has been deprecated in favor of workload identities; however thanos expects the default
# credentials to be available when it starts.
resource "kubernetes_secret" "storage-creds" {
  provider = kubernetes
  metadata {
    name      = "thanos-storage"
    namespace = var.namespace
  }

  data = {
    "credentials.json" = base64decode(google_service_account_key.storage-service-account-key.private_key)
  }
  depends_on = [kubernetes_namespace.thanos-namespace]
}

resource "random_id" "bucket_name_suffix" {
  byte_length = 16
}

resource "google_storage_bucket" "thanos-bucket" {
  name     = "${var.thanos_bucket_name}-${random_id.bucket_name_suffix.hex}"
  location = var.bucket_location
  project  = var.project_id
  force_destroy = var.bucket_force_destroy
}


resource "kubernetes_config_map" "thanos-storage-config" {
  metadata {
    name      = "thanos-storage"
    namespace = var.namespace
  }
  data = {
    "config.yaml" = yamlencode({
    "type" : "GCS"
    "config" : {
      "bucket" : google_storage_bucket.thanos-bucket.name
    }
  })
  }
  depends_on = [google_storage_bucket.thanos-bucket, kubernetes_namespace.thanos-namespace]
}

locals {
  prometheus_config = templatefile("${path.module}/config/resources/prometheus.yaml.tmpl", {})
  thanos_rule_alert = templatefile("${path.module}/config/resources/thanos-rule-alerts.yaml", {})
}

# Prometheus configuration
# used-by: Prometheus
resource "kubernetes_config_map" "prometheus-config" {
  metadata {
    name      = "prometheus"
    namespace = var.namespace
  }
  data = {
    "prometheus.yaml.tmpl" = local.prometheus_config
  }
  depends_on = [kubernetes_namespace.thanos-namespace]
}

# alerts to monitor Thanos infrastructure
# used-by: Prometheus
resource "kubernetes_config_map" "thanos-rule-alerts" {
  metadata {
    name      = "thanos-rule-alerts"
    namespace = var.namespace
  }
  data = {
    "thanos-rule.yaml" = local.thanos_rule_alert
  }
  depends_on = [kubernetes_namespace.thanos-namespace]
}

# Service discovery config of Prometheus instances to aggregate query results
# used-by: Thanos Query
locals {
  thanos_query_config = templatefile("${path.module}/config/resources/store-sd.yaml.tftpl", {
    additional_stores = var.thanos_query_sd_domains
  })
}
resource "kubernetes_config_map" "thanos-query-config" {
  metadata {
    name      = "thanos-query"
    namespace = var.namespace
  }
  data = {
    "store-sd.yaml" = local.thanos_query_config
  }
  depends_on = [kubernetes_namespace.thanos-namespace]
}
