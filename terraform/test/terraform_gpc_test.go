package test

import (
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// An example of how to test the Opstrace GCP Terraform module using Terratest.
func TestTerraformBasicExample(t *testing.T) {
	t.Parallel()

	cwd, err := os.Getwd()
	assert.Nil(t, err)

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// Set the path to the Terraform code that will be tested. The path to
		// where our Terraform code is located
		TerraformDir: "../modules/opstrace/gcp",

		VarFiles: []string{
			// Use the variables defined for the testing environment.
			"../../../environments/test/terraform.tfvars",
			// Override the project_id setting to match the project used by
			// GOOGLE_APPLICATION_CREDENTIALS.
			"../../../test/terratest.tfvars",
		},

		EnvVars: map[string]string{
			// Requires the full path to the credentials file.
			"GOOGLE_APPLICATION_CREDENTIALS": cwd + "/../../secrets/gcp-svc-acc-ci-shard-aaa.json",
		},

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor: true,
	})

	// Clean up resources with "terraform destroy". Using "defer" runs the
	// command at the end of the test, whether the test succeeds or fails. At
	// the end of the test, run `terraform destroy` to clean up any resources
	// that were created
	defer terraform.Destroy(t, terraformOptions)

	// Run "terraform init" and "terraform apply". This will run `terraform
	// init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// At this point we could trigger a set of tests against the Opstrace
	// instance.
}
