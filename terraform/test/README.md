# Opstrace Terraform Tests

This is, for now, just a simple prototype of how we can use the terratest framework to write the tests with golang and not rely on terraform CLI binary.

## Requirements

In the main repo run:

```bash
make fetch-secreats
```

To ensure you have the required credentials files.

## Instructions

**NOTE**: Be careful running these because they might leak resources if the test crashes.

Run with a high enough timeout to give it time to create all the infrastructure.

```bash
go test -timeout 45min
```
