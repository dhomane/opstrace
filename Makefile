# Project makefile with shared CI targets and generic dev commands.

# Include all support/makefiles/*.mk files
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

include $(mkfile_dir)$/support/makefiles/*.mk

# Load all go projects to be used for target generation
GO_PROJECTS ?= $(shell grep '\- GOMOD_PATH: ' .gitlab-ci.yml | cut -d: -f 2 | tr -d ' ')

# The version string representing the current checkout / working directory.
# This for example defines the controller docker image tag. The default `dev`
# suffix represents a local dev environment. Override CHECKOUT_VERSION_STRING
# with a different suffix (e.g. `ci`) in the CI environment so that the
# version string attached to build artifacts reveals the environment that the
# build artifact was created in.
export CHECKOUT_VERSION_STRING ?= $(shell git rev-parse --short=9 HEAD)-dev

# CI image used as a base image for most of the CI jobs.
# This can be used locally to run Makefile targets as if running in CI.
CI_IMAGE ?= $(DOCKER_IMAGE_REGISTRY)/ci:$(CHECKOUT_VERSION_STRING)

# Public interface. Below are the important targets, used on a daily basis by
# people and CI. These targets need to stabilize (first the concepts, then the names).

# Standard targets to be implemented in each Go project Makefile.
# Used for convenience and to allow parameterised CI jobs.
# Each target can be used for a specific project, e.g. build-scheduler to build just the scheduler binaries.
# lint-code: code linting for go code.
# build: build go code and output binaries.
# regenerate: update any auto-generated code or manifests.
# unit-tests: run unit tests.
# docker-build: build all relevant docker containers.
# docker: build and push all relevant docker containers.
# print-docker-images: print the name and tag of all relevant docker images.
STANDARD_GO_TARGETS = lint-code build regenerate unit-tests docker-build docker-push docker-ensure docker print-docker-images kind-load-docker-images
define make-go-target
$1_targets = $(addprefix $1-, $(GO_PROJECTS))
.PHONY: $1
$1: $$($1_targets)
$1-%:
	make -C $$* $1
endef

$(foreach target,$(STANDARD_GO_TARGETS),$(eval $(call make-go-target,$(target))))


# Use this target locally to run the markdown linter.
.PHONY: lint-docs
lint-docs:
	docker run -v $(shell pwd):/workdir ghcr.io/igorshubovych/markdownlint-cli:latest "docs/**/*.md"

# Files specific to Opstrace, Inc. development.
# TODO(joe): this should be removed in favor of GitLab CI hidden/masked variables or files,
# it is only currently used in disabled e2e tests which are themselves deprecated.
.PHONY: fetch-secrets
fetch-secrets:
	@echo "--- fetching secrets"
	@# Fetch secrets, expected to be done before every cloud deployment.
	aws s3 cp "s3://buildkite-managedsecretsbucket-100ljuov8ugv2/" secrets/ --recursive --exclude "*" \
	--include "aws-credentials.json" \
	--include "aws-dev-svc-acc-env.sh" \
	--include "docker-credentials.json" \
	--include "opstrace_dockerhub_creds.sh" \
	--include "ci.id_rsa" \
	--include "ci.id_rsa.pub" \
	--include "opstrace-collection-cluster-authtoken-secrets.yaml" \
	--include "dns-service-login-for-ci.json" \
	--include "dns-service-magic-id-token-for-ci" \
	--include "gcp-svc-acc-dev-dns-service.json" \
	--include "gcp-svc-acc-ci-shard-aaa.json" \
	--include "gcp-svc-acc-ci-shard-bbb.json" \
	--include "gcp-svc-acc-ci-shard-ccc.json" \
	--include "gcp-svc-acc-ci-shard-ddd.json" \
	--include "gcp-svc-acc-ci-shard-eee.json" \
	--include "gcp-svc-acc-ci-shard-fff.json" \
	--include "gcp-svc-acc-ci-shard-ggg.json" \
	--include "gcp-svc-acc-ci-shard-hhh.json" \
	--include "gcp-svc-acc-ci-shard-iii.json" \
	--include "gcp-svc-acc-cost-test-1.json"
	chmod 600 secrets/ci.id_rsa


# build and push api images in go/ directory
.PHONY: api-images
api-images:
	make -C go docker

# build and push image for the scheduler only
.PHONY: scheduler-image
scheduler-image:
	make -C scheduler docker

# build and push image for the tenant-operator only
.PHONY: tenant-operator-image
tenant-operator-image:
	make -C tenant-operator docker

# build and push image for the gatekeeper only
.PHONY: gatekeeper-image
gatekeeper-image:
	make -C gatekeeper docker

# install CRDs and deploy scheduler
.PHONY: deploy
deploy:
	make -C scheduler install
	make -C scheduler deploy

# create a local kind cluster
.PHONY: kind
kind:
	make -C scheduler kind

.PHONY: e2e-test
e2e-test: # run scheduler e2e tests
	make -C scheduler e2e-test

.PHONY: e2e-test-suite
e2e-test-suite:
	make -C test/e2e e2e-test

# destroy a local kind cluster
.PHONY: destroy
destroy:
	make -C scheduler kind-delete

PHONY: publish-artifacts
publish-artifacts: fetch-secrets
	@# If in doubt: never trigger this manually (this is used by CI)
	source secrets/aws-dev-svc-acc-env.sh && bash ci/publish-artifacts.sh

.PHONY: rebuild-ci-container-image
rebuild-ci-container-image: BUILD_ARGS=
rebuild-ci-container-image:
	docker pull "${CI_IMAGE}" || echo "${CI_IMAGE} image is not available. Will not use remote cache."
	docker build --cache-from "${CI_IMAGE}" ${BUILD_ARGS} --build-arg CIUID=$(shell id -u) --build-arg CIGID=$(shell id -g) \
		-t "${CI_IMAGE}"  \
		. -f containers/ci/opstrace-ci.Dockerfile

.PHONY: push-ci-container-image
push-ci-container-image:
	docker push "${CI_IMAGE}"


.PHONY: deploy-testremote-teardown
deploy-testremote-teardown:
	bash "ci/deploy-testremote-teardown.sh"
