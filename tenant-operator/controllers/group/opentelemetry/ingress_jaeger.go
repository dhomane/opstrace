package opentelemetry

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	netv1 "k8s.io/api/networking/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getJaegerIngressTLS(cr *v1alpha1.Group) []netv1.IngressTLS {
	return []netv1.IngressTLS{
		{
			Hosts: []string{cr.Spec.GetHost()},
		},
	}
}

func getJaegerIngressLabels(cr *v1alpha1.Group) map[string]string {
	return map[string]string{}
}

func getJaegerIngressAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	gatekeeperURL := config.Get().GetGatekeeperURL()
	domain := cr.Spec.GetHostURL()
	annotations := common.MergeMap(
		existing,
		common.GetAuthIngressAnnotations(fmt.Sprint(cr.Spec.ID), gatekeeperURL, domain),
	)
	annotations["nginx.ingress.kubernetes.io/rewrite-target"] = "/api/traces"
	return annotations
}

func getJaegerIngressSpec(cr *v1alpha1.Group) netv1.IngressSpec {
	pathType := netv1.PathTypePrefix

	// we still proxy requests to the same service/underlying pods, just on a different port
	serviceName := GetOtelDeploymentName(cr)
	serviceBackendPort := netv1.ServiceBackendPort{
		Name: constants.OtelJaegerIngressPortName,
	}

	return netv1.IngressSpec{
		TLS: getJaegerIngressTLS(cr),
		Rules: []netv1.IngressRule{
			{
				Host: cr.Spec.GetHost(),
				IngressRuleValue: netv1.IngressRuleValue{
					HTTP: &netv1.HTTPIngressRuleValue{
						Paths: []netv1.HTTPIngressPath{
							{
								Path:     fmt.Sprintf("/v1/jaegertraces/%d", cr.Spec.ID),
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: serviceName,
										Port: serviceBackendPort,
									},
									Resource: nil,
								},
							},
						},
					},
				},
			},
		},
	}
}

func getJaegerIngressName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.OtelJaegerIngressNamePrefix, cr.Spec.ID)
}

func JaegerIngress(cr *v1alpha1.Group) *netv1.Ingress {
	return &netv1.Ingress{
		ObjectMeta: v1.ObjectMeta{
			Name:        getJaegerIngressName(cr),
			Namespace:   cr.Namespace,
			Labels:      getJaegerIngressLabels(cr),
			Annotations: getJaegerIngressAnnotations(cr, nil),
		},
		Spec: getJaegerIngressSpec(cr),
	}
}

func JaegerIngressMutator(cr *v1alpha1.Group, current *netv1.Ingress) error {
	currentSpec := &current.Spec
	spec := getJaegerIngressSpec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.OpenTelemetry.Components.JaegerIngress.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getJaegerIngressAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.OpenTelemetry.Components.JaegerIngress.Annotations,
	)
	current.Labels = common.MergeMap(
		getJaegerIngressLabels(cr),
		cr.Spec.Overrides.OpenTelemetry.Components.JaegerIngress.Labels,
	)

	return nil
}

func JaegerIngressSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getJaegerIngressName(cr),
	}
}
