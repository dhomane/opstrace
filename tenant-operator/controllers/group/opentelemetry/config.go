package opentelemetry

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var defaultOtelConfig = `
 {
	"receivers":{
	   "otlp":{
		  "protocols":{
			 "http":{
				"endpoint":":4318"
			 }
		  }
	   },
	   "jaeger":{
		  "protocols":{
		    "thrift_http": {}
		  }
	   }
	},
	"processors":{
	   "memory_limiter":{
		  "check_interval":"1s",
		  "limit_mib":512,
		  "spike_limit_mib":128
	   },
	   "tail_sampling":{
		  "decision_wait":"10s",
		  "num_traces":100,
		  "expected_new_traces_per_sec":10,
		  "policies":[
			 {
				"name":"rate_100spans",
				"type":"rate_limiting",
				"rate_limiting":{
				   "spans_per_second":100
				}
			 }
		  ]
	   },
	   "batch":{
		  "send_batch_size":1000,
		  "timeout":"5s"
	   }
	},
	"exporters":{
	   "jaeger":{
		  "endpoint":"%s.%s.svc.cluster.local:14250",
		  "tls":{
			 "insecure":true
		  }
	   },
	   "logging":{
		  "verbosity": "basic"
	   }
	},
	"extensions":{
	   "memory_ballast":{
		  "size_mib":128
	   }
	},
	"service":{
	   "extensions":[
		  "memory_ballast"
	   ],
	   "pipelines":{
		  "traces":{
			 "receivers":[
				"otlp",
				"jaeger"
			 ],
			 "processors":[
				"memory_limiter",
				"tail_sampling",
				"batch"
			 ],
			 "exporters":[
				"jaeger",
				"logging"
			 ]
		  }
	   }
	}
 }
`

// Default configuration for the opentelemetry collector
func NewOtelConfigDefaults(cr *v1alpha1.Group) []byte {
	return []byte(fmt.Sprintf(defaultOtelConfig, jaeger.GetJaegerName(cr), cr.Namespace))
}

func getDefaultsWithOverrides(cr *v1alpha1.Group) ([]byte, error) {
	defaults := NewOtelConfigDefaults(cr)
	return utils.PatchBytes(defaults, cr.Spec.Overrides.OpenTelemetry.Config.Raw)
}

func GetOtelConfigHash(cr *v1alpha1.Group) (string, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr)
	if err != nil {
		return "", err
	}
	sha := sha256.Sum256(defaultsWithOverrides)

	return hex.EncodeToString(sha[:28]), nil
}

func Config(cr *v1alpha1.Group) (*v1.ConfigMap, error) {
	configMap := &v1.ConfigMap{}
	configMap.ObjectMeta = metav1.ObjectMeta{
		Name:      GetOtelDeploymentConfigName(cr),
		Namespace: cr.Namespace,
	}
	hash, err := GetOtelConfigHash(cr)
	if err != nil {
		return configMap, err
	}
	// Store the hash of the current configuration for later
	// comparisons
	configMap.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}

	data, err := getData(cr)
	if err != nil {
		return configMap, err
	}

	configMap.Data = data

	return configMap, err
}

func getData(cr *v1alpha1.Group) (map[string]string, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr)
	if err != nil {
		return nil, err
	}
	return map[string]string{
		"config.yaml": string(defaultsWithOverrides),
	}, nil
}

func ConfigMutator(cr *v1alpha1.Group, current *v1.ConfigMap) error {
	hash, err := GetOtelConfigHash(cr)
	if err != nil {
		return err
	}

	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	data, err := getData(cr)
	if err != nil {
		return err
	}
	if data != nil {
		current.Data = data
	}

	return nil
}

func ConfigSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetOtelDeploymentConfigName(cr),
	}
}
