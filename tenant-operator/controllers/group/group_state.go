package group

import (
	"context"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

type GroupState struct {
	ErrorTracking ErrorTrackingState
	Tracing       TracingState
}

func NewGroupState() *GroupState {
	return &GroupState{}
}

func (i *GroupState) Read(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	if err := i.readTracingState(ctx, cr, client); err != nil {
		return err
	}
	if err := i.readErrorTrackingState(ctx, cr, client); err != nil {
		return err
	}
	return nil
}

func (i *GroupState) readErrorTrackingState(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	errorTrackingState := NewErrorTrackingState()
	if err := errorTrackingState.Read(ctx, cr, client); err != nil {
		return err
	}
	i.ErrorTracking = *errorTrackingState
	return nil
}

func (i *GroupState) readTracingState(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	tracingState := NewTracingState()
	if err := tracingState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Tracing = *tracingState
	return nil
}
