package group

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ErrorTrackingState struct {
	// track clickhouse credentials
	ErrorTrackingClickHouseCredentials *v1.Secret
	// track clickhouse quotas
	ErrorTrackingClickHouseQuotas *v1.ConfigMap
	// track deployment readiness
	Deployment *appsv1.Deployment
	// track ingress readiness
	Ingress *netv1.Ingress
	// trace ingress v2 readiness
	IngressV2 *netv1.Ingress
}

func NewErrorTrackingState() *ErrorTrackingState {
	return &ErrorTrackingState{}
}

func (e *ErrorTrackingState) Read(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	if err := e.readErrorTrackingClickHouseCredentials(ctx, cr, client); err != nil {
		return err
	}
	if err := e.readErrorTrackingClickHouseQuotas(ctx, cr, client); err != nil {
		return err
	}
	if err := e.readDeployment(ctx, cr, client); err != nil {
		return err
	}
	if err := e.readIngress(ctx, cr, client); err != nil {
		return err
	}
	if err := e.readIngressV2(ctx, cr, client); err != nil {
		return err
	}
	return nil
}

func (e *ErrorTrackingState) readErrorTrackingClickHouseCredentials(
	ctx context.Context,
	cr *v1alpha1.Group,
	client client.Client,
) error {
	currentState := &v1.Secret{}
	selector := errortracking.ClickHouseCredentialsSecretSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	e.ErrorTrackingClickHouseCredentials = currentState.DeepCopy()
	return nil
}

func (e *ErrorTrackingState) ClickHouseUsername() (string, error) {
	if e.ErrorTrackingClickHouseCredentials == nil ||
		e.ErrorTrackingClickHouseCredentials.Data[constants.ClickHouseCredentialsUserKey] == nil {
		return "", fmt.Errorf("%s key not set in errortracking clickhouse credentials secret",
			constants.ClickHouseCredentialsUserKey,
		)
	}
	return string(e.ErrorTrackingClickHouseCredentials.Data[constants.ClickHouseCredentialsUserKey]), nil
}

func (e *ErrorTrackingState) readErrorTrackingClickHouseQuotas(
	ctx context.Context,
	cr *v1alpha1.Group,
	client client.Client,
) error {
	currentState := &v1.ConfigMap{}
	selector := errortracking.ClickHouseQuotasConfigMapSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	e.ErrorTrackingClickHouseQuotas = currentState.DeepCopy()
	return nil
}

func (e *ErrorTrackingState) readDeployment(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := errortracking.DeploymentSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	e.Deployment = currentState.DeepCopy()
	return nil
}

func (e *ErrorTrackingState) readIngress(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := errortracking.IngressSelector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	e.Ingress = currentState.DeepCopy()
	return nil
}

func (e *ErrorTrackingState) readIngressV2(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := errortracking.IngressV2Selector(cr)
	if err := client.Get(ctx, selector, currentState); err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	e.IngressV2 = currentState.DeepCopy()
	return nil
}
