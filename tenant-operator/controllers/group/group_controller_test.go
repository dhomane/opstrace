package group

import (
	"net/url"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/opentelemetry"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	groupName      = "test-group"
	groupName2     = "test-group-2"
	groupNamespace = "default"
	groupID2       = groupID + 1
)

var _ = Describe("group controller", Ordered, func() {
	domain := "example.com"
	var group *v1alpha1.Group
	var group2 *v1alpha1.Group
	var jaegerDeploy *appsv1.Deployment
	var jaegerDeploy2 *appsv1.Deployment

	BeforeAll(func() {
		By("create required clickhouse secrets")
		u, err := url.Parse("localhost")
		Expect(err).NotTo(HaveOccurred())
		g := &v1alpha1.Group{
			ObjectMeta: metav1.ObjectMeta{
				Name:      groupName,
				Namespace: groupNamespace,
			},
			Spec: v1alpha1.GroupSpec{
				ID: groupID,
			},
		}
		g2 := &v1alpha1.Group{
			ObjectMeta: metav1.ObjectMeta{
				Name:      groupName2,
				Namespace: groupNamespace,
			},
			Spec: v1alpha1.GroupSpec{
				ID: groupID2,
			},
		}
		Expect(k8sClient.Create(ctx, jaeger.ClickHouseCredentialsSecret(g, *u))).To(Succeed())
		Expect(k8sClient.Create(ctx, errortracking.ClickHouseCredentialsSecret(g, *u))).To(Succeed())
		Expect(k8sClient.Create(ctx, jaeger.ClickHouseCredentialsSecret(g2, *u))).To(Succeed())
		Expect(k8sClient.Create(ctx, errortracking.ClickHouseCredentialsSecret(g2, *u))).To(Succeed())
	})

	BeforeEach(func() {
		group = &v1alpha1.Group{
			ObjectMeta: metav1.ObjectMeta{
				Name:      groupName,
				Namespace: groupNamespace,
			},
			Spec: v1alpha1.GroupSpec{
				Domain: &domain,
				ID:     groupID,
			},
		}

		By("create dummy jaeger deployment")
		// this would be created by an operator, if it was running
		jaegerDef := jaeger.Jaeger(group)
		// create a dummy jaeger deployment that would be created by the Jaeger operator
		r := int32(1)
		jaegerDeploy = &appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      jaegerDef.Name,
				Namespace: jaegerDef.Namespace,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: &r,
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app": "jaeger",
					},
				},
				Template: corev1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app": "jaeger",
						},
					},
					Spec: corev1.PodSpec{
						Containers: []corev1.Container{
							{
								Name:  "jaeger",
								Image: "jaeger",
							},
						},
					},
				},
			},
		}
		Expect(k8sClient.Create(ctx, jaegerDeploy)).Should(Succeed())
		setDeploymentReady(Default, jaegerDeploy)
	})

	AfterEach(func() {
		Expect(k8sClient.Delete(ctx, jaegerDeploy)).To(Succeed())
		Expect(k8sClient.Delete(ctx, group)).To(Succeed())

		Eventually(func(g Gomega) {

			err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group), group)
			g.Expect(err).To(HaveOccurred())
			g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
		}, timeout, interval).Should(Succeed())
	})

	Context("when creating a group", func() {
		It("should reconcile a group's resources", func() {
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())

			verifyGroupReconciled(group)
		})

		It("should updated an outdated Jaeger datasource", func() {
			By("create a datasource that we expect to be replaced")
			ds := jaeger.Datasource(group)
			Expect(ds.Spec.Datasources).NotTo(BeEmpty())
			pristineDatasourceSpec := ds.Spec.Datasources[0]
			d := pristineDatasourceSpec.DeepCopy()
			// mess up some fields
			d.JsonData = nil
			d.Name = "potato"
			d.GroupId = d.GroupId + 1
			Expect(ds.Spec.Datasources[0]).NotTo(Equal(d))
			ds.Spec.Datasources = []v1alpha1.DataSourceFields{*d}

			Expect(k8sClient.Create(ctx, ds)).To(Succeed())
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())

			verifyGroupReconciled(group)

			createdDatasource := jaeger.Datasource(group)
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(createdDatasource), createdDatasource)).Should(Succeed())
			Expect(createdDatasource.Spec.Datasources).NotTo(BeEmpty())
			Expect(createdDatasource.Spec.Datasources[0]).To(Equal(pristineDatasourceSpec))
		})
	})

	Context("when creating two groups", func() {

		BeforeEach(func() {
			group2 = &v1alpha1.Group{
				ObjectMeta: metav1.ObjectMeta{
					Name:      groupName2,
					Namespace: groupNamespace,
				},
				Spec: v1alpha1.GroupSpec{
					Domain: &domain,
					ID:     groupID2,
				},
			}
			// this would be created by an operator, if it was running
			jaegerDef := jaeger.Jaeger(group2)
			// create a dummy jaeger deployment that would be created by the Jaeger operator
			r := int32(1)
			jaegerDeploy2 = &appsv1.Deployment{
				ObjectMeta: metav1.ObjectMeta{
					Name:      jaegerDef.Name,
					Namespace: jaegerDef.Namespace,
				},
				Spec: appsv1.DeploymentSpec{
					Replicas: &r,
					Selector: &metav1.LabelSelector{
						MatchLabels: map[string]string{
							"app": "jaeger",
						},
					},
					Template: corev1.PodTemplateSpec{
						ObjectMeta: metav1.ObjectMeta{
							Labels: map[string]string{
								"app": "jaeger",
							},
						},
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Name:  "jaeger",
									Image: "jaeger",
								},
							},
						},
					},
				},
			}
			Expect(k8sClient.Create(ctx, jaegerDeploy2)).Should(Succeed())
			setDeploymentReady(Default, jaegerDeploy2)
		})

		It("should reconcile both group's resources", func() {
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())
			Expect(k8sClient.Create(ctx, group2)).Should(Succeed())
			verifyGroupReconciled(group)
			verifyGroupReconciled(group2)

			// Clean up group2 and jaegerDeploy2 which will not be cleaned by AfterEach
			Expect(k8sClient.Delete(ctx, jaegerDeploy2)).To(Succeed())
			Expect(k8sClient.Delete(ctx, group2)).To(Succeed())

			Eventually(func(g Gomega) {

				err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group2), group2)
				g.Expect(err).To(HaveOccurred())
				g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
			}, timeout, interval).Should(Succeed())
		})

		It("should reconcile one group after deleting the other one", func() {
			// Create the two groups
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())
			Expect(k8sClient.Create(ctx, group2)).Should(Succeed())
			verifyGroupReconciled(group)
			verifyGroupReconciled(group2)
			// Delete group 2 and make sure it is deleted
			Expect(k8sClient.Delete(ctx, jaegerDeploy2)).To(Succeed())
			Expect(k8sClient.Delete(ctx, group2)).To(Succeed())
			Eventually(func(g Gomega) {
				err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group2), group2)
				g.Expect(err).To(HaveOccurred())
				g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
			}, timeout, interval).Should(Succeed())
			// Verify that group1 reconciles correctly
			verifyGroupReconciled(group)

		})
	})
})

func verifyGroupReconciled(group *v1alpha1.Group) {
	By("check resources are eventually created")

	errorTracking := errortracking.Deployment(group)
	errorIngress := errortracking.Ingress(group)
	jaegerDef := jaeger.Jaeger(group)
	jaegerIngress := jaeger.Ingress(group)
	otelDeploy := opentelemetry.Deployment(group)
	otelIngress := opentelemetry.Ingress(group)
	jaegerDataSource := jaeger.Datasource(group)

	Eventually(func(g Gomega) {
		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(errorTracking), errorTracking)).Should(Succeed())

		setDeploymentReady(g, errorTracking)

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(errorIngress), errorIngress)).To(Succeed())

		setIngressReady(g, errorIngress)

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(jaegerDef), jaegerDef)).To(Succeed())

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(jaegerIngress), jaegerIngress)).To(Succeed())

		setIngressReady(g, jaegerIngress)

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(otelDeploy), otelDeploy)).Should(Succeed())

		setDeploymentReady(g, otelDeploy)

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(otelIngress), otelIngress)).Should(Succeed())

		setIngressReady(g, otelIngress)

		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(jaegerDataSource), jaegerDataSource)).To(Succeed())

		checkGroup := &v1alpha1.Group{}
		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(group), checkGroup)).To(Succeed())
		g.Expect(
			meta.IsStatusConditionTrue(checkGroup.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}, timeout, interval).Should(Succeed())

	DeferCleanup(func() {
		By("check all resources are deleted when the group is cleaned up")
		for _, o := range []client.Object{
			errorTracking,
			errorIngress,
			jaegerDef,
			jaegerIngress,
			otelDeploy,
			otelIngress,
			jaegerDataSource,
		} {
			expectDeleted(Default, o)
		}
	})
}

func setDeploymentReady(g Gomega, d *appsv1.Deployment) {
	r := *d.Spec.Replicas
	d.Status.ObservedGeneration = d.Generation
	d.Status.Replicas = r
	d.Status.AvailableReplicas = r
	d.Status.ReadyReplicas = r
	d.Status.UpdatedReplicas = r
	g.Expect(k8sClient.Status().Update(ctx, d)).To(Succeed())
}

func setIngressReady(g Gomega, i *netv1.Ingress) {
	i.Status.LoadBalancer.Ingress = []corev1.LoadBalancerIngress{
		{
			IP:       "1.2.3.4",
			Hostname: "example.com",
		},
	}
	g.Expect(k8sClient.Status().Update(ctx, i)).To(Succeed())
}

func expectDeleted(g Gomega, obj client.Object) {
	err := k8sClient.Get(ctx, client.ObjectKeyFromObject(obj), obj)
	g.Expect(err).To(HaveOccurred())
	g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
}
