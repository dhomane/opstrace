package errortracking

import (
	"fmt"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Group) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(cr.Name),
			Namespace:   cr.Namespace,
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Group, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName(cr.Name)
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec(cr)
	// apply default overrides
	if err := utils.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// apply CR overrides
	crOverrides := cr.Spec.Overrides.ErrorTracking.Components.ServiceMonitor.Spec
	if err := utils.PatchObject(currentSpec, &crOverrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	return nil
}

func getServiceMonitorName(groupName string) string {
	return fmt.Sprintf("%s-%s", constants.ErrorTrackingNamePrefix, groupName)
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"app":    constants.ErrorTrackingNamePrefix,
		"tenant": "system",
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec(cr *v1alpha1.Group) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app": fmt.Sprintf("%s-%s", constants.ErrorTrackingNamePrefix, cr.Name),
			},
		},
		JobLabel: constants.ErrorTrackingAPIName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace},
		},
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "metrics",
			Path:     "/metrics",
			Interval: "30s",
		},
	}
}
