package errortracking

import (
	"fmt"
	"strconv"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

func getDatasourceName(cr *v1alpha1.Group) string {
	return GetErrorTrackingAPIDeploymentName(cr)
}

func Datasource(cr *v1alpha1.Group) *v1alpha1.DataSource {
	return &v1alpha1.DataSource{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getDatasourceName(cr),
			Namespace: cr.Namespace,
		},
		Spec: getDataSourceSpec(cr),
	}
}

func DatasourceMutator(cr *v1alpha1.Group, current *v1alpha1.DataSource) {
	current.Spec = getDataSourceSpec(cr)
}

func DatasourceSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getDatasourceName(cr),
	}
}

func getDataSourceSpec(cr *v1alpha1.Group) v1alpha1.DataSourceSpec {
	return v1alpha1.DataSourceSpec{
		// Name of the yaml file that is mounted in provisioning directory
		Name: fmt.Sprintf("%s.yaml", getDatasourceName(cr)),
		Datasources: []v1alpha1.DataSourceFields{
			{
				Name:            "Gitlab Error Tracking",
				Type:            "gitlab-errortracking-datasource",
				IsDefault:       false,
				Access:          "proxy",
				Editable:        false,
				Version:         1,
				GroupId:         int(cr.Spec.ID),
				WithCredentials: false,
				JsonData: &v1alpha1.DataSourceJsonData{
					Url:     fmt.Sprintf("%s/v2/errortracking/%d", cr.Spec.GetHostURL(), cr.Spec.ID),
					OrgSlug: strconv.Itoa(int(cr.Spec.ID)),
					KeepCookies: []string{
						constants.SessionCookieName,
					},
				},
			},
		},
	}
}
