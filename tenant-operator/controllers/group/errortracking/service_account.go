package errortracking

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func ServiceAccount(cr *v1alpha1.Group) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(cr),
			Namespace:   cr.Namespace,
			Labels:      getServiceAccountLabels(cr),
			Annotations: getServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getServiceAccountImagePullSecrets(cr),
	}
}

func ServiceAccountSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceAccountName(cr),
	}
}

func ServiceAccountMutator(cr *v1alpha1.Group, current *v1.ServiceAccount) {
	current.Labels = getServiceAccountLabels(cr)
	current.Annotations = getServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getServiceAccountImagePullSecrets(cr)
}

func getServiceAccountName(cr *v1alpha1.Group) string {
	return GetErrorTrackingAPIDeploymentName(cr)
}

func getServiceAccountLabels(cr *v1alpha1.Group) map[string]string {
	return GetErrorTrackingAPIDeploymentLabels(cr)
}

func getServiceAccountAnnotations(cr *v1alpha1.Group, existing map[string]string) map[string]string {
	return existing
}

func getServiceAccountImagePullSecrets(cr *v1alpha1.Group) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}
