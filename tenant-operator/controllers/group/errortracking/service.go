package errortracking

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func Service(cr *v1alpha1.Group) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(cr),
			Namespace:   cr.Namespace,
			Labels:      getServiceLabels(cr),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(cr),
	}
}

func ServiceMutator(cr *v1alpha1.Group, current *v1.Service) error {
	current.Name = getServiceName(cr)
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ErrorTracking.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ErrorTracking.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(cr),
		cr.Spec.Overrides.ErrorTracking.Components.Service.Labels,
	)

	return nil
}

func ServiceSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getServiceName(cr),
	}
}

func getServiceName(cr *v1alpha1.Group) string {
	return GetErrorTrackingAPIDeploymentName(cr)
}

func getServiceLabels(cr *v1alpha1.Group) map[string]string {
	return GetErrorTrackingAPIDeploymentLabels(cr)
}

func getServiceAnnotations(_ *v1alpha1.Group, existing map[string]string) map[string]string {
	if existing == nil {
		existing = make(map[string]string)
	}
	existing["cloud.google.com/neg"] = `{"ingress": false}`
	return existing
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       "http",
			Port:       8080,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "metrics",
			Port:       8081,
			TargetPort: intstr.FromString("metrics"),
		},
	}
}

func getServiceSpec(cr *v1alpha1.Group) v1.ServiceSpec {
	return v1.ServiceSpec{
		Ports:    getServicePorts(),
		Selector: GetErrorTrackingAPIDeploymentLabels(cr),
		Type:     v1.ServiceTypeClusterIP,
	}
}
