package errortracking

import (
	"fmt"
	"net/url"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func ClickHouseCredentialsSecret(cr *v1alpha1.Group, clickHouseURL url.URL) *corev1.Secret {
	data := getClickhouseCredentialsData(cr, nil, &clickHouseURL)

	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getClickHouseCredentialsName(cr),
			Namespace: cr.Namespace,
		},
		Data: data,
		Type: corev1.SecretTypeOpaque,
	}
}

func getClickhouseCredentialsData(
	cr *v1alpha1.Group,
	current *corev1.Secret,
	clickHouseURL *url.URL,
) map[string][]byte {
	user := GetClickHouseCredentialsUser(cr, current)
	pass := GetClickHouseCredentialsPassword(current)
	// set the new user/password on the same clickhouse address for this group's Jaeger plugin
	clickHouseURL.User = url.UserPassword(user, pass)

	credentials := map[string][]byte{
		constants.ClickHouseCredentialsUserKey:           []byte(user),
		constants.ClickHouseCredentialsPasswordKey:       []byte(pass),
		constants.ClickHouseCredentialsNativeEndpointKey: []byte(clickHouseURL.String()),
	}
	return credentials
}

func GetClickHouseCredentialsUser(cr *v1alpha1.Group, current *corev1.Secret) string {
	if current != nil && current.Data[constants.ClickHouseCredentialsUserKey] != nil {
		return string(current.Data[constants.ClickHouseCredentialsUserKey])
	}
	return getClickHouseUser(cr)
}

func GetClickHouseCredentialsPassword(current *corev1.Secret) string {
	if current != nil && current.Data[constants.ClickHouseCredentialsPasswordKey] != nil {
		return string(current.Data[constants.ClickHouseCredentialsPasswordKey])
	}
	return utils.RandStringRunes(10)
}

func GetClickHouseCredentialsNativeEndpoint(current *corev1.Secret) string {
	if current != nil && current.Data[constants.ClickHouseCredentialsNativeEndpointKey] != nil {
		return string(current.Data[constants.ClickHouseCredentialsNativeEndpointKey])
	}
	return "cluster.default.svc.cluster.local:9000"
}

func ClickHouseCredentialsSecretMutator(cr *v1alpha1.Group, current *corev1.Secret, clickHouseURL url.URL) {
	data := getClickhouseCredentialsData(cr, current, &clickHouseURL)
	current.Data = data
}

func ClickHouseCredentialsSecretSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getClickHouseCredentialsName(cr),
	}
}

func getClickHouseUser(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s_%d", constants.ErrorTrackingNamePrefix, cr.Spec.ID)
}

func getClickHouseCredentialsName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.ErrorTrackingClickhouseSecretName, cr.Spec.ID)
}
