package jaeger

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/url"

	jaegerv1 "github.com/jaegertracing/jaeger-operator/apis/v1"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

const (
	MemoryRequest         = "256Mi"
	CpuRequest            = "100m"
	MemoryLimit           = "1024Mi"
	CpuLimit              = "500m"
	pluginSchemaMountPath = "/plugin-schema"
)

func GetJaegerName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.JaegerNamePrefix, cr.Spec.ID)
}

func PluginConfigName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.JaegerPluginConfigPrefix, cr.Spec.ID)
}

func PluginInitSQLDirName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-init-sql-dir-%d", constants.JaegerPluginConfigPrefix, cr.Spec.ID)
}

func PluginSchemaConfigMapName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-schemas-%d", constants.JaegerPluginConfigPrefix, cr.Spec.ID)
}

func GetJaegerSelector(cr *v1alpha1.Group) map[string]string {
	return map[string]string{
		"app": "jaeger",
		// This one is created by the Jaeger operator
		"app.kubernetes.io/instance": fmt.Sprintf("jaeger-%d", cr.Spec.ID),
	}
}

func getResources() v1.ResourceRequirements {
	return v1.ResourceRequirements{
		Requests: v1.ResourceList{
			v1.ResourceMemory: resource.MustParse(MemoryRequest),
			v1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: v1.ResourceList{
			v1.ResourceMemory: resource.MustParse(MemoryLimit),
			v1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() *appsv1.DeploymentStrategy {
	var maxUnaval intstr.IntOrString = intstr.FromInt(25)
	var maxSurge intstr.IntOrString = intstr.FromInt(25)
	return &appsv1.DeploymentStrategy{
		Type: "RollingUpdate",
		RollingUpdate: &appsv1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnaval,
			MaxSurge:       &maxSurge,
		},
	}
}

func getJaegerAnnotations(
	cr *v1alpha1.Group,
	existing map[string]string,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) map[string]string {
	hash, err := PluginConfigHash(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return existing
	}

	return utils.MergeMap(existing, map[string]string{
		constants.LastConfigAnnotation: hash,
	})
}

func getVolumes(cr *v1alpha1.Group) []v1.Volume {
	var volumes []v1.Volume
	// Volume to mount the plugin config file from a secret
	volumes = append(volumes, []v1.Volume{
		{
			Name: PluginConfigName(cr),
			VolumeSource: v1.VolumeSource{
				Secret: &v1.SecretVolumeSource{
					SecretName: PluginConfigName(cr),
				},
			},
		},
		{
			Name: PluginSchemaConfigMapName(cr),
			VolumeSource: v1.VolumeSource{
				ConfigMap: &v1.ConfigMapVolumeSource{
					LocalObjectReference: v1.LocalObjectReference{Name: PluginSchemaConfigMapName(cr)},
				},
			},
		},
	}...)

	return volumes
}

func getVolumeMounts(cr *v1alpha1.Group) []v1.VolumeMount {
	var mounts []v1.VolumeMount

	mounts = append(mounts, []v1.VolumeMount{
		{
			Name:      PluginConfigName(cr),
			MountPath: "/plugin-config",
		},
		{
			Name:      PluginSchemaConfigMapName(cr),
			ReadOnly:  true,
			MountPath: pluginSchemaMountPath,
		},
	}...)

	return mounts
}

// reference: https://www.jaegertracing.io/docs/1.27/cli/#jaeger-all-in-one-grpc-plugin
func getJaegerAllInOneOptions(cr *v1alpha1.Group) jaegerv1.Options {
	defaults := map[string]interface{}{
		"query": map[string]interface{}{
			"base-path": fmt.Sprintf("/v1/jaeger/%d", cr.Spec.ID),
		},
	}
	return jaegerv1.NewOptions(defaults)
}

func getGRPCPluginStorageOptions() jaegerv1.Options {
	defaults := map[string]interface{}{
		// "log-level": "debug",
		"binary":             "/plugin/jaeger-clickhouse",
		"configuration-file": "/plugin-config/config.yaml",
	}

	return jaegerv1.NewOptions(map[string]interface{}{"grpc-storage-plugin": defaults})
}

func getSamplingSpec() jaegerv1.JaegerSamplingSpec {
	return jaegerv1.JaegerSamplingSpec{Options: jaegerv1.NewFreeForm(map[string]interface{}{
		// Traces per second (safe default). See also Jaeger client metrics:
		//   "sum(rate(jaeger_tracer_traces_total[1m])) by (job,state,sampled)"
		"default_strategy": map[string]interface{}{
			"type":  "ratelimiting",
			"param": 1,
		},
	})}
}

func getJaegerSpec(
	cr *v1alpha1.Group,
	annotations map[string]string,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) jaegerv1.JaegerSpec {
	var no = false
	return jaegerv1.JaegerSpec{
		AllInOne: jaegerv1.JaegerAllInOneSpec{
			Image:    constants.OpstraceImages().JaegerAllInOneImage,
			Options:  getJaegerAllInOneOptions(cr),
			Strategy: getDeploymentStrategy(),
		},
		Storage: jaegerv1.JaegerStorageSpec{
			Type:    "grpc-plugin",
			Options: getGRPCPluginStorageOptions(),
			GRPCPlugin: jaegerv1.GRPCPluginSpec{
				Image: constants.OpstraceImages().JaegerClickHouseImage,
			},
		},
		Ingress: jaegerv1.JaegerIngressSpec{
			Enabled: &no,
		},
		Sampling: getSamplingSpec(),
		// Strategy can be allInOne (default), production (separate pods), or streaming (separate pods + kafka).
		// For now we use allInOne for each (per-group) instance
		// reference: https://www.jaegertracing.io/docs/1.27/operator/#allinone-default-strategy
		// Would be great if we could move this to a central multitenant deployment instead of one
		// deployment per tenant (or in our case, per-tenant-group)
		Strategy: jaegerv1.DeploymentStrategyAllInOne,
		JaegerCommonSpec: jaegerv1.JaegerCommonSpec{
			Labels:       GetJaegerSelector(cr),
			Annotations:  getJaegerAnnotations(cr, annotations, clickHouseURL, remoteStorageEnabled),
			Resources:    getResources(),
			Volumes:      getVolumes(cr),
			VolumeMounts: getVolumeMounts(cr),
		},
	}
}

// Returns the sha256 checksum for the Jaeger definition.
func GetJaegerHash(j *jaegerv1.Jaeger) (string, error) {
	if j.Annotations == nil {
		j.Annotations = map[string]string{}
	}
	c := j.Annotations[constants.LastConfigAnnotation]
	// remove hash annotation so it's not recursively including the hash in the hash
	j.Annotations[constants.LastConfigAnnotation] = ""
	data, err := json.Marshal(j)
	if err != nil {
		return "", err
	}
	sha := sha256.Sum256(data)

	j.Annotations[constants.LastConfigAnnotation] = c
	return hex.EncodeToString(sha[:28]), nil
}

// Performs a deepequal by comparing the checksum instead of using the
// reflect package in controllerutil.createOrUpdate. jaegerv1.Jaeger has
// unexported fields that cause a panic.
func DeepEqual(a, b *jaegerv1.Jaeger) (bool, error) {
	// remove hash annotation so it's not recursively including the hash in the hash
	aHash, err := GetJaegerHash(a)
	if err != nil {
		return false, err
	}
	bHash, err := GetJaegerHash(b)
	if err != nil {
		return false, err
	}
	return aHash == bHash, nil
}

func Jaeger(cr *v1alpha1.Group) *jaegerv1.Jaeger {
	return &jaegerv1.Jaeger{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetJaegerName(cr),
			Namespace: cr.Namespace,
		},
	}
}

func JaegerSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      GetJaegerName(cr),
	}
}

func JaegerMutator(
	cr *v1alpha1.Group,
	current *jaegerv1.Jaeger,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) error {
	currentSpec := &current.Spec
	spec := getJaegerSpec(cr, current.Spec.Annotations, clickHouseURL, remoteStorageEnabled)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	overrides := cr.Spec.Overrides.Jaeger.Components.Deployment.Spec
	if overrides != nil {
		// Turn currentSpec into bytes and merge with overrides (already in bytes)
		c, err := json.Marshal(currentSpec)
		if err != nil {
			return err
		}
		// Apply CR overrides
		patched, err := utils.PatchBytes(
			c,
			*overrides,
		)
		if err != nil {
			return err
		}
		// Unmarshall into original spec
		if err := json.Unmarshal(patched, currentSpec); err != nil {
			return err
		}
	}

	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getJaegerAnnotations(cr, current.Annotations, clickHouseURL, remoteStorageEnabled),
		cr.Spec.Overrides.Jaeger.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		GetJaegerSelector(cr),
		cr.Spec.Overrides.Jaeger.Components.Deployment.Labels,
	)
	hash, err := GetJaegerHash(current)
	if err != nil {
		return err
	}
	current.Annotations[constants.LastConfigAnnotation] = hash
	return nil
}
