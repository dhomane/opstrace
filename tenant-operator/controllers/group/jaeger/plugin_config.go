package jaeger

import (
	"crypto/sha256"
	"embed"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
	"text/template"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
)

// PluginDefaults returns default configuration for the Jaeger Plugin.
func PluginDefaults(
	cr *v1alpha1.Group,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) *v1alpha1.JaegerPluginConfigOptions {
	databaseName := ""

	if remoteStorageEnabled {
		switch config.Get().GetPlatformTarget() {
		case common.GCP:
			databaseName = constants.JaegerGCSDatabaseName
		case common.AWS:
			databaseName = constants.JaegerS3DatabaseName
		}
	} else {
		databaseName = constants.JaegerDatabaseName
	}

	pass, _ := clickHouseURL.User.Password()
	// Disable expanding of traceID to its hexadecimal form while writing.
	expandTraceID := false
	return &v1alpha1.JaegerPluginConfigOptions{
		Address:            fmt.Sprintf("tcp://%s", clickHouseURL.Host),
		MaxSpanCount:       100_000,
		BatchWriteSize:     10_000,
		BatchFlushInterval: "5s",
		Encoding:           "protobuf",
		Password:           pass,
		Username:           clickHouseURL.User.Username(),
		Database:           databaseName,
		Tenant:             fmt.Sprint(cr.Spec.ID),
		MetricsEndpoint:    "0.0.0.0:9090",
		Replication:        true,
		TTLDays:            10,
		InitSQLScriptsDir:  pluginSchemaMountPath,
		InitTables:         false,
		ExpandTraceID:      &expandTraceID,
	}
}

func PluginConfigHash(cr *v1alpha1.Group, clickHouseURL *url.URL, remoteStorageEnabled bool) (string, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return "", err
	}
	data, err := json.Marshal(*defaultsWithOverrides)
	if err != nil {
		return "", err
	}
	sha := sha256.Sum256(data)

	return hex.EncodeToString(sha[:28]), nil
}

func getDefaultsWithOverrides(
	cr *v1alpha1.Group,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) (*v1alpha1.JaegerPluginConfigOptions, error) {
	defaults := PluginDefaults(cr, clickHouseURL, remoteStorageEnabled)
	err := common.PatchObject(defaults, cr.Spec.Overrides.Jaeger.Config)

	return defaults, err
}

func getData(cr *v1alpha1.Group, clickHouseURL *url.URL, remoteStorageEnabled bool) (*map[string][]byte, error) {
	defaultsWithOverrides, err := getDefaultsWithOverrides(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return nil, err
	}
	data, err := yaml.Marshal(*defaultsWithOverrides)
	if err != nil {
		return nil, err
	}

	return &map[string][]byte{
		"config.yaml": data,
	}, nil
}

func PluginConfig(cr *v1alpha1.Group, clickHouseURL *url.URL, remoteStorageEnabled bool) (*v1.Secret, error) {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      PluginConfigName(cr),
		Namespace: cr.Namespace,
	}
	hash, err := PluginConfigHash(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return secret, err
	}
	// Store the hash of the current configuration for later
	// comparisons
	secret.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}

	data, err := getData(cr, clickHouseURL, remoteStorageEnabled)
	if data != nil {
		secret.Data = *data
	}

	return secret, err
}

func PluginConfigMutator(
	cr *v1alpha1.Group,
	current *v1.Secret,
	clickHouseURL *url.URL,
	remoteStorageEnabled bool,
) error {
	hash, err := PluginConfigHash(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return err
	}
	current.Annotations = map[string]string{
		constants.LastConfigAnnotation: hash,
	}
	data, err := getData(cr, clickHouseURL, remoteStorageEnabled)
	if err != nil {
		return err
	}
	if data != nil {
		current.Data = *data
	}

	return nil
}

func PluginConfigSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      PluginConfigName(cr),
	}
}

func PluginSchema(cr *v1alpha1.Group, labels map[string]string) *v1.ConfigMap {
	configMap := &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      PluginSchemaConfigMapName(cr),
			Namespace: cr.Namespace,
			Labels:    labels,
		},
	}
	return configMap
}

func PluginSchemaMutator(current *v1.ConfigMap, remoteStorageEnabled bool) error {
	data, err := renderSQLScripts(remoteStorageEnabled)
	if err != nil {
		return err
	}
	current.Data = data
	return nil
}

func PluginSchemaSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      PluginSchemaConfigMapName(cr),
	}
}

//go:embed templates/*
var templatesFS embed.FS

var templates = template.Must(template.ParseFS(templatesFS, "templates/*.tmpl.*"))

type tableArgs struct {
	DatabaseName         string
	RemoteStorageBackend bool
}

func renderSQLScripts(useRemoteStorageBackend bool) (map[string]string, error) {
	// Note: The order of schema files during execution in Clickhouse is important as distributed tables depend on
	// local tables to be existed.
	// We rely on the plugin itself to make sure it sorts file names before execution.
	//nolint:lll
	// See https://gitlab.com/gitlab-org/opstrace/jaeger-clickhouse/-/blob/bd2e35ef8fa326750ee658cc47f8bba2b576a0ff/storage/store.go#L243
	// As maps are unordered in Go and configMap won't accept any different type.
	tpls := templates.Templates()
	results := make(map[string]string, len(tpls))
	databaseName := ""
	if useRemoteStorageBackend {
		switch config.Get().GetPlatformTarget() {
		case common.GCP:
			databaseName = constants.JaegerGCSDatabaseName
		case common.AWS:
			databaseName = constants.JaegerS3DatabaseName
		}
	} else {
		databaseName = constants.JaegerDatabaseName
	}

	args := &tableArgs{
		DatabaseName:         databaseName,
		RemoteStorageBackend: useRemoteStorageBackend,
	}
	for _, tpl := range tpls {
		var statement strings.Builder
		filename := tpl.Name()
		err := tpl.Execute(&statement, args)
		if err != nil {
			return results, fmt.Errorf("error while rendering template: %w", err)
		}
		results[filename] = statement.String()
	}
	return results, nil
}
