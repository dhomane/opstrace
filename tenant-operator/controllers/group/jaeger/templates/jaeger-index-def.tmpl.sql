CREATE TABLE IF NOT EXISTS jaeger_index_local ON CLUSTER '{cluster}'
(
    `tenant` LowCardinality(String),
    `timestamp` DateTime64(6, 'UTC') CODEC(Delta(4), ZSTD(1)), -- Precision till microseconds
    `traceID` FixedString(16),
    `service` String,
    `operation` String,
    `durationUs` UInt64 CODEC(ZSTD(1)),
    `tags.key` Array(String),
    `tags.value` Array(String)
)
ENGINE = ReplicatedMergeTree -- MergeTree params are omitted because they are controlled via clickhouse-operator
PARTITION BY toYYYYMM(timestamp)
PRIMARY KEY(tenant, service)
-- additional columns can be added later eg: (tenant, service, operation, traceID, timestamp)
ORDER BY (tenant, service, operation)
TTL toDateTime(timestamp) + toIntervalDay(30)
{{- if .RemoteStorageBackend }}
SETTINGS storage_policy = 'gcs_main'
{{- end}}
;