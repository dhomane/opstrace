CREATE TABLE IF NOT EXISTS  jaeger_spans_archive_local ON CLUSTER '{cluster}'
(
    `tenant` LowCardinality(String),
    `timestamp` DateTime64(6, 'UTC') CODEC(Delta(4), ZSTD(1)),
    `traceID` FixedString(16),
    -- model has the full encoded span
    `model` String CODEC(ZSTD(3))
)
ENGINE = ReplicatedMergeTree -- MergeTree params are omitted because they are controlled via clickhouse-operator
PARTITION BY toYYYYMM(timestamp)
PRIMARY KEY(tenant, traceID)
ORDER BY (tenant, traceID, timestamp)
TTL toDateTime(timestamp) + toIntervalDay(30)
{{- if .RemoteStorageBackend }}
SETTINGS storage_policy = 'gcs_main'
{{- end}}
;
