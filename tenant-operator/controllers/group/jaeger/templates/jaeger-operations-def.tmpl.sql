CREATE TABLE IF NOT EXISTS jaeger_operations_local ON CLUSTER '{cluster}'
(
    `tenant` LowCardinality(String),
    `date` Date32,
    `service` LowCardinality(String),
    `operation` LowCardinality(String),
    `count` UInt64,
    `spankind` String
)
ENGINE = ReplicatedSummingMergeTree -- MergeTree params are omitted because they are controlled via clickhouse-operator
PARTITION BY toYYYYMM(date)
PRIMARY KEY(date, tenant, service)
ORDER BY (date, tenant, service, operation) -- this should be the correct ascending order of cardinality
TTL toDate(date) + toIntervalDay(30)
{{- if .RemoteStorageBackend }}
SETTINGS storage_policy = 'gcs_main'
{{- end}}
;