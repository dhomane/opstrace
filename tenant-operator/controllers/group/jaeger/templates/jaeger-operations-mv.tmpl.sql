CREATE MATERIALIZED VIEW IF NOT EXISTS jaeger_operations_local_mv ON CLUSTER '{cluster}' TO {{.DatabaseName}}.jaeger_operations_local
AS
SELECT
    tenant,
    toDate32(timestamp) AS date,
    service,
    operation,
    count() AS count,
    if(has(tags.key, 'span.kind'), tags.value[indexOf(tags.key, 'span.kind')], '') AS spankind
FROM {{.DatabaseName}}.jaeger_index_local
GROUP BY
    tenant,
    date,
    service,
    operation,
    tags.key, -- do we have to group by tags key and value ? (expanding array values can get quite expensive)
    tags.value;