package jaeger

import (
	"encoding/json"
	"fmt"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ClickHouseQuotasConfigmap(cr *v1alpha1.Group, defaultUser string) *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getClickHouseQuotasConfigMapName(cr),
			Namespace: cr.Namespace,
		},
		Data: getClickHouseQuotasConfigmapData(cr, nil, defaultUser),
	}
}

func ClickHouseQuotasConfigMapSelector(cr *v1alpha1.Group) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getClickHouseQuotasConfigMapName(cr),
	}
}

func ClickHouseQuotasConfigMapMutator(cr *v1alpha1.Group, current *corev1.ConfigMap, defaultUser string) {
	data := getClickHouseQuotasConfigmapData(cr, current, defaultUser)
	overrides := cr.Spec.Overrides.Jaeger.Components.Quotas.Data
	current.Data = utils.MergeMap(data, overrides)
}

func getClickHouseQuotasConfigMapName(cr *v1alpha1.Group) string {
	return fmt.Sprintf("%s-%d", constants.JaegerClickhouseQuotaConfigmapName, cr.Spec.ID)
}

//nolint:unparam
func getClickHouseQuotasConfigmapData(
	cr *v1alpha1.Group,
	current *corev1.ConfigMap,
	defaultUser string,
) map[string]string {
	// add default quota(s) for the concerned jaeger user
	defaultQuotaName := fmt.Sprintf("jaeger_default_%d", cr.Spec.ID)
	// translating
	//nolint:lll
	// CREATE QUOTA jaeger_default_$ID ON CLUSTER '{cluster}' NOT KEYED FOR INTERVAL 3600 second MAX queries = 1000 TO $defaultUser
	// into our DSL
	defaultQuotaDefinition := map[string]string{
		"keyedBy": "NOT KEYED",
		"quotas":  "FOR INTERVAL 3600 second MAX queries = 1000",
		"users":   defaultUser,
	}
	//nolint:errcheck
	defaultQuotaDefinitionStr, _ := json.Marshal(defaultQuotaDefinition)
	return map[string]string{
		defaultQuotaName: string(defaultQuotaDefinitionStr),
	}
}
