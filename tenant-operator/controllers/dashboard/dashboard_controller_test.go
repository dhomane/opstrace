package dashboard

import (
	"testing"

	"github.com/stretchr/testify/require"

	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var dashboard = &opstracev1alpha1.Dashboard{
	TypeMeta: metav1.TypeMeta{},
	ObjectMeta: metav1.ObjectMeta{
		Name:      "dashboard2",
		Namespace: "grafana",
	},
	Spec: opstracev1alpha1.DashboardSpec{
		Url:     "url1",
		GroupID: 1,
	},
}

var knownDashboardsEmpty = []*opstracev1alpha1.DashboardRef{
	{
		Name: "dashboard1",
	},
	{
		Name: "dashboard2",
	},
}

var knownDashboards = []*opstracev1alpha1.DashboardRef{
	{
		Name:    "dashboard1",
		GroupID: 1,
		Hash:    "1234",
		UID:     "uid1234",
	},
	{
		Name:    "dashboard2",
		GroupID: 1,
		Hash:    "5678",
		UID:     "uid5678",
	},
}

var dashboardList = &opstracev1alpha1.DashboardList{
	TypeMeta: metav1.TypeMeta{},
	ListMeta: metav1.ListMeta{},
	Items: []opstracev1alpha1.Dashboard{
		{
			TypeMeta: metav1.TypeMeta{},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "dashboard1",
				Namespace: "grafana",
			},
			Spec: opstracev1alpha1.DashboardSpec{
				GroupID: 1,
				Url:     "url1",
			},
		},
		{
			TypeMeta: metav1.TypeMeta{},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "dashboard2",
				Namespace: "foo",
			},
			Spec: opstracev1alpha1.DashboardSpec{
				GroupID: 2,
				Url:     "url2",
			},
		},
	},
}

func TestFindHashEmpty(t *testing.T) {
	output := findHash(knownDashboardsEmpty, dashboard)
	require.Equal(t, "", output)
}

func TestFindHash(t *testing.T) {
	output := findHash(knownDashboards, dashboard)
	require.Equal(t, "5678", output)
}

func TestTrueInTenant(t *testing.T) {
	status := inTenant(dashboardList, knownDashboards[0])
	require.Equal(t, true, status)
}

func TestFalseInTenant(t *testing.T) {
	status := inTenant(dashboardList, knownDashboards[1])
	require.Equal(t, false, status)
}

func TestFindUidEmpty(t *testing.T) {
	output := findUid(knownDashboardsEmpty, dashboard)
	require.Equal(t, "", output)
}

func TestFindUid(t *testing.T) {
	output := findUid(knownDashboards, dashboard)
	require.Equal(t, "uid5678", output)
}
