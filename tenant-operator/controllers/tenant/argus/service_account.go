package argus

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getServiceAccountLabels() map[string]string {
	return map[string]string{
		"app": constants.ArgusPodLabel,
	}
}

func getServiceAccountAnnotations(cr *v1alpha1.Tenant, existing map[string]string) map[string]string {
	return existing
}

func getServiceAccountImagePullSecrets(cr *v1alpha1.Tenant) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func ServiceAccount(cr *v1alpha1.Tenant) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        constants.ArgusServiceAccountName,
			Namespace:   cr.Namespace,
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getServiceAccountImagePullSecrets(cr),
	}
}

func ServiceAccountSelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.ArgusServiceAccountName,
	}
}

func ServiceAccountMutator(cr *v1alpha1.Tenant, current *v1.ServiceAccount) {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getServiceAccountImagePullSecrets(cr)
}
