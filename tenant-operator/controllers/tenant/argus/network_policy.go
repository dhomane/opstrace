package argus

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	netv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func NetworkPolicySelector(cr *v1alpha1.Tenant) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      getNetworkPolicyName(),
	}
}

func NetworkPolicy(cr *v1alpha1.Tenant) *netv1.NetworkPolicy {
	return &netv1.NetworkPolicy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getNetworkPolicyName(),
			Namespace: cr.Namespace,
		},
		Spec: getNetworkPolicySpec(cr),
	}
}

func NetworkPolicyMutator(cr *v1alpha1.Tenant, current *netv1.NetworkPolicy) {
	current.Spec = getNetworkPolicySpec(cr)
}

func getNetworkPolicyName() string {
	return constants.ArgusServiceName
}

func getNetworkPolicySpec(cr *v1alpha1.Tenant) netv1.NetworkPolicySpec {
	return netv1.NetworkPolicySpec{
		PodSelector: metav1.LabelSelector{
			MatchLabels: getPodLabels(),
		},
		PolicyTypes: []netv1.PolicyType{
			netv1.PolicyTypeEgress,
		},
		Egress: cr.Spec.GOUI.EgressRules,
	}
}
