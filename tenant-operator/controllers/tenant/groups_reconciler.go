package tenant

import (
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

type GroupsReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewGroupsReconciler(teardown bool, logger logr.Logger) *GroupsReconciler {
	return &GroupsReconciler{
		Teardown: teardown,
		Log:      logger.WithName("tenant-groups"),
	}
}

func (i *GroupsReconciler) Reconcile(state *GroupsState, cr *v1alpha1.Tenant) common.DesiredState {
	desired := common.DesiredState{}
	desired = desired.AddActions(i.getGroupsTeardown(state, cr))

	return desired
}

func (i *GroupsReconciler) getGroupsTeardown(state *GroupsState, cr *v1alpha1.Tenant) []common.Action {
	// We only need to act when this tenant is being torn down.
	// Delete all groups in this tenant to ensure proper clean up of resources and deletion of data
	if !i.Teardown {
		return []common.Action{}
	}
	// Handle case where we failed to read the GroupsList when building state
	if state.TenantGroups == nil {
		return []common.Action{
			common.LogAction{
				Msg:   fmt.Sprintf("failed to read groups state in tenant: %s", cr.Namespace),
				Error: errors.New("failed loading GroupsList, will retry on next reconcile"),
			},
		}
	}

	if state.IsEmpty() {
		return []common.Action{
			common.LogAction{
				Msg: fmt.Sprintf("all groups have been torn down and removed in tenant: %s", cr.Namespace),
			},
		}
	}

	teardownActions := []common.Action{}

	for _, group := range state.TenantGroups.Items {
		group := group
		obj := &group
		// only delete if not already deleting
		if group.DeletionTimestamp.IsZero() {
			teardownActions = append(teardownActions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("delete group %d in tenant: %s", group.Spec.ID, cr.Namespace),
			})
		}
	}

	remainingGroups := len(state.TenantGroups.Items)

	// append a single generic log action with an error to block downstream actions
	// (and ultimately the removal of the finalizer) while we wait for all groups to delete
	teardownActions = append(teardownActions, common.LogAction{
		Msg: fmt.Sprintf("waiting for %d groups to finish deleting in tenant: %s", remainingGroups, cr.Namespace),
		// The error is only shown with debug level logging so it's ok if the message is the same
		Error: fmt.Errorf("waiting for %d groups to finish deleting in tenant: %s", remainingGroups, cr.Namespace),
	})

	return teardownActions
}
