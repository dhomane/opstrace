package tenant

import (
	"context"
	stdErr "errors"
	"fmt"
	"os"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	ControllerName = "tenant-controller"
	finalizerName  = "tenant.opstrace.com/finalizer"
)

//nolint:lll
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants;tenants/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileTenant) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Tenant{}).
		// enqueue a reconcile for this tenant if any groups change.
		// this is primarily useful for tracking tenant teardown
		Watches(&source.Kind{Type: &opstracev1alpha1.Group{}},
			handler.EnqueueRequestsFromMapFunc(func(a client.Object) []reconcile.Request {
				return []reconcile.Request{
					{NamespacedName: types.NamespacedName{
						Name:      constants.TenantName,
						Namespace: a.GetNamespace(),
					}},
				}
			})).
		Owns(&appsv1.Deployment{}).
		Owns(&netv1.Ingress{}).
		Owns(&v1.ConfigMap{}).
		Owns(&v1.Service{}).
		Owns(&v1.ServiceAccount{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileTenant{}

// ReconcileTenant reconciles a Grafana object
type ReconcileTenant struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client       client.Client
	Scheme       *runtime.Scheme
	Plugins      *PluginsHelperImpl
	Log          logr.Logger
	Config       *config.ControllerConfig
	Recorder     record.EventRecorder
	StatusPoller *polling.StatusPoller
}

// Reconcile reads the state of the cluster for a Tenant object and makes changes based on the state read
// and what's defined in its spec.
//
//nolint:funlen,cyclop
func (r *ReconcileTenant) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result reconcile.Result, err error) {
	tenant := &opstracev1alpha1.Tenant{}
	err = r.Client.Get(ctx, request.NamespacedName, tenant)
	if err != nil {
		if errors.IsNotFound(err) {
			r.Log.Info(
				"Tenant has been removed from API, deleting all remaining config in namespace",
				"name", request.Name, "namespace", request.Namespace,
			)

			if err := r.Config.RemoveAllConfig(r.Client, ctx, request.Namespace); err != nil {
				return reconcile.Result{}, err
			}
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := tenant.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.TenantFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()

	// Add installed dashboards to local config on start up
	if !r.Config.GetConfigBool(constants.ArgusConfigDashboardsSynced, false) {
		err = r.Config.HydrateKnownDashboards(r.Client, ctx, request.Namespace)
		if err != nil {
			r.Log.Error(err, "error reading known dashboards from cluster")
			// stop all reconciliation to prevent anything getting out of whack
			// TODO(prozlach): find a graceful way to handle it, for now
			// silience the linter errors:
			//nolint:gocritic
			os.Exit(1)
		}
		r.Log.Info(fmt.Sprintf("hydrated %d known dashboards from cluster", len(r.Config.GetDashboards())))
		r.Config.AddConfigItem(constants.ArgusConfigDashboardsSynced, true)
	}

	// Read current state
	currentState := NewTenantState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}
	// Get the actions required to reach the desired state
	groupsReconciler := NewGroupsReconciler(teardown, r.Log)
	desiredState := groupsReconciler.Reconcile(&currentState.Groups, cr)

	if teardown {
		// Stop any anciliary reconciliation against Argus,
		// but make sure to do this after groups reconciliation above
		config.Get().SetState(config.ControllerState{
			ArgusReady: false,
		})
	}

	argusReconciler := NewArgusReconciler(teardown, r.Log)
	desiredState = append(desiredState, argusReconciler.Reconcile(&currentState.Argus, cr)...)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.ClusterFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	// Run the config map reconciler to discover jsonnet libraries
	err = r.reconcileConfigMaps(ctx, cr)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		err = r.manageSuccess(cr, currentState)
		if err != nil {
			r.manageError(cr, err)
			return reconcile.Result{}, err
		}
	}

	return reconcile.Result{}, nil
}

func (r *ReconcileTenant) manageError(cr *opstracev1alpha1.Tenant, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	r.Config.InvalidateDashboards()

	config.Get().SetState(config.ControllerState{
		ArgusReady: false,
	})
}

// Try to find a suitable url to grafana
func (r *ReconcileTenant) getAdminUrl(cr *opstracev1alpha1.Tenant, state *ArgusState) (string, error) {
	var servicePort = int32(argus.GetArgusPort(cr))

	if state.AdminSecret != nil {
		user := argus.GetAdminUser(cr, state.AdminSecret)
		pass := argus.GetAdminPassword(cr, state.AdminSecret)
		// Otherwise rely on the service
		if state.ArgusService != nil {
			return fmt.Sprintf("http://%s:%s@%v.%v.svc.cluster.local:%d",
				user,
				pass,
				state.ArgusService.Name,
				cr.Namespace,
				servicePort,
			), nil
		}
	}

	return "", stdErr.New("failed to find admin url")
}

func (r *ReconcileTenant) manageSuccess(cr *opstracev1alpha1.Tenant, state *TenantState) error {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	// Make the Grafana API URL available to the dashboard controller
	url, err := r.getAdminUrl(cr, &state.Argus)
	if err != nil {
		return err
	}
	cr.Status.Argus.URL = &url

	// Publish controller state
	controllerState := config.ControllerState{
		AdminUrl:   url,
		ArgusReady: true,
	}

	config.Get().SetState(controllerState)

	r.Log.Info("desired Tenant state met")

	return nil
}
