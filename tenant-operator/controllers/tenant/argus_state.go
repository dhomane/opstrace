package tenant

import (
	"context"
	stdErr "errors"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ArgusState struct {
	ArgusService           *v1.Service
	ArgusServiceAccount    *v1.ServiceAccount
	ArgusConfig            *v1.ConfigMap
	ArgusIngress           *netv1.Ingress
	DataSourceConfig       *v1.ConfigMap
	AdminSecret            *v1.Secret
	PostgresEndpointSecret *v1.Secret
	ArgusServiceMonitor    *monitoring.ServiceMonitor
	ArgusNetworkPolicy     *netv1.NetworkPolicy
	ArgusStatefulSet       *appsv1.StatefulSet
}

func NewArgusState() ArgusState {
	return ArgusState{}
}

func (i *ArgusState) Read(ctx context.Context, cr *v1alpha1.Tenant, c client.Client) error {
	readState := []func(context.Context, *v1alpha1.Tenant, client.Client) error{
		i.readArgusService,
		i.readArgusServiceAccount,
		i.readArgusConfig,
		i.readDatasourceConfig,
		i.readArgusStatefulSet,
		i.readArgusAdminUserSecret,
		i.readPostgresEndpointSecret,
		i.readArgusIngress,
		i.readArgusServiceMonitor,
		i.readArgusNetworkPolicy,
	}

	for _, r := range readState {
		if err := r(ctx, cr, c); err != nil {
			return err
		}
	}

	return nil
}

// Get the Postgres user and password for the Jaeger clickhouse plugin
func (i *ArgusState) PostgresEndpoint() (*string, error) {
	var endpoint string
	var s = i.PostgresEndpointSecret

	if s != nil && s.Data[constants.ArgusPostgresURLKey] != nil {
		endpoint = string(s.Data[constants.ArgusPostgresURLKey])
	} else {
		return nil, stdErr.New("postgres endpoint secret not set")
	}

	return &endpoint, nil
}

func (i *ArgusState) readArgusService(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Service{}
	selector := argus.ServiceSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusService = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusServiceAccount(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.ServiceAccount{}
	selector := argus.ServiceAccountSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusServiceAccount = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusConfig(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState, err := argus.Config(cr)
	if err != nil {
		return err
	}
	selector := argus.ConfigSelector(cr)
	err = client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusConfig = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readDatasourceConfig(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.ConfigMap{}
	selector := argus.DatasourceConfigSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.DataSourceConfig = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusIngress(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &netv1.Ingress{}
	selector := argus.IngressSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusIngress = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusStatefulSet(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &appsv1.StatefulSet{}
	selector := argus.StatefulSetSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusStatefulSet = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusAdminUserSecret(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Secret{}
	selector := argus.AdminSecretSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.AdminSecret = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readPostgresEndpointSecret(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &v1.Secret{}
	selector := argus.PostgresEndpointSecretSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.PostgresEndpointSecret = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusServiceMonitor(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &monitoring.ServiceMonitor{}
	selector := argus.ServiceMonitorSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusServiceMonitor = currentState.DeepCopy()
	return nil
}

func (i *ArgusState) readArgusNetworkPolicy(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	currentState := &netv1.NetworkPolicy{}
	selector := argus.NetworkPolicySelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.ArgusNetworkPolicy = currentState.DeepCopy()
	return nil
}
