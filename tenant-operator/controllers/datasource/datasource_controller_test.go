package datasource

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	tenantapiv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("datasource controller", func() {
	var defaultClient client.Client
	var existingConfigMap *corev1.ConfigMap
	var createDatasource *tenantapiv1alpha1.DataSource

	BeforeEach(func() {
		defaultClient = client.NewNamespacedClient(k8sClient, "default")

		By("create existing datasource configmap to populate")
		existingConfigMap = &corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name: constants.DatasourcesConfigMapName,
			},
		}
		Expect(defaultClient.Create(ctx, existingConfigMap)).To(Succeed())
	})

	JustBeforeEach(func() {
		if createDatasource != nil {
			Expect(defaultClient.Create(ctx, createDatasource)).To(Succeed())
		}
	})

	AfterEach(func() {
		Expect(defaultClient.Delete(ctx, existingConfigMap)).To(Succeed())
		if createDatasource != nil {
			Expect(defaultClient.Delete(ctx, createDatasource)).To(Succeed())
		}
		createDatasource = nil
	})

	Context("create datasource without known datasources", func() {
		When("datasource is not set", func() {
			It("should leave configmap as is", func() {
				Consistently(func(g Gomega) {
					g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(existingConfigMap), existingConfigMap)).To(Succeed())

					g.Expect(existingConfigMap.Data).To(HaveKey("default_test-noexists.yaml"))
					g.Expect(existingConfigMap.Annotations).To(HaveKey("last-config"))
				}).ShouldNot(Succeed())
			})
		})

		When("datasource is set", func() {
			BeforeEach(func() {
				createDatasource = &tenantapiv1alpha1.DataSource{
					ObjectMeta: metav1.ObjectMeta{
						Name: "test-noexists",
					},
					Spec: tenantapiv1alpha1.DataSourceSpec{
						Name: "noexists.yaml",
						Datasources: []tenantapiv1alpha1.DataSourceFields{
							{
								Name:      "Tracing",
								Type:      "jaeger",
								Url:       "example.com",
								IsDefault: true,
								GroupId:   groupID,
							},
						},
					},
				}
			})

			It("populates configmap for existing group", func() {

				Eventually(func(g Gomega) {
					g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(existingConfigMap), existingConfigMap)).To(Succeed())

					g.Expect(existingConfigMap.Data).To(HaveKey("default_test-noexists.yaml"))
					g.Expect(existingConfigMap.Annotations).To(HaveKey("last-config"))
					data := &struct {
						Datasources []tenantapiv1alpha1.DataSourceFields
					}{}
					g.Expect(yaml.Unmarshal([]byte(existingConfigMap.Data["default_test-noexists.yaml"]), &data)).To(Succeed())
					g.Expect(data.Datasources).To(HaveLen(1))
					g.Expect(data.Datasources[0]).To(gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
						"Name":      Equal("Tracing"),
						"Type":      Equal("jaeger"),
						"Url":       Equal("example.com"),
						"IsDefault": BeTrue(),
						"GroupId":   BeNumerically("==", groupID),
					}))
				}).Should(Succeed())
			})
		})
	})

	Context("create datasource with known existing configmap datasources", func() {
		const defaultJaeger = "default_jaeger.yaml"

		BeforeEach(func() {
			existingConfigMap.Annotations = map[string]string{
				"last-config": "6c6a27d99c1fc57b737b5d49430b87e83b58b4a427ebf5925f2c011593e4fe67",
			}
			existingConfigMap.Data = map[string]string{
				defaultJaeger: `apiVersion: 1
datasources:
- access: proxy
  basicAuth: false
  editable: false
  groupId: 2
  isDefault: true
  name: Tracing
  type: jaeger
  url: http://jaeger-2.2.svc.cluster.local:16686/v1/jaeger/2/
  version: 1
  withCredentials: false`,
			}

			Expect(defaultClient.Update(ctx, existingConfigMap)).To(Succeed())
		})

		When("datasource is not set", func() {
			It("should leave configmap as is", func() {
				Consistently(func(g Gomega) {
					g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(existingConfigMap), existingConfigMap)).To(Succeed())

					g.Expect(existingConfigMap.Data).To(HaveKey(defaultJaeger))
					g.Expect(existingConfigMap.Annotations).To(HaveKeyWithValue("last-config", "6c6a27d99c1fc57b737b5d49430b87e83b58b4a427ebf5925f2c011593e4fe67"))
					data := &struct {
						Datasources []tenantapiv1alpha1.DataSourceFields
					}{}
					g.Expect(yaml.Unmarshal([]byte(existingConfigMap.Data[defaultJaeger]), &data)).To(Succeed())
					g.Expect(data.Datasources).To(HaveLen(1))
					g.Expect(data.Datasources[0]).To(gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
						"Name":            Equal("Tracing"),
						"Type":            Equal("jaeger"),
						"Url":             Equal("http://jaeger-2.2.svc.cluster.local:16686/v1/jaeger/2/"),
						"IsDefault":       BeTrue(),
						"GroupId":         BeNumerically("==", groupID),
						"Editable":        BeFalse(),
						"Access":          Equal("proxy"),
						"Version":         BeNumerically("==", 1),
						"WithCredentials": BeFalse(),
						"BasicAuth":       BeFalse(),
					}))
				}).Should(Succeed())
			})
		})

		When("datasource is added with same name", func() {
			BeforeEach(func() {
				createDatasource = &tenantapiv1alpha1.DataSource{
					ObjectMeta: metav1.ObjectMeta{
						Name: "jaeger",
					},
					Spec: tenantapiv1alpha1.DataSourceSpec{
						Name: "jaeger.yaml",
						Datasources: []tenantapiv1alpha1.DataSourceFields{
							{
								Name: "Tracing",
								Type: "jaeger",
								Url:  "example.com/v1/jaeger/2",
								// Make this the default datasource that is
								// loaded automatically on the explore view
								IsDefault:       true,
								Access:          "proxy",
								Editable:        false,
								Version:         1,
								GroupId:         2,
								WithCredentials: false,
								JsonData: &tenantapiv1alpha1.DataSourceJsonData{
									KeepCookies: []string{
										constants.SessionCookieName,
									},
								},
							},
						},
					},
				}
			})

			// this test mimics the regression found in https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1937
			It("should replace the existing configmap item", func() {
				Eventually(func(g Gomega) {
					g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(existingConfigMap), existingConfigMap)).To(Succeed())

					g.Expect(existingConfigMap.Data).To(HaveKey(defaultJaeger))
					g.Expect(existingConfigMap.Annotations).To(HaveKey("last-config"))
					data := &struct {
						Datasources []tenantapiv1alpha1.DataSourceFields
					}{}
					g.Expect(yaml.Unmarshal([]byte(existingConfigMap.Data[defaultJaeger]), &data)).To(Succeed())
					g.Expect(data.Datasources).To(HaveLen(1))
					g.Expect(data.Datasources[0]).To(gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
						"Name":            Equal("Tracing"),
						"Type":            Equal("jaeger"),
						"Url":             Equal("example.com/v1/jaeger/2"),
						"IsDefault":       BeTrue(),
						"Access":          Equal("proxy"),
						"GroupId":         BeNumerically("==", groupID),
						"Editable":        BeFalse(),
						"Version":         BeNumerically("==", 1),
						"WithCredentials": BeFalse(),
						"JsonData": gstruct.PointTo(Equal(tenantapiv1alpha1.DataSourceJsonData{
							KeepCookies: []string{
								constants.SessionCookieName,
							},
						})),
					}))
				}).Should(Succeed())
			})
		})

		When("datasource is added with different name", func() {
			BeforeEach(func() {
				createDatasource = &tenantapiv1alpha1.DataSource{
					ObjectMeta: metav1.ObjectMeta{
						Name: "jaeger2",
					},
					Spec: tenantapiv1alpha1.DataSourceSpec{
						Name: "jaeger2.yaml",
						Datasources: []tenantapiv1alpha1.DataSourceFields{
							{
								Name:    "Tracing",
								Type:    "jaeger",
								Url:     "example.com/v1/jaeger/2",
								GroupId: groupID,
							},
						},
					},
				}
			})

			It("should be added and delete the existing configmap item", func() {
				Eventually(func(g Gomega) {
					g.Expect(defaultClient.Get(ctx, client.ObjectKeyFromObject(existingConfigMap), existingConfigMap)).To(Succeed())

					g.Expect(existingConfigMap.Data).NotTo(HaveKey(defaultJaeger))
					g.Expect(existingConfigMap.Data).To(HaveKey("default_jaeger2.yaml"))
					g.Expect(existingConfigMap.Annotations).To(HaveKey("last-config"))
					data := &struct {
						Datasources []tenantapiv1alpha1.DataSourceFields
					}{}
					g.Expect(yaml.Unmarshal([]byte(existingConfigMap.Data["default_jaeger2.yaml"]), &data)).To(Succeed())
					g.Expect(data.Datasources).To(HaveLen(1))
					g.Expect(data.Datasources[0]).To(gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
						"Name": Equal("Tracing"),
						"Type": Equal("jaeger"),
						"Url":  Equal("example.com/v1/jaeger/2"),
					}))
				}).Should(Succeed())
			})
		})
	})
})
