# Tenant Operator

An operator to provision and manage GOB Tenants, including a GOUI Instance, Dashboards, Datasources and Plugins.

This Operator began life as a fork of [`grafana-operator`](https://github.com/grafana-operator/grafana-operator) and has since been significantly altered to fit the GitLab usecase.

## Supported Custom Resources

The following GOB resources are supported:

* [`Argus`](./api/v1alpha1/tenant_types.go) - GitLab Observability UI
* [`Dashboard`](./api/v1alpha1/dashboard_types.go)
* [`Datasource`](./api/v1alpha1/datasource_types.go)
* [`Group`](./api/v1alpha1/group_types.go)
* [`Tenant`](./api/v1alpha1/tenant_types.go)

All custom resources use the api group `opstrace.com` and version `v1alpha1`.
To get a overview of the available tenant-operator CRD see [API docs](./documentation/api.md).

## Design

### How `tenant-operator` interacts with GitLab namespace/group/project hierarchy

For Kubernetes, each namespace maps to a root level namespace in GitLab, e.g. `gitlab.com/opstrace`. Further enabling observability on a nested group like `gitlab.com/opstrace/opstrace-ui` would then create a new Group CR within the same namespace that is associated with `gitlab.com/opstrace`.

Per that definition, for the underlying CRs - `Group.Spec.ID` may not always one-to-one map with the name of the Kubernetes namespace, while `Tenant.Spec.ID` will always be equal to the namespace it gets provisioned in.

When you enable observability on the root level namespace in GitLab, e.g. `gitlab.com/opstrace` then the `Group` ID can be the same as the `Tenant` ID, but nested groups will have different `Group` IDs while being contained in the same `Tenant` mapped to the root level namespace.


## Development and Local Deployment

### Using the Makefile

If you want to develop/build/test the operator, here are some instructions how to set up your dev-environment: [follow me](./documentation/develop.md)

## Debug

We have documented a few steps to help you debug the [tenant-operator](documentation/debug.md).
