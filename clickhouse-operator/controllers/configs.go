package controllers

import (
	"embed"
	"fmt"
	"strings"
	"text/template"
)

//go:embed templates/*
var templatesFS embed.FS

var templates = template.Must(template.New("configs").Funcs(customFuncs).
	ParseFS(templatesFS, "templates/*.tmpl.*")).Funcs(customFuncs)

var customFuncs template.FuncMap = template.FuncMap{
	// lookupPassword used in proxy yaml config
	"lookupPassword": func(dict map[string]string, bucket, key string) string {
		result, ok := dict[fmt.Sprintf("%s/%s", bucket, key)]
		if !ok {
			return ""
		}

		return result
	},
}

func render(filename string, args interface{}) (string, error) {
	var statement strings.Builder
	err := templates.ExecuteTemplate(&statement, filename, args)
	if err != nil {
		return "", fmt.Errorf("rendering template: %w", err)
	}
	return statement.String(), nil
}
