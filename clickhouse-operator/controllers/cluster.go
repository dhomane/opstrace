package controllers

import (
	"errors"
	"fmt"
	"strings"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

// clickHouseCluster represents a full cluster based on CR.
type clickHouseCluster struct {
	*clickhousev1alpha1.ClickHouse

	// KeeperImage is derived from the spec.
	KeeperImage string

	// Shards is desired shard count (currently always 1).
	Shards int32

	// Replicas is desired replica count currently, per shard.
	Replicas int32

	// Hosts represents the service names of all replicas.
	// Hosts are in format {name}-{shard}-{replica}.
	Hosts []string

	// ConfigXML is general config XML for the server.
	ConfigXML string
	// UsersXML is user/profile/quota specific config.
	UsersXML string
	// ProxyYAML is optional config loaded from the CHProxy.
	ProxyYAML string
	// ClientConfigXML is config XML for the clickhouse-client that is available on the pods.
	ClientConfigXML string
	// KeeperXML is clickhouse-keeper specific XML
	KeeperXML string

	// InternalSSL denotes if SSL should be enabled in the cluster.
	// Should be read from ClickHouse.Spec.Deploy.InternalSSL
	InternalSSL bool
	// DHParams is optional dh parameters config that will be loaded to Cluster if InternalSSL is enabled.
	// Only applicable when InternalSSL is true.
	DHParams string

	// ObjectStorageEnabled denotes if S3/GCS endpoints are to be used by Clickhouse.
	ObjectStorageEnabled bool
}

func newClickHouseCluster(cr *clickhousev1alpha1.ClickHouse) (*clickHouseCluster, error) {
	if cr.Spec.Image == "" {
		return nil, errors.New("image is required")
	}

	var (
		// Note(joe): only one shard currently supported
		shards    = 1
		replicas  = 0
		chCluster = &clickHouseCluster{
			ClickHouse: cr,
		}
	)
	if cr.Spec.Replicas != nil {
		replicas = int(*cr.Spec.Replicas)
	}

	keeperImage, err := chCluster.keeperImage()
	if err != nil {
		return nil, err
	}
	chCluster.KeeperImage = keeperImage

	// hostnames in format {name}-{shard}-{replica}
	hosts := make([]string, shards*replicas)
	for s := 0; s < shards; s++ {
		for r := 0; r < replicas; r++ {
			hosts[s*replicas+r] = fmt.Sprintf("%s-%d-%d", cr.Name, s, r)
		}
	}

	chCluster.Hosts = hosts
	chCluster.Replicas = int32(replicas)
	chCluster.Shards = int32(shards)

	if cr.Spec.InternalSSL != nil && *cr.Spec.InternalSSL {
		chCluster.InternalSSL = *cr.Spec.InternalSSL
	}

	if cr.Spec.ObjectStorage != nil {
		chCluster.ObjectStorageEnabled = true
	}

	configsXML, err := render("config.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render config: %w", err)
	}
	usersXML, err := render("users.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render users: %w", err)
	}
	clientXML, err := render("client-config.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render client config: %w", err)
	}
	keeperXML, err := render("keeper.tmpl.xml", chCluster)
	if err != nil {
		return nil, fmt.Errorf("render keeper config: %w", err)
	}
	chCluster.ConfigXML = configsXML
	chCluster.UsersXML = usersXML
	chCluster.ClientConfigXML = clientXML
	chCluster.KeeperXML = keeperXML

	if chCluster.InternalSSL {
		chCluster.DHParams, err = render("dhparam.tmpl.txt", chCluster)
		if err != nil {
			return nil, fmt.Errorf("render dhparams: %w", err)
		}
	}

	return chCluster, nil
}

// labels returns a new map containing ClickHouse CR level labels.
func (c *clickHouseCluster) labels() map[string]string {
	return map[string]string{
		"app":  "clickhouse",
		"name": c.Name,
	}
}

// labelsMerged merges standard labels with ones to merge in.
func (c *clickHouseCluster) labelsMerged(merge map[string]string) map[string]string {
	std := c.labels()
	for k, v := range merge {
		std[k] = v
	}
	return std
}

func (c *clickHouseCluster) proxyEnabled() bool {
	return c.Spec.Proxy != nil
}

func (c *clickHouseCluster) configureProxy(passwords map[string]string) error {
	if len(c.Spec.Proxy.AllowedNetworks) == 0 {
		c.Spec.Proxy.AllowedNetworks = []string{"10.0.0.0/8"}
	}

	if len(c.Spec.Proxy.ManagementNetworks) == 0 {
		c.Spec.Proxy.ManagementNetworks = []string{"10.0.0.0/8"}
	}

	if c.Spec.Proxy.Listeners.HTTP.Address == "" {
		c.Spec.Proxy.Listeners.HTTP.Address = ":80"
	}

	for i, u := range c.Spec.Proxy.Users {
		if (u.ToUser == nil || *u.ToUser == "") && len(c.Spec.AdminUsers) > 0 {
			adminUser := c.Spec.AdminUsers[0].Name
			c.Spec.Proxy.Users[i].ToUser = &adminUser
		}
	}

	proxyYAML, err := render("proxy.tmpl.yaml", map[string]interface{}{
		"CH":        c,
		"Passwords": passwords,
	})
	if err != nil {
		return fmt.Errorf("render proxy config: %w", err)
	}
	c.ProxyYAML = proxyYAML

	return nil
}

// Auxiliary function that just prepends cluster name to a field. Used for naming operator specific resources.
func (c *clickHouseCluster) prependClusterName(field string) string {
	return c.Name + "-" + field
}

// Return the explicitly set keeper image or derive it.
// Attempts to derive the keeper image by assuming clickhouse-server image
// is stored in the same registry as clickhouse-keeper and we can pull
// an equivalent version.
// So clickhouse/clickhouse-server:latest-alpine becomes
// clickhouse/clickhouse-keeper:latest-alpine.
func (c *clickHouseCluster) keeperImage() (string, error) {
	if c.Spec.KeeperImage != nil && *c.Spec.KeeperImage != "" {
		return *c.Spec.KeeperImage, nil
	}

	// remove optional image digest as this can contain ':'.
	beforeDigest := strings.Split(c.Spec.Image, "@")[0]

	// image hostname can have ports specified
	tagIndex := strings.LastIndex(beforeDigest, ":")
	if tagIndex < 0 {
		return "", fmt.Errorf("deriving keeper image from %s: no tag specified", c.Spec.Image)
	}

	tagPart := beforeDigest[tagIndex:]
	beforeTag := beforeDigest[:tagIndex]

	// assume image has hostname/component format
	componentIndex := strings.LastIndex(beforeTag, "/")
	if componentIndex < 0 {
		return "", fmt.Errorf("deriving keeper image from %s: no component path in image name", c.Spec.Image)
	}

	return beforeTag[:componentIndex] + "/clickhouse-keeper" + tagPart, nil
}
