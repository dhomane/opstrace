/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

package controllers

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/imdario/mergo"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	//+kubebuilder:scaffold:imports
)

// +kubebuilder:docs-gen:collapse=Imports

// Define utility constants for object names and testing timeouts/durations and intervals.
const (
	timeout  = time.Second * 10
	interval = time.Millisecond * 250
)

func ensureCHManifests(yamlPath string) (*clickhousev1alpha1.ClickHouse, []client.Object) {
	By("Reading yaml file with CH manifests")
	filename, _ := filepath.Abs(yamlPath)
	exampleYamlBytes, err := os.ReadFile(filename)
	Expect(err).NotTo(HaveOccurred())

	decoder := scheme.Codecs.UniversalDeserializer()

	// Break up the yaml into separate strings as we'll have multiple docs per file
	exampleYamlRecs := strings.Split(string(exampleYamlBytes), "---")

	By("Ensuring CH manifests")
	var clickhouseDefinition *clickhousev1alpha1.ClickHouse
	var otherDefinitions []client.Object
	for _, f := range exampleYamlRecs {
		runtimeObj, groupKind, err := decoder.Decode([]byte(f), nil, nil)
		Expect(err).NotTo(HaveOccurred())

		if groupKind.GroupVersion() == clickhousev1alpha1.GroupVersion {
			// Prevent overwriting what we found already:
			Expect(clickhouseDefinition).To(BeNil())

			clickhouseDefinition = runtimeObj.(*clickhousev1alpha1.ClickHouse)
		} else {
			otherDefinitions = append(otherDefinitions, runtimeObj.(client.Object))
		}

		Expect(k8sClient.Create(ctx, runtimeObj.(client.Object))).To(Succeed())
	}

	Expect(clickhouseDefinition).ToNot(BeNil())
	return clickhouseDefinition, otherDefinitions
}

func waitClickhouseDependencies(ch *clickhousev1alpha1.ClickHouse) {
	name := client.ObjectKeyFromObject(ch)

	By("Waiting for CH cluster resources to appear")
	// Wait for configmap to appear
	configMap := &corev1.ConfigMap{}
	Eventually(
		func() error {
			return k8sClient.Get(ctx, name, configMap)
		},
		timeout, interval,
	).Should(Succeed())

	// Wait for cluster service to appear
	clusterService := &corev1.Service{}
	Eventually(
		func() error {
			return k8sClient.Get(ctx, name, clusterService)
		},
		timeout, interval,
	).Should(Succeed())

	// Wait for per-node services and statefulsets to appear
	for idx := int32(0); idx < *ch.Spec.Replicas; idx++ {
		nodeNameKey := types.NamespacedName{
			Name:      fmt.Sprintf("%s-0-%d", ch.Name, idx),
			Namespace: ch.Namespace,
		}

		nodeService := &corev1.Service{}
		Eventually(
			func() error {
				return k8sClient.Get(ctx, nodeNameKey, nodeService)
			},
			timeout, interval,
		).Should(Succeed())

		nodeStatefulSet := &appsv1.StatefulSet{}
		Eventually(
			func() error {
				return k8sClient.Get(ctx, nodeNameKey, nodeStatefulSet)
			},
			timeout, interval,
		).Should(Succeed())
	}
}

func removeStringFromSlice(slice []string, element string) []string {
	for i, v := range slice {
		if element == v {
			ret := make([]string, 0, len(slice)-1)
			ret = append(ret, slice[:i]...)
			return append(ret, slice[i+1:]...)
		}
	}
	return slice
}

func removeObjects(ch *clickhousev1alpha1.ClickHouse, objs []client.Object) {
	// We are passing ch object separately to be able to do mass-removal, as
	// for this we need the Namespace where CR resided.

	// Be careful about ordering, CH CR needs to go first:
	objs = append([]client.Object{ch}, objs...)
	for _, obj := range objs {
		if _, ok := obj.(*corev1.PersistentVolumeClaim); ok {
			By("Removing finalizers from PVC to allow its deletion")
			pvc := new(corev1.PersistentVolumeClaim)
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(obj), pvc)).To(Succeed())
			pvc.Finalizers = removeStringFromSlice(pvc.Finalizers, "kubernetes.io/pvc-protection")
			Expect(k8sClient.Update(ctx, pvc)).To(Succeed())
		}

		By(fmt.Sprintf("Deleting %s/%s", obj.GetNamespace(), obj.GetName()))
		Expect(k8sClient.Delete(ctx, obj)).To(Succeed())
	}

	By("Ensure all managed resources are deleted")
	// EnvTest doesn't handle owner refs and delete these, we need to do it
	// ourselves
	managed := []client.Object{
		&appsv1.StatefulSet{},
		&corev1.Service{},
		&corev1.Secret{},
		&corev1.ConfigMap{},
	}
	for _, m := range managed {
		Expect(k8sClient.DeleteAllOf(ctx, m, client.InNamespace(ch.Namespace))).To(Succeed())
	}
}

func waitForCHStatus(ch *clickhousev1alpha1.ClickHouse, status clickhousev1alpha1.ClickHouseClusterStatus) {
	By(fmt.Sprintf("Check for status %s in ClickHouse status", status))

	f := func(g Gomega) {
		tmp := &clickhousev1alpha1.ClickHouse{}
		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(ch), tmp)).To(Succeed())
		g.Expect(tmp.Status.ObservedGeneration).To(Equal(tmp.Generation))
		g.Expect(tmp.Status.Status).To(Equal(status))
	}

	Eventually(f, timeout, interval).Should(Succeed())
}

func verifyStatefulSets(ch *clickhousev1alpha1.ClickHouse) {
	By("get statefulsets by labels")
	ss := &appsv1.StatefulSetList{}
	labels := map[string]string{
		"app":  "clickhouse",
		"name": ch.Name,
	}
	Expect(
		k8sClient.List(ctx, ss, client.MatchingLabels(labels), client.InNamespace(ch.Namespace)),
	).Should(Succeed())

	ssi := ss.Items
	Expect(ssi).To(HaveLen(int(*ch.Spec.Replicas)))

	defaultAffinity := &corev1.Affinity{
		PodAntiAffinity: &corev1.PodAntiAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
				{
					PodAffinityTerm: corev1.PodAffinityTerm{
						LabelSelector: &metav1.LabelSelector{
							MatchLabels: labels,
						},
						TopologyKey: "kubernetes.io/hostname",
					},
					Weight: 1,
				},
			},
		},
	}

	for _, s := range ssi {
		By("check affinity is set correctly")
		if ch.Spec.Affinity != nil {
			aff := ch.Spec.Affinity.DeepCopy()
			Expect(mergo.Merge(aff, *defaultAffinity, mergo.WithAppendSlice)).To(Succeed())
			Expect(s.Spec.Template.Spec.Affinity).To(Equal(aff))
		} else {
			Expect(s.Spec.Template.Spec.Affinity).To(Equal(defaultAffinity))
		}

		By("check nodeSelector is set correctly")
		Expect(s.Spec.Template.Spec.NodeSelector).To(Equal(ch.Spec.NodeSelector))

		By("check tolerations are set correctly")
		Expect(s.Spec.Template.Spec.Tolerations).To(Equal(ch.Spec.Tolerations))
	}
}

func ensurePvcs(ch *clickhousev1alpha1.ClickHouse) []*corev1.PersistentVolumeClaim {
	res := make([]*corev1.PersistentVolumeClaim, int(*ch.Spec.Replicas))

	// Can't take ref from a const:
	persistentVolFs := corev1.PersistentVolumeFilesystem
	standardStorageClass := "standard"
	for replica := int32(0); replica < *ch.Spec.Replicas; replica++ {
		pvc := &corev1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Name:      fmt.Sprintf("data-%s-0-%d-0", ch.Name, replica),
				Namespace: ch.Namespace,
				Labels: map[string]string{
					"app":     "clickhouse",
					"name":    ch.Name,
					"replica": strconv.Itoa(int(replica)),
					"shard":   "0",
				},
			},
			Spec: corev1.PersistentVolumeClaimSpec{
				AccessModes: []corev1.PersistentVolumeAccessMode{
					corev1.ReadWriteOnce,
				},
				Resources: corev1.ResourceRequirements{
					Requests: map[corev1.ResourceName]resource.Quantity{
						corev1.ResourceStorage: ch.Spec.StorageSize,
					},
				},
				StorageClassName: &standardStorageClass,
				VolumeMode:       &persistentVolFs,
			},
		}
		Expect(k8sClient.Create(ctx, client.Object(pvc))).To(Succeed())
		res[replica] = pvc
	}

	return res
}

func ensurePvcsStatus(ch *clickhousev1alpha1.ClickHouse) {
	// NOTE(prozlach): Updating objects selected via labels makes no sense as
	// we are the ones that created these objects in the first place.

	for replica := int32(0); replica < *ch.Spec.Replicas; replica++ {
		pvc := new(corev1.PersistentVolumeClaim)
		pvcKey := types.NamespacedName{
			Name:      fmt.Sprintf("data-%s-0-%d-0", ch.Name, replica),
			Namespace: ch.Namespace,
		}
		Expect(k8sClient.Get(ctx, pvcKey, pvc)).To(Succeed())

		pvc.Status.Capacity = corev1.ResourceList{
			corev1.ResourceStorage: ch.Spec.StorageSize,
		}
		pvc.Status.Phase = corev1.ClaimBound
		Expect(k8sClient.Status().Patch(ctx, pvc, client.Merge)).To(Succeed())
	}
}

func waitCHClusterHasSynchronized(ch *clickhousev1alpha1.ClickHouse) {
	Eventually(
		func(g Gomega) {
			tmp := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(ch), tmp)).To(Succeed())
			g.Expect(tmp.Status.ReadyReplicas).To(BeNumerically("==", *ch.Spec.Replicas), "readyReplicas")
			g.Expect(tmp.Status.ReadyShards).To(BeNumerically("==", 1), "readyShards")
			g.Expect(tmp.Status.ReadyStorage).To(BeNumerically("==", *ch.Spec.Replicas), "readyStorage")
		},
		timeout, interval,
	).Should(Succeed())

	waitForCHStatus(ch, clickhousev1alpha1.StatusCompleted)
}

func ensureSTSesAreReady(cr *clickhousev1alpha1.ClickHouse) {
	for replica := int32(0); replica < *cr.Spec.Replicas; replica++ {
		stsKey := types.NamespacedName{
			Name:      fmt.Sprintf("%s-0-%d", cr.Name, replica),
			Namespace: cr.Namespace,
		}
		setStsStatusReady(stsKey)
	}
}

func setStsStatusReady(key types.NamespacedName) {
	sts := new(appsv1.StatefulSet)
	Expect(k8sClient.Get(ctx, key, sts)).To(Succeed())

	sts.Status.Replicas = *sts.Spec.Replicas
	sts.Status.ReadyReplicas = *sts.Spec.Replicas
	sts.Status.ObservedGeneration = sts.Generation
	sts.Status.CurrentReplicas = *sts.Spec.Replicas
	sts.Status.UpdatedReplicas = *sts.Spec.Replicas
	sts.Status.CurrentRevision = sts.Status.UpdateRevision
	Expect(k8sClient.Status().Update(ctx, sts)).To(Succeed())
}

func setStsStatusUnready(key types.NamespacedName) {
	sts := new(appsv1.StatefulSet)
	Expect(k8sClient.Get(ctx, key, sts)).To(Succeed())

	sts.Status.Replicas = int32(0)
	sts.Status.ReadyReplicas = int32(0)
	sts.Status.ObservedGeneration = sts.Generation
	sts.Status.CurrentReplicas = int32(0)
	sts.Status.UpdatedReplicas = int32(0)
	sts.Status.CurrentRevision = sts.Status.UpdateRevision
	Expect(k8sClient.Status().Update(ctx, sts)).To(Succeed())
}

func removeStsOrphanFinalizer(cr *clickhousev1alpha1.ClickHouse) {
	uids := make([]types.UID, int(*cr.Spec.Replicas))

	// first remove the finalizers:
	for idx := int32(0); idx < *cr.Spec.Replicas; idx++ {
		idx := idx
		Eventually(
			func(g Gomega) {
				stsNameKey := types.NamespacedName{
					Name:      fmt.Sprintf("%s-0-%d", cr.Name, idx),
					Namespace: cr.Namespace,
				}
				sts := new(appsv1.StatefulSet)
				g.Expect(k8sClient.Get(ctx, stsNameKey, sts)).To(Succeed())

				// NOTE(prozlach): And here is the fun part - orphaning pods when
				// deleting STS results in `orphan` finalizer being set which
				// prevents object deletion in envtest as STS controller is simply
				// not there. A solution is this workaround - we delete the
				// finalizer ourselves to unblock CH reconciliation.
				g.Expect(sts.Finalizers).To(Not(BeEmpty()))
				uids[idx] = sts.UID

				By(fmt.Sprintf("Applying `finalizers` workaround for STS %s/%s", sts.Namespace, sts.Name))
				sts.Finalizers = removeStringFromSlice(sts.Finalizers, "orphan")
				g.Expect(k8sClient.Update(ctx, sts)).To(Succeed())
			},
			timeout, interval,
		).Should(Succeed())
	}

	// now wait for k8s to finish removing the sts and for controller to
	// recreate them
	for idx := int32(0); idx < *cr.Spec.Replicas; idx++ {
		idx := idx
		Eventually(
			func(g Gomega) {
				stsNameKey := types.NamespacedName{
					Name:      fmt.Sprintf("%s-0-%d", cr.Name, idx),
					Namespace: cr.Namespace,
				}
				sts := new(appsv1.StatefulSet)
				g.Expect(k8sClient.Get(ctx, stsNameKey, sts)).To(Succeed())
				g.Expect(sts.UID).To(Not(Equal(uids[idx])))
				g.Expect(sts.Finalizers).To(BeEmpty())
			},
			timeout, interval,
		).Should(Succeed())
	}
}

var _ = Describe("ClickHouse controller", func() {

	var clickHouseCR *clickhousev1alpha1.ClickHouse

	Context("Creating Clickhouse CR", func() {

		var pvcs []*corev1.PersistentVolumeClaim

		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example.yaml")
			pvcs = ensurePvcs(clickHouseCR)
			for _, pvc := range pvcs {
				objsToRemove = append(objsToRemove, client.Object(pvc))
			}
			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)

			waitClickhouseDependencies(clickHouseCR)
			verifyStatefulSets(clickHouseCR)
		})

		It("results in status of the CR being in sync with the state of dependant k8s objects", func() {
			By("Initial state after creation should be `InProgress`")
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)

			// in EnvTest there are no controllers, so we will manipulate the state
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())
			Expect(cr.Status.ReadyReplicas).To(BeZero())
			Expect(cr.Status.ReadyShards).To(BeZero())
			Expect(cr.Status.ReadyStorage).To(BeZero())

			By("Update owned StatefulSets to have ready replicas")
			ensureSTSesAreReady(cr)

			// Because PVCs are still missing
			By("Ensure that status got updated but state is still `InProgress`")
			Eventually(
				func(g Gomega) {
					tmp := &clickhousev1alpha1.ClickHouse{}
					Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), tmp)).To(Succeed())
					g.Expect(tmp.Status.ReadyReplicas).To(BeNumerically("==", *clickHouseCR.Spec.Replicas))
					g.Expect(tmp.Status.ReadyShards).To(BeNumerically("==", 1))
					g.Expect(tmp.Status.ReadyStorage).To(BeZero())
				},
				timeout, interval,
			).Should(Succeed())

			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)

			By("Update PVCs so that they appear to be ready/provisioned")
			ensurePvcsStatus(clickHouseCR)

			By("Ensure that both status is in sync as well as state of the CH DB is `Completed` ")
			waitCHClusterHasSynchronized(clickHouseCR)
		})
	})

	Context("Resizing cluster's storage", func() {

		var pvcs []*corev1.PersistentVolumeClaim

		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example.yaml")
			pvcs = ensurePvcs(clickHouseCR)
			for _, pvc := range pvcs {
				objsToRemove = append(objsToRemove, client.Object(pvc))
			}
			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)

			waitClickhouseDependencies(clickHouseCR)
			verifyStatefulSets(clickHouseCR)
		})

		It("resizes cluster volumes", func() {
			By("Update PVCs so that cluster can reach `Completed` state")
			ensurePvcsStatus(clickHouseCR)

			By("Ensure STSes are ready")
			ensureSTSesAreReady(clickHouseCR)

			By("Wait for cluster to reach `Completed` state")
			waitCHClusterHasSynchronized(clickHouseCR)

			By("Expand cluster's storage")
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())
			cr.Spec.StorageSize.Add(resource.MustParse("1Gi"))
			Expect(k8sClient.Update(ctx, cr)).To(Succeed())

			By("Hack sts finalizers, and wait for sts to be recreated")
			removeStsOrphanFinalizer(clickHouseCR)

			By("Wait for StatefulSets to be updated")
			Eventually(
				func(g Gomega) {
					for idx := int32(0); idx < *clickHouseCR.Spec.Replicas; idx++ {
						stsNameKey := types.NamespacedName{
							Name:      fmt.Sprintf("%s-0-%d", clickHouseCR.Name, idx),
							Namespace: clickHouseCR.Namespace,
						}
						sts := new(appsv1.StatefulSet)
						g.Expect(k8sClient.Get(ctx, stsNameKey, sts)).To(Succeed())

						dataVolumeFound := false
						for _, pvc := range sts.Spec.VolumeClaimTemplates {
							if pvc.Name != CHDataVolumeName {
								continue
							}
							cur := pvc.Spec.Resources.Requests[corev1.ResourceStorage]
							expected := cr.Spec.StorageSize
							g.Expect(
								cur.Equal(expected),
							).To(
								BeTrue(),
								"current: %s, requested: %s", (&cur).String(), (&expected).String(),
							)
							dataVolumeFound = true
						}
						g.Expect(dataVolumeFound).To(BeTrue())
					}
				},
				timeout, interval,
			).Should(Succeed())

			By("Wait for PVCs to be updated")
			Eventually(
				func(g Gomega) {
					for _, pvcSrc := range pvcs {
						pvc := new(corev1.PersistentVolumeClaim)
						g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(pvcSrc), pvc)).To(Succeed())

						cur := pvc.Spec.Resources.Requests[corev1.ResourceStorage]
						expected := cr.Spec.StorageSize
						g.Expect(
							cur.Equal(expected),
						).To(
							BeTrue(),
							"current: %s, requested: %s", (&cur).String(), (&expected).String(),
						)
					}
				},
				timeout, interval,
			).Should(Succeed())

			By("Update PVCs so that they appear to be ready/provisioned")
			ensurePvcsStatus(cr)

			By("Ensure re-created STSes are marked as ready")
			// Deletion + recreation cleared status. We need to re-apply it.
			ensureSTSesAreReady(clickHouseCR)

			By("Ensure that both status is in sync as well as state of the CH DB is `Completed` ")
			waitCHClusterHasSynchronized(clickHouseCR)
		})

		It("waits for STSes to get ready", func() {
			By("Update PVCs and STSes so that cluster can reach `Completed` state")
			ensurePvcsStatus(clickHouseCR)
			ensureSTSesAreReady(clickHouseCR)

			By("Wait for cluster to reach `Completed` state")
			waitCHClusterHasSynchronized(clickHouseCR)

			By("Mark one of STSes as unready")
			unreadyStsKey := types.NamespacedName{
				Name:      fmt.Sprintf("%s-0-0", clickHouseCR.Name),
				Namespace: clickHouseCR.Namespace,
			}
			setStsStatusUnready(unreadyStsKey)

			By("Wait for cluster to reach `InProgress` state")
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)

			By("Expand cluster's storage")
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())
			cr.Spec.StorageSize.Add(resource.MustParse("1Gi"))
			Expect(k8sClient.Update(ctx, cr)).To(Succeed())

			By("Update PVCs so that they appear to be ready/provisioned")
			ensurePvcsStatus(cr)

			By("Check that cluster does not get resized")
			// NOTE(prozlach): Ideally, we would check the logs of the
			// controller for a specific log message. For now let's just wait a
			// bit and make sure that pod does not get touched by controller.
			Consistently(
				func(g Gomega) {
					scaledStses := make([]appsv1.StatefulSet, 0)

					stsList := &appsv1.StatefulSetList{}
					labels := map[string]string{
						"app":  "clickhouse",
						"name": clickHouseCR.Name,
					}
					g.Expect(
						k8sClient.List(ctx, stsList, client.InNamespace(clickHouseCR.Namespace), client.MatchingLabels(labels)),
					).To(Succeed())

					for _, sts := range stsList.Items {
						dataVolumeFound := false
						for _, pvc := range sts.Spec.VolumeClaimTemplates {
							if pvc.Name != CHDataVolumeName {
								continue
							}
							if pvc.Spec.Resources.Requests[corev1.ResourceStorage].Equal(cr.Spec.StorageSize) {
								scaledStses = append(scaledStses, sts)
							}
							dataVolumeFound = true
						}
						g.Expect(dataVolumeFound).To(BeTrue())
					}

					g.Expect(scaledStses).To(BeEmpty())
				},
				2*timeout, time.Second,
			).Should(Succeed())

			By("Make unready STS ready so that cluster can recover")
			// We need to update only this particular STS, otherwise we will be
			// racing with the controller
			setStsStatusReady(unreadyStsKey)

			By("Hack sts finalizers, and wait for sts to be recreated")
			removeStsOrphanFinalizer(clickHouseCR)

			By("Ensure re-created STSes are marked as ready")
			// Deletion + recreation cleared status. We need to re-apply it.
			ensureSTSesAreReady(clickHouseCR)

			By("Ensure that both status is in sync as well as state of the CH DB is `Completed` ")
			waitCHClusterHasSynchronized(clickHouseCR)
		})

		It("does not allow for shrinking volumes", func() {
			By("Update PVCs and STSes so that cluster can reach `Completed` state")
			ensurePvcsStatus(clickHouseCR)
			ensureSTSesAreReady(clickHouseCR)

			By("Wait for cluster to reach `Completed` state")
			waitCHClusterHasSynchronized(clickHouseCR)

			By("Shrink cluster's storage")
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())
			cr.Spec.StorageSize.Sub(resource.MustParse("1Gi"))
			Expect(k8sClient.Update(ctx, cr)).To(Succeed())

			By("Check that cluster does not get resized")
			// NOTE(prozlach): Ideally, we would check the logs of the
			// controller for a specific log message. For now let's just wait a
			// bit and make sure that pod does not get touched by controller.
			Consistently(
				func(g Gomega) {
					scaledStses := make([]appsv1.StatefulSet, 0)

					stsList := &appsv1.StatefulSetList{}
					labels := map[string]string{
						"app":  "clickhouse",
						"name": clickHouseCR.Name,
					}
					g.Expect(
						k8sClient.List(ctx, stsList, client.InNamespace(clickHouseCR.Namespace), client.MatchingLabels(labels)),
					).To(Succeed())

					for _, sts := range stsList.Items {
						dataVolumeFound := false
						for _, pvc := range sts.Spec.VolumeClaimTemplates {
							if pvc.Name != CHDataVolumeName {
								continue
							}
							if pvc.Spec.Resources.Requests[corev1.ResourceStorage].Equal(cr.Spec.StorageSize) {
								scaledStses = append(scaledStses, sts)
							}
							dataVolumeFound = true
						}
						g.Expect(dataVolumeFound).To(BeTrue())
					}

					g.Expect(scaledStses).To(BeEmpty())
				},
				2*timeout, time.Second,
			).Should(Succeed())
		})
	})

	Context("When CHProxy is enabled", func() {
		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example-with-proxy.yaml")
			waitClickhouseDependencies(clickHouseCR)

			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)
		})

		It("Should create proxy objects", func() {
			// Wait for proxy Deployment to appear
			proxyDeployment := &appsv1.Deployment{
				ObjectMeta: metav1.ObjectMeta{
					Name:      fmt.Sprintf("%s-proxy", clickHouseCR.Name),
					Namespace: clickHouseCR.Namespace,
				},
			}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(proxyDeployment), proxyDeployment)).To(Succeed())

			// Wait for proxy Deployment to appear
			proxyService := &corev1.Service{
				ObjectMeta: metav1.ObjectMeta{
					Name:      fmt.Sprintf("%s-proxy", clickHouseCR.Name),
					Namespace: clickHouseCR.Namespace,
				},
			}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(proxyService), proxyService)).To(Succeed())

			verifyStatefulSets(clickHouseCR)
		})
	})

	Context("When affinity is set", func() {
		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example-with-affinity.yaml")
			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)

			waitClickhouseDependencies(clickHouseCR)
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)
		})

		It("Should merge default affinity with custom", func() {
			verifyStatefulSets(clickHouseCR)
		})

		It("Should allow adding extra pod anti affinity rules apart from default", func() {
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())
			crBase := cr.DeepCopy()
			cr.Spec.Affinity.PodAntiAffinity = &corev1.PodAntiAffinity{
				PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
					{
						Weight: 3,
						PodAffinityTerm: corev1.PodAffinityTerm{
							Namespaces: []string{"foo"},
						},
					},
				},
			}
			Expect(k8sClient.Patch(ctx, cr, client.MergeFrom(crBase))).To(Succeed())

			verifyStatefulSets(clickHouseCR)
		})
	})

	Context("When nodeSelector is set", func() {
		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example-with-nodeSelector.yaml")
			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)

			waitClickhouseDependencies(clickHouseCR)
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)
		})

		It("Should copy defined nodeSelector to definition", func() {
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())

			Expect(cr.Spec.NodeSelector).NotTo(BeEmpty())
			verifyStatefulSets(clickHouseCR)
		})
	})

	Context("When tolerations are set", func() {
		BeforeEach(func() {
			var objsToRemove []client.Object
			clickHouseCR, objsToRemove = ensureCHManifests("../config/samples/example-with-tolerations.yaml")
			DeferCleanup(removeObjects, clickHouseCR, objsToRemove)

			waitClickhouseDependencies(clickHouseCR)
			waitForCHStatus(clickHouseCR, clickhousev1alpha1.StatusInProgress)
		})

		It("Should copy defined tolerations to definition", func() {
			cr := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(clickHouseCR), cr)).To(Succeed())

			Expect(cr.Spec.Tolerations).NotTo(BeEmpty())
			verifyStatefulSets(clickHouseCR)
		})
	})
})
