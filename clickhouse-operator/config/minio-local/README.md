### Minio vanilla helm chart overriden with custom arguments

Source of this resource is from Minio vanilla [helm chart](https://github.com/minio/minio/tree/master/helm/minio),
which is then rendered via `helm template`.

Additionally, we also provide values for default buckets, users and policies as needed by our local E2E test setup.

The values file looks like: [Reference](https://github.com/minio/minio/blob/master/helm/minio/values.yaml)
```yaml
# This should be a factor of 2 or 16. See erasure coding in MinIO [https://docs.min.io/docs/minio-erasure-code-quickstart-guide]
replicas: 4

resources:
  requests:
    memory: 500Mi
    cpu: 1

nodeSelector: {}
rootUser: "rootuser"
rootPassword: "rootpass"

persistence:
  enabled: true
  annotations: {}

  storageClass: ""
  VolumeName: ""
  accessMode: ReadWriteOnce
  size: 5Gi

  subPath: ""

## List of buckets to be created after minio install
##
buckets:
  #   # Name of the bucket
  - name: clickhouse-sandbox/root
    # Policy to be set on the
    # bucket [none|download|upload|public]
    policy: none
    # Purge if bucket exists already
    purge: false
    # set versioning for
    # bucket [true|false]
    versioning: false

## List of command to run after minio install
## NOTE: the mc command TARGET is always "myminio"
customCommands:
  - command: "admin user add myminio clickhouse clickhouses3secret"
  - command: "admin policy set myminio rwpolicy user=clickhouse"

## Currently there seems to be an issue when users providing via this block, thus we leverage `customCommands`.
## Also set the following to empty because chart picks up empty usernames.
users: []

## List of policies to be created after minio install
policies:
## rwpolicy grants creation or deletion of buckets with name
## starting with clickhouse-sandbox. In addition, grants objects write permissions on buckets under
## clickhouse-sandbox.
- name: rwpolicy
  statements:
    - resources:
        - 'arn:aws:s3:::clickhouse-sandbox/*'
      actions:
        - "s3:AbortMultipartUpload"
        - "s3:GetObject"
        - "s3:DeleteObject"
        - "s3:PutObject"
        - "s3:ListMultipartUploadParts"
    - resources:
        - 'arn:aws:s3:::clickhouse-sandbox*'
      actions:
        - "s3:CreateBucket"
        - "s3:DeleteBucket"
        - "s3:GetBucketLocation"
        - "s3:ListBucket"
        - "s3:ListBucketMultipartUploads"
```

Any update to the values file should be persisted as(release name is `minio-dev`):
```bash
helm template  -f values.yaml minio-dev  minio/minio  > minio.yaml
```