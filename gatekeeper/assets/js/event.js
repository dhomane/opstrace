(function () {
    /**
     *   We need to let the enclosing GitLab instance know that the page has loaded.
     */
    window.addEventListener("DOMContentLoaded", function () {
        // TO-DO - use gitlab url (staging/prod) instead of *
        window.parent?.postMessage({ type: "GOUI_LOADED" }, "*");
    });
})();