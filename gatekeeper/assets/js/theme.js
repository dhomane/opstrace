(function () {
    function applyTheme(theme = "light") {
        document.body.classList.add(theme);
    }

    function getTheme() {
        return new URLSearchParams(window.location.search).get("theme");
    }

    const theme = getTheme();
    applyTheme(theme);
})();