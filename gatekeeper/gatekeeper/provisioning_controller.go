package gatekeeper

import (
	"context"
	"crypto/tls"
	stderrors "errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

type NamespaceParams struct {
	ID     string `uri:"namespace_id" binding:"required"`
	Action string `uri:"action"`
}

// Handles incoming requests for a Gitlab namespace for namespaces that do not
// yet exist in Opstrace. If this namespace existed, nginx-ingress would match it and
// route the request to the Argus instance in that namespace. If the request lands here,
// we need to provision the namespace in Opstrace. This is a lazy provisioner that only
// provisions namespaces when the first request comes in for that namespace. Request must
// have Owner permissions on the respective GitLab namespace to proceed.
//
// See the namespace API docs in gitlab https://docs.gitlab.com/ee/api/namespaces.html
//
//nolint:funlen,gocyclo,cyclop
func HandleNamespaceProvisioning(ctx *gin.Context) {
	var params NamespaceParams
	if err := ctx.ShouldBindUri(&params); err != nil {
		AbortWithError(ctx, 404, "invalid namespace_id", err, false)
		return
	}
	g := GetGitLabService(ctx)
	user, err := g.CurrentUser()
	if err != nil {
		HandleError(ctx, err)
		return
	}

	var topLevelNamespace *GitLabNamespace
	var namespace *GitLabNamespace
	canAccess, accessLevel, err := g.CanAccessNamespace(params.ID)
	// Only abort if it's due to an authorization issue
	if stderrors.Is(err, ErrUnauthorized) {
		HandleError(ctx, err)
		return
	}

	if canAccess && accessLevel != gitlab.OwnerPermission {
		// Show message that owner must enable observability for this namespace
		ctx.HTML(200, "error.gohtml", gin.H{
			"title":   "Please contact an Owner of this namespace",
			"message": "Must be an Owner to enable observability for this namespace",
		})
		return
	}

	//nolint:nestif
	if canAccess && accessLevel == gitlab.OwnerPermission {
		// Has all the permissions to enable observability for this namespace
		namespace, err = g.GetNamespace(params.ID)
		if err != nil {
			HandleError(ctx, err)
			return
		}
		// Get top level namespace because that maps to an Opstrace namespace
		if namespace.IsTopLevel() {
			topLevelNamespace = namespace
		} else {
			topLevelID := namespace.GetTopLevelNamespaceID()
			topLevelNamespace, err = g.GetNamespace(topLevelID)
		}
		if err != nil {
			HandleError(ctx, err)
			return
		}
	} else {
		// User does not have any access to this namespace
		Abort(ctx, 404, "not found", false)
		log.WithFields(log.Fields{
			"user":      user.ID,
			"namespace": params.ID,
		}).Info("attempting to access namespace without membership")

		return
	}

	c := GetConfig(ctx)

	avatar := ""

	if namespace.AvatarURL != nil {
		// Prepend with gitlab instance host
		avatar = strings.Join([]string{c.GitlabAddr, *namespace.AvatarURL}, "")
	}

	selector := client.ObjectKey{
		Name:      fmt.Sprint(namespace.ID),
		Namespace: c.Namespace,
	}
	obj := opstracev1alpha1.GitLabNamespace{}

	err = c.K8sClient.Get(context.TODO(), selector, &obj)
	if err != nil && !errors.IsNotFound(err) {
		HandleError(ctx, fmt.Errorf("failed to read namespace %s from kubernetes client cache: %w", namespace.FullPath, err))
		return
	}

	current := &opstracev1alpha1.GitLabNamespace{
		ObjectMeta: v1.ObjectMeta{
			Name:      fmt.Sprint(namespace.ID),
			Namespace: c.Namespace,
		},
	}

	//nolint:nestif
	if errors.IsNotFound(err) {
		// Create it
		current.Spec.ID = int64(namespace.ID)
		current.Spec.TopLevelNamespaceID = int64(topLevelNamespace.ID)
		current.Spec.Name = namespace.Name
		current.Spec.Path = namespace.Path
		current.Spec.FullPath = namespace.FullPath
		current.Spec.AvatarURL = avatar
		current.Spec.WebURL = namespace.WebURL

		err := c.K8sClient.Create(context.TODO(), current)
		// Don't care if there is a conflict because it means another process succeeded with the update
		if err != nil && !errors.IsConflict(err) {
			HandleError(ctx, fmt.Errorf("create failed for namespace %s: %w", namespace.FullPath, err))
			return
		}
		log.Info(fmt.Sprintf(
			"namespace %s [ID:%d] created",
			namespace.FullPath,
			namespace.ID,
		))
	} else {
		// Examine DeletionTimestamp to determine if object is under deletion
		if !obj.ObjectMeta.DeletionTimestamp.IsZero() {
			HandleError(ctx, fmt.Errorf("namespace %s is deleting", namespace.FullPath))
			return
		}
		// Create a copy, mutate it and then check if anything changed before updating.
		current = obj.DeepCopy()
		current.Spec.ID = int64(namespace.ID)
		current.Spec.TopLevelNamespaceID = int64(topLevelNamespace.ID)
		current.Spec.Name = namespace.Name
		current.Spec.Path = namespace.Path
		current.Spec.FullPath = namespace.FullPath
		current.Spec.AvatarURL = avatar
		current.Spec.WebURL = namespace.WebURL

		if !reflect.DeepEqual(obj, current) {
			err := c.K8sClient.Update(context.TODO(), current)
			// Don't care if there is a conflict because it means another process succeeded with the update
			if err != nil && !errors.IsConflict(err) {
				HandleError(ctx, fmt.Errorf("update failed for namespace %s: %w", namespace.FullPath, err))
				return
			}
			log.Info(fmt.Sprintf(
				"namespace %s [ID:%d] updated",
				namespace.FullPath,
				namespace.ID,
			))
		}
	}

	// This is a ballpark that we should hone. The goal isn't for this to be correct
	// but to communicate an expectation back to the user that it will take tiime to provision
	// and to give them an idea of how much longer they need to wait
	usualProvisioningTime := 120 * time.Second

	// examine DeletionTimestamp to determine if object is under deletion
	if !current.ObjectMeta.DeletionTimestamp.IsZero() {
		ctx.HTML(200, "deleting.gohtml", gin.H{
			"namespaceFullPath": namespace.FullPath,
		})
		return
	}

	provisioningFor := time.Since(current.ObjectMeta.CreationTimestamp.Time)
	progressPercent := int64(provisioningFor.Seconds() / usualProvisioningTime.Seconds() * 100)
	// never let it go above 97% so it doesn't cause confusion for the user
	if progressPercent > 97 {
		progressPercent = 97
	}

	if current.Status.ArgusURL == nil {
		log.Info(fmt.Sprintf(
			"namespace %s [ID:%d] waiting for argus url to propagate",
			namespace.FullPath,
			namespace.ID,
		))
		ctx.HTML(200, "provisioning.gohtml", gin.H{
			"namespaceFullPath": namespace.FullPath,
			"progressPercent":   progressPercent,
		})
		return
	}

	group := int64(namespace.ID)

	argus, err := common.NewArgusClientFromURL(
		*current.Status.ArgusURL,
		group,
		/* #nosec G402 */
		&http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	)
	if err != nil {
		AbortWithError(ctx, 500, "failed to create argus client", err, true)
		return
	}

	err = argus.GroupExists(group)
	if err != nil {
		log.Info(fmt.Sprintf(
			"namespace %s [ID:%d] waiting for argus group to be provisioned, err: %v",
			namespace.FullPath,
			namespace.ID,
			err,
		))
		ctx.HTML(200, "provisioning.gohtml", gin.H{
			"namespaceFullPath": namespace.FullPath,
			"progressPercent":   progressPercent,
		})
		return
	}

	// Prepend with gitlab instance host
	user.AvatarURL = strings.Join([]string{c.GitlabAddr, user.AvatarURL}, "")

	userID, err := argus.CreateOrUpdateUser(user)
	if err != nil {
		AbortWithError(ctx, 500, "failed to update user in namespace", err, true)
		return
	}
	user.ID = int(userID)

	err = argus.CreateOrUpdateGroupUser(group, user, common.GrafanaRoleFromGroupAccessLevel(accessLevel))
	if err != nil {
		AbortWithError(ctx, 500, "failed to update group user", err, true)
		return
	}

	// Good to go
	queryParams := ctx.Request.URL.Query()
	queryParams.Set("groupId", fmt.Sprintf("%d", namespace.ID))
	ctx.Redirect(302, fmt.Sprintf("/%d%s?%s", topLevelNamespace.ID, params.Action, queryParams.Encode()))
}
