package gatekeeper

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
)

func TestAuthHandlerFactory(t *testing.T) {
	// goui sub server with fixed responses
	gouiStub := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Authorization") != "Bearer token" {
			w.WriteHeader(403)
			return
		}

		assert.NoError(t, json.NewEncoder(w).Encode(argusapi.Group{
			ID:   1234,
			Name: "1234",
		}))
	}))
	gouiURL := gouiStub.URL
	defer gouiStub.Close()

	router := gin.Default()
	// in-memory session storage
	store := memstore.NewStore([]byte("secret"))
	router.Use(sessions.Sessions("mysession", store))

	// templates needed to render error pages
	router.LoadHTMLGlob("../templates/*.gohtml")

	scheme := runtime.NewScheme()
	assert.NoError(t, schedulerv1alpha1.AddToScheme(scheme))
	// config middleware with mocks
	router.Use(Config(&ConfigOptions{
		Namespace: "default",
		K8sClient: fake.NewClientBuilder().WithScheme(scheme).WithRuntimeObjects(
			&schedulerv1alpha1.GitLabNamespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "1234",
					Namespace: "default",
				},
				Status: schedulerv1alpha1.GitLabNamespaceStatus{
					ArgusURL: &gouiURL,
				},
			}).Build(),
	}))

	// mock redis cache
	router.Use(func(ctx *gin.Context) {
		ctx.Set(cacheClientKey, newMockRedis())
	})

	SetRoutes(router)

	for _, tc := range []struct {
		name      string
		namespace string
		headers   []pair
		expected  int
	}{
		{
			"should 404 if namespace is missing",
			"",
			nil,
			404,
		},
		{
			"empty session returns 401",
			"1234",
			nil,
			401,
		},
		{
			"invalid session id returns 403",
			"@",
			nil,
			403,
		},
		{
			"namespace id is unknown, returns 403",
			"0",
			[]pair{
				{
					key:   "Authorization",
					value: "Bearer token",
				},
			},
			403,
		},
		{
			"bearer token is invalid, returns 403",
			"1234",
			[]pair{
				{
					key:   "Authorization",
					value: "Bearer badtoken",
				},
			},
			403,
		},
		{
			"bearer token is valid, returns 200",
			"1234",
			[]pair{
				{
					key:   "Authorization",
					value: "Bearer token",
				},
			},
			200,
		},
		{
			"basic auth password as invalid bearer token, returns 403",
			"1234",
			[]pair{
				{
					key:   "Authorization",
					value: "Basic " + base64.StdEncoding.EncodeToString([]byte(":badtoken")),
				},
			},
			403,
		},
		{
			"basic auth password as valid bearer token, returns 200",
			"1234",
			[]pair{
				{
					key:   "Authorization",
					value: "Basic " + base64.StdEncoding.EncodeToString([]byte(":token")),
				},
			},
			200,
		},
		// TODO(joe): test session auth - requires much more set up
	} {
		t.Run(tc.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "/v1/auth/webhook/"+tc.namespace, nil)
			assert.Nil(t, err)

			// add headers to test case request
			for _, h := range tc.headers {
				req.Header.Set(h.key, h.value)
			}

			router.ServeHTTP(w, req)
			assert.Equal(t, tc.expected, w.Code, tc.name)
		})
	}
}
