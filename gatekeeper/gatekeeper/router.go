package gatekeeper

import (
	"github.com/gin-gonic/gin"
)

// Sets up the gatekeeper routes.
func SetRoutes(router *gin.Engine) {
	v1 := router.Group("/v1")
	{
		auth := v1.Group("/auth")
		{
			// Start the oauth2 flow
			auth.GET("/start", HandleAuthStart)
			// Finish the oauth2 flow
			auth.GET("/callback", HandleAuthFinish)
			// Handle the external auth webhook request from nginx-ingress.
			// https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
			// This is the endpoint that all ingresses (except Argus) should use.
			auth.GET(
				"/webhook/:namespace_id",
				// Do not set autoAuth on the webhook because nginx-ingress is the direct client.
				// This request from nginx-ingress expects a 401 or 403 if auth fails.
				// nginx-ingress can then redirect to the /v1/auth/start flow.
				AuthHandlerFactory(false),
			)
			// Specific route for Argus because the :namespace_id is always the topLevelNamespace and we have to handle
			// auth a little differently
			auth.GET(
				"/argus/webhook/:root_namespace_id",
				// Do not set autoAuth on the webhook because nginx-ingress is the direct client.
				// This request from nginx-ingress expects a 401 or 403 if auth fails.
				// nginx-ingress can then redirect to the /v1/auth/start flow.
				ArgusAuthHandlerFactory(false),
			)
		}
		// Provision the namespace
		v1.GET(
			"/provision/:namespace_id/*action",
			// Configure `AuthenticatedSessionRequired` to autoAuth since the web browser is the direct client
			AuthenticatedSessionRequired(true, HandleNamespaceProvisioning),
		)

		// Add internal API to list project of a group
		// don't auto-auth redirect, assume we must already have a session.
		v1.GET("/:group_id/projects", AuthenticatedSessionRequired(false, ListGroupProjectsHandler))
	}
	h := NewErrorTrackingAuthHandler()
	errortracking := v1.Group("/error_tracking")
	{
		errortracking.GET("/auth", h.HandleErrorTrackingAuth)
	}

	catchAll := router.Group("/-")
	{
		// Catches any gitlab URL that is prepended with /-/, e.g. /-/<gitlab_namespace>/<action>.
		catchAll.GET(
			"/:namespace_id/*action",
			// Configure `AuthenticatedSessionRequired` to autoAuth since the web browser is the direct client
			AuthenticatedSessionRequired(true, HandlePath),
		)
	}

	router.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})
}
