package gatekeeper

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/cache/v8"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/argusapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ArgusAuthWebhookURIParams struct {
	RootID string `uri:"root_namespace_id" binding:"required"`
}

type AuthWebhookURIParams struct {
	ID string `uri:"namespace_id" binding:"required"`
}

type AuthWebhookQuery struct {
	URI string `form:"uri"  binding:"required"`
}

type AuthWebhookHeader struct {
	ArgusNamespaceID string `header:"x-grafana-org-id"`
	NamespaceID      string `header:"x-namespace-id"`
}

// NamespaceIDs are required to be alphanumeric in order for the query hostname to work below.
// Gitlab namespaces are numeric, but special case "system" namespace must also be allowed through.
var namespaceIDPattern = regexp.MustCompile(`^[A-Za-z0-9-]+$`)

// Specific middleware for Argus ingress.
// Returns an auth handler that will handle several cases of auth.
// First it will check if there is an Authorization header
// and then seek to verify the token if the header exists.
// If the header does not exist, the controller will fall
// back to requiring an authenticated session and will verify
// auth against the session.
// Only supports argus auth tokens.
//
//nolint:funlen,gocyclo,cyclop
func ArgusAuthHandlerFactory(autoAuth bool) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var header AuthWebhookHeader
		if err := ctx.ShouldBindHeader(&header); err != nil {
			AbortWithError(ctx, 403, "invalid header", err, false)
			return
		}
		var params ArgusAuthWebhookURIParams
		if err := ctx.ShouldBindUri(&params); err != nil {
			AbortWithError(ctx, 403, "invalid params", err, false)
			return
		}
		var query AuthWebhookQuery
		if err := ctx.ShouldBindQuery(&query); err != nil {
			AbortWithError(ctx, 403, "invalid params", err, false)
			return
		}

		uri, err := url.Parse(query.URI)
		if err != nil {
			AbortWithError(ctx, 400, "invalid uri query parameter", err, false)
			return
		}
		// Let Argus static assets through.
		if strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/build/", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/public/", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/robots.txt", params.RootID)) ||
			strings.HasPrefix(uri.Path, fmt.Sprintf("/%s/avatar/", params.RootID)) {
			ctx.String(200, "Success")
			return
		}

		// Use the root level namespace.
		// Admin uris in Argus, e.g.
		//
		// 		/<rootNamespaceID>/datasources
		//		/<rootNamespaceID>/plugins
		// 		/<rootNamespaceID>/preferences
		//
		// will not send a header containing x-grafana-org-id and will send
		// a query param of groupId=-1 if the browser page is refreshed. This presents
		// an issue where under these circumstances, we'll have to do a little more
		// work to get the namespaceID.
		rootNamespaceID := params.RootID
		namespaceID := ""

		// Use groupId if it exists. This query param is sent by the Argus UI.
		// Argus UI uses a mix of sending groupId param or setting the header
		// that we check for below.
		if uri.Query().Has("groupId") && uri.Query().Get("groupId") != "" {
			namespaceID = uri.Query().Get("groupId")
		}
		// Allow users to set an Opstrace specific header for selecting namespace
		// x-namespace-id
		if header.NamespaceID != "" {
			namespaceID = header.NamespaceID
		}
		// Use standard Argus header if it exists (x-grafana-org-id)
		// This header is sent in requests from the Argus UI.
		if header.ArgusNamespaceID != "" {
			namespaceID = header.ArgusNamespaceID
		}

		// Make sure the rootNamespaceID is legit.
		if !namespaceIDPattern.MatchString(rootNamespaceID) {
			Abort(ctx, 404, fmt.Sprintf("rootNamespaceID='%s' is invalid", rootNamespaceID), false)
			return
		}
		// Make sure the namespaceID is legit if it exists at this point.
		if namespaceID != "" && !namespaceIDPattern.MatchString(namespaceID) {
			Abort(ctx, 404, fmt.Sprintf("namespaceID='%s' is invalid", namespaceID), false)
			return
		}

		// TODO: Simplify this logic
		//nolint:nestif
		if bearer, got := bearerToken(ctx); got {
			if namespaceID == "" {
				// Attempt to use the bearerToken to retrieve the current namespace in argus.
				// Since a token is scoped to a Group in Argus, it should return
				// the group it's scoped to.
				argusURL, err := GetArgusURLFromRootNamespaceID(ctx, rootNamespaceID)
				if err != nil {
					log.Error("GetArgusURLFromRootNamespaceID failed:", err)
					AbortWithError(ctx, 403, "unauthorized", err, false)
				}
				namespaceID, err = GetNamespaceFromArgusForToken(ctx, argusURL, bearer)
				if err != nil {
					log.Error("GetNamespaceFromArgusForToken failed:", err)
					AbortWithError(ctx, 403, "unauthorized", err, false)
					return
				}
			}
			if namespaceID == "" {
				log.Error("target namespace not discovered")
				AbortWithError(ctx, 403, "unauthorized", err, false)
				return
			}
			// Set the namespaceID on the context for downstream handlers
			SetNamespace(ctx, namespaceID)
			HandleTokenAuth(ctx)
		} else {
			// Fallback to authorization via session cookie. This request comes from the browser.
			AuthenticatedSessionRequired(autoAuth, func(_ *gin.Context) {
				if namespaceID == "" {
					// Now that we have a session, we can retrieve the user and
					// attempt to retrieve their last known namespace that they used
					// in this Argus instance.
					var lastUsedNamespace = &LastNamespace{}
					GetLastUsedNamespaceForUser(ctx, rootNamespaceID, lastUsedNamespace)
					if lastUsedNamespace.ID != "" {
						namespaceID = lastUsedNamespace.ID
					}
					if err != nil {
						log.Error("GetNamespaceFromArgusForToken failed:", err)
						AbortWithError(ctx, 403, "unauthorized", err, false)
						return
					}
				} else {
					// Update the last known namespaceID in the cache
					err := SetLastUsedNamespaceForUser(ctx, rootNamespaceID, LastNamespace{ID: namespaceID})
					if err != nil {
						log.Error("failed to update last known namespace in cache")
						// This probably doesn't need to block auth moving forward,
						// since it's not critical to operation but it will ensure we catch
						// any service errors that we should resolve.
						AbortWithError(ctx, 403, "unauthorized", err, false)
						return
					}
				}
				if namespaceID == "" {
					log.Error("target namespace not discovered")
					AbortWithError(ctx, 403, "unauthorized", err, false)
					return
				}
				// Set the namespaceID on the context for downstream handlers
				SetNamespace(ctx, namespaceID)
				HandleSessionAuth(ctx)
			})(ctx)
		}
	}
}

// Returns an auth handler that will handle several cases of auth.
// First it will check if there is an Authorization header
// and then seek to verify the token if the header exists.
// If the header does not exist, the controller will fall
// back to requiring an authenticated session and will verify
// auth against the session.
// Only supports argus auth tokens.
func AuthHandlerFactory(autoAuth bool) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var params AuthWebhookURIParams
		if err := ctx.ShouldBindUri(&params); err != nil {
			AbortWithError(ctx, 403, "invalid params", err, false)
			return
		}

		// This namespace maps to a GitLabNamespace. It is not the root level namespace.
		namespaceID := params.ID

		// Make sure the namespaceID is legit.
		if !namespaceIDPattern.MatchString(namespaceID) {
			Abort(ctx, 403, fmt.Sprintf("namespaceID='%s' is invalid", namespaceID), false)
			return
		}
		// Set the namespaceID on the context for downstream handlers
		SetNamespace(ctx, namespaceID)

		// Check for Authorization header first
		if _, gotBearer := bearerToken(ctx); gotBearer {
			HandleTokenAuth(ctx)
		} else {
			// Fallback to authorization via session cookie. This request comes from the browser
			// and allows users to interact with the API in the browser which is helpful for creating
			// datasources in Argus that can query across different namespaces and will only return
			// successfully if user has permission to those namespaces
			// (panels on the dashboard can render a permission not granted
			// for example if datasource returned 401/403/404)
			AuthenticatedSessionRequired(autoAuth, func(_ *gin.Context) {
				HandleSessionAuth(ctx)
			})(ctx)
		}
	}
}

// Takes the rootlevelnamespace and attempts a lookup against the local
// cache to retrieve the argus url.
func GetArgusURLFromRootNamespaceID(ctx *gin.Context, rootNamespaceID string) (string, error) {
	// We've got to do that extra work.
	// Get the Tenant CR and retrieve the argus URL from it.
	selector := client.ObjectKey{
		Name:      constants.TenantName,
		Namespace: rootNamespaceID,
	}
	obj := tenantOperator.Tenant{}

	c := GetConfig(ctx)

	err := c.K8sClient.Get(context.TODO(), selector, &obj)
	if err != nil {
		return "", fmt.Errorf("failed to read tenant in namespace %s from kubernetes client cache: %w", rootNamespaceID, err)
	}
	// Examine DeletionTimestamp to determine if object is under deletion
	if !obj.ObjectMeta.DeletionTimestamp.IsZero() {
		return "", fmt.Errorf("tenant in namespace %s is deleting", rootNamespaceID)
	}
	if obj.Status.Argus.URL == nil {
		return "", fmt.Errorf("argusURL for tenant in namespace %s not available", rootNamespaceID)
	}
	return *obj.Status.Argus.URL, nil
}

// Takes the namespaceID and attempts a lookup against the local
// cache to retrieve the argus url.
func GetArgusURLFromNamespaceID(ctx *gin.Context, namespaceID string) (string, error) {
	c := GetConfig(ctx)
	// We've got to do that extra work.
	// Get the Tenant CR and retrieve the argus URL from it.
	selector := client.ObjectKey{
		Name:      namespaceID,
		Namespace: c.Namespace,
	}
	obj := v1alpha1.GitLabNamespace{}

	err := c.K8sClient.Get(context.TODO(), selector, &obj)
	if err != nil {
		return "", fmt.Errorf("failed to read tenant in namespace %s from kubernetes client cache: %w", namespaceID, err)
	}
	// Examine DeletionTimestamp to determine if object is under deletion
	if !obj.ObjectMeta.DeletionTimestamp.IsZero() {
		return "", fmt.Errorf("GitlabNamespace %s is deleting", namespaceID)
	}
	if obj.Status.ArgusURL == nil {
		return "", fmt.Errorf("argusURL for GitlabNamespace %s not available", namespaceID)
	}
	return *obj.Status.ArgusURL, nil
}

// Attempts to retrieve the org for the identity.
// Return the namespace that the token is scoped to.
// https://grafana.com/docs/grafana/latest/http_api/org/#get-current-organization
func GetNamespaceFromArgusForToken(
	ctx *gin.Context, argusURL string, bearerToken string,
) (string, error) {
	group := argusapi.Group{}

	c := GetCache(ctx)
	key := fmt.Sprintf("argus-%s", strings.ReplaceAll(bearerToken, " ", ""))

	err := c.Get(ctx, key, &group)
	//nolint:nestif
	if err != nil {
		u, err := url.Parse(argusURL)
		if err != nil {
			return "", err
		}
		// make sure basic auth isn't used
		u.User = nil
		// https://grafana.com/docs/grafana/latest/http_api/org/#get-current-organization
		u.Path = "/api/group/"

		req, err := http.NewRequestWithContext(ctx, "GET", u.String(), nil)
		if err != nil {
			return "", err
		}
		req.Header.Add("Authorization", bearerToken)
		req.Header.Add("Content-Type", "application/json")

		log.Debug(fmt.Sprintf("GetNamespaceFromArgusForToken request: %+v", req))

		client := &http.Client{
			Timeout: time.Second * 5,
		}
		resp, err := client.Do(req)
		if err != nil {
			return "", err
		}
		defer resp.Body.Close()

		bodyContents, err := io.ReadAll(resp.Body)

		if err != nil {
			return "", err
		}
		err = json.Unmarshal(bodyContents, &group)
		if err != nil {
			return "", err
		}
		// request was made, now cache the response and ignore any errors
		if err = c.Set(&cache.Item{
			Ctx:   ctx,
			Key:   key,
			Value: group,
			// TODO: bump the cache setting?
			// TODO: should it match the nginx ingress auth-cache setting?
			TTL: 2 * time.Minute,
		}); err != nil {
			log.WithField("err", err).Debug("failed to set cache key")
		}
	}

	if group.ID <= 1 {
		return "", fmt.Errorf("current namespace context returned a default value")
	}
	return fmt.Sprint(group.ID), nil
}

// get a bearer token from the request, returning in format "Bearer {token}".
// Gets either directly from header "Authorization: Bearer {token}" or from password part of Basic Auth.
func bearerToken(ctx *gin.Context) (string, bool) {
	const bearerPrefix = "Bearer "

	auth := ctx.GetHeader("Authorization")
	if auth == "" {
		return "", false
	}

	if strings.HasPrefix(auth, bearerPrefix) {
		return auth, true
	}

	_, token, basic := ctx.Request.BasicAuth()
	if basic && token != "" {
		return bearerPrefix + token, true
	}

	return "", false
}
