package gatekeeper

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v8"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"golang.org/x/oauth2"
)

// Helper function to accept and format both the project ID or name as project
// identifier for all API calls.
// This function is copied from github.com/xanzy/go-gitlab.
func parseID(id interface{}) (string, error) {
	switch v := id.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("invalid ID type %#v, the ID must be an int or a string", id)
	}
}

type GitLabService struct {
	token             *oauth2.Token
	g                 *gitlab.Client
	req               *gin.Context
	tokenSource       oauth2.TokenSource
	invalidateSession bool
}

// Returns an instance of GitLabService that uses an http.Client
// configured with the authToken taken from the request context.
// The http.Client will handle token.access_token expiry and request a new
// access_token using token.refresh_token.
// All GET requests against GitLab are cached.
func NewGitLabService(ctx *gin.Context) (*GitLabService, error) {
	auth := GetAuthConfig(ctx)
	token := GetAuthToken(ctx)
	c := context.TODO()

	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		t := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		tc := &http.Client{Transport: t}
		c = context.WithValue(c, oauth2.HTTPClient, tc)
	}

	serverConfig := GetConfig(ctx)

	tokenSource := auth.TokenSource(c, token)

	client, err := gitlab.NewOAuthClient(
		token.AccessToken,

		// Wire up the base URL for the Gitlab instance
		gitlab.WithBaseURL(serverConfig.GitlabAddr),

		// The HTTP Client returned by auth.Client is supposed to automatically
		// refresh the authToken if it has expired
		gitlab.WithHTTPClient(oauth2.NewClient(c, tokenSource)),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create gitlab client: %w", err)
	}

	return &GitLabService{
		g:                 client,
		token:             token,
		req:               ctx,
		tokenSource:       tokenSource,
		invalidateSession: false,
	}, nil
}

// Returns latest issued token. A new token may have been issued
// through a refresh request and we need to store it.
// Returns nil if token is invalid (i.e. issued a refresh request and it failed).
func (s *GitLabService) Token() (*oauth2.Token, error) {
	return s.tokenSource.Token()
}

// TODO: Investigate usage.
func (s *GitLabService) HandleError(err error) error {
	if _, err2 := s.Token(); err2 != nil {
		return ErrUnauthorized
	} else {
		return err
	}
}

// Get the current user.
func (s *GitLabService) CurrentUser() (*gitlab.User, error) {
	key := fmt.Sprintf("currentuser:%s", s.token.AccessToken)

	var user gitlab.User
	if hit := s.cacheGet(key, &user); hit {
		return &user, nil
	}

	u, _, err := s.g.Users.CurrentUser()
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve currentuser: %w", err))
	}
	s.cacheSet(key, u, 30*time.Second)

	return u, nil
}

// Check if current user can access the system namespace.
func (s *GitLabService) CanAccessSystemNamespace() (bool, gitlab.AccessLevelValue, error) {
	c := GetConfig(s.req)

	membership, err := s.getGroupMembership(c.GroupAllowedSystemAccess)
	if err != nil {
		return false, gitlab.NoPermissions, err
	}
	return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
		membership.AccessLevel,
		nil
}

// Check if current user can access the namespace by Id.
func (s *GitLabService) CanAccessNamespace(nid interface{}) (bool, gitlab.AccessLevelValue, error) {
	// A namespace could be a group or a user.
	ns, err := s.GetNamespace(nid)
	if err != nil || ns == nil {
		return false, gitlab.NoPermissions, err
	}
	if ns.Kind == GroupNamespaceType {
		membership, err := s.getGroupMembership(nid)
		if err != nil {
			return false, gitlab.NoPermissions, err
		}
		return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
			membership.AccessLevel,
			nil
	}
	if ns.Kind == UserNamespaceType {
		// User must be the owner of their own user namespace
		return true, gitlab.OwnerPermission, nil
	}
	// Don't think there are any other Kinds, so this code path shouldn't execute but
	// good to be safe.
	return false, gitlab.NoPermissions, nil
}

// Get GitLab namespace by Id.
func (s *GitLabService) GetNamespace(nid interface{}) (*GitLabNamespace, error) {
	key := fmt.Sprintf("ns:%d", nid)

	var ns *gitlab.Namespace
	if hit := s.cacheGet(key, &ns); hit {
		return &GitLabNamespace{ns}, nil
	}

	namespace, _, err := s.g.Namespaces.GetNamespace(nid)
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve namespace: %w", err))
	}
	s.cacheSet(key, namespace, 30*time.Second)

	return &GitLabNamespace{namespace}, nil
}

// Get the current user's group membership by group Id.
func (s *GitLabService) getGroupMembership(gid interface{}) (*gitlab.GroupMember, error) {
	u, err := s.CurrentUser()
	if err != nil {
		return nil, err
	}
	key := fmt.Sprintf("groupmember:%d:%d", gid, u.ID)

	var groupMember gitlab.GroupMember
	if hit := s.cacheGet(key, &groupMember); hit {
		return &groupMember, nil
	}

	g, _, err := s.GetInheritedGroupMember(gid, u.ID)
	// membership can return nil without an error
	if err != nil || g == nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve groupmembership: %w", err))
	}

	s.cacheSet(key, g, 30*time.Second)

	return g, nil
}

// GetGroupMember gets a member of a group, including inherited membership.
// This method is added here because it doesn't exist in the underlying library
//
// GitLab API docs:
// https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project-including-inherited-and-invited-members
//
//nolint:lll
func (s *GitLabService) GetInheritedGroupMember(
	gid interface{}, user int) (*gitlab.GroupMember, *gitlab.Response, error) {
	group, err := parseID(gid)
	if err != nil {
		return nil, nil, err
	}
	u := fmt.Sprintf("groups/%s/members/all/%d", gitlab.PathEscape(group), user)

	req, err := s.g.NewRequest(http.MethodGet, u, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	gm := new(gitlab.GroupMember)
	resp, err := s.g.Do(req, gm)
	if err != nil {
		return nil, resp, err
	}

	return gm, resp, err
}

// Get key from cache and return true if the cache was hit.
func (s *GitLabService) cacheGet(key string, value interface{}) bool {
	c := GetCache(s.req)
	ctx := context.TODO()

	err := c.Get(ctx, key, &value)
	if err == nil || errors.Is(err, cache.ErrCacheMiss) {
		return err == nil && value != nil
	}
	log.Errorf("cache retrieval failure for key [%s]: %+v", key, err)
	return false
}

// Set key/value in cache with TTL.
func (s *GitLabService) cacheSet(key string, value interface{}, ttl time.Duration) {
	c := GetCache(s.req)
	ctx := context.TODO()

	err := c.Set(&cache.Item{
		Ctx:   ctx,
		Key:   key,
		Value: value,
		TTL:   ttl,
	})
	if err != nil {
		log.Errorf("cache set failure for key [%s]: %+v", key, err)
	}
}

const projectCacheTTL = 1 * time.Minute

// GetGroupProjects return all the projects available under passed Group ID.
func (s *GitLabService) GetGroupProjects(gid interface{}) ([]*gitlab.BasicProject, error) {
	var (
		key          = fmt.Sprintf("group:%s:projects", gid)
		baseProjects []*gitlab.BasicProject
	)

	if hit := s.cacheGet(key, &baseProjects); hit {
		return baseProjects, nil
	}

	// include projects from the subgroups will have a performance cost.
	// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1824#note_1189263082
	includeSubGroups := false

	// TODO(Arun): Handle pagination if the number of projects won't fit in a single page.
	projects, _, err := s.g.Groups.ListGroupProjects(gid, &gitlab.ListGroupProjectsOptions{
		IncludeSubGroups: &includeSubGroups,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
		},
	})
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve group projects: %w", err))
	}

	baseProjects = make([]*gitlab.BasicProject, len(projects))
	for i, proj := range projects {
		baseProjects[i] = &gitlab.BasicProject{
			ID:                proj.ID,
			Name:              proj.Name,
			NameWithNamespace: proj.NameWithNamespace,
			PathWithNamespace: proj.PathWithNamespace,
		}
	}
	jitter, err := rand.Int(rand.Reader, big.NewInt(5000))
	if err != nil {
		return nil, s.HandleError(fmt.Errorf("failed to retrieve random jitter: %w", err))
	}
	ttl := projectCacheTTL + time.Duration(jitter.Int64())*time.Millisecond
	s.cacheSet(key, baseProjects, ttl)
	return baseProjects, nil
}
