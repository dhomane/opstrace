package gatekeeper

import (
	"net/url"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	configKey = "gatekeeper/serverConfig"
)

type ConfigOptions struct {
	Port                        string
	UseSecureCookie             bool
	CookieName                  string
	CookieSecret                string
	OauthClientID               string
	OauthClientSecret           string
	ServerDomain                string
	GitlabAddr                  string
	GroupAllowedAccess          string
	GroupAllowedSystemAccess    string
	GitlabInternalEndpointAddr  string
	GitlabInternalEndpointToken string
	K8sClient                   client.Client
	Namespace                   string
}

func (c *ConfigOptions) GetHostname() string {
	p, err := url.Parse(c.ServerDomain)
	if err != nil {
		// unrecoverable, configured wrong
		log.Fatal("DOMAIN env var not a valid url")
	}
	return p.Hostname()
}

// Middleware to set the config options on the context
// for downstream handlers.
func Config(c *ConfigOptions) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(configKey, c)
	}
}

// Helper to get the server config from the context.
func GetConfig(ctx *gin.Context) *ConfigOptions {
	conf, exists := ctx.Get(configKey)
	if !exists {
		log.Fatal("must use Config() middleware")
	}
	return conf.(*ConfigOptions)
}
