package gatekeeper

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v8"
	redis "github.com/go-redis/redis/v8"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
)

const (
	cacheClientKey = "gatekeeper/redisCache"
)

type CacheOptions struct {
	RedisAddr          string
	RedisPassword      string
	ConnectionPoolSize int
	Registry           prometheus.Registerer
}

type RedisCache interface {
	Set(item *cache.Item) error
	Get(ctx context.Context, key string, value interface{}) error
}

var _ RedisCache = (*CacheMetricsWrapper)(nil)

type CacheMetricsWrapper struct {
	c *cache.Cache

	cacheGets *prometheus.CounterVec
	cacheSets *prometheus.CounterVec

	cacheGetTime prometheus.Histogram
	cacheSetTime prometheus.Histogram

	cacheHits   prometheus.Gauge
	cacheMisses prometheus.Gauge
}

func NewCacheMetricsWrapper(c *cache.Cache, registry prometheus.Registerer) RedisCache {
	res := &CacheMetricsWrapper{
		c: c,
		cacheGets: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cache_gets",
				Help: "number of Get() request to cache",
			},
			[]string{"object_type"},
		),
		cacheSets: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cache_sets",
				Help: "number of Set() request to cache",
			},
			[]string{"object_type"},
		),
		cacheGetTime: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Name:    "cache_get_time",
				Help:    "the time it took to fetch item from cache",
				Buckets: prometheus.DefBuckets,
			},
		),
		cacheSetTime: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Name:    "cache_set_time",
				Help:    "the time it took to set item in cache",
				Buckets: prometheus.DefBuckets,
			},
		),
		cacheHits: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name: "cache_hits",
				Help: "number of hits when accesing cache",
			},
		),
		cacheMisses: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name: "cache_misses",
				Help: "number of misses when accesing cache",
			},
		),
	}

	registry.MustRegister(
		res.cacheGets,
		res.cacheSets,
		res.cacheGetTime,
		res.cacheSetTime,
		res.cacheHits,
		res.cacheMisses,
	)

	return res
}

func (cwm *CacheMetricsWrapper) getObjectType(item interface{}) string {
	switch item.(type) {
	case LastNamespace:
		return "lastNamespace"
	case gitlab.GroupMember:
		return "groupMember"
	case gitlab.Namespace:
		return "namespace"
	case gitlab.User:
		return "user"
	case EmptyValue:
		return "emptyValue"
	case bool:
		return "bool"
	default:
		return "unrecognized"
	}
}

func (cwm *CacheMetricsWrapper) Set(item *cache.Item) error {
	objectType := cwm.getObjectType(item.Value)
	cwm.cacheSets.WithLabelValues(objectType).Inc()

	startTime := time.Now()
	err := cwm.c.Set(item)
	latency := time.Since(startTime)

	cwm.cacheSetTime.Observe(latency.Seconds())

	return err
}

func (cwm *CacheMetricsWrapper) Get(
	ctx context.Context,
	key string,
	value interface{},
) error {
	startTime := time.Now()
	err := cwm.c.Get(ctx, key, value)
	latency := time.Since(startTime)

	objectType := cwm.getObjectType(value)
	cwm.cacheGets.WithLabelValues(objectType).Inc()

	cwm.cacheGetTime.Observe(latency.Seconds())
	stats := cwm.c.Stats()
	cwm.cacheHits.Set(float64(stats.Hits))
	cwm.cacheMisses.Set(float64(stats.Misses))

	return err
}

// Middleware to set the database clients on the context for downstream handlers.
func Cache(client redis.Cmdable, registry prometheus.Registerer) gin.HandlerFunc {
	// Cache heavily used items with an LFU locally so subsequent resource access
	// within this instance of gatekeeper does not require any network requests.
	// Caching up to 10000 items for 10 seconds seems reasonable to start with.
	//
	// The JSON response returned by /v4/groups/:id for example
	// is ~1.16KB, so 10000 of those is around 11MB.
	//
	// After that, the cache will lookup the key in redis (great for multiple instances
	// of gatekeeper to access a common cache layer).
	// If that returns redis.NIL then finally we'll hit the gitlab API.
	c := cache.New(&cache.Options{
		Redis:        client,
		LocalCache:   cache.NewTinyLFU(10000, 10*time.Second),
		StatsEnabled: true,
	})

	cm := NewCacheMetricsWrapper(c, registry)

	return func(ctx *gin.Context) {
		ctx.Set(cacheClientKey, cm)
	}
}

// Helper to get the Redis client from the context.
func GetCache(ctx *gin.Context) RedisCache {
	client, exists := ctx.Get(cacheClientKey)
	if !exists {
		log.Fatal("must use Cache() middleware")
	}
	return client.(RedisCache)
}
