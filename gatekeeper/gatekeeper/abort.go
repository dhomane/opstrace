package gatekeeper

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var ErrUnauthorized = errors.New("unauthorized")

// Returns a 401 if the error is due to an authorization issue,
// or aborts with the error and a generic message back to the user.
func HandleError(ctx *gin.Context, err error) {
	if errors.Is(err, ErrUnauthorized) {
		Abort(ctx, 401, "unauthorized", false)
	} else {
		AbortWithError(ctx, 500, "something went wrong", err, true)
	}
}

// AbortWithError will log the full error and then call Abort.
//
// If `mask` is true, a generic response will be sent to the client to avoid leaking
// specifics about system internals.
func AbortWithError(ctx *gin.Context, status int, message string, err error, mask bool) {
	if ctx.IsAborted() {
		// No need to log cascading failures
		return
	}
	Abort(ctx, status, message, mask)
	log.Error("AbortError:", err)
}

// Abort will log the error message, return an error to the client and abort
// the request context to prevent downstream request handlers/middleware being invoked.
//
// If `mask` is true, a generic response will be sent to the client to avoid leaking
// specifics about system internals.
func Abort(ctx *gin.Context, status int, message string, mask bool) {
	if ctx.IsAborted() {
		// No need to log cascading failures
		return
	}
	log.Errorf("AbortMessage: %s", message)
	title := "Error"

	if status == http.StatusNotFound {
		title = "Not Found"
	}
	if mask {
		title = "Internal Server Error"
		message = "Something went wrong"
		status = 500
	}
	ctx.HTML(status, "error.gohtml", gin.H{"title": title, "message": message})
	ctx.Abort()
}
