# Contributing

We believe in a world where [everyone can contribute](https://about.gitlab.com/company/mission/).

Please join us and help with your contributions!

<!-- TODO: Unify into GitLab docs. -->
* Start by reading the [Contributing guide](./CONTRIBUTING.md) to become familiar with the process.
* Then review our [Development guide](./docs/guides/contributor/setting-up-your-dev-env.md) to learn how to set up your environment and build the code.

Take a look at our [high-level roadmap](./docs/references/roadmap.md) to see where we're heading.

IMPORTANT NOTE: We welcome contributions from developers of all backgrounds.
We encode that in our [Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).
By participating in this project, you agree to abide by its terms.
