#!/usr/bin/env bash

# Import helper functions.
source ci/utils.sh

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

echo "+++ deploy-testremote-teardown.sh start"

echo "running $(basename $0)"
echo "current working directory: $(pwd)"
REPO_PWD=$PWD

# From https://docs.aws.amazon.com/cli/latest/topic/config-vars.html:
# "Credentials from environment variables have precedence over credentials from
# the shared credentials and AWS CLI config file. Credentials specified in the
# shared credentials file have precedence over credentials in the AWS CLI
# config file."
# Note that this exposes these environment variables to much more processes
# than needed (a potential confusion concern more, not so much a security
# concern).
# Note: sourcing this file exports AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
source secrets/aws-dev-svc-acc-env.sh

# Set our dockerhub env vars so that opstrace create will deploy the image pull secret
# therefore authenticating with dockerhub to avoid the aggressive anonymous user rate limits
source secrets/opstrace_dockerhub_creds.sh

# Set GCP service account credentials (also used for opstrace create gcp ...)

#export GOOGLE_APPLICATION_CREDENTIALS=./secrets/gcp-credentials.json
#export OPSTRACE_GCP_PROJECT_ID="vast-pad-240918"

# Shard across GCP CI projects. `shuf` from coreutils: "shuf shuffles its input
# by outputting a random permutation of its input lines. Each output
# permutation is equally likely". Also see
# https://github.com/opstrace/opstrace/pull/128#issuecomment-742519078 and
# https://stackoverflow.com/q/5189913/145400.
#OPSTRACE_GCP_PROJECT_ID=$(shuf -n1 -e ci-shard-aaa ci-shard-bbb ci-shard-ccc)
# remove aaa, ddd, eee, fff, iii for now, see issue #293
# added ggg, hhh due to #293, see issue opstrace/private#191
OPSTRACE_GCP_PROJECT_ID=$(shuf -n1 -e ci-shard-bbb ci-shard-ccc ci-shard-ggg ci-shard-hhh)
echo "--- random choice for GCP project ID: ${OPSTRACE_GCP_PROJECT_ID}"
export GOOGLE_APPLICATION_CREDENTIALS=${REPO_PWD}/secrets/gcp-svc-acc-${OPSTRACE_GCP_PROJECT_ID}.json

# Name for deriving DNS config relating to the GitLab instance
export GITLAB_INSTANCE_NAME="gitlab-${OPSTRACE_CLUSTER_NAME}"

# The root opstraceaws.com zone ID in route53.
ROOT_OPSTRACEAWS_ZONEID=ZG5OGHVAQ79V5
AWS_CLI_REGION="us-west-2"
GCLOUD_CLI_ZONE="us-west2-a"

# `opstrace create ...` is going to write to this.
OPSTRACE_CLI_WRITE_KUBECFG_FILEPATH="${OPSTRACE_BUILD_DIR}/kubeconfig_${OPSTRACE_CLUSTER_NAME}"

echo "--- gcloud auth activate-service-account: ${GOOGLE_APPLICATION_CREDENTIALS}"
# Log in to GCP with service account credentials. Note(JP): the authentication
# state is I think stored in a well-known location in the home dir.
gcloud auth activate-service-account \
    --key-file=${GOOGLE_APPLICATION_CREDENTIALS} \
    --project ${OPSTRACE_GCP_PROJECT_ID}


start_data_collection_deployment_loop() {
    # Run this as a child process in the background. Rely on it to
    # terminate by itself.
    bash ci/data-collection-deployment-loop.sh "$OPSTRACE_CLI_WRITE_KUBECFG_FILEPATH" &
}

teardown() {
    # Ensure we're in the correct working directory
    cd $REPO_PWD
    # Store exit code of the last failed command (could be of `test-remote` or
    # anything before that), or 0 if everything before teardown succeeded.
    LAST_EXITCODE_BEFORE_TEARDOWN=$?

    echo "+++ Exit status before entering teardown(): $LAST_EXITCODE_BEFORE_TEARDOWN"

    echo "--- initiate teardown"

    # Revoke script-global errexit option.
    set +e

    # When cluster creation failed then maybe the k8s cluster was set up
    # correctly, but the deployment phase failed. In that case the command
    # `opstrace create` below fails, beaming us to right here, w/o kubectl
    # having been configured against said k8s cluster (that happens when make
    # install-gcp succeeds). Here, perform a best effort: try to connect
    # kubectl to the k8s cluster, ignoring errors (rely on +e before). Also see
    # opstrace-prelaunch/issues/865
    configure_kubectl_aws_or_gcp

    echo "--- create cluster artifacts"
    # pragmatic way for collecting controller logs and other artifacts as part
    # of teardown. do not fail teardown when a command in this script fails
    # (rely on +e before).
    source ci/create-cluster-artifacts.sh

    echo "--- destroy cluster"
    if [[ "${OPSTRACE_CLOUD_PROVIDER}" == "aws" ]]; then
        ./build/bin/opstrace destroy aws ${OPSTRACE_CLUSTER_NAME} --log-level=debug --yes
        EXITCODE_DESTROY=$?

        # The destroy command searches for the cluster in all the available AWS
        # regions and sometimes it can timeout and fail to cleanup resources
        # properly.
        if [[ "${EXITCODE_DESTROY}" -ne 0 ]]; then
            echo "--- initial destroy failed, retrying..."
            ./build/bin/opstrace destroy aws ${OPSTRACE_CLUSTER_NAME} \
                --region=${AWS_CLI_REGION} \
                --log-level=debug \
                --yes
        fi

        if [ -f upsert-changeset.json ]; then
            # Generate the changeset in json format to remove the NS entries
            # from the root opstraceaws zone
            sed 's/UPSERT/DELETE/; s/Add NS/Remove NS/' upsert-changeset.json > delete-changeset.json
            # Apply the changeset to delete the instance NS records from the
            # root opstraceaws zone.
            aws route53 change-resource-record-sets --hosted-zone-id ${ROOT_OPSTRACEAWS_ZONEID} --change-batch file://delete-changeset.json
            # Extract the instance hosted zone id and remove the /hostedzone/
            # prefix.
            HOSTED_ZONE_ID=$(cat hosted-zone.json | jq -r '.HostedZone.Id' | cut -d'/' -f3)
            # Use list-resource-record-sets to find all of the current DNS
            # entries in the hosted zone and filter out the SOA and NS records.
            # For each entry, generate a changeset in json format and save to a
            # file with the change-record-set prefix
            aws route53 list-resource-record-sets --hosted-zone-id ${HOSTED_ZONE_ID} | \
                jq --compact-output "[.ResourceRecordSets[] | select(.Type != \"NS\" and .Type != \"SOA\") | {Action:\"DELETE\", ResourceRecordSet:.}] | _nwise(1) | {Changes: .}" | split - -l 1 change-recordset-
            # Delete the DNS entries by applying the changeset in the files.
            for CHANGESET_FILE in change-recordset-*; do
                aws route53 change-resource-record-sets --hosted-zone-id ${HOSTED_ZONE_ID} --change-batch file://${CHANGESET_FILE}
            done
            # Delete the new custom hosted zone.
            aws route53 delete-hosted-zone --id ${HOSTED_ZONE_ID}
        fi
    else
        cd terraform/environments/test
        terraform destroy -auto-approve
        EXITCODE_DESTROY=$?
        cd $REPO_PWD

        # The custom_dns_name feature requires 'manual' setup of a DNS zone
        # before Opstrace instance creation, and therefore also manual cleanup.
        source ci/wipe-gcp-dns-subzone.sh

        # First, delete sub zone using the CI shard's GCP credentials
        gcloud_wipe_and_delete_dns_sub_zone "zone-${OPSTRACE_CLUSTER_NAME}"
        # Next, delete GitLab instance sub zone using the CI shard's GCP credentials
        gcloud_wipe_and_delete_dns_sub_zone "zone-${GITLAB_INSTANCE_NAME}"

        # Then temporarily switch credentials and wipe NS records from root DNS
        # zone.
        gcloud auth activate-service-account \
            --key-file="./secrets/gcp-svc-acc-dev-dns-service.json" --project "vast-pad-240918"

        gcloud_remove_ns_records_from_root_opstracegcp "${OPSTRACE_CLUSTER_NAME}.opstracegcp.com"
        gcloud_remove_ns_records_from_root_opstracegcp "${GITLAB_INSTANCE_NAME}.opstracegcp.com"
        # Revert gcloud CLI authentication state to GCP project CI shard.
        gcloud auth activate-service-account \
            --key-file=${GOOGLE_APPLICATION_CREDENTIALS} \
            --project ${OPSTRACE_GCP_PROJECT_ID}
    fi

    echo "+++ Exit status of destroy: $EXITCODE_DESTROY"

    # Collect more info to track down
    # https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1628
    if [[ "${OPSTRACE_CLOUD_PROVIDER}" == "gcp" ]]; then
        # Collect status of PVs and PVCs
        kubectl describe pv -A > ${OPSTRACE_ARTIFACT_DIR}/kubectl_describe_pv.log
        kubectl describe pvc -A > ${OPSTRACE_ARTIFACT_DIR}/kubectl_describe_pvc.log
        kubectl describe volumeattachments -A > ${OPSTRACE_ARTIFACT_DIR}/kubectl_describe_volumeattachments.log
        # Collect logs from GCP CSI driver pods
        kubectl -n kube-system logs -l k8s-app=gcp-compute-persistent-disk-csi-driver --all-containers > ${OPSTRACE_ARTIFACT_DIR}/kubectl_csi.log
    fi

    # Copy CLI log files "again" to artifact collection dir (for `destroy` log).
    # do not exit when this fails (rely on +e before).
    cp -n opstrace_cli_*log ${OPSTRACE_ARTIFACT_DIR}

    # See opstrace-prelaunch/issues/323
    # and opstrace-prelaunch/issues/1077
    # do not exit when this fails (rely on +e before).
    echo "--- invoke bucket deletion"
    if [[ "${OPSTRACE_CLOUD_PROVIDER}" == "aws" ]]; then
        python3 ci/delete-empty-ci-buckets-aws.py
    else
        python3 ci/delete-empty-ci-buckets-gcp.py
    fi
    echo "* exit code of delete-empty-ci-buckets: $?"

    if [ "${EXITCODE_DESTROY}" -ne 0 ]; then
        echo "teardown() not yet finished, destroy failed. Exit with exitcode of destroy"
        exit "${EXITCODE_DESTROY}"
    fi

    # Exit this program with the exit code of `test-remote`.
    echo "--- teardown() finished. Exit with last exitcode before entering teardown(): $LAST_EXITCODE_BEFORE_TEARDOWN"

    # echo "the N largest files and directories in pwd"
    # pwd
    # du -ha . | sort -r -h | head -n 100 || true

    exit $LAST_EXITCODE_BEFORE_TEARDOWN
}
trap "teardown" EXIT

set -o xtrace

# For debugging potential issues. `gcloud` is a moving target in our CI and
# if something fails around the gcloud CLI it's good to know exactly which
# version we ran.
gcloud --version


echo "--- file system usage after entering CI container"
df -h

echo "--- set up dns-service credentials"
# The `access.jwt` file is what the CLI is going to look for.
cp secrets/dns-service-magic-id-token-for-ci access.jwt


# CI_DATA_COLLECTION is an env var set in the Buildkite pipeline -- when set to
# "enabled", we want to send logs and metrics from the to-be-tested Opstrace
# cluster in this CI run to an aggregation Opstrace cluster. Run this function
# in the background. Do not apply further timeout / job control.
if [[ "${CI_DATA_COLLECTION}" == "enabled" ]]; then
    echo "--- setup: start_data_collection_deployment_loop"
    start_data_collection_deployment_loop &
    # Take a quick, short break before generating more log output, so that
    # the output from the first loop iteration goes into the "proper" section
    # in the build log.
    sleep 5
fi

# Define default for OPSTRACE_INSTANCE_DNS_NAME.
export OPSTRACE_INSTANCE_DNS_NAME="${OPSTRACE_CLUSTER_NAME}.opstrace.io"

# Run opstrace installer locally. The installer will deploy the controller into
# the cluster and wait until deployments are 'ready'.
echo "--- create cluster "
if [[ "${OPSTRACE_CLOUD_PROVIDER}" == "aws" ]]; then

    # Create Opstrace instance in AWS. Use custom_dns_name and
    # gitlab oauth features.
    export OPSTRACE_INSTANCE_DNS_NAME="${OPSTRACE_CLUSTER_NAME}.opstraceaws.com"

    # Add install-time parameter to Opstrace install-time configuration file.
    # The Auth0 client ID corresponds to an Auth0 app configured for our CI
    echo -e "\ncustom_dns_name: ${OPSTRACE_INSTANCE_DNS_NAME}" >> ci/cluster-config.yaml
    # rely on this custom auth0 client id to already be present in the config:

    # Create a hosted zone in Route53 with the given OPSTRACE_INSTANCE_DNS_NAME.
    # It outputs a json object which we save for further processing.
    # Use bash's '$RANDOM' to make caller reference unique.
    aws route53 create-hosted-zone --name ${OPSTRACE_INSTANCE_DNS_NAME} --caller-reference ${OPSTRACE_CLUSTER_NAME}-${RANDOM} > hosted-zone.json

    # Extract the new zone nameservers and generate the changeset in json format
    # to add them to the root opstraceaws zone.
    cat hosted-zone.json | jq -r ".DelegationSet.NameServers | { Comment: \"Add NS records for the instance ${OPSTRACE_CLUSTER_NAME}\", Changes: [{ Action: \"UPSERT\", ResourceRecordSet: { Name: \"${OPSTRACE_INSTANCE_DNS_NAME}\", Type: \"NS\", TTL: 30, ResourceRecords: map(. | {Value: .}) } }] }" > upsert-changeset.json

    # Apply the changeset and add the NS entry in the root opstraceaws zone
    # pointing to the new zone.
    aws route53 change-resource-record-sets --hosted-zone-id ${ROOT_OPSTRACEAWS_ZONEID} --change-batch file://upsert-changeset.json

    cat ci/cluster-config.yaml | ./build/bin/opstrace create aws ${OPSTRACE_CLUSTER_NAME} \
        --log-level=debug --yes \
        --write-kubeconfig-file "${OPSTRACE_CLI_WRITE_KUBECFG_FILEPATH}"

    # Context: issue opstrace-prelaunch/issues/1905.
    # Copy outfile to prebuild/preamble dir. Required by
    # `make cli-publish-to-s3`.
    FNAME="cli-aws-mutating-api-calls-${CHECKOUT_VERSION_STRING}.txt" && \
        bash ci/gen-cli-aws-mutating-api-calls-list.sh "${FNAME}" && \
        cp "${FNAME}" ${OPSTRACE_ARTIFACT_DIR}
else
    # Create Opstrace instance in GCP.

    ############# Start Opstrace DNS setup #####################
    export OPSTRACE_INSTANCE_DNS_NAME="${OPSTRACE_CLUSTER_NAME}.opstracegcp.com"

    # Create a new managed DNS zone, for <foo>.opstracegcp.com -- in the CI
    # shard GCP project.
    SUBZONE_NAME="zone-${OPSTRACE_CLUSTER_NAME}"
    gcloud dns managed-zones create "${SUBZONE_NAME}" \
        --description="zone used by CI cluster ${OPSTRACE_CLUSTER_NAME}" \
        --dns-name="${OPSTRACE_INSTANCE_DNS_NAME}." # Trailing dot is important (FQDN)

    # Get nameservers corresponding to this zone, in a space-separated list.
    SUBZONE_NAMESERVERS="$(gcloud dns managed-zones describe "${SUBZONE_NAME}" \
        --format="value(nameServers)" | sed 's/;/ /g')"
    # Example output:
    # ns-cloud-c1.googledomains.com. ns-cloud-c2.googledomains.com. ns-cloud-c3.googledomains.com. ns-cloud-c4.googledomains.com.
    echo "SUBZONE_NAMESERVERS: ${SUBZONE_NAMESERVERS}"

    # We use the GCP project CI shard service account credentials to configure
    # the DNS sub zone ${OPSTRACE_CLUSTER_NAME}.opstracegcp.com. The root DNS
    # zone (opstracegcp.com.) however can only be managed in/by one specific
    # GCP project. That is, the svc accounts for the GCP project CI shards
    # cannot change settings for this root DNS zone. Connecting the two zones
    # via NS records needs to be done with a svc account for the GCP project
    # that that manages the root zone. Hence, switch GCP credentials
    # temporarily here.
    gcloud auth activate-service-account \
        --key-file="./secrets/gcp-svc-acc-dev-dns-service.json" --project "vast-pad-240918"

    # Now add an NS record to the opstracegcp.com zone for the
    # <foo>.opstracegcp.com DNS name, pointing to the name servers that are
    # authoritative for the sub zone. Upon execution of the transaction we
    # sometimes get a 412 precondition not met, also see Context:
    # https://github.com/opstrace/opstrace/issues/1068. Probably as of the SOA
    # serial number update failing. Rebuild and retry transaction until
    # success. TODO: maybe don't retry forever.
    while true
    do
        TRFNAME="gcloud_dns_transaction_${RANDOM}"
        gcloud dns record-sets transaction start \
            --zone=root-opstracegcp --transaction-file=${TRFNAME}
        gcloud dns record-sets transaction add ${SUBZONE_NAMESERVERS} \
            --transaction-file=${TRFNAME} \
            --name=${OPSTRACE_INSTANCE_DNS_NAME}. \
            --ttl=300 --type=NS --zone=root-opstracegcp
        echo "transaction file content:"
        cat ${TRFNAME}
        # temporarily lift errexit option, this is where the `412 precondition
        # not met` err may be thrown.
        set +e
        gcloud dns record-sets transaction execute --zone=root-opstracegcp --transaction-file=${TRFNAME}
        EXITCODE_DNS_TRANS=$?
        set -e

        if [ "${EXITCODE_DNS_TRANS}" -ne 0 ]; then
            echo "gcloud dns ... transaction execute failed.. retry in 10 s "
            sleep 10
        else
            echo "EXITCODE_DNS_TRANS is 0 -> transaction was accepted, leave loop"
            break
        fi
    done

    # Revert gcloud CLI authentication state to GCP project CI shard.
    gcloud auth activate-service-account \
        --key-file=${GOOGLE_APPLICATION_CREDENTIALS} \
        --project ${OPSTRACE_GCP_PROJECT_ID}

    ############# End Opstrace DNS setup #####################

    ############# Start GitLab DNS setup #####################
    # NOTE: this block and the above block should be dried up
    export GITLAB_INSTANCE_DNS_NAME="${GITLAB_INSTANCE_NAME}.opstracegcp.com"

    # Create a new managed DNS zone, for <foo>.opstracegcp.com -- in the CI
    # shard GCP project.
    SUBZONE_NAME="zone-${GITLAB_INSTANCE_NAME}"
    gcloud dns managed-zones create "${SUBZONE_NAME}" \
        --description="zone used by CI cluster ${GITLAB_INSTANCE_NAME}" \
        --dns-name="${GITLAB_INSTANCE_DNS_NAME}." # Trailing dot is important (FQDN)

    # Get nameservers corresponding to this zone, in a space-separated list.
    SUBZONE_NAMESERVERS="$(gcloud dns managed-zones describe "${SUBZONE_NAME}" \
        --format="value(nameServers)" | sed 's/;/ /g')"
    # Example output:
    # ns-cloud-c1.googledomains.com. ns-cloud-c2.googledomains.com. ns-cloud-c3.googledomains.com. ns-cloud-c4.googledomains.com.
    echo "SUBZONE_NAMESERVERS: ${SUBZONE_NAMESERVERS}"

    # We use the GCP project CI shard service account credentials to configure
    # the DNS sub zone ${GITLAB_INSTANCE_NAME}.opstracegcp.com. The root DNS
    # zone (opstracegcp.com.) however can only be managed in/by one specific
    # GCP project. That is, the svc accounts for the GCP project CI shards
    # cannot change settings for this root DNS zone. Connecting the two zones
    # via NS records needs to be done with a svc account for the GCP project
    # that that manages the root zone. Hence, switch GCP credentials
    # temporarily here.
    gcloud auth activate-service-account \
        --key-file="./secrets/gcp-svc-acc-dev-dns-service.json" --project "vast-pad-240918"

    # Now add an NS record to the opstracegcp.com zone for the
    # <foo>.opstracegcp.com DNS name, pointing to the name servers that are
    # authoritative for the sub zone. Upon execution of the transaction we
    # sometimes get a 412 precondition not met, also see Context:
    # https://github.com/opstrace/opstrace/issues/1068. Probably as of the SOA
    # serial number update failing. Rebuild and retry transaction until
    # success. TODO: maybe don't retry forever.
    while true
    do
        TRFNAME="gcloud_dns_transaction_${RANDOM}"
        gcloud dns record-sets transaction start \
            --zone=root-opstracegcp --transaction-file=${TRFNAME}
        gcloud dns record-sets transaction add ${SUBZONE_NAMESERVERS} \
            --transaction-file=${TRFNAME} \
            --name=${GITLAB_INSTANCE_DNS_NAME}. \
            --ttl=300 --type=NS --zone=root-opstracegcp
        echo "transaction file content:"
        cat ${TRFNAME}
        # temporarily lift errexit option, this is where the `412 precondition
        # not met` err may be thrown.
        set +e
        gcloud dns record-sets transaction execute --zone=root-opstracegcp --transaction-file=${TRFNAME}
        EXITCODE_DNS_TRANS=$?
        set -e

        if [ "${EXITCODE_DNS_TRANS}" -ne 0 ]; then
            echo "gcloud dns ... transaction execute failed.. retry in 10 s "
            sleep 10
        else
            echo "EXITCODE_DNS_TRANS is 0 -> transaction was accepted, leave loop"
            break
        fi
    done

    # Revert gcloud CLI authentication state to GCP project CI shard.
    gcloud auth activate-service-account \
        --key-file=${GOOGLE_APPLICATION_CREDENTIALS} \
        --project ${OPSTRACE_GCP_PROJECT_ID}

    # This var is set in the CI/CD settings. Just putting it here for completeness
    export TF_VAR_registry_auth_token="${TF_VAR_registry_auth_token}"

    cd terraform/environments/test
    echo "--- teraform init"
    terraform init
    echo "--- teraform apply. Output is saved in the terraform_apply.log job artifact (https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)"
    terraform apply -auto-approve -var-file=terraform.tfvars \
        -var kubeconfig_path="${OPSTRACE_CLI_WRITE_KUBECFG_FILEPATH}" \
        -var instance_name="${OPSTRACE_CLUSTER_NAME}" \
        -var project_id="${OPSTRACE_GCP_PROJECT_ID}" \
        -var domain="${OPSTRACE_INSTANCE_DNS_NAME}" \
        -var gitlab_domain="${GITLAB_INSTANCE_DNS_NAME}" \
        -var cloud_dns_zone="${SUBZONE_NAME}" |& tee -a "${OPSTRACE_ARTIFACT_DIR}/terraform_apply.log" >/dev/null

    cd $REPO_PWD

fi

echo "--- connect kubectl to the CI cluster"
configure_kubectl_aws_or_gcp

echo "--- wait for Opstrace Cluster to be ready"
kubectl wait --for=condition=ready cluster/${OPSTRACE_CLUSTER_NAME} --timeout=20m

# Makefile logic uses `OPSTRACE_KUBECFG_FILEPATH_ONHOST` to mount kubectl config into
# the test-remote container. Point to the file written by the Opstrace CLI.
export OPSTRACE_KUBECFG_FILEPATH_ONHOST="${OPSTRACE_CLI_WRITE_KUBECFG_FILEPATH}"

# TODO: remove when we add cloud provider managed certificates and remove the
# use of insecure_skip_verify in the tests
echo "--- checking cluster is using certificate issued by LetsEncrypt"

retry_check_certificate ${OPSTRACE_INSTANCE_DNS_NAME}:443/v1/provision/1

# Basic e2e test that checks:
# - if Opstrace was deployed correctly (i.e endpoint works)
# - follows redirect to auth UI on connected GitLab
FOUND=$(curl -L https://${OPSTRACE_INSTANCE_DNS_NAME}/-/1 | grep '<title>Sign in · GitLab</title>')
if ["${FOUND}" -eq "0"]; then
    echo "ERROR: failed to reach GitLab Sign in page when attempting to access Opstrace"
    exit 1
fi
# This runs the bulk of the tests against the Opstrace instance, also invoked
# from the upgrade test pipeline and therefore in its own file.
# TODO: reenable once tests have been reworked
# source ci/test-core.sh

# One child process was spawned (see start_data_collection_deployment_loop()).
# Be a good citizen and join that explicitly (expect that to have terminated by
# now, long ago).
wait
