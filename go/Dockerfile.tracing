FROM --platform=${BUILDPLATFORM} golang:1.19 AS build-env
ENV CGO_ENABLED=0
ENV GOPATH=/go

# Prepare and enter src directory
WORKDIR /go/src/gitlab.com/gitlab-org/opstrace/opstrace/go/

# Cache dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download -x

# Add the sources and proceed with build
COPY cmd/tracing cmd/tracing
COPY pkg/ pkg/

ARG GO_BUILD_LDFLAGS
ARG TARGETARCH
ARG TARGETOS
# Call go build directly instead of make target because otherwise we have to
# also pull in Makefile.shared from the parent directory and use a wider docker
# build context. Using `bash -c` format to avoid quoting issues.
RUN ["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -o tracing-api ./cmd/tracing/"]

FROM scratch
COPY --from=build-env /go/src/gitlab.com/gitlab-org/opstrace/opstrace/go/tracing-api /
ENTRYPOINT ["/tracing-api"]
