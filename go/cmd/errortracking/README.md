# An Open API based rest API for GitLab's Error Tracking

## Generation

To generate the code required for this application you need:

* [go-swagger](https://goswagger.io/)



```bash
> cd opstrace/go/pkg/errortracking
> swagger generate server -t gen -f ./swagger.yaml --exclude-main -A ErrorTracking
```
