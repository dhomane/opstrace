package main

import (
	"fmt"
	"net/http"

	//nolint:gosec
	_ "net/http/pprof"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/go-openapi/loads"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

const (
	ErrorTrackingQueueName    string = "errortracking"
	ErrortrackingQueueDirPath string = "/etc/errortracking/queue"
)

var (
	port                   = getEnvInt("PORT", 8080)
	metricsPort            = getEnvInt("METRICS_PORT", 8081)
	clickHouseDsn          = getEnv("CLICKHOUSE_DSN", "tcp://localhost:9000")
	apiBaseURL             = getEnv("API_BASE_URL", "")
	clickhouseMaxOpenConns = getEnvInt("CLICKHOUSE_MAX_OPEN_CONNS", 100)
	clickhouseMaxIdleConns = getEnvInt("CLICKHOUSE_MAX_IDLE_CONNS", 20)
	// TODO: Adjust error tracking API deployment to override log level from cluster spec.
	logLevel = getEnv("LOG_LEVEL", "debug")

	gatekeeperURL = getEnv("GATEKEEPER_URL", "https://gatekeeper.svc.cluster.local")

	debugEnabled               = getEnvBool("DEBUG_ENABLED", false)
	useRemoteStorage           = getEnv("USE_REMOTE_STORAGE", "")
	dbCompressionEnabled       = getEnvBool("DB_USE_COMPRESSION", false)
	clientSideBufferingEnabled = getEnvBool("CLIENT_SIDE_BUFFERING_ENABLED", false)
	maxBatchSize               = getEnvInt("MAX_BATCH_SIZE", 100)
	maxProcessorCount          = getEnvInt("MAX_PROCESSOR_COUNT", 1)
)

type DebugServer struct {
	*http.Server
}

func NewDebugServer(addr string) *DebugServer {
	return &DebugServer{
		//nolint:gosec
		&http.Server{
			Addr:    addr,
			Handler: http.DefaultServeMux,
		},
	}
}

func getEnvBool(key string, fallback bool) bool {
	if _, ok := os.LookupEnv(key); ok {
		if v, err := strconv.ParseBool(os.Getenv(key)); err == nil {
			return v
		}
	}
	return fallback
}

func getEnvInt(key string, fallback int) int {
	s := os.Getenv(key)
	if len(s) == 0 {
		return fallback
	}

	v, err := strconv.Atoi(s)
	if err != nil {
		return fallback
	}

	return v
}

func getEnv(key string, fallback string) string {
	s := os.Getenv(key)
	if s == "" {
		return fallback
	}
	return s
}

func main() {
	level, err := log.ParseLevel(logLevel)
	if err != nil {
		log.Fatalf("failed to parse %v as log level", logLevel)
	}
	log.SetLevel(level)
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
	})

	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.WithError(err).Fatal("failed to load embedded swagger")
	}

	if clickHouseDsn == "" {
		log.Fatal("CLICKHOUSE_DSN env variable not set")
	}
	if apiBaseURL == "" {
		log.Fatal("API_BASE_URL env variable not set")
	}
	if gatekeeperURL == "" {
		log.Fatal("GATEKEEPER_URL env variable not set")
	}

	baseURL, err := url.Parse(apiBaseURL)
	if err != nil {
		log.WithError(err).Fatal("invalid base domain url")
	}

	gkURL, err := url.Parse(gatekeeperURL)
	if err != nil {
		log.WithError(err).Fatal("invalid gatekeeper URL")
	}

	opts := &errortracking.DatabaseOptions{
		MaxOpenConns:     clickhouseMaxOpenConns,
		MaxIdleConns:     clickhouseMaxIdleConns,
		UseCompression:   dbCompressionEnabled,
		UseRemoteStorage: useRemoteStorage,
	}

	db, err := errortracking.NewDB(clickHouseDsn, opts)
	if err != nil {
		log.WithError(err).Fatal("failed to set up clickhouse")
	}

	var queue *persistentqueue.Queue
	if clientSideBufferingEnabled {
		queue, err = persistentqueue.NewQueue(
			persistentqueue.WithQueueName(ErrorTrackingQueueName),
			persistentqueue.WithDirPath(ErrortrackingQueueDirPath),
			persistentqueue.WithBatchSize(maxBatchSize),
			// processor is configured when errortracking controller is contructed
			persistentqueue.WithProcessorCount(maxProcessorCount),
		)
		if err != nil {
			log.WithError(err).Fatal("failed to setup buffer")
		}
	}

	ctrl := errortracking.NewController(baseURL, db, gkURL, queue)

	api := operations.NewErrorTrackingAPI(swaggerSpec)
	// Use logrus for logging.
	api.Logger = log.Infof

	// Set up the ingestion endpoints at the
	// /projects/{projectId}/(store|envelope) routes.
	api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(
		ctrl.PostEnvelopeHandler,
	)
	api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(
		ctrl.PostStoreHandler,
	)

	// Set up the /projects/{projectId}/errors route that returns a list of
	// errors.
	api.ErrorsListErrorsHandler = errors.ListErrorsHandlerFunc(
		ctrl.ListErrors,
	)

	// Set up the GET /projects/{projectId}/errors/{fingerprint} route handler
	// that returns information about an error.
	api.ErrorsGetErrorHandler = errors.GetErrorHandlerFunc(
		ctrl.GetError,
	)

	// Set up the PUT /projects/{projectId}/errors/{fingerprint} route handler
	// that updates information about an error.
	api.ErrorsUpdateErrorHandler = errors.UpdateErrorHandlerFunc(
		ctrl.UpdateError,
	)

	// Set up the GET /projects/{projectID}/errors/{fingerprint}/events router
	// handle that returns information about the events related to an error.
	api.ErrorsListEventsHandler = errors.ListEventsHandlerFunc(
		ctrl.ListEvents,
	)

	// Set up the DELETE /projects/{projectId} route handle that deletes errors
	// from the given project.
	api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(
		ctrl.DeleteProject,
	)

	api.ErrorsV2ListErrorsV2Handler = errors_v2.ListErrorsV2HandlerFunc(
		ctrl.ListErrorsV2,
	)

	api.ErrorsV2ListProjectsHandler = errors_v2.ListProjectsHandlerFunc(
		ctrl.ListProjects,
	)

	api.MessagesListMessagesHandler = messages.ListMessagesHandlerFunc(
		ctrl.ListMessages,
	)

	// Set up base metrics before we instrument the middleware
	prometheus.MustRegister(metrics.ConfiguredCollectors()...)
	// Set up per-project metrics
	prometheus.MustRegister(errortracking.ConfiguredCollectors()...)

	server := restapi.NewServer(api)
	server.ConfigureAPI()
	//nolint:errcheck
	defer server.Shutdown()

	server.Port = port
	server.EnabledListeners = []string{"http"}

	// Set up metrics server
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	go func() {
		metricsAddr := fmt.Sprintf("0.0.0.0:%d", metricsPort)
		log.Debugf("metrics server starting at %s", metricsAddr)
		srv := &http.Server{
			Addr:              metricsAddr,
			Handler:           mux,
			ReadHeaderTimeout: time.Second * 3,
		}
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.WithError(err).Error("unable to start metric server")
			}
		}
	}()

	// Set up debug server
	if debugEnabled {
		debugServer := NewDebugServer("localhost:8082")
		go func() {
			if err := debugServer.ListenAndServe(); err != nil {
				log.WithError(err).Error("unable to start debug server")
			}
		}()
	}

	if err := server.Serve(); err != nil {
		log.WithError(err).Fatal("server failed")
	}
}
