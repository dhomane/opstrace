package main

import (
	"fmt"
	"log"

	// There's still a jaegerexporter in the stock/non-contrib collector
	// at "go.opentelemetry.io/collector/exporter/jaegerexporter",
	// however it hasn't had changes as recently.
	"github.com/open-telemetry/opentelemetry-collector-contrib/exporter/jaegerexporter"
	"github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension"
	"github.com/open-telemetry/opentelemetry-collector-contrib/processor/tailsamplingprocessor"
	"github.com/open-telemetry/opentelemetry-collector-contrib/receiver/jaegerreceiver"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter/loggingexporter"
	"go.opentelemetry.io/collector/extension/ballastextension"
	"go.opentelemetry.io/collector/extension/zpagesextension"
	"go.opentelemetry.io/collector/processor/batchprocessor"
	"go.opentelemetry.io/collector/processor/memorylimiterprocessor"
	"go.opentelemetry.io/collector/receiver/otlpreceiver"
	"go.opentelemetry.io/collector/service"
)

// Components returns the enabled components in our collector.
func Components() (component.Factories, error) {
	extensions, err := component.MakeExtensionFactoryMap(
		ballastextension.NewFactory(), // for performance optimization via memory pool
		pprofextension.NewFactory(),   // for in-process debugging if needed
		zpagesextension.NewFactory(),  // for in-process debugging if needed
	)
	if err != nil {
		return component.Factories{}, fmt.Errorf("failed to make extension factory %w", err)
	}

	receivers, err := component.MakeReceiverFactoryMap(
		otlpreceiver.NewFactory(),
		jaegerreceiver.NewFactory(),
	)
	if err != nil {
		return component.Factories{}, fmt.Errorf("failed to make receiver factory %w", err)
	}

	// see also recommendations here:
	// https://github.com/open-telemetry/opentelemetry-collector/tree/main/processor#recommended-processors
	processors, err := component.MakeProcessorFactoryMap(
		memorylimiterprocessor.NewFactory(),
		tailsamplingprocessor.NewFactory(),
		batchprocessor.NewFactory(),
	)
	if err != nil {
		return component.Factories{}, fmt.Errorf("failed to make processor factory %w", err)
	}

	exporters, err := component.MakeExporterFactoryMap(
		jaegerexporter.NewFactory(),
		loggingexporter.NewFactory(), // optional, for debugging
	)
	if err != nil {
		return component.Factories{}, fmt.Errorf("failed to make exporter factory %w", err)
	}

	return component.Factories{
		Extensions: extensions,
		Receivers:  receivers,
		Processors: processors,
		Exporters:  exporters,
	}, nil
}

func main() {
	factories, err := Components()
	if err != nil {
		log.Fatalf("failed to build components: %v", err)
	}

	info := component.BuildInfo{
		Command:     "otelcol-opstrace",
		Description: "OpenTelemetry Collector + Opstrace auth",
		Version:     "",
	}

	if err = runInteractive(service.CollectorSettings{BuildInfo: info, Factories: factories}); err != nil {
		log.Fatal(err)
	}
}

func runInteractive(params service.CollectorSettings) error {
	cmd := service.NewCommand(params)
	if err := cmd.Execute(); err != nil {
		return fmt.Errorf("collector server run finished with error: %w", err)
	}

	return nil
}
