package argusapi

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// GroupUser represents a Grafana group user.
type GroupUser struct {
	GroupID int64  `json:"groupId"`
	UserID  int64  `json:"userId"`
	Email   string `json:"email"`
	Login   string `json:"login"`
	Role    string `json:"role"`
}

// GroupUsersCurrent returns all group users within the current groupanization.
// This endpoint is accessible to users with group admin role.
func (c *Client) GroupUsersCurrent() ([]GroupUser, error) {
	users := make([]GroupUser, 0)
	err := c.request("GET", "/api/group/users", nil, nil, &users)
	if err != nil {
		return nil, err
	}
	return users, err
}

// GroupUsers fetches and returns the users for the group whose ID it's passed.
func (c *Client) GroupUsers(groupID int64) ([]GroupUser, error) {
	users := make([]GroupUser, 0)
	err := c.request("GET", fmt.Sprintf("/api/groups/%d/users", groupID), nil, nil, &users)
	if err != nil {
		return users, err
	}

	return users, err
}

// AddGroupUser adds a user to an group with the specified role.
func (c *Client) AddGroupUser(groupID int64, user, role string) error {
	dataMap := map[string]string{
		"loginOrEmail": user,
		"role":         role,
	}
	data, err := json.Marshal(dataMap)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		//nolint:wrapcheck
		return err
	}

	return c.request("POST", fmt.Sprintf("/api/groups/%d/users", groupID), nil, bytes.NewBuffer(data), nil)
}

// UpdateGroupUser updates and group user.
func (c *Client) UpdateGroupUser(groupID, userID int64, role string) error {
	dataMap := map[string]string{
		"role": role,
	}
	data, err := json.Marshal(dataMap)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		//nolint:wrapcheck
		return err
	}

	return c.request("PATCH", fmt.Sprintf("/api/groups/%d/users/%d", groupID, userID), nil, bytes.NewBuffer(data), nil)
}

// RemoveGroupUser removes a user from an group.
func (c *Client) RemoveGroupUser(groupID, userID int64) error {
	return c.request("DELETE", fmt.Sprintf("/api/groups/%d/users/%d", groupID, userID), nil, nil, nil)
}
