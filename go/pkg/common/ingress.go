package common

import (
	"fmt"
)

func GetAuthIngressAnnotations(groupID, gatekeeperURL, domain string) map[string]string {
	return map[string]string{
		"external-dns.alpha.kubernetes.io/ttl": "30",
		"nginx.ingress.kubernetes.io/auth-url": fmt.Sprintf(
			"%s/v1/auth/webhook/%s?uri=$escaped_request_uri", gatekeeperURL, groupID,
		),
		"nginx.ingress.kubernetes.io/auth-signin":           fmt.Sprintf("%s/v1/auth/start?rt=$escaped_request_uri", domain),
		"nginx.ingress.kubernetes.io/auth-response-headers": "X-WEBAUTH-EMAIL, X-GRAFANA-ORG-ID",
		// Enable CORS explicitly on all data APIs
		"nginx.ingress.kubernetes.io/enable-cors":        "true",
		"nginx.ingress.kubernetes.io/cors-allow-origin":  "*",
		"nginx.ingress.kubernetes.io/cors-allow-methods": "PUT, GET, POST, OPTIONS, DELETE",
		// Disallow credentials to protect against CSRF
		"nginx.ingress.kubernetes.io/cors-allow-credentials": "false",
		// Cache the auth response to avoid querying the auth endpoint too frequently.
		// "nginx.ingress.kubernetes.io/auth-cache-key": "$remote_user$http_authorization",
		// 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
		// 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
		// 50x: If Argus is returning 500 errors, allow them to clear up quickly when Argus comes back,
		//      but allow some caching to avoid Argus getting hammered with retries when down.
		// "nginx.ingress.kubernetes.io/auth-cache-duration": "401 5m, 200 202 2m, 500 503 30s",
	}
}

// Specific to Argus ingress where we use a different webhook endpoint.
func GetArgusAuthIngressAnnotations(topLevelNamespaceID, gatekeeperURL, domain string) map[string]string {
	return map[string]string{
		"external-dns.alpha.kubernetes.io/ttl": "30",
		"nginx.ingress.kubernetes.io/auth-url": fmt.Sprintf(
			"%s/v1/auth/argus/webhook/%s?uri=$escaped_request_uri", gatekeeperURL, topLevelNamespaceID,
		),
		"nginx.ingress.kubernetes.io/auth-signin":           fmt.Sprintf("%s/v1/auth/start?rt=$escaped_request_uri", domain),
		"nginx.ingress.kubernetes.io/auth-response-headers": "X-WEBAUTH-EMAIL, X-GRAFANA-ORG-ID",
		// Cache the auth response to avoid querying the auth endpoint too frequently.
		// "nginx.ingress.kubernetes.io/auth-cache-key": "$remote_user$http_authorization",
		// 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
		// 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
		// 50x: If Argus is returning 500 errors, allow them to clear up quickly when Argus comes back,
		//      but allow some caching to avoid Argus getting hammered with retries when down.
		// "nginx.ingress.kubernetes.io/auth-cache-duration": "401 5m, 200 202 2m, 500 503 30s",
	}
}
