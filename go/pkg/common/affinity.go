package common

import (
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func WithPodAntiAffinity(selector v1.LabelSelector, affinity *corev1.Affinity) *corev1.Affinity {
	podAntiAffinityTerm := corev1.WeightedPodAffinityTerm{
		PodAffinityTerm: corev1.PodAffinityTerm{
			LabelSelector: &selector,
			TopologyKey:   "kubernetes.io/hostname",
		},
		//
		// https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
		//
		// "The weight field in preferredDuringSchedulingIgnoredDuringExecution
		// is in the range 1-100. For each node that meets all of the scheduling
		// requirements (resource request, RequiredDuringScheduling affinity
		// expressions, etc.), the scheduler will compute a sum by iterating
		// through the elements of this field and adding "weight" to the sum if
		// the node matches the corresponding MatchExpressions. This score is
		// then combined with the scores of other priority functions for the
		// node. The node(s) with the highest total score are the most
		// preferred."
		//
		// Give the maximum weight to this rule to prevent the pod from being
		// scheduled to a node already running a pod for the given
		// deployment.
		Weight: 100,
	}

	if affinity == nil {
		return &corev1.Affinity{
			PodAntiAffinity: &corev1.PodAntiAffinity{
				PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
					podAntiAffinityTerm,
				},
			},
		}
	}

	if affinity.PodAntiAffinity == nil {
		affinity.PodAntiAffinity = &corev1.PodAntiAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{},
		}
	}

	affinity.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution =
		append(
			affinity.PodAntiAffinity.PreferredDuringSchedulingIgnoredDuringExecution,
			podAntiAffinityTerm,
		)
	return affinity
}
