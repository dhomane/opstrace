package errortracking

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type SentryExceptionManager struct {
	i Ingester
}

func (se *SentryExceptionManager) Validate(e *types.Event) error {
	err := e.ValidateException()
	if err != nil {
		return fmt.Errorf("error in SentryExceptionManager exception validation: %w", err)
	}
	return nil
}

func (se *SentryExceptionManager) InsertEvent(projectID uint64, payloadType int, e *types.Event, payload []byte) error {
	err := se.i.IngestEventData(projectID, payloadType, payload, e)
	if err != nil {
		return fmt.Errorf("error in SentryExceptionManager inserting exception: %w", err)
	}
	return nil
}
