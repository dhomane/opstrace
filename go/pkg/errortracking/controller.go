package errortracking

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"time"

	"github.com/go-openapi/runtime/middleware"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

var (
	// Note: This is a temporary measure to get insights on per-project usage. It is better suited to be tracked with
	// application telemetry like traces or product intelligence pipelines.
	// These counters are optimal only upto 100,000 series values.
	projectStoreCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_store_total",
			Help: "number of store requests by project ID",
		},
		[]string{"projectID"},
	)
	projectEnvelopeCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_envelope_total",
			Help: "number of envelope requests by project ID",
		},
		[]string{"projectID"},
	)
	projectListErrorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_list_errors_total",
			Help: "number of list error requests by project ID",
		},
		[]string{"projectID"},
	)
	projectGetErrorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_get_error_total",
			Help: "number of get error requests by project ID",
		},
		[]string{"projectID"},
	)
	projectUpdateErrorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_update_error_total",
			Help: "number of update error requests by project ID",
		},
		[]string{"projectID"},
	)
	projectListEventCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "project_list_events_total",
			Help: "number of list events requests by project ID",
		},
		[]string{"projectID"},
	)
	eventPayloadSizeHist = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "event_payload_size_bytes",
			Help:    "size of the received event payload in bytes",
			Buckets: []float64{512, 1024, 2048, 4096, 8192},
		},
		[]string{"type"},
	)
)

func ConfiguredCollectors() []prometheus.Collector {
	return []prometheus.Collector{
		projectStoreCounter,
		projectEnvelopeCounter,
		projectListErrorCounter,
		projectGetErrorCounter,
		projectUpdateErrorCounter,
		projectListEventCounter,
		eventPayloadSizeHist,
	}
}

type Controller struct {
	db            Database
	baseURL       *url.URL
	gateKeeperURL *url.URL
	httpClient    *http.Client
	ingester      Ingester
}

func NewController(
	baseURL *url.URL,
	db Database,
	gatekeeperURL *url.URL,
	queue *persistentqueue.Queue,
) *Controller {
	c := &Controller{
		baseURL:       baseURL,
		db:            db,
		gateKeeperURL: gatekeeperURL,
		httpClient: &http.Client{
			Timeout: time.Second * 3,
		},
	}

	// setup ingester
	if queue != nil {
		// setup queue processor
		queue.SetProcessor(c.QueueProcessor)
		c.ingester = NewQueueIngester(queue)
	} else {
		c.ingester = NewClickhouseIngester(db)
	}
	return c
}

func readCompressedBody(req *http.Request) ([]byte, error) {
	r, err := gzip.NewReader(req.Body)
	if err != nil {
		// TODO: Fix error check here to be wrapped before returning
		//nolint:wrapcheck
		return nil, err
	}
	defer r.Close()
	//nolint:wrapcheck
	return io.ReadAll(r)
}

func readBody(req *http.Request) ([]byte, error) {
	// Parse the http request body into a string and unmarshall it manually.
	defer req.Body.Close()
	bts, err := io.ReadAll(req.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %w", err)
	}
	return bts, nil
}

func isGzipEncoded(req *http.Request) bool {
	return req.Header.Get("Content-Encoding") == "gzip"
}

func uncompressPayload(payload []byte) ([]byte, error) {
	raw, err := base64.StdEncoding.DecodeString(string(payload))
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 payload: %w", err)
	}

	r := bytes.NewReader(raw)
	zr, err := zlib.NewReader(r)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}

	bts, err := io.ReadAll(zr)
	if err != nil {
		return nil, fmt.Errorf("failed to read compressed payload: %w", err)
	}
	return bts, nil
}

// PostEnvelopeHandler handles the POST request sent to the
// /projects/{projectID}/envelope route.
func (c *Controller) PostEnvelopeHandler(
	params events.PostProjectsAPIProjectIDEnvelopeParams,
) middleware.Responder {
	var err error
	var payload []byte
	projectEnvelopeCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	if isGzipEncoded(params.HTTPRequest) {
		payload, err = readCompressedBody(params.HTTPRequest)
	} else {
		payload, err = readBody(params.HTTPRequest)
	}

	if err != nil {
		log.WithError(err).Error("failed to read envelope body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	e, err := types.NewEnvelopeFrom(payload)
	if err != nil {
		log.WithError(err).Error("failed to parse envelope payload")
		if log.IsLevelEnabled(log.DebugLevel) {
			//nolint:errcheck
			bts, _ := httputil.DumpRequest(params.HTTPRequest, false)
			fmt.Println(string(bts))
			fmt.Println(string(payload))
		}
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// Data model for sentry envelope can have multiple item types.
	// See https://develop.sentry.dev/sdk/envelopes/#data-model
	// We currently accept only event[https://develop.sentry.dev/sdk/envelopes/#event]
	// envelopes (exceptions and messages) and sessions.
	// Transaction[https://develop.sentry.dev/sdk/envelopes/#transaction] envelopes were a noop in Rails implementation.
	// See details of transaction payload[https://develop.sentry.dev/sdk/event-payloads/transaction/] which can include
	// an exception, but we don't support it yet.
	if !supportedEnvelopedType(e.Type.Type) {
		log.WithField("type", e.Type.Type).Error("envelope type not supported")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	switch e.Type.Type {
	case types.EventType:
		return c.processEnvelopeEvent(e, payload, params.ProjectID)
	case types.SessionType:
		return c.processEnvelopeSession(e, payload, params.ProjectID)
	default:
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}
}

func (c *Controller) processEnvelopeEvent(e *types.Envelope, payload []byte, projectID uint64) middleware.Responder {
	if e.Type.Type != types.EventType && e.Event == nil {
		log.Error("Event type is nil or type is not event")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	dataItemType := e.Event.ExtractDataItemType()
	if dataItemType == types.UnsupportedDataItem {
		log.Error("failed to process unknown data item type")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	eventHandler, err := GetEventManager(dataItemType, c.ingester)
	if err != nil {
		log.WithError(err).Error("failed to process unknown data item type")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	err = eventHandler.Validate(e.Event)
	if err != nil {
		log.WithError(err).Error("failed to validate event body")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	// observe the size of the received payload
	eventPayloadSizeHist.WithLabelValues(
		"envelope",
	).Observe(float64(len(payload)))

	err = eventHandler.InsertEvent(projectID, PayloadTypeEnvelope, e.Event, payload)
	if err != nil {
		return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
	}

	return events.NewPostProjectsAPIProjectIDEnvelopeOK()
}

func (c *Controller) processEnvelopeSession(e *types.Envelope, payload []byte, projectID uint64) middleware.Responder {
	if err := e.Session.Validate(); err != nil {
		log.WithError(err).Error("failed to validate session")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	// observe the size of the received payload
	eventPayloadSizeHist.WithLabelValues(
		"envelope",
	).Observe(float64(len(payload)))

	if err := c.ingester.IngestSessionData(projectID, PayloadTypeEnvelope, payload, e.Session); err != nil {
		log.WithError(err).Error("failed to ingest session")
		return events.NewPostProjectsAPIProjectIDEnvelopeInternalServerError()
	}

	return events.NewPostProjectsAPIProjectIDEnvelopeOK()
}

// Use a map for finding keys fast.
var supportedTypes = map[string]uint8{
	types.EventType:   uint8(1),
	types.SessionType: uint8(2),
}

func supportedEnvelopedType(envelopeType string) bool {
	_, ok := supportedTypes[envelopeType]
	return ok
}

func (c *Controller) PostStoreHandler(
	params events.PostProjectsAPIProjectIDStoreParams,
) middleware.Responder {
	var err error
	var payload []byte
	projectStoreCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	if isGzipEncoded(params.HTTPRequest) {
		payload, err = readCompressedBody(params.HTTPRequest)
	} else {
		payload, err = readBody(params.HTTPRequest)
	}

	if err != nil {
		log.WithError(err).Error("failed to read store body")
		return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
	}

	// Sentry client SDK weird behavior alert!
	//
	// Some client sdks set the content-type to x-sentry-envelope. But this
	// might or might not be a json string. When it's not a json object it comes
	// as base64 encoded blob of the zlib compressed json string.
	if !json.Valid(payload) {
		payload, err = uncompressPayload(payload)
		if err != nil {
			log.WithError(err).Error("failed to uncompress store body")
			return events.NewPostProjectsAPIProjectIDEnvelopeBadRequest()
		}
	}

	e, err := types.NewEventFrom(payload)
	if err != nil {
		log.WithError(err).Error("failed to parse store payload")
		if log.IsLevelEnabled(log.DebugLevel) {
			//nolint:errcheck
			bts, _ := httputil.DumpRequest(params.HTTPRequest, false)
			fmt.Println(string(bts))
			fmt.Println(string(payload))
		}
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	// A list of data items can be found on https://develop.sentry.dev/sdk/envelopes/#data-model
	dataItemType := e.ExtractDataItemType()
	if dataItemType == types.UnsupportedDataItem {
		log.WithError(err).Error("failed to process unknown data item type")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	eventHandler, err := GetEventManager(dataItemType, c.ingester)
	if err != nil {
		log.WithError(err).Error("failed to process unknown data item type")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	err = eventHandler.Validate(e)
	if err != nil {
		log.WithError(err).Error("failed to validate event body")
		return events.NewPostProjectsAPIProjectIDStoreBadRequest()
	}

	// observe the size of the received payload
	eventPayloadSizeHist.WithLabelValues(
		"store",
	).Observe(float64(len(payload)))

	err = eventHandler.InsertEvent(params.ProjectID, PayloadTypeEvent, e, payload)
	if err != nil {
		return events.NewPostProjectsAPIProjectIDStoreInternalServerError()
	}
	return events.NewPostProjectsAPIProjectIDStoreOK()
}

func (c *Controller) ListErrors(params errors.ListErrorsParams) middleware.Responder {
	projectListErrorCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("status", *params.Status).
		WithField("limit", *params.Limit)

	if params.Query != nil {
		logger = logger.WithField("query", *params.Query)
	}

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListErrors(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListErrorsOK()
	resp.Payload = res
	link, err := buildListErrorsLink(c.baseURL, params)
	if err != nil {
		log.WithError(err).Error("failed to build link for listing errors")
		return errors.NewListErrorsInternalServerError()
	}
	resp.Link = link

	return resp
}

func buildListErrorsLink(baseURL *url.URL, params errors.ListErrorsParams) (string, error) {
	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListErrorsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Query != nil {
			q.Set("query", *params.Query)
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		if params.Status != nil {
			q.Set("status", *params.Status)
		}
		cursor, err := encodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListErrorsLinkPage(prev)
		if err != nil {
			return "", err
		}
	}
	nextLink, err := buildListErrorsLinkPage(next)
	if err != nil {
		return "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, nil
}

func (c *Controller) GetError(params errors.GetErrorParams) middleware.Responder {
	projectGetErrorCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint)

	logger.Debug("got get error request")

	res, err := c.db.GetError(params)
	if err != nil {
		logger.WithError(err).Error("get error")
		return errors.NewGetErrorInternalServerError()
	}

	if res == nil {
		return errors.NewGetErrorNotFound()
	}

	return errors.NewGetErrorOK().WithPayload(res)
}

func (c *Controller) UpdateError(params errors.UpdateErrorParams) middleware.Responder {
	projectUpdateErrorCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("fingerprint", params.Fingerprint).
		WithField("userID", params.Body.UpdatedByID).
		WithField("status", params.Body.Status)

	logger.Debug("got update error request")

	res, err := c.db.UpdateError(params)
	if err != nil {
		logger.WithError(err).Error("update error")
		return errors.NewUpdateErrorInternalServerError()
	}

	if res == nil {
		return errors.NewUpdateErrorNotFound()
	}

	return errors.NewUpdateErrorOK().WithPayload(res)
}

func (c *Controller) ListEvents(params errors.ListEventsParams) middleware.Responder {
	projectListEventCounter.WithLabelValues(strconv.FormatUint(params.ProjectID, 10)).Inc()

	logger := log.
		WithField("projectId", params.ProjectID).
		WithField("sort", *params.Sort).
		WithField("limit", *params.Limit)

	if params.Cursor != nil {
		logger = logger.WithField("cursor", *params.Cursor)
	}
	logger.Debug("got list errors request")

	res, err := c.db.ListEvents(params)
	if err != nil {
		log.WithError(err).Error("listing errors")
		return errors.NewListErrorsInternalServerError()
	}

	resp := errors.NewListEventsOK()
	resp.Payload = res
	link, err := buildListEventsLink(c.baseURL, params)
	if err != nil {
		log.WithError(err).Error("failed to build link for listing events")
		return errors.NewListErrorsInternalServerError()
	}
	resp.Link = link

	return resp
}

func buildListEventsLink(baseURL *url.URL, params errors.ListEventsParams) (string, error) {
	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1

	buildListEventsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseURL.String() + params.HTTPRequest.URL.String())
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		cursor, err := encodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListEventsLinkPage(prev)
		if err != nil {
			return "", err
		}
	}
	nextLink, err := buildListEventsLinkPage(next)
	if err != nil {
		return "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, nil
}

func (c *Controller) DeleteProject(params projects.DeleteProjectParams) middleware.Responder {
	logger := log.WithField("projectId", params.ID)
	logger.Debug("got delete project request")
	// TODO: handle project not found but since this endpoint is to be used for
	// testing just go ahead and proceed with the deletion.
	err := c.db.DeleteProject(params)
	if err != nil {
		return projects.NewDeleteProjectInternalServerError()
	}

	return projects.NewDeleteProjectCreated()
}

func (c *Controller) ListErrorsV2(params errors_v2.ListErrorsV2Params) middleware.Responder {
	logger := log.
		WithField("project", params.Project)

	logger.Debug("got list errors v2 request")

	results, err := c.db.ListErrorsV2(params)
	if err != nil {
		log.WithError(err).Error("listing errorsV2")
		return errors_v2.NewListErrorsV2InternalServerError()
	}
	resp := errors_v2.NewListErrorsV2OK()
	resp.Payload = results
	return resp
}

// ListProjects calls back the gatekeeper with the auth headers or user session so we can list GitLab projects.
func (c *Controller) ListProjects(params errors_v2.ListProjectsParams) middleware.Responder {
	log.Debugf("got the request to list projects")

	// grab Auth and GOB cookie headers and forward them on
	// Note(joe): gatekeeper doesn't support project list with bearer token yet.
	// See proposal: https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2051
	authHeader := params.HTTPRequest.Header.Get("Authorization")
	cookie, err := params.HTTPRequest.Cookie(constants.SessionCookieName)
	if authHeader == "" && err != nil {
		log.WithField("groupID", params.GroupID).Warn("list projects called without auth header or session cookie")
		return errors_v2.NewListProjectsUnauthorized()
	}

	projectURL := c.gateKeeperURL.String() + fmt.Sprintf("/v1/%d/projects", params.GroupID)

	req, err := http.NewRequest(http.MethodGet, projectURL, nil)
	if err != nil {
		log.WithError(err).Error("failed to create request to gatekeeper")
		return errors_v2.NewListProjectsInternalServerError()
	}

	req.Header.Set("Authorization", authHeader)
	if cookie != nil {
		req.AddCookie(cookie)
	}
	log.WithField("req headers", req.Header).Debugf("sending request to gatekeeper at %v", projectURL)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		log.WithError(err).Error("failed to get response from gatekeeper")
		return errors_v2.NewListProjectsInternalServerError()
	}
	if resp.StatusCode != http.StatusOK {
		log.Errorf("received non-200 status code: %v from gatekeeper", resp.StatusCode)
		return errors_v2.NewListProjectsUnauthorized()
	}
	bts, err := io.ReadAll(resp.Body)
	if err != nil {
		log.WithError(err).Error("failed to read response from gatekeeper")
		return errors_v2.NewListProjectsInternalServerError()
	}

	var basicProjects []gitlab.BasicProject
	err = json.Unmarshal(bts, &basicProjects)
	if err != nil {
		log.WithError(err).Error("failed to unmarshal response from gatekeeper")
		return errors_v2.NewListProjectsInternalServerError()
	}
	payload := make([]*models.Project, len(basicProjects))
	for i, proj := range basicProjects {
		payload[i] = &models.Project{
			ID:   strconv.Itoa(proj.ID),
			Name: proj.Name,
			Slug: proj.PathWithNamespace,
		}
	}
	return errors_v2.NewListProjectsOK().WithPayload(payload)
}

func (c *Controller) GetStatsV2(_ errors_v2.GetStatsV2Params) middleware.Responder {
	return errors_v2.NewGetStatsV2OK()
}

func (c *Controller) ListMessages(params messages.ListMessagesParams) middleware.Responder {
	logger := log.WithField("project", params.ProjectID)

	logger.Debug("got list messages request")

	results, err := c.db.ListMessages(params)
	if err != nil {
		log.WithError(err).Error("listing messages")
		return messages.NewListMessagesInternalServerError()
	}
	resp := messages.NewListMessagesOK()
	resp.Payload = results
	return resp
}
