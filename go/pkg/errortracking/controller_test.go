package errortracking

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/go-openapi/loads"
	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
)

//
//nolint:thelper
func GetAPIHandler(t *testing.T, ctrl *Controller) *operations.ErrorTrackingAPI {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	assert.Nil(t, err, "failed to load swagger definition")

	api := operations.NewErrorTrackingAPI(swaggerSpec)
	api.Logger = t.Logf

	api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(
		ctrl.PostEnvelopeHandler,
	)
	api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(
		ctrl.PostStoreHandler,
	)
	api.ErrorsListErrorsHandler = errors.ListErrorsHandlerFunc(
		ctrl.ListErrors,
	)
	api.ErrorsGetErrorHandler = errors.GetErrorHandlerFunc(
		ctrl.GetError,
	)
	api.ErrorsUpdateErrorHandler = errors.UpdateErrorHandlerFunc(
		ctrl.UpdateError,
	)
	api.ErrorsListEventsHandler = errors.ListEventsHandlerFunc(
		ctrl.ListEvents,
	)
	api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(
		ctrl.DeleteProject,
	)
	api.ErrorsV2ListErrorsV2Handler = errors_v2.ListErrorsV2HandlerFunc(
		ctrl.ListErrorsV2,
	)
	api.MessagesListMessagesHandler = messages.ListMessagesHandlerFunc(
		ctrl.ListMessages,
	)
	api.ErrorsV2ListProjectsHandler = errors_v2.ListProjectsHandlerFunc(
		ctrl.ListProjects,
	)

	err = api.Validate()
	assert.Nil(t, err, "invalid api")

	return api
}

type mockDB struct {
	err                 error
	event               *ErrorTrackingErrorEvent
	listErrorParams     *errors.ListErrorsParams
	listErrors          []*models.Error
	getErrorParams      *errors.GetErrorParams
	getError            *models.Error
	updateErrorParams   *errors.UpdateErrorParams
	updateError         *models.Error
	listEventsParams    *errors.ListEventsParams
	listEvents          []*models.ErrorEvent
	deleteProjectParams *projects.DeleteProjectParams
	eventV2             *ErrorEventV2
	listErrorV2Params   *errors_v2.ListErrorsV2Params
	listErrorV2         []*models.ErrorV2
	message             *ErrorTrackingMessageEvent
	listMessages        []*models.MessageEvent
	listMessageParams   *messages.ListMessagesParams
	session             *ErrorTrackingSession
}

func (m *mockDB) GetConn() (*clickhouse.Conn, error) {
	return nil, nil
}

func (m *mockDB) GetTZ() (*time.Location, error) {
	return nil, nil
}

func (m *mockDB) InsertErrorTrackingErrorEvent(e *ErrorTrackingErrorEvent) error {
	m.event = e
	return m.err
}

func (m *mockDB) ListErrors(params errors.ListErrorsParams) ([]*models.Error, error) {
	m.listErrorParams = &params
	return m.listErrors, m.err
}

func (m *mockDB) GetError(params errors.GetErrorParams) (*models.Error, error) {
	m.getErrorParams = &params
	return m.getError, m.err
}

func (m *mockDB) UpdateError(params errors.UpdateErrorParams) (*models.Error, error) {
	m.updateErrorParams = &params
	return m.updateError, m.err
}

func (m *mockDB) ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error) {
	m.listEventsParams = &params
	return m.listEvents, m.err
}

func (m *mockDB) DeleteProject(params projects.DeleteProjectParams) error {
	m.deleteProjectParams = &params
	return m.err
}

func (m *mockDB) InsertErrorEventV2(e *ErrorEventV2) error {
	m.eventV2 = e
	return m.err
}

func (m *mockDB) ListErrorsV2(params errors_v2.ListErrorsV2Params) ([]*models.ErrorV2, error) {
	m.listErrorV2Params = &params
	return m.listErrorV2, m.err
}

func (m *mockDB) InsertErrorTrackingMessageEvent(e *ErrorTrackingMessageEvent) error {
	m.message = e
	return m.err
}

func (m *mockDB) ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error) {
	m.listMessageParams = &params
	return m.listMessages, m.err
}

func (m *mockDB) InsertErrorTrackingSession(e *ErrorTrackingSession) error {
	m.session = e
	return m.err
}

func TestPostEnvelopeHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	payloadEvent, err := os.ReadFile("testdata/exceptions/envelope.txt")
	assert.Nil(t, err, "failed to load test data")

	payloadSession, err := os.ReadFile("testdata/sessions/js_session_envelope.txt")
	assert.Nil(t, err, "failed to load test data")

	payloadUnsupportedEventType, err := os.ReadFile("testdata/message/error_cases/unknown_event_envelope.txt")
	assert.Nil(t, err, "failed to load test data")

	payloadNonValidMessage, err := os.ReadFile("testdata/message/error_cases/non_valid_message_envelope.txt")
	assert.Nil(t, err, "failed to load test data")

	// List of content-type headers we need to support. See
	// gen/restapi/configure_error_tracking.go for more details.
	contentTypes := []string{
		"application/json",
		"application/x-sentry-envelope",
		"application/octet-stream",
		// empty string because NodeJS does not set a content-type in the request
		"",
		// javascript browser SDK sets "text/plain"
		"text/plain",
		"text/plain;charset=UTF-8",
	}

	for _, tc := range []struct {
		name       string
		payload    []byte
		statusCode int
		setup      func()
		teardown   func()
		encoded    bool
	}{
		{
			name:       "should insert a valid error event",
			payload:    payloadEvent,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid session",
			payload:    payloadSession,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid gzip'ed error event",
			payload:    payloadEvent,
			statusCode: http.StatusOK,
			encoded:    true,
		},
		{
			name:       "should fail to insert empty error event",
			payload:    []byte(`{}`),
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail to insert an unsupported event type",
			payload:    payloadUnsupportedEventType,
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail to validate event",
			payload:    payloadNonValidMessage,
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail when db insert fails",
			payload:    payloadEvent,
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				assert.Equal(t, uint64(1), db.event.ProjectID)
				assert.Equal(t, "development", db.event.Environment)
				assert.Equal(t, "ruby", db.event.Platform)
				// reset mock database error state
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		for _, contentType := range contentTypes {
			t.Logf("\twhen content type is %s", contentType)

			r := bytes.NewReader(tc.payload)
			if tc.encoded {
				var buf bytes.Buffer
				zw := gzip.NewWriter(&buf)
				zw.Write(tc.payload)
				zw.Close()
				r = bytes.NewReader(buf.Bytes())
			}

			req, err := http.NewRequest("POST", ts.URL+apiBasePath+"/projects/api/1/envelope", r)

			assert.Nil(t, err)
			req.Header.Set("Content-Type", contentType)
			if tc.encoded {
				req.Header.Set("Content-Encoding", "gzip")
			}

			res, err := http.DefaultClient.Do(req)

			assert.Nil(t, err)
			assert.Equal(t, tc.statusCode, res.StatusCode)
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestPostStoreHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	payload, err := os.ReadFile("testdata/exceptions/ruby_event.json")
	assert.Nil(t, err, "failed to load test data")

	base64compressedPayload, err := os.ReadFile("testdata/exceptions/ruby_base64_zlib.txt")
	assert.Nil(t, err, "failed to load test data")

	payloadMessage, err := os.ReadFile("testdata/message/go_message.json")
	assert.Nil(t, err, "failed to load test data")

	payloadMessageNotValid, err := os.ReadFile("testdata/message/error_cases/non_valid_message.json")
	assert.Nil(t, err, "failed to load test data")

	payloadErrorNotValid, err := os.ReadFile("testdata/exceptions/error_cases/non_valid_exception.json")
	assert.Nil(t, err, "failed to load test data")

	for _, tc := range []struct {
		name       string
		payload    []byte
		statusCode int
		setup      func()
		teardown   func()
	}{
		{
			name:       "should insert a valid error event",
			payload:    payload,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid base64 encoded and zlib compressed error event",
			payload:    base64compressedPayload,
			statusCode: http.StatusOK,
		},
		{
			name:       "should insert a valid message event",
			payload:    payloadMessage,
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail to insert empty error event",
			payload:    []byte(`{}`),
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail to insert non valid message event",
			payload:    payloadMessageNotValid,
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "should fail to insert non valid error event",
			payload:    payloadErrorNotValid,
			statusCode: http.StatusBadRequest,
			setup: func() {
				// set up a mock error
				fmt.Println()
			},
		},
		{
			name:       "should fail when db insert fails",
			payload:    payload,
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				assert.Equal(t, uint64(1), db.event.ProjectID)
				assert.Equal(t, "development", db.event.Environment)
				assert.Equal(t, "ruby", db.event.Platform)
				// reset mock database error state
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		r := bytes.NewReader(tc.payload)
		req, err := http.NewRequest("POST", ts.URL+apiBasePath+"/projects/api/1/store", r)
		req.Header.Set("Content-Type", "application/json")

		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListErrorsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	makeError := func(name string, projectID uint64, fingerprint uint32) *models.Error {
		return &models.Error{
			Actor:                 "actor",
			ApproximatedUserCount: 1,
			Description:           "description",
			EventCount:            1,
			Fingerprint:           fingerprint,
			Name:                  name,
			ProjectID:             projectID,
			Status:                "unresolved",
		}
	}

	db.listErrors = []*models.Error{
		makeError("a", 1, 1),
		makeError("b", 1, 1),
		makeError("c", 1, 1),
	}

	for _, tc := range []struct {
		name       string
		statusCode int
		query      string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should list errors",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept last_seen_desc sort parameter",
			query:      "sort=last_seen_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept first_seen_desc sort parameter",
			query:      "sort=first_seen_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept frequency_desc sort parameter",
			query:      "sort=frequency_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with unknown sort parameter",
			query:      "sort=foo",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept unresolved status parameter",
			query:      "status=unresolved",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept resolved status parameter",
			query:      "status=resolved",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept ignored status parameter",
			query:      "status=ignored",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept ignored status parameter",
			query:      "status=ignored",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid status parameter",
			query:      "status=foobar",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept query parameter",
			query:      "query=foobar",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept limit parameter",
			query:      "limit=10",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid limit parameter",
			query:      "limit=a",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/projects/1/errors?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := io.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var errors []*models.Error
			err = json.Unmarshal(body, &errors)
			assert.Nil(t, err)
			assert.ElementsMatch(t, errors, db.listErrors)

			// only check the link header is set, there are tests to check the
			// link is built appropriately
			assert.NotEmpty(t, res.Header.Get("Link"))
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestBuildErrorsLink(t *testing.T) {
	baseURL, _ := url.Parse("http://localhost:8080")
	route := "/errortracking/api/v1/projects/2/errors"
	testURL, _ := url.Parse(route)
	endpoint := baseURL.String() + route

	assert.Equal(t, "http://localhost:8080/errortracking/api/v1/projects/2/errors", endpoint)

	for _, tc := range []struct {
		name   string
		params errors.ListErrorsParams
		link   string
	}{
		{
			name: "should return valid next link",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
			},
			link: fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
		},
		{
			name: "should return valid next and prev links",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with limit",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Limit:  int64Pointer(2),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&limit=2>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with query",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Query:  stringPointer("foobar"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&query=foobar>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&query=foobar>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with sort",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("first_seen_desc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&sort=first_seen_desc>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&sort=first_seen_desc>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with status",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Status: stringPointer("resolved"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&status=resolved>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&status=resolved>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next link with all params",
			params: errors.ListErrorsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				Limit:  int64Pointer(2),
				Query:  stringPointer("foobar"),
				Sort:   stringPointer("first_seen_desc"),
				Status: stringPointer("resolved"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&query=foobar&sort=first_seen_desc&status=resolved>; rel="next"`,
				endpoint,
			),
		},
	} {
		t.Log(tc.name)
		link, err := buildListErrorsLink(baseURL, tc.params)
		assert.Nil(t, err)
		assert.Equal(t, tc.link, link)
	}
}

func TestGetErrorsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should get error",
			statusCode: http.StatusOK,
			uri:        apiBasePath + "/projects/1/errors/1",
			setup: func() {
				db.getError = &models.Error{}
			},
			teardown: func() {
				db.getError = nil
			},
		},
		{
			name:       "should return not found",
			statusCode: http.StatusNotFound,
			uri:        apiBasePath + "/projects/1/errors/1",
		},
		{
			name:       "should fail when db query fails",
			uri:        apiBasePath + "/projects/1/errors/1",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+tc.uri, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestUpdateErrorHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		params     map[string]interface{}
		setup      func()
		teardown   func()
	}{
		{
			name:       "should update error",
			statusCode: http.StatusOK,
			uri:        apiBasePath + "/projects/1/errors/1",
			params: map[string]interface{}{
				"status":        "ignored",
				"updated_by_id": 1,
			},
			setup: func() {
				db.updateError = &models.Error{}
			},
			teardown: func() {
				db.updateError = nil
			},
		},
		{
			name:       "should return not found",
			statusCode: http.StatusNotFound,
			uri:        apiBasePath + "v1/projects/1/errors/1",
		},
		{
			name:       "should fail when db query fails",
			uri:        apiBasePath + "/projects/1/errors/1",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		body, err := json.Marshal(tc.params)
		assert.Nil(t, err)

		req, err := http.NewRequest("PUT", ts.URL+tc.uri, bytes.NewReader(body))
		assert.Nil(t, err)
		req.Header.Set("Content-type", "application/json")

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)

		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListEventsHandler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	apiBasePath := api.Context().BasePath()

	makeErrorEvent := func(name string, projectID uint64, fingerprint uint32) *models.ErrorEvent {
		return &models.ErrorEvent{
			Actor:       "actor",
			Description: "description",
			Environment: "env",
			Fingerprint: fingerprint,
			Name:        name,
			Payload:     "payload",
			Platform:    "platform",
			ProjectID:   projectID,
		}
	}

	db.listEvents = []*models.ErrorEvent{
		makeErrorEvent("a", 1, 1),
		makeErrorEvent("b", 1, 1),
		makeErrorEvent("c", 1, 1),
	}

	for _, tc := range []struct {
		name       string
		statusCode int
		query      string
		setup      func()
		teardown   func()
		events     []*models.ErrorEvent
	}{
		{
			name:       "should list error events",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept occurred_at_desc sort parameter",
			query:      "sort=occurred_at_desc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept occurred_at_asc sort parameter",
			query:      "sort=occurred_at_asc",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with unknown sort parameter",
			query:      "sort=foo",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept limit parameter",
			query:      "limit=10",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with invalid limit parameter",
			query:      "limit=a",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/projects/1/errors/1/events?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := io.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var events []*models.ErrorEvent
			err = json.Unmarshal(body, &events)
			assert.Nil(t, err)
			assert.ElementsMatch(t, events, db.listEvents)

			// only check the link header is set, there are tests to check the
			// link is built appropriately
			assert.NotEmpty(t, res.Header.Get("Link"))
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestBuildEventsLink(t *testing.T) {
	baseURL, _ := url.Parse("http://localhost:8080")
	route := "/projects/2/errors/1/events"
	testURL, _ := url.Parse(route)
	endpoint := baseURL.String() + route

	assert.Equal(t, "http://localhost:8080/projects/2/errors/1/events", endpoint)

	for _, tc := range []struct {
		name   string
		params errors.ListEventsParams
		link   string
	}{
		{
			name: "should return valid next link",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
			},
			link: fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
		},
		{
			name: "should return valid next and prev links",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with limit",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Limit:  int64Pointer(2),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&limit=2>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next and prev links with sort",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&sort=occurred_at_desc>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D&sort=occurred_at_desc>; rel="next"`,
				endpoint,
				endpoint,
			),
		},
		{
			name: "should return valid next link with all params",
			params: errors.ListEventsParams{
				HTTPRequest: &http.Request{
					URL: testURL,
				},
				Limit: int64Pointer(2),
				Sort:  stringPointer("occurred_at_asc"),
			},
			link: fmt.Sprintf(
				`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&sort=occurred_at_asc>; rel="next"`,
				endpoint,
			),
		},
	} {
		t.Log(tc.name)
		link, err := buildListEventsLink(baseURL, tc.params)
		assert.Nil(t, err)
		assert.Equal(t, tc.link, link)
	}
}

func TestDeleteProject(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	for _, tc := range []struct {
		name       string
		statusCode int
		uri        string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should delete a project",
			statusCode: http.StatusCreated,
			uri:        apiBasePath + "/projects/1",
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			uri:        apiBasePath + "/projects/1",
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}

		req, err := http.NewRequest("DELETE", ts.URL+tc.uri, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)

		assert.Equal(t, tc.statusCode, res.StatusCode)

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListErrorsV2Handler(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	makeErrorV2 := func(id string, projectID string, count, status, title string) *models.ErrorV2 {
		return &models.ErrorV2{
			Actor: "actor",
			Count: count,
			ID:    id,
			Project: &models.Project{
				ID: projectID,
			},
			UserCount: 1,
			Status:    status,
			Title:     title,
		}
	}

	db.listErrorV2 = []*models.ErrorV2{
		makeErrorV2("a", "1", "1", "unresolved", "error"),
		makeErrorV2("b", "1", "1", "unresolved", "error2"),
		makeErrorV2("c", "1", "1", "resolved", "error3"),
	}

	for _, tc := range []struct {
		name       string
		statusCode int
		query      string
		setup      func()
		teardown   func()
	}{
		{
			name:       "should list errors",
			statusCode: http.StatusOK,
			query:      "project=1",
		},
		{
			name:       "should accept date sort parameter & start - end",
			query:      "sort=date&project=1&project=2&start=2022-12-23T06%3A47%3A26&end=2022-12-23T07%3A47%3A26",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept new sort parameter",
			query:      "sort=new&project=1",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept freq sort parameter",
			query:      "sort=freq&project=2",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept user sort parameter",
			query:      "sort=user&project=2",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept priority sort parameter",
			query:      "sort=priority&project=2",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail with unknown sort parameter",
			query:      "sort=foo&project=1",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should fail when project is missing",
			query:      "status=unresolved",
			statusCode: http.StatusUnprocessableEntity,
		},
		{
			name:       "should accept query parameter",
			query:      "query=foobar&project=1",
			statusCode: http.StatusOK,
		},
		{
			name:       "should accept limit parameter",
			query:      "limit=10&project=1",
			statusCode: http.StatusOK,
		},
		{
			name:       "should fail when db query fails",
			statusCode: http.StatusInternalServerError,
			query:      "query=foobar&project=1",
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/api/0/organizations/1/issues?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := io.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var errors []*models.ErrorV2
			err = json.Unmarshal(body, &errors)
			assert.Nil(t, err)
			assert.ElementsMatch(t, errors, db.listErrorV2)
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestListMessages(t *testing.T) {
	db := &mockDB{}
	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, db, nil, nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	makeMessage := func(pID uint64, env string, eID string, lvl string, msg string, plt string, rel string) *models.MessageEvent {
		return &models.MessageEvent{
			ProjectID:   pID,
			Environment: env,
			EventID:     eID,
			Level:       lvl,
			Message:     msg,
			Platform:    plt,
			Release:     rel,
		}
	}

	multipleMessages := []*models.MessageEvent{
		makeMessage(1234, "dev", "1", "info", "message", "go", "v1"),
		makeMessage(1234, "staging", "2", "warning", "message", "go", "v1"),
		makeMessage(1234, "prod", "3", "error", "message", "go", "v1"),
	}
	oneMessage := []*models.MessageEvent{
		makeMessage(1234, "dev", "1", "info", "message", "go", "v1"),
	}
	db.listMessages = multipleMessages

	for _, tc := range []struct {
		name           string
		statusCode     int
		expectedParams messages.ListMessagesParams
		query          string
		setup          func()
		teardown       func()
	}{
		{
			name:           "should return internal server error",
			statusCode:     http.StatusInternalServerError,
			expectedParams: messages.ListMessagesParams{Limit: int64Pointer(20), ProjectID: 1234},
			query:          "",
			setup: func() {
				// set up a mock error
				db.err = fmt.Errorf("fake error")
			},
			teardown: func() {
				db.err = nil
			},
		},
		{
			name:           "should list messages",
			statusCode:     http.StatusOK,
			expectedParams: messages.ListMessagesParams{Limit: int64Pointer(20), ProjectID: 1234},
			query:          "",
		},
		{
			name:           "should list one message for limit = 1",
			statusCode:     http.StatusOK,
			expectedParams: messages.ListMessagesParams{Limit: int64Pointer(1), ProjectID: 1234},
			query:          "limit=1",
			setup: func() {
				db.listMessages = oneMessage
			},
			teardown: func() {
				db.listMessages = multipleMessages
			},
		},
	} {
		t.Logf("%s", tc.name)
		if tc.setup != nil {
			tc.setup()
		}
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/projects/1234/messages?"+tc.query, nil)
		assert.Nil(t, err)

		res, err := http.DefaultClient.Do(req)
		assert.Nil(t, err)
		assert.Equal(t, tc.statusCode, res.StatusCode)

		// check the response payload matches the mock values
		if tc.statusCode == http.StatusOK {
			body, err := io.ReadAll(res.Body)
			assert.Nil(t, err)
			defer res.Body.Close()

			var messages []*models.MessageEvent
			err = json.Unmarshal(body, &messages)
			assert.Nil(t, err)
			assert.ElementsMatch(t, messages, db.listMessages)
			assert.Equal(t, tc.expectedParams.Limit, db.listMessageParams.Limit)
			assert.Equal(t, tc.expectedParams.ProjectID, db.listMessageParams.ProjectID)
		}

		if tc.teardown != nil {
			tc.teardown()
		}
	}
}

func TestController_ListProjects(t *testing.T) {
	var gatekeeperWriter func(w http.ResponseWriter)
	gatekeeperStub := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		gatekeeperWriter(w)
	}))
	defer gatekeeperStub.Close()

	url, _ := url.Parse("http://localhost:8080")
	ctrl := NewController(url, nil, common.MustParse(gatekeeperStub.URL), nil)

	api := GetAPIHandler(t, ctrl)
	apiBasePath := api.Context().BasePath()
	server := restapi.NewServer(api)
	server.ConfigureAPI()

	ts := httptest.NewServer(server.GetHandler())
	defer ts.Close()

	mkReq := func(mutate func(*http.Request)) *http.Request {
		req, err := http.NewRequest("GET", ts.URL+apiBasePath+"/api/0/organizations/2/projects/", nil)
		assert.NoError(t, err, "new request")
		if mutate != nil {
			mutate(req)
		}
		return req
	}

	tests := []struct {
		name             string
		gatekeeperWriter func(w http.ResponseWriter)
		request          *http.Request
		wantStatusCode   int
		wantProjects     []models.Project
	}{
		{
			"no auth header or cookie returns 401",
			nil,
			mkReq(nil),
			401,
			nil,
		},
		{
			"no response from gatekeeper returns 500",
			func(w http.ResponseWriter) {
				panic("d'oh")
			},
			mkReq(func(r *http.Request) {
				r.Header.Add("Authorization", "foo")
			}),
			500,
			nil,
		},
		{
			"404 from gatekeeper returns 401",
			func(w http.ResponseWriter) {
				w.WriteHeader(404)
			},
			mkReq(func(r *http.Request) {
				r.Header.Add("Authorization", "foo")
			}),
			401,
			nil,
		},
		{
			"malformed gatekeeper response returns 500",
			func(w http.ResponseWriter) {
				fmt.Fprint(w, "potato")
			},
			mkReq(func(r *http.Request) {
				r.AddCookie(&http.Cookie{
					Name: constants.SessionCookieName,
				})
			}),
			500,
			nil,
		},
		{
			"gatekeeper projects are successfully returned",
			func(w http.ResponseWriter) {
				ps := []gitlab.BasicProject{
					{
						ID:                1,
						Name:              "One",
						PathWithNamespace: "/one",
					},
					{
						ID:                2,
						Name:              "Two",
						PathWithNamespace: "/two",
					},
				}
				assert.NoError(t, json.NewEncoder(w).Encode(ps))
			},
			mkReq(func(r *http.Request) {
				r.AddCookie(&http.Cookie{
					Name: constants.SessionCookieName,
				})
			}),
			200,
			[]models.Project{
				{
					ID:   "1",
					Name: "One",
					Slug: "/one",
				},
				{
					ID:   "2",
					Name: "Two",
					Slug: "/two",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gatekeeperWriter = tt.gatekeeperWriter

			res, err := http.DefaultClient.Do(tt.request)
			assert.NoError(t, err, "get projects")
			assert.Equal(t, tt.wantStatusCode, res.StatusCode, "status code")

			if res.StatusCode == 200 {
				var ps []models.Project
				assert.NoError(t, json.NewDecoder(res.Body).Decode(&ps), "decode projects")
				assert.ElementsMatch(t, ps, tt.wantProjects, "projects")
			}
		})
	}
}
