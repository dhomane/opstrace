package errortracking

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type ISentryEventHandler interface {
	Validate(e *types.Event) error
	InsertEvent(projectID uint64, payloadType int, e *types.Event, payload []byte) error
}

func GetEventManager(dataItemType string, ingester Ingester) (ISentryEventHandler, error) {
	if dataItemType == types.SupportedTypeException {
		return &SentryExceptionManager{ingester}, nil
	}

	if dataItemType == types.SupportedTypeMessage {
		return &SentryMessageManager{ingester}, nil
	}

	return nil, fmt.Errorf("wrong data item type passed")
}
