package errortracking

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type SentryMessageManager struct {
	i Ingester
}

func (se *SentryMessageManager) Validate(e *types.Event) error {
	err := e.ValidateMessage()
	if err != nil {
		return fmt.Errorf("error in SentryMessageManager message validation: %w", e.ValidateException())
	}
	return nil
}

func (se *SentryMessageManager) InsertEvent(projectID uint64, payloadType int, e *types.Event, payload []byte) error {
	err := se.i.IngestMessageData(projectID, payloadType, payload, e)
	if err != nil {
		return fmt.Errorf("error in SentryMessageManager inserting message: %w", err)
	}
	return nil
}
