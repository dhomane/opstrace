package errortracking

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

func TestGetSentryDataItemManager(t *testing.T) {
	assert := assert.New(t)

	t.Run("Gets Sentry event manager for error data item ", func(t *testing.T) {
		matches, err := filepath.Glob("testdata/exceptions/python_event.json")
		assert.Nil(err)
		assert.Equal(1, len(matches))
		payload, err := os.ReadFile(matches[0])
		assert.Nil(err)
		e, err := types.NewEventFrom(payload)
		assert.Nil(err)

		d := &SentryDataItem{Type: "event", DataItem: e}
		r, err := getSentryDataItemManager(d)
		assert.Nil(err)
		assert.Equal("*errortracking.EventErrorHandler", reflect.TypeOf(r).String())
	})

	t.Run("Gets Sentry event manager for message data item ", func(t *testing.T) {
		d := &SentryDataItem{Type: "event", DataItem: &types.Event{
			Message: "some message",
		}}
		r, err := getSentryDataItemManager(d)
		assert.Nil(err)
		assert.Equal("*errortracking.EventMessageHandler", reflect.TypeOf(r).String())
	})

	t.Run("Gets Sentry event manager for session data item ", func(t *testing.T) {
		d := &SentryDataItem{Type: "session"}
		r, err := getSentryDataItemManager(d)
		assert.Nil(err)
		assert.Equal("*errortracking.SessionHandler", reflect.TypeOf(r).String())
	})

	t.Run("Fails if type is not correct", func(t *testing.T) {
		d := &SentryDataItem{Type: "unknown"}
		r, err := getSentryDataItemManager(d)
		assert.Nil(r)
		assert.Equal(fmt.Errorf("unknown sentry data item type, cannot get a SentryDataItemManager"), err)
	})
}

func TestNewSentryDataItem(t *testing.T) {
	assert := assert.New(t)

	t.Run("Returns Sentry item with event", func(t *testing.T) {
		s, err := NewSentryDataItem(types.EventType)
		assert.Nil(err)
		_, ok := s.DataItem.(types.Event)
		assert.True(ok)
		assert.Equal(types.EventType, s.Type)
	})

	t.Run("Returns Sentry item with session", func(t *testing.T) {
		s, err := NewSentryDataItem(types.SessionType)
		assert.Nil(err)
		_, ok := s.DataItem.(types.Session)
		assert.True(ok)
		assert.Equal(types.SessionType, s.Type)
	})

	t.Run("Fails due to  unknown type", func(t *testing.T) {
		s, err := NewSentryDataItem("unknown")
		assert.Nil(s)
		assert.NotNil(err)
	})
}

func TestParsePayload(t *testing.T) {
	assert := assert.New(t)
	t.Run("Should fail if argument is not an array of bytes", func(t *testing.T) {
		item, err := parsePayload(3)
		assert.Nil(item)
		assert.Equal(fmt.Errorf("received item is not a byte slice, cannot unmarshall"), err)
	})

	t.Run("Should fail if unmarshal fails", func(t *testing.T) {
		b, err := json.Marshal([]byte{1, 2, 3})
		assert.Nil(err)
		item, err := parsePayload(b)
		assert.Nil(item)
		assert.True(strings.Contains(err.Error(), "unmarshalling received item"))
	})

	t.Run("Should succeed", func(t *testing.T) {
		expectedItem := &QueueItem{
			Type:      PayloadTypeEnvelope,
			Payload:   []byte{1, 2, 3},
			ProjectID: uint64(1234),
		}
		b, err := json.Marshal(expectedItem)
		assert.Nil(err)

		item, err := parsePayload(b)
		assert.Nil(err)
		assert.Equal(expectedItem, item)
	})
}

func TestParseQueueItem(t *testing.T) {
	assert := assert.New(t)

	t.Run("Should return Envelope from envelope type", func(t *testing.T) {
		matches, err := filepath.Glob("testdata/exceptions/java-sdk-envelope.txt")
		assert.Nil(err)
		assert.Equal(1, len(matches))
		payload, err := os.ReadFile(matches[0])
		assert.Nil(err)

		item, err := parseQueueItem(&QueueItem{
			Type:      PayloadTypeEnvelope,
			Payload:   payload,
			ProjectID: uint64(1234),
		})
		assert.Nil(err)
		assert.Equal(item.Type, types.EventType)
		_, ok := item.DataItem.(*types.Event)
		assert.True(ok)
	})

	t.Run("Should return Event from payload type", func(t *testing.T) {
		matches, err := filepath.Glob("testdata/exceptions/ruby_event.json")
		assert.Nil(err)
		assert.Equal(1, len(matches))
		payload, err := os.ReadFile(matches[0])
		assert.Nil(err)

		item, err := parseQueueItem(&QueueItem{
			Type:      PayloadTypeEvent,
			Payload:   payload,
			ProjectID: uint64(1234),
		})
		assert.Nil(err)
		assert.Equal(item.Type, types.EventType)
		_, ok := item.DataItem.(*types.Event)
		assert.True(ok)
	})

	t.Run("Should fail for unknown type", func(t *testing.T) {
		item, err := parseQueueItem(&QueueItem{
			Type:      222,
			Payload:   []byte{},
			ProjectID: uint64(1234),
		})
		assert.Nil(item)
		assert.Equal(fmt.Errorf("cannot process sentry data item"), err)
	})

	t.Run("Should fail for unknown type", func(t *testing.T) {

	})
}
