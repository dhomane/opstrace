CREATE TABLE IF NOT EXISTS error_tracking_group_local ON CLUSTER '{cluster}'
(
    project_id UInt64,
    id FixedString(32),
    error_count UInt64,

    title Nullable(String),
    actor Nullable(String),

    status AggregateFunction(argMax, Nullable(UInt8), DateTime64(6, 'UTC')),
    approximated_user_count AggregateFunction(uniq, Nullable(String)),
    last_seen_at SimpleAggregateFunction(max, DateTime64(6, 'UTC')),
    first_seen_at SimpleAggregateFunction(min, DateTime64(6, 'UTC'))
)
ENGINE = ReplicatedSummingMergeTree
ORDER BY (project_id, id)
SETTINGS index_granularity = 8192, storage_policy = 'gcs_main';
