-- session id can be in the form of a string or uuid depending on the SDK. Hence we use string.
-- user_id can be null
-- started refers to when the session started
-- occured_at referes to when this particular session entry occured
-- init is true only when this is the first session message. Sometimes the first is also the last message for a session. Then status is exited or crashed.
-- duration is a double(alias for float64). We usually get numbers like 0.01337122917175293
-- We order based on the logical order of projects having sessions per release. Each release might have multiple session ids, and each session id might have one or more session data entries
-- Environment can be empty string if not set by the SDK.
CREATE TABLE IF NOT EXISTS error_tracking_sessions_local ON CLUSTER '{cluster}' (
  project_id UInt64,
  session_id String,
  user_id Nullable(String),
  init UInt8,
  payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
  started DateTime64(6, 'UTC'),
  occurred_at DateTime64(6, 'UTC'),
  duration DOUBLE,
  status  Enum('abnormal', 'exited', 'crashed', 'ok'),
  release String,
  environment Nullable(String),
) ENGINE = ReplicatedMergeTree
PARTITION BY toYYYYMM(occurred_at)
ORDER BY (project_id, release, session_id, occurred_at)
SETTINGS index_granularity = 8192, storage_policy = 'gcs_main';
