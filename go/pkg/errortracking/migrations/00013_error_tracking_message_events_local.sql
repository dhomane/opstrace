CREATE TABLE IF NOT EXISTS error_tracking_message_events_local ON CLUSTER '{cluster}' (
  event_id String,
  project_id UInt64,
  timestamp DateTime64(6, 'UTC'), -- Precision till microseconds
  is_deleted UInt8,
  fingerprint FixedString(32),

  environment Nullable(String),
  level Nullable(String),
  message String,
  actor Nullable(String),
  platform LowCardinality(Nullable(String)),
  release Nullable(String),
  sdk Nested
  (
    name String,
    version String
  ),
  server_name Nullable(String),
  stacktrace_frames Nested
  (
      abs_path Nullable(String),
      colno Nullable(UInt32),
      filename Nullable(String),
      function Nullable(String),
      lineno Nullable(UInt32),
      in_app Nullable(UInt8),
      package Nullable(String),
      module Nullable(String),
      context_line Nullable(String)
  ),
  payload String CODEC(LZ4HC(9)) -- AVG 20K bytes

)
ENGINE = ReplicatedReplacingMergeTree(is_deleted)
PARTITION BY toYYYYMM(timestamp)
ORDER BY (project_id, timestamp, fingerprint, event_id);
