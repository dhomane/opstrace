CREATE TABLE IF NOT EXISTS error_tracking_errors ON CLUSTER '{cluster}'
(
    project_id UInt64,
    fingerprint UInt32,
    name String,
    description String,
    actor String,
    event_count UInt64,
    approximated_user_count AggregateFunction(uniq, String),
    last_seen_at SimpleAggregateFunction(max, DateTime64(6, 'UTC')),
    first_seen_at SimpleAggregateFunction(min, DateTime64(6, 'UTC'))
)
ENGINE = ReplicatedSummingMergeTree
ORDER BY (project_id, fingerprint);
