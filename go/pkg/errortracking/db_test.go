package errortracking

import (
	"context"
	e "errors"
	"fmt"
	"testing"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

func TestQueryBuilder(t *testing.T) {
	b := &queryBuilder{}

	for _, tc := range []struct {
		name     string
		sql      string
		params   []interface{}
		expected string
	}{
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = 1",
			params:   []interface{}{},
			expected: "SELECT * FROM table WHERE thing = 1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ?",
			params:   []interface{}{1},
			expected: "SELECT * FROM table WHERE thing = $1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ? AND otherthing = ?",
			params:   []interface{}{1, 2},
			expected: "SELECT * FROM table WHERE thing = $1 AND otherthing = $2",
		},
	} {
		t.Log(tc.name)

		sql := "SELECT * FROM table"
		b.reset(sql)

		assert.Equal(t, b.sql, sql)
		assert.Len(t, b.args, 0)

		b.build(tc.sql, tc.params...)
		assert.EqualValues(t, tc.params, b.args)
		assert.Equal(t, tc.expected, b.sql)
	}
}

func stringPointer(s string) *string {
	return &s
}

func int64Pointer(i int64) *int64 {
	return &i
}

func TestBuildListErrorsQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.ListErrorsParams
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("last_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY last_seen_at DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(0), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("unresolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(0), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = TRUE ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $2 OFFSET $3",
			args:     []interface{}{uint64(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom query parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     stringPointer("foo"),
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("ignored"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = TRUE AND (error_tracking_errors.name ILIKE $2 OR error_tracking_errors.description ILIKE $3) ORDER BY first_seen_at DESC, fingerprint DESC LIMIT $4 OFFSET $5",
			args:     []interface{}{uint64(1), "%foo%", "%foo%", int64(20), 0},
			err:      nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(10),
				Query:     nil,
				Cursor:    stringPointer("foobar"),
				Sort:      stringPointer("frequency_desc"),
				Status:    stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(9),
				Query:     nil,
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY event_count DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(1), int64(9), 18},
			err:      nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = $2 ORDER BY event_count DESC, fingerprint DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint8(1), int64(7), 0},
			err:      nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				Query:     nil,
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
		{
			name: "should fail with unexpected status",
			params: errors.ListErrorsParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
				Query:     nil,
				Cursor:    nil,
				Sort:      stringPointer("first_seen_desc"),
				Status:    stringPointer("_ignored"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected error status _ignored"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListErrorsQuery(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildGetErrorQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.GetErrorParams
		expected string
		args     []interface{}
	}{
		{
			name: "should build a simple query",
			params: errors.GetErrorParams{
				ProjectID:   1,
				Fingerprint: 1,
			},
			expected: "\nSELECT\n    error_tracking_errors.project_id AS project_id,\n    error_tracking_errors.fingerprint AS fingerprint,\n    error_tracking_errors.name AS name,\n    error_tracking_errors.description AS description,\n    error_tracking_errors.actor AS actor,\n    error_tracking_errors.event_count AS event_count,\n    error_tracking_errors.approximated_user_count AS approximated_user_count,\n    error_tracking_errors.last_seen_at AS last_seen_at,\n    error_tracking_errors.first_seen_at AS first_seen_at,\n    COALESCE(error_tracking_error_status.status, 1) AS status,\n    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored\nFROM (\nSELECT\n    project_id,\n    fingerprint,\n    any(name) as name,\n    any(description) as description,\n    any(actor) as actor,\n    sum(event_count) as event_count,\n    uniqMerge(approximated_user_count) as approximated_user_count,\n    max(last_seen_at) as last_seen_at,\n    min(first_seen_at) as first_seen_at\n  FROM error_tracking_errors_mv\n  GROUP BY project_id, fingerprint\n) as error_tracking_errors\nLEFT JOIN (\n  SELECT project_id, argMax(status, updated_at) as status, fingerprint\n  FROM error_tracking_error_status\n  GROUP BY project_id, fingerprint\n) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND\n  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint\nLEFT JOIN (\n  SELECT project_id, fingerprint, TRUE AS ignored\n  FROM error_tracking_ignored_errors\n  GROUP BY project_id, fingerprint\n) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND\n  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint\nWHERE project_id = $1 AND fingerprint = $2",
			args:     []interface{}{uint64(1), uint32(1)},
		},
	} {
		t.Log(tc.name)
		sql, args := buildGetErrorQuery(tc.params)
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildListEventsQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.ListEventsParams
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_asc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at ASC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(20), 0},
			err:      nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(10),
				Cursor:      stringPointer("foobar"),
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(9),
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(9), 18},
			err:      nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(7),
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "SELECT * FROM error_tracking_error_events WHERE project_id = $1 AND fingerprint = $2  ORDER BY occurred_at DESC LIMIT $3 OFFSET $4",
			args:     []interface{}{uint64(1), uint32(1), int64(7), 0},
			err:      nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListEventsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListEventsQuery(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildListEventV2Query(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors_v2.ListErrorsV2Params
		expected string
		args     []interface{}
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23T06:47:26"),
				End:     stringPointer("2022-12-23T07:47:26"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 AND _last_seen_at >= $3 AND _last_seen_at <= $4 ORDER BY _last_seen_at DESC LIMIT $5`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), time.Date(2022, time.December, 23, 6, 47, 26, 0, time.UTC), time.Date(2022, time.December, 23, 7, 47, 26, 0, time.UTC), int64(20)},
			err:  nil,
		},
		{
			name: "should build a query with resolved status",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("resolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _last_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(1), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of first_seen_at",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("new"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _first_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of count",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("freq"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY count DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with sort of user",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("user"),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY user_count DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should build query with default sort of date",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Status:  stringPointer("unresolved"),
			},
			expected: `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
WHERE project_id IN ( $1 ) 
GROUP BY
project_id,
id
 HAVING status = $2 ORDER BY _last_seen_at DESC LIMIT $3`,
			args: []interface{}{[]uint64{1, 2}, uint8(0), int64(20)},
			err:  nil,
		},
		{
			name: "should fail because start param is incorrect formatted",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23-06:47:26"),
				End:     stringPointer("2022-12-23T07:47:26"),
			},
			expected: "",
			err:      fmt.Errorf("failed to parse `start` param"),
		},
		{
			name: "should fail because end param is incorrect formatted",
			params: errors_v2.ListErrorsV2Params{
				GroupID: 1,
				Project: []uint64{1, 2},
				Limit:   int64Pointer(20),
				Sort:    stringPointer("date"),
				Status:  stringPointer("unresolved"),
				Start:   stringPointer("2022-12-23T06:47:26"),
				End:     stringPointer("2022-12-23-07:47:26"),
			},
			expected: "",
			err:      fmt.Errorf("failed to parse `end` param"),
		},
	} {
		t.Log(tc.name)
		sql, args, err := buildListErrorsV2Query(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

func TestBuildListMessageQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   messages.ListMessagesParams
		expected string
		args     []interface{}
	}{
		{
			name: "should build a query with specific limit",
			params: messages.ListMessagesParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
			},
			expected: `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM error_tracking_message_events
WHERE project_id IN ( $1 )  LIMIT $2`,
			args: []interface{}{uint64(1), int64(20)},
		},
		{
			name: "should build a query with default limit",
			params: messages.ListMessagesParams{
				ProjectID: 1,
			},
			expected: `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM error_tracking_message_events
WHERE project_id IN ( $1 )  LIMIT $2`,
			args: []interface{}{uint64(1), defaultListIssueLimit},
		},
	} {
		t.Log(tc.name)
		sql, args := buildListMessageQuery(tc.params)
		assert.Equal(t, tc.expected, sql)
		assert.EqualValues(t, tc.args, args)
	}
}

type MockClickhouseConn struct {
	mock.Mock
	driver.Conn
}

func (m *MockClickhouseConn) Query(ctx context.Context, query string, args ...interface{}) (driver.Rows, error) {
	var listOfArguments []interface{}
	listOfArguments = append(listOfArguments, ctx)
	listOfArguments = append(listOfArguments, query)
	listOfArguments = append(listOfArguments, args...)
	arguments := m.Called(listOfArguments...)
	return arguments.Get(0).(driver.Rows), arguments.Error(1)
}

type MockClickhouseRows struct {
	mock.Mock
	driver.Rows
}

func (m *MockClickhouseRows) Next() bool {
	args := m.Called()
	return args.Bool(0)
}

func (m *MockClickhouseRows) ScanStruct(dest interface{}) error {
	args := m.Called(dest)
	return args.Error(0)
}

func (m *MockClickhouseRows) Close() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MockClickhouseRows) Err() error {
	args := m.Called()
	return args.Error(0)
}

func TestDbListMessages(t *testing.T) {
	assert := assert.New(t)
	expectedQuery := "SELECT\n\tevent_id,\n\tproject_id,\n\ttimestamp,\n\tenvironment,\n\tlevel,\n\tmessage,\n\tactor,\n\tplatform,\n\trelease,\n\tserver_name\nFROM error_tracking_message_events\nWHERE project_id IN ( $1 )  LIMIT $2"
	dbMock := &MockClickhouseConn{}
	nilResults := []*models.MessageEvent(nil)
	mockErr := e.New("mock")
	database := database{
		conn: dbMock,
	}

	t.Run("Fails if Query returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to list messages: mock")
		dbMock.On("Query", mock.Anything, expectedQuery, uint64(1), int64(20)).
			Times(1).Return(&MockClickhouseRows{}, mockErr)

		res, err := database.ListMessages(messages.ListMessagesParams{
			ProjectID: 1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if ScanStruct returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to scan struct into ErrorTrackingMessageEvent: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*errortracking.ErrorTrackingMessageEvent")).Return(mockErr)
		dbMock.On("Query", mock.Anything, expectedQuery, uint64(1), int64(20)).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			ProjectID: 1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if Close returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to close connection while reading from clickhouse: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*errortracking.ErrorTrackingMessageEvent")).Return(nil)
		rows.On("Close").Times(1).Return(mockErr)
		dbMock.On("Query", mock.Anything, expectedQuery, uint64(1), int64(20)).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			ProjectID: 1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if rows.Err returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to read events from clickhouse: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*errortracking.ErrorTrackingMessageEvent")).Return(nil)
		rows.On("Close").Times(1).Return(nil)
		rows.On("Err").Times(1).Return(mockErr)
		dbMock.On("Query", mock.Anything, expectedQuery, uint64(1), int64(20)).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			ProjectID: 1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Succeeds with 2 messages", func(t *testing.T) {
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Twice()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*errortracking.ErrorTrackingMessageEvent")).Return(nil).Twice().Run(func(args mock.Arguments) {
			e := args.Get(0).(*ErrorTrackingMessageEvent)
			e.Environment = "dev"
			e.Level = "error"
		})
		rows.On("Close").Times(1).Return(nil)
		rows.On("Err").Times(1).Return(nil)
		dbMock.On("Query", mock.Anything, expectedQuery, uint64(1), int64(20)).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			ProjectID: 1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Nil(err)
		assert.Equal(2, len(res))
		for i := 0; i < 2; i++ {
			assert.Equal(models.MessageEvent{Environment: "dev", Level: "error"}, *res[0])
		}
	})
}

func TestNewErrorTrackingSession(t *testing.T) {
	assert := assert.New(t)
	payload := []byte{1, 2}
	projectID := uint64(1234455634)
	s := types.Session{
		SessionID: "1",
		UserID:    "2",
		Init:      uint8(1),
		Started:   time.Date(2022, 11, 17, 20, 34, 58, 651387237, time.UTC),
		OccuredAt: time.Date(2022, 11, 17, 20, 34, 58, 651387237, time.UTC),
		Duration:  0.123,
		Status:    "ok",
		Attributes: types.SessionAttributes{
			Release:     "v1",
			Environment: "dev",
		},
	}
	expectedStmt := "INSERT INTO error_tracking_sessions (\n\t\tproject_id,\n\t\tsession_id,\n\t\tuser_id,\n\t\tinit,\n\t\tpayload,\n\t\tstarted,\n\t\toccurred_at,\n\t\tduration,\n\t\tstatus,\n\t\trelease,\n\t\tenvironment\n\t) VALUES (1234455634,'1','2',1,'\x01\x02',toDateTime64('2022-11-17 20:34:58.651387', 6,'UTC'),toDateTime64('2022-11-17 20:34:58.651387', 6,'UTC'),0.123000,'ok','v1','dev')"

	ets := NewErrorTrackingSession(projectID, &s, payload)

	assert.Equal(projectID, ets.ProjectID)
	assert.Equal(s.SessionID, ets.SessionID)
	assert.Equal(s.UserID, ets.UserID)
	assert.Equal(s.Init, ets.Init)
	assert.Equal(string(payload), ets.Payload)
	assert.Equal(s.Started, ets.Started)
	assert.Equal(s.OccuredAt, ets.OccurredAt)
	assert.Equal(s.Duration, ets.Duration)
	assert.Equal(s.Status, ets.Status)
	assert.Equal(s.Attributes.Release, ets.Release)
	assert.Equal(s.Attributes.Environment, ets.Environment)
	assert.Equal(expectedStmt, ets.AsInsertStmt(time.UTC))
}
