package errortracking

import (
	"context"
	"database/sql"
	"embed"
	"encoding/hex"
	stdErrors "errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/OneOfOne/xxhash"
	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
	"github.com/zeebo/blake3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors_v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

type errorStatus uint8

const (
	errorUnresolved errorStatus = iota
	errorResolved
	errorIgnored
)

type errorStatusStr string

const (
	errorUnresolvedStr = "unresolved"
	errorResolvedStr   = "resolved"
	errorIgnoredStr    = "ignored"
)

var errorStatusToInt = map[errorStatusStr]errorStatus{
	errorUnresolvedStr: errorUnresolved,
	errorResolvedStr:   errorResolved,
	errorIgnoredStr:    errorIgnored,
}

var errorStatusToStr = map[errorStatus]errorStatusStr{
	errorUnresolved: errorUnresolvedStr,
	errorResolved:   errorResolvedStr,
	errorIgnored:    errorIgnoredStr,
}

// Embed all the files in the migrations directory.
//
//go:embed migrations
var dbMigrations embed.FS

//go:embed gcsmigrations
var dbMigrationsGCS embed.FS

type Insertable interface {
	AsInsertStmt(tz *time.Location) string
}

func quote(v string) string {
	return "'" + strings.NewReplacer(`\`, `\\`, `'`, `\'`).Replace(v) + "'"
}

func formatTime(value time.Time, tz *time.Location) string {
	switch value.Location().String() {
	case "Local":
		return fmt.Sprintf("toDateTime64('%d', 6)", value.UnixMicro())
	case tz.String():
		return fmt.Sprintf("toDateTime64('%s', 6,'%s')", value.Format("2006-01-02 15:04:05.000000"), tz.String())
	}
	return fmt.Sprintf("toDateTime64('%s', 6, '%s')",
		value.Format("2006-01-02 15:04:05.000000"),
		value.Location().String(),
	)
}

// ErrorTrackingErrorEvent maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingErrorEvent struct {
	ProjectID      uint64    `ch:"project_id"`
	Fingerprint    uint32    `ch:"fingerprint"`
	Name           string    `ch:"name"`
	Description    string    `ch:"description"`
	Actor          string    `ch:"actor"`
	Environment    string    `ch:"environment"`
	Platform       string    `ch:"platform"`
	Level          string    `ch:"level"`
	UserIdentifier string    `ch:"user_identifier"`
	Payload        string    `ch:"payload"`
	OccurredAt     time.Time `ch:"occurred_at"`
}

func (e ErrorTrackingErrorEvent) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO error_tracking_error_events VALUES (%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
		e.ProjectID,
		e.Fingerprint,
		quote(e.Name),
		quote(e.Description),
		quote(e.Actor),
		quote(e.Environment),
		quote(e.Platform),
		quote(e.Level),
		quote(e.UserIdentifier),
		quote(e.Payload),
		formatTime(e.OccurredAt, tz),
	)
}

// NewErrorTrackingErrorEvent is a helper that returns a ErrorTrackingErrorEvent
// from the given parameters and calculates a fingerprint using a 64-bit xxHash
// algorithm form the error event name, actor and platform.
func NewErrorTrackingErrorEvent(projectID uint64, e *types.Event, payload []byte) *ErrorTrackingErrorEvent {
	// Choose a constant seed as we need same fingerprints for same underlying content
	hash := xxhash.NewS32(100)
	// WriteString never returns an error
	//nolint:errcheck
	hash.WriteString(fmt.Sprintf("%s|%s|%s", e.Name(), e.ExceptionActor(), e.Platform))
	fingerprint32 := hash.Sum32()

	return &ErrorTrackingErrorEvent{
		ProjectID:   projectID,
		Name:        e.Name(),
		Description: e.Description(),
		Actor:       e.ExceptionActor(),
		Platform:    e.Platform,
		OccurredAt:  e.Timestamp,
		Environment: e.Environment,
		Level:       e.Level,
		Payload:     string(payload),
		Fingerprint: fingerprint32,
	}
}

// ErrorTrackingErrorStatus maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingErrorStatus struct {
	ProjectID   uint64 `ch:"project_id"`
	Fingerprint uint32 `ch:"fingerprint"`
	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	Status uint8  `ch:"status"`
	UserID uint64 `ch:"user_id"`
	// Actor is a code:
	//   0 - status changed by user
	//   1 - status changed by system (new event happened after resolve)
	//   2 - status changed by computer (not a user)
	Actor     uint8     `ch:"actor"`
	UpdatedAt time.Time `ch:"updated_at"`
}

func (e ErrorTrackingErrorStatus) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO error_tracking_error_status VALUES (%d,%d,%d,%d,%d,%s)",
		e.ProjectID,
		e.Fingerprint,
		e.Status,
		e.UserID,
		e.Actor,
		formatTime(e.UpdatedAt, tz),
	)
}

// ErrorTrackingError maps to the corresponding table in the clickhouse database.
type ErrorTrackingError struct {
	ProjectID             uint64    `ch:"project_id"`
	Fingerprint           uint32    `ch:"fingerprint"`
	Name                  string    `ch:"name"`
	Description           string    `ch:"description"`
	Actor                 string    `ch:"actor"`
	EventCount            uint64    `ch:"event_count"`
	ApproximatedUserCount uint64    `ch:"approximated_user_count"`
	LastSeenAt            time.Time `ch:"last_seen_at"`
	FirstSeenAt           time.Time `ch:"first_seen_at"`
	Status                uint8     `ch:"status"`
	Ignored               bool      `ch:"ignored"`
}

// ErrorTrackingIgnoredError maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingIgnoredError struct {
	ProjectID   uint64    `ch:"project_id"`
	Fingerprint uint32    `ch:"fingerprint"`
	UserID      uint64    `ch:"user_id"`
	UpdatedAt   time.Time `ch:"updated_at"`
}

func (e ErrorTrackingIgnoredError) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO error_tracking_ignored_errors VALUES (%d,%d,%d,%s)",
		e.ProjectID,
		e.Fingerprint,
		e.UserID,
		formatTime(e.UpdatedAt, tz),
	)
}

// NewErrorTrackingMessageEvent is a helper that returns an ErrorTrackingMessageEvent
// from the given parameters and calculates a fingerprint using a 64-bit xxHash
// algorithm form the message event message and platform.
func NewErrorTrackingMessageEvent(projectID uint64, e *types.Event, payload []byte) *ErrorTrackingMessageEvent {
	fingerprint := blake3.Sum256([]byte(fmt.Sprintf("%s|%s|%s", e.Message, e.MessageActor(payload), e.Platform)))
	return &ErrorTrackingMessageEvent{
		EventID:     e.EventID,
		ProjectID:   projectID,
		Timestamp:   e.Timestamp,
		IsDeleted:   uint8(0),
		Fingerprint: fingerprint[:],
		Environment: e.Environment,
		Level:       e.Level,
		Message:     e.Message,
		Actor:       e.MessageActor(payload),
		Platform:    e.Platform,
		Release:     e.Release,
		ServerName:  e.ServerName,
		Payload:     string(payload),
	}
}

// ErrorTrackingMessageEvent maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingMessageEvent struct {
	EventID     string    `json:"event_id,omitempty" ch:"event_id"`
	ProjectID   uint64    `json:"project_id" ch:"project_id"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	IsDeleted   uint8     `ch:"is_deleted"`
	Fingerprint []byte    `json:"fingerprint" ch:"fingerprint"`

	Environment string `json:"environment,omitempty" ch:"environment"`
	Level       string `json:"level,omitempty" ch:"level"`
	Message     string `json:"message,omitempty" ch:"message"`
	Actor       string `ch:"actor"`
	Platform    string `json:"platform,omitempty" ch:"platform"`
	Release     string `json:"release,omitempty" ch:"release"`
	ServerName  string `json:"server_name,omitempty" ch:"server_name"`
	// sdk and stacktrace_frames are left out for now
	Payload string `json:"payload,omitempty" ch:"payload"`
}

func (e ErrorTrackingMessageEvent) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf(`INSERT INTO error_tracking_message_events (
		event_id,
		project_id,
		timestamp,
		is_deleted,
		fingerprint,
		environment,
		level,
		message,
		actor,
		platform,
		release,
		server_name,
		payload
	) VALUES (%s,%d,%s,%d,unhex(%s),%s,%s,%s,%s,%s,%s,%s,%s)`,
		quote(e.EventID),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		quote(e.Environment),
		quote(e.Level),
		quote(e.Message),
		quote(e.Actor),
		quote(e.Platform),
		quote(e.Release),
		quote(e.ServerName),
		quote(e.Payload),
	)
}

func NewErrorTrackingSession(projectID uint64, e *types.Session, payload []byte) *ErrorTrackingSession {
	return &ErrorTrackingSession{
		ProjectID:   projectID,
		SessionID:   e.SessionID,
		UserID:      e.UserID,
		Init:        e.Init,
		Payload:     string(payload),
		Started:     e.Started,
		OccurredAt:  e.OccuredAt,
		Duration:    e.Duration,
		Status:      e.Status,
		Release:     e.Attributes.Release,
		Environment: e.Attributes.Environment,
	}
}

// ErrorTrackingSession maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingSession struct {
	ProjectID   uint64    `json:"project_id" ch:"project_id"`
	SessionID   string    `json:"session_id" ch:"session_id"`
	UserID      string    `json:"user_id" ch:"user_id"`
	Init        uint8     `json:"init" ch:"init"`
	Payload     string    `json:"payload,omitempty" ch:"payload"`
	Started     time.Time `json:"started" ch:"started"`
	OccurredAt  time.Time `json:"occurred_at" ch:"occurred_at"`
	Duration    float64   `json:"duration,omitempty" ch:"duration"`
	Status      string    `json:"status,omitempty" ch:"status"`
	Release     string    `json:"release,omitempty" ch:"release"`
	Environment string    `json:"environment,omitempty" ch:"environment"`
}

func (e ErrorTrackingSession) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf(`INSERT INTO error_tracking_sessions (
		project_id,
		session_id,
		user_id,
		init,
		payload,
		started,
		occurred_at,
		duration,
		status,
		release,
		environment
	) VALUES (%d,%s,%s,%d,%s,%s,%s,%f,%s,%s,%s)`,
		e.ProjectID,
		quote(e.SessionID),
		quote(e.UserID),
		e.Init,
		quote(e.Payload),
		formatTime(e.Started, tz),
		formatTime(e.OccurredAt, tz),
		e.Duration,
		quote(e.Status),
		quote(e.Release),
		quote(e.Environment),
	)
}

type Database interface {
	GetConn() (*clickhouse.Conn, error)
	GetTZ() (*time.Location, error)
	InsertErrorTrackingErrorEvent(e *ErrorTrackingErrorEvent) error
	ListErrors(params errors.ListErrorsParams) ([]*models.Error, error)
	GetError(params errors.GetErrorParams) (*models.Error, error)
	UpdateError(params errors.UpdateErrorParams) (*models.Error, error)
	ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error)
	DeleteProject(params projects.DeleteProjectParams) error
	InsertErrorEventV2(e *ErrorEventV2) error
	ListErrorsV2(params errors_v2.ListErrorsV2Params) ([]*models.ErrorV2, error)
	InsertErrorTrackingMessageEvent(e *ErrorTrackingMessageEvent) error
	ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error)
	InsertErrorTrackingSession(e *ErrorTrackingSession) error
}

// Implements the Database interface.
type database struct {
	// db holds the clickhouse connection
	conn clickhouse.Conn
	tz   *time.Location
}

type DatabaseOptions struct {
	MaxOpenConns     int
	MaxIdleConns     int
	UseCompression   bool
	UseRemoteStorage string
}

// NOTE: This clickhouse database initialization procedure should move to a new
// location so we can handle migrations.
type tplArgs struct {
	DatabaseName string
}

func renderSQL(args *tplArgs, opts *DatabaseOptions) ([]string, error) {
	var temp *template.Template
	switch opts.UseRemoteStorage {
	case string(common.GCP):
		temp = template.Must(template.New("migrations").ParseFS(dbMigrationsGCS, "gcsmigrations/*"))
	case string(common.AWS):
		// temp = template.Must(template.New("migrations").ParseFS(dbMigrationsS3, "s3migrations/*"))
	default:
		temp = template.Must(template.New("migrations").ParseFS(dbMigrations, "migrations/*"))
	}

	var (
		tpls    = temp.Templates()
		results = make([]string, len(tpls))
	)
	// Replace the database placeholder with the provided name
	// This is necessary because Clickhouse misses expansion of source tables when used with `ON CLUSTER`.
	// See https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1646#note_1024886551
	if args.DatabaseName == "" {
		args.DatabaseName = "default"
	}

	sort.SliceStable(tpls, func(i, j int) bool { return tpls[i].Name() < tpls[j].Name() })

	for i, tpl := range tpls {
		statement := strings.Builder{}
		err := tpl.Execute(&statement, args)
		if err != nil {
			return nil, fmt.Errorf("failed to render errortracking sql %w", err)
		}
		results[i] = statement.String()
	}
	return results, nil
}

func NewDB(clickHouseDsn string, opts *DatabaseOptions) (Database, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = opts.MaxOpenConns
	dbOpts.MaxIdleConns = opts.MaxIdleConns
	dbOpts.ConnMaxLifetime = 1 * time.Hour

	// Requirement per
	// https://gitlab.com/ahegyi/error-tracking-data-generator#example-queries
	dbOpts.Settings["join_use_nulls"] = 1

	if opts.UseCompression {
		dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}
	}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	migrations, err := renderSQL(&tplArgs{DatabaseName: dbOpts.Auth.Database}, opts)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	for _, entry := range migrations {
		err = conn.Exec(ctx, entry)
		if err != nil {
			return nil, fmt.Errorf("can't process migration %w", err)
		}
	}

	params, err := conn.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("getting server version: %w", err)
	}

	return &database{
		conn: conn,
		tz:   params.Timezone,
	}, nil
}

func (db *database) GetConn() (*clickhouse.Conn, error) {
	return &db.conn, nil
}

func (db *database) GetTZ() (*time.Location, error) {
	return db.tz, nil
}

// InsertErrorTrackingErrorEvent inserts the given error event in the
// error_tracking_error_event table in the clickhouse database. It then proceeds
// to upsert error_tracking_error_status as well.
func (db *database) InsertErrorTrackingErrorEvent(e *ErrorTrackingErrorEvent) error {
	err := db.insert(e)
	if err != nil {
		return fmt.Errorf("inserting error tracking error event: %w", err)
	}

	s := &ErrorTrackingErrorStatus{
		ProjectID:   e.ProjectID,
		Fingerprint: e.Fingerprint,
		Status:      uint8(errorUnresolved),
		UserID:      uint64(0),
		Actor:       uint8(2),
		UpdatedAt:   time.Now(),
	}

	err = db.insert(s)
	if err != nil {
		return fmt.Errorf("inserting error tracking error status: %w", err)
	}

	return nil
}

// helper function to insert the given value
//
// TODO: https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1732
// Check db.conn.AsyncInsert but handle SQL injection or find an alternative.
func (db *database) insert(e Insertable) error {
	ctx := context.Background()
	//nolint:wrapcheck
	return db.conn.AsyncInsert(ctx, e.AsInsertStmt(db.tz), false)
}

func (db *database) ListErrors(params errors.ListErrorsParams) ([]*models.Error, error) {
	ctx := context.Background()
	var result []*models.Error

	query, args, err := buildListErrorsQuery(params)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to query clickhouse: %w", err)
	}
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struct
		e := &ErrorTrackingError{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
		}
		status, ok := errorStatusToStr[errorStatus(e.Status)]
		if !ok {
			return nil, fmt.Errorf("unexpected error status value %v", e.Status)
		}
		// Convert the ErrorTrackingError struct to a models.Error. This was
		// done because golang doesn't allow adding tags dinamically at runtime
		// to structs to be able to decode a models.Error which only defines
		// json tags.
		result = append(result, &models.Error{
			Actor:                 e.Actor,
			ApproximatedUserCount: e.ApproximatedUserCount,
			Description:           e.Description,
			EventCount:            e.EventCount,
			Fingerprint:           e.Fingerprint,
			FirstSeenAt:           strfmt.DateTime(e.FirstSeenAt),
			LastSeenAt:            strfmt.DateTime(e.LastSeenAt),
			Name:                  e.Name,
			ProjectID:             e.ProjectID,
			Status:                string(status),
		})
	}
	rows.Close()

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read errors from clickhouse: %w", err)
	}
	return result, nil
}

const baseQuery = `
SELECT
    error_tracking_errors.project_id AS project_id,
    error_tracking_errors.fingerprint AS fingerprint,
    error_tracking_errors.name AS name,
    error_tracking_errors.description AS description,
    error_tracking_errors.actor AS actor,
    error_tracking_errors.event_count AS event_count,
    error_tracking_errors.approximated_user_count AS approximated_user_count,
    error_tracking_errors.last_seen_at AS last_seen_at,
    error_tracking_errors.first_seen_at AS first_seen_at,
    COALESCE(error_tracking_error_status.status, 1) AS status,
    COALESCE(error_tracking_ignored_errors.ignored, FALSE) AS ignored
FROM (
SELECT
    project_id,
    fingerprint,
    any(name) as name,
    any(description) as description,
    any(actor) as actor,
    sum(event_count) as event_count,
    uniqMerge(approximated_user_count) as approximated_user_count,
    max(last_seen_at) as last_seen_at,
    min(first_seen_at) as first_seen_at
  FROM error_tracking_errors_mv
  GROUP BY project_id, fingerprint
) as error_tracking_errors
LEFT JOIN (
  SELECT project_id, argMax(status, updated_at) as status, fingerprint
  FROM error_tracking_error_status
  GROUP BY project_id, fingerprint
) error_tracking_error_status ON error_tracking_error_status.project_id = error_tracking_errors.project_id AND
  error_tracking_error_status.fingerprint = error_tracking_errors.fingerprint
LEFT JOIN (
  SELECT project_id, fingerprint, TRUE AS ignored
  FROM error_tracking_ignored_errors
  GROUP BY project_id, fingerprint
) error_tracking_ignored_errors ON error_tracking_ignored_errors.project_id = error_tracking_errors.project_id AND
  error_tracking_ignored_errors.fingerprint = error_tracking_errors.fingerprint
`

// Helper struct to help construct a sql query.
type queryBuilder struct {
	sql  string
	args []interface{}
	idx  int
}

func (s *queryBuilder) reset(sql string) {
	s.sql = sql
	s.idx = 0
	s.args = make([]interface{}, 0)
}

// build takes the given sql string replaces any ? with the equivalent $<idx>
// and appends elems to the args slice.
func (s *queryBuilder) build(stmt string, elems ...interface{}) {
	// add the query params to the args slice, if any
	s.args = append(s.args, elems...)
	q := stmt
	// replace ? with corresponding $<idx>
	for range elems {
		s.idx += 1
		// placeholder that builds the string, for example, $1 when idx is 1
		p := fmt.Sprintf("$%d", s.idx)
		// replace the first ? found in the string
		q = strings.Replace(q, "?", p, 1)
	}
	// add the sanitized query statement to the current sql query
	s.sql += q
}

func buildListErrorsQuery(params errors.ListErrorsParams) (string, []interface{}, error) {
	q := &queryBuilder{}
	q.reset(baseQuery)

	q.build("WHERE project_id = ?", params.ProjectID)

	// Status default value is unresolved so we can skip the nil check.
	status, ok := errorStatusToInt[errorStatusStr(*params.Status)]
	if !ok {
		return "", nil, fmt.Errorf("unexpected error status %v", *params.Status)
	}
	if status == errorIgnored {
		q.build(" AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = TRUE")
	} else {
		q.build(
			" AND COALESCE(error_tracking_ignored_errors.ignored, FALSE) = FALSE AND error_tracking_error_status.status = ?",
			uint8(status),
		)
	}

	if params.Query != nil && len(*params.Query) > 2 {
		// clickhouse ILIKE search operator uses percentage character % to match
		// any byte. golang requires %% to escape the percentage % character.
		wildcard := fmt.Sprintf("%%%s%%", *params.Query)
		q.build(
			" AND (error_tracking_errors.name ILIKE ? OR error_tracking_errors.description ILIKE ?)",
			wildcard,
			wildcard,
		)
	}

	// Sort default value is last_seen_desc so we can skip the nil check.
	orderBy := " ORDER BY"
	switch *params.Sort {
	case "first_seen_desc":
		orderBy += " first_seen_at DESC, fingerprint DESC"
	case "frequency_desc":
		orderBy += " event_count DESC, fingerprint DESC"
	default:
		orderBy += " last_seen_at DESC, fingerprint DESC"
	}
	q.build(orderBy)

	q.build(" LIMIT ?", *params.Limit)

	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", nil, err
	}
	// Limit default value is 20 so we can skip the nil check.
	offset := (page - 1) * int(*params.Limit)
	q.build(" OFFSET ?", offset)

	return q.sql, q.args, err
}

func (db *database) GetError(params errors.GetErrorParams) (*models.Error, error) {
	ctx := context.Background()
	var result *models.Error

	query, args := buildGetErrorQuery(params)

	row := db.conn.QueryRow(ctx, query, args...)
	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to query error : %w", err)
	}

	// Scan the row as a ErrorTrackingError struct
	e := &ErrorTrackingError{}
	err := row.ScanStruct(e)

	if err != nil {
		if stdErrors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
	}
	status, ok := errorStatusToStr[errorStatus(e.Status)]
	if !ok {
		return nil, fmt.Errorf("unexpected error status: %v", e.Status)
	}
	// Convert the ErrorTrackingError struct to a models.Error. This was
	// done because golang doesn't allow adding tags dinamically at runtime
	// to structs to be able to decode a models.Error which only defines
	// json tags.
	result = &models.Error{
		Actor:                 e.Actor,
		ApproximatedUserCount: e.ApproximatedUserCount,
		Description:           e.Description,
		EventCount:            e.EventCount,
		Fingerprint:           e.Fingerprint,
		FirstSeenAt:           strfmt.DateTime(e.FirstSeenAt),
		LastSeenAt:            strfmt.DateTime(e.LastSeenAt),
		Name:                  e.Name,
		ProjectID:             e.ProjectID,
		Status:                string(status),
	}

	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to read error from clickhouse: %w", err)
	}
	return result, nil
}

func buildGetErrorQuery(params errors.GetErrorParams) (string, []interface{}) {
	q := &queryBuilder{}
	q.reset(baseQuery)

	q.build("WHERE project_id = ? AND fingerprint = ?",
		params.ProjectID,
		params.Fingerprint,
	)

	return q.sql, q.args
}

// See: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86544/diffs#7e6d17e7e7c8bf3957ef0d12127876ba75b05591_0_35
func (db *database) UpdateError(params errors.UpdateErrorParams) (*models.Error, error) {
	// Check the error exists before proceeding.
	res, err := db.GetError(errors.GetErrorParams{ProjectID: params.ProjectID, Fingerprint: params.Fingerprint})
	if err != nil {
		return nil, err
	}

	if res == nil {
		return nil, fmt.Errorf("failed to find the corresponding error for fingerprint: %v", params.Fingerprint)
	}
	if errorStatusStr(params.Body.Status) == errorIgnoredStr {
		err = db.InsertErrorTrackingIgnoredError(params)
	} else {
		err = db.UpdateErrorTrackingIgnoredError(params)
	}

	// The status was updated but the object doesn't yet reflect that change. To
	// avoid doing another query we return the object with the updated status.
	if err == nil {
		res.Status = params.Body.Status
	}

	return res, err
}

func (db *database) InsertErrorTrackingIgnoredError(params errors.UpdateErrorParams) error {
	value := &ErrorTrackingIgnoredError{
		ProjectID:   params.ProjectID,
		Fingerprint: params.Fingerprint,
		UserID:      uint64(params.Body.UpdatedByID),
		UpdatedAt:   time.Now(),
	}
	return db.insert(value)
}

func (db *database) UpdateErrorTrackingIgnoredError(params errors.UpdateErrorParams) error {
	status, ok := errorStatusToInt[errorStatusStr(params.Body.Status)]
	if !ok {
		return fmt.Errorf("unexpected error status: %v", params.Body.Status)
	}
	errorStatus := &ErrorTrackingErrorStatus{
		ProjectID:   params.ProjectID,
		Fingerprint: params.Fingerprint,
		Status:      uint8(status),
		UserID:      uint64(params.Body.UpdatedByID),
		Actor:       uint8(0),
		UpdatedAt:   time.Now(),
	}
	err := db.insert(errorStatus)
	if err != nil {
		return err
	}

	q := &queryBuilder{}
	q.reset("ALTER TABLE error_tracking_ignored_errors DELETE WHERE")
	q.build(" project_id = ?", params.ProjectID)
	q.build(" AND fingerprint = ? ", params.Fingerprint)

	err = db.conn.Exec(context.Background(), q.sql, q.args...)
	if err != nil {
		return fmt.Errorf("failed to update error: %w", err)
	}
	return nil
}

func (db *database) ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error) {
	ctx := context.Background()
	var result []*models.ErrorEvent

	query, args, err := buildListEventsQuery(params)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to list events: %w", err)
	}
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struct
		e := &ErrorTrackingErrorEvent{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingErrorEvent: %w", err)
		}
		// Convert the ErrorTrackingError struct to a models.Error. This was
		// done because golang doesn't allow adding tags dinamically at runtime
		// to structs to be able to decode a models.Error which only defines
		// json tags.
		result = append(result, &models.ErrorEvent{
			Actor:       e.Actor,
			Description: e.Description,
			Environment: e.Environment,
			Fingerprint: e.Fingerprint,
			Name:        e.Name,
			Payload:     e.Payload,
			Platform:    e.Platform,
			ProjectID:   e.ProjectID,
		})
	}
	rows.Close()

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

func buildListEventsQuery(params errors.ListEventsParams) (string, []interface{}, error) {
	q := &queryBuilder{}
	q.reset("SELECT * FROM error_tracking_error_events")

	q.build(" WHERE project_id = ?", params.ProjectID)
	q.build(" AND fingerprint = ? ", params.Fingerprint)

	// Sort default value is last_seen_desc so we can skip the nil check.
	orderBy := " ORDER BY"
	switch *params.Sort {
	case "occurred_at_asc":
		orderBy += " occurred_at ASC"
	case "occurred_at_desc":
		orderBy += " occurred_at DESC"
	}
	q.build(orderBy)

	q.build(" LIMIT ?", *params.Limit)

	page, err := decodePage(params.Cursor)
	if err != nil {
		return "", nil, err
	}
	// Limit default value is 20 so we can skip the nil check.
	offset := (page - 1) * int(*params.Limit)
	q.build(" OFFSET ?", offset)

	return q.sql, q.args, err
}

func (db *database) DeleteProject(params projects.DeleteProjectParams) error {
	ctx := context.Background()
	q := &queryBuilder{}

	runDeleteQuery := func(query string) error {
		q.reset(query)
		q.build(" project_id = ?", params.ID)

		err := db.conn.Exec(ctx, q.sql, q.args)
		if err != nil {
			return fmt.Errorf("failed to delete project %w", err)
		}
		return nil
	}
	err := runDeleteQuery("ALTER TABLE error_tracking_error_events DELETE WHERE")
	if err != nil {
		return err
	}
	err = runDeleteQuery("ALTER TABLE error_tracking_error_status DELETE WHERE")
	if err != nil {
		return err
	}

	return err
}

const (
	errorEventsTableV2 = "error_tracking_events_v2"
	errorStatusTableV2 = "error_tracking_error_status_v2"
)

type ErrorEventV2 struct {
	Environment string    `json:"environment,omitempty"`
	EventID     uuid.UUID `json:"event_id,omitempty" ch:"event_id"`
	Level       string    `json:"level,omitempty" ch:"level"`
	Message     string    `json:"message,omitempty" ch:"message"`
	Platform    string    `json:"platform,omitempty" ch:"platform"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	Transaction string    `json:"transaction,omitempty" ch:"transaction"`
	// Exception can be an object with the attribute values [] or a flat list of objects.
	// See https://develop.sentry.dev/sdk/event-payloads/exception/.
	// This is field is decoded in a special way
	Exception   []*types.Exception `json:"-"`
	Fingerprint []byte             `ch:"fingerprint"`
	Actor       string             `ch:"actor"`
	IsDeleted   uint8              `ch:"is_deleted"`
	ProjectID   uint64             `ch:"project_id"`
	Payload     string
}

type ErrorStatusV2 struct {
	ProjectID   uint64    `ch:"project_id"`
	EventID     uuid.UUID `json:"event_id,omitempty" ch:"event_id"`
	Fingerprint []byte    `ch:"fingerprint"`
	IsDeleted   uint8     `ch:"is_deleted"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	Status uint8  `ch:"status"`
	UserID string `ch:"user_id"`
	Actor  string `ch:"actor"`
	// TODO: Title needs to be computed
	Title string `ch:"title"`

	UpdatedAt time.Time `ch:"updated_at"`
	// UpdatedBy is a code:
	//   0 - status changed by user
	//   1 - status changed by system (new event happened after resolve)
	//   2 - status changed by computer (not a user)
	UpdatedBy uint8 `ch:"updated_by"`
}

type ErrorV2 struct {
	ProjectID uint64 `ch:"project_id"`
	// Currently this ID is same as the fingerprint calculated from the error event
	ID          string `json:"id,omitempty" ch:"_id"`
	Fingerprint []byte `ch:"fingerprint"`

	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	//   2 - ignored
	Status uint8  `ch:"status"`
	Actor  string `ch:"actor"`
	// TODO: Title needs to be computed
	Title string `ch:"title"`

	Count       uint64    `ch:"count"`
	UserCount   uint64    `ch:"user_count"`
	LastSeenAt  time.Time `ch:"_last_seen_at"`
	FirstSeenAt time.Time `ch:"_first_seen_at"`
}

func NewErrorEventV2(projectID uint64, e *types.Event, payload []byte) (*ErrorEventV2, error) {
	// TODO: Tune fingerprinting of the event
	fingerprint := blake3.Sum256([]byte(fmt.Sprintf("%s|%s|%s", e.Name(), e.ExceptionActor(), e.Platform)))

	eventID, err := formatUUID(e.EventID)
	if err != nil {
		return nil, err
	}
	return &ErrorEventV2{
		ProjectID:   projectID,
		Actor:       e.ExceptionActor(),
		Platform:    e.Platform,
		Environment: e.Environment,
		Level:       e.Level,
		Payload:     string(payload),
		Fingerprint: fingerprint[:],
		Timestamp:   e.Timestamp,
		EventID:     eventID,
		Message:     e.Message,
		Transaction: e.Transaction,
		IsDeleted:   uint8(0),
	}, nil
}

func (e *ErrorEventV2) AsInsertStmt(tz *time.Location) string {
	stmt := `
INSERT INTO %s (
	event_id,
	project_id,
	timestamp,
	is_deleted,
	fingerprint,
	actor,
	platform,
	environment,
	level,
	payload,
	message,
	transaction
) VALUES (toUUID(%s), %d, %s, %d, unhex(%s), %s, %s, %s, %s, %s, %s, %s)`

	return fmt.Sprintf(stmt,
		errorEventsTableV2,
		quote(e.EventID.String()),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		quote(e.Actor),
		quote(e.Platform),
		quote(e.Environment),
		quote(e.Level),
		quote(e.Payload),
		quote(e.Message),
		quote(e.Transaction),
	)
}

func formatUUID(id string) (uuid.UUID, error) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to parse as UUID %w", err)
	}
	return uid, nil
}

func (e ErrorStatusV2) AsInsertStmt(tz *time.Location) string {
	//nolint:lll
	stmt := `INSERT INTO %s (event_id, project_id, timestamp, is_deleted, fingerprint, status, actor, updated_by, updated_at)
	VALUES (
		toUUID(%s),
	    %d,
	    %s,
	    %d,
	    unhex(%s),
	    %d,
	    %s,
	    %d,
	    %s)
`
	query := fmt.Sprintf(stmt,
		errorStatusTableV2,
		quote(e.EventID.String()),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		e.Status,
		quote(e.Actor),
		e.UpdatedBy,
		formatTime(e.UpdatedAt, tz),
	)

	return query
}

// InsertErrorEventV2 inserts the given error event in the
// error_tracking_error_event_v2 table in the clickhouse database. It then proceeds
// to upsert error_tracking_error_status_v2 as well.
func (db *database) InsertErrorEventV2(e *ErrorEventV2) error {
	err := db.insert(e)
	if err != nil {
		return err
	}

	errorStatus := &ErrorStatusV2{
		ProjectID:   e.ProjectID,
		EventID:     e.EventID,
		Timestamp:   e.Timestamp,
		IsDeleted:   e.IsDeleted,
		Fingerprint: e.Fingerprint,
		Actor:       e.Actor,
		//Title:     e.Title, // TODO: Title is to be computed.
		//UserID: // TODO: Compute userID if exists from the event.
		Status:    uint8(errorUnresolved),
		UpdatedBy: uint8(2),
		UpdatedAt: time.Now(),
	}
	return db.insert(errorStatus)
}

const baseQueryErrorsV2 = `SELECT
    project_id,
    lower(hex(id)) as _id,
    sum(error_count) AS count,
    argMaxMerge(status) AS status,
    any(actor) as actor,
    uniqMerge(approximated_user_count) AS user_count,
    max(last_seen_at) AS _last_seen_at,
    min(first_seen_at) AS _first_seen_at
FROM error_tracking_group
`
const groupByErros = `
GROUP BY
project_id,
id
`

const baseQueryListMessages = `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM error_tracking_message_events
`

const defaultListIssueLimit = 1000

func (db *database) ListErrorsV2(params errors_v2.ListErrorsV2Params) ([]*models.ErrorV2, error) {
	ctx := context.Background()
	var result []*models.ErrorV2

	query, args, err := buildListErrorsV2Query(params)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to list events: %w", err)
	}
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struc....
		e := &ErrorV2{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorV2: %w", err)
		}
		status, ok := errorStatusToStr[errorStatus(e.Status)]
		if !ok {
			return nil, fmt.Errorf("unexpected error status value %v", e.Status)
		}
		result = append(result, &models.ErrorV2{
			Actor:     e.Actor,
			UserCount: e.UserCount,
			Count:     strconv.Itoa(int(e.Count)),
			FirstSeen: strfmt.DateTime(e.FirstSeenAt),
			ID:        e.ID,
			LastSeen:  strfmt.DateTime(e.LastSeenAt),
			Project: &models.Project{
				ID: strconv.Itoa(int(e.ProjectID)),
			},
			Status: string(status),
			//Title:                 "",
		})
	}
	err = rows.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close connection while reading from clickhouse: %w", err)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

func buildListErrorsV2Query(params errors_v2.ListErrorsV2Params) (string, []interface{}, error) {
	q := &queryBuilder{}
	q.reset(baseQueryErrorsV2)

	q.build("WHERE project_id IN ( ? ) ", params.Project)

	q.build(groupByErros)
	// Status default value is unresolved so we can skip the nil check.
	status, ok := errorStatusToInt[errorStatusStr(*params.Status)]
	if !ok {
		return "", nil, fmt.Errorf("unexpected error status %v", *params.Status)
	}
	q.build(
		" HAVING status = ?",
		uint8(status),
	)
	if params.Start != nil {
		start, err := time.Parse("2006-01-02T15:04:05", *params.Start)
		if err != nil {
			return "", nil, fmt.Errorf("failed to parse `start` param: %w", err)
		}
		q.build(" AND _last_seen_at >= ?", start)
	}

	if params.End != nil {
		end, err := time.Parse("2006-01-02T15:04:05", *params.End)
		if err != nil {
			return "", nil, fmt.Errorf("failed to parse `end` param: %w", err)
		}
		q.build(" AND _last_seen_at <= ?", end)
	}

	// TODO: query param handling
	// See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2002
	// if params.Query != nil && len(*params.Query) > 2 {
	//	// clickhouse ILIKE search operator uses percentage character % to match
	//	// any byte. golang requires %% to escape the percentage % character.
	//	wildcard := fmt.Sprintf("%%%s%%", *params.Query)
	//	q.build(
	//		" AND (error_tracking_errors.name ILIKE ? OR error_tracking_errors.description ILIKE ?)",
	//		wildcard,
	//		wildcard,
	//	)
	// }

	orderBy := ""
	const (
		orderByDate = " ORDER BY _last_seen_at DESC"
		orderByNew  = " ORDER BY _first_seen_at DESC"
		orderByFreq = " ORDER BY count DESC"
		orderByUser = " ORDER BY user_count DESC"
	)
	if params.Sort != nil {
		switch *params.Sort {
		case "date":
			orderBy = orderByDate
		case "new":
			orderBy = orderByNew
		case "freq":
			orderBy = orderByFreq
		case "user":
			orderBy = orderByUser
		default:
			orderBy = orderByDate
		}
	} else {
		orderBy = orderByDate
	}
	q.build(orderBy)

	if params.Limit != nil {
		q.build(" LIMIT ?", *params.Limit)
	} else {
		q.build(" LIMIT ?", defaultListIssueLimit)
	}

	return q.sql, q.args, nil
}

// InsertErrorTrackingMessageEvent inserts the given message event in the
// error_tracking_message_event table in the clickhouse database.
func (db *database) InsertErrorTrackingMessageEvent(e *ErrorTrackingMessageEvent) error {
	err := db.insert(e)
	if err != nil {
		return fmt.Errorf("inserting error tracking message event: %w", err)
	}

	return nil
}

func (db *database) ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error) {
	ctx := context.Background()
	var result []*models.MessageEvent

	query, args := buildListMessageQuery(params)

	rows, err := db.conn.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("failed to list messages: %w", err)
	}
	for rows.Next() {
		e := &ErrorTrackingMessageEvent{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingMessageEvent: %w", err)
		}

		result = append(result, &models.MessageEvent{
			Environment: e.Environment,
			EventID:     e.EventID,
			Level:       e.Level,
			Message:     e.Message,
			Platform:    e.Platform,
			ProjectID:   e.ProjectID,
			Release:     e.Release,
			Timestamp:   strfmt.DateTime(e.Timestamp),
		})
	}
	err = rows.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close connection while reading from clickhouse: %w", err)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

func buildListMessageQuery(params messages.ListMessagesParams) (string, []interface{}) {
	q := &queryBuilder{}
	q.reset(baseQueryListMessages)

	q.build("WHERE project_id IN ( ? ) ", params.ProjectID)

	if params.Limit != nil {
		q.build(" LIMIT ?", *params.Limit)
	} else {
		q.build(" LIMIT ?", defaultListIssueLimit)
	}

	return q.sql, q.args
}

// InsertErrorTrackingSession inserts the given session in the
// error_tracking_session table in the clickhouse database.
func (db *database) InsertErrorTrackingSession(e *ErrorTrackingSession) error {
	err := db.insert(e)
	if err != nil {
		return fmt.Errorf("inserting error tracking session: %w", err)
	}

	return nil
}
