// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"mime"
	"net/http"
	"regexp"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations"
	errorsops "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/events"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/metrics"
)

//go:generate swagger generate server --target ../../gen --name ErrorTracking --spec ../../swagger.yaml --principal interface{} --exclude-main

func configureFlags(api *operations.ErrorTrackingAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.ErrorTrackingAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.EventsPostProjectsAPIProjectIDEnvelopeHandler == nil {
		api.EventsPostProjectsAPIProjectIDEnvelopeHandler = events.PostProjectsAPIProjectIDEnvelopeHandlerFunc(func(params events.PostProjectsAPIProjectIDEnvelopeParams) middleware.Responder {
			return middleware.NotImplemented("operation events.PostProjectsAPIProjectIDEnvelope has not yet been implemented")
		})
	}
	if api.EventsPostProjectsAPIProjectIDStoreHandler == nil {
		api.EventsPostProjectsAPIProjectIDStoreHandler = events.PostProjectsAPIProjectIDStoreHandlerFunc(func(params events.PostProjectsAPIProjectIDStoreParams) middleware.Responder {
			return middleware.NotImplemented("operation events.PostProjectsAPIProjectIDStore has not yet been implemented")
		})
	}
	if api.ProjectsDeleteProjectHandler == nil {
		api.ProjectsDeleteProjectHandler = projects.DeleteProjectHandlerFunc(func(params projects.DeleteProjectParams) middleware.Responder {
			return middleware.NotImplemented("operation projects.DeleteProject has not yet been implemented")
		})
	}
	if api.ErrorsGetErrorHandler == nil {
		api.ErrorsGetErrorHandler = errorsops.GetErrorHandlerFunc(func(params errorsops.GetErrorParams) middleware.Responder {
			return middleware.NotImplemented("operation errors.GetError has not yet been implemented")
		})
	}
	if api.ErrorsListErrorsHandler == nil {
		api.ErrorsListErrorsHandler = errorsops.ListErrorsHandlerFunc(func(params errorsops.ListErrorsParams) middleware.Responder {
			return middleware.NotImplemented("operation errors.ListErrors has not yet been implemented")
		})
	}
	if api.ErrorsListEventsHandler == nil {
		api.ErrorsListEventsHandler = errorsops.ListEventsHandlerFunc(func(params errorsops.ListEventsParams) middleware.Responder {
			return middleware.NotImplemented("operation errors.ListEvents has not yet been implemented")
		})
	}
	if api.ErrorsUpdateErrorHandler == nil {
		api.ErrorsUpdateErrorHandler = errorsops.UpdateErrorHandlerFunc(func(params errorsops.UpdateErrorParams) middleware.Responder {
			return middleware.NotImplemented("operation errors.UpdateError has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also
// applies to serving the swagger.json document. So this is a good place to plug
// in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			ct := r.Header.Get("Content-Type")
			mt := ""
			if ct != "" { // mime.ParseMediaType panics on an empty content-type
				mt, _, _ = mime.ParseMediaType(ct)
			}
			// Sentry NodeJS sdk doesn't set the content-type header in post
			// requests to the store and envelope endpoints. Other Sentry clients can set
			// the content-type to other values. We override them here to
			// some values to make the go-swagger framework happy and forward
			// the request to the proper handlers.
			switch mt {
			case "", "application/x-sentry-envelope", "application/octet-stream", "text/plain":
				r.Header.Set("Content-Type", "application/json")
				r.Header.Set("Content-Length", "0")
			}
		}
		// also wrap each handler with our common Prometheus HTTP metrics middleware
		metrics.PrometheusMiddleware(next, stripPath).ServeHTTP(w, r)
	})
}

var (
	ApiStorePath    = regexp.MustCompile(`projects/api/(\d+)/store`)
	ApiEnvelopePath = regexp.MustCompile(`projects/api/(\d+)/envelope`)
	// ErrorsPath includes paths for listing errors and describing an error
	ErrorsPath      = regexp.MustCompile(`projects/(\d+)/errors`)
	ErrorEventsPath = regexp.MustCompile(`projects/(\d+)/errors/(\d+)/events`)
)

const (
	StrippedStorePath       = "projects/api/store"
	StrippedEnvelopePath    = "projects/api/envelope"
	StrippedErrorEventsPath = "projects/errors/events"
	StrippedErrorsPath      = "projects/errors"
)

// stripPath removes projectID/fingerprints from the URL path
// Active URL paths can be looked in go/pkg/errortracking/swagger.yaml
// Currently active schemes are of form : projects/{projectID}/... or projects/api/{projectID}/...
// Note: `regexp` guarantees that the match will finish in time linear to size of input. As paths itself are quite
// small, we don't expect any significant performance delay with this function.
func stripPath(path string) string {

	if ApiStorePath.MatchString(path) {
		return StrippedStorePath
	} else if ApiEnvelopePath.MatchString(path) {
		return StrippedEnvelopePath
	} else if ErrorEventsPath.MatchString(path) {
		return StrippedErrorEventsPath
	} else if ErrorsPath.MatchString(path) {
		return StrippedErrorsPath
	}

	return path
}
