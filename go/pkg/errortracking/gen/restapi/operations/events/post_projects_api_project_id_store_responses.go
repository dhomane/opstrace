// Code generated by go-swagger; DO NOT EDIT.

package events

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
)

// PostProjectsAPIProjectIDStoreOKCode is the HTTP code returned for type PostProjectsAPIProjectIDStoreOK
const PostProjectsAPIProjectIDStoreOKCode int = 200

/*
PostProjectsAPIProjectIDStoreOK Accepted. Event will be created async

swagger:response postProjectsApiProjectIdStoreOK
*/
type PostProjectsAPIProjectIDStoreOK struct {

	/*
	  In: Body
	*/
	Payload *models.ErrorEvent `json:"body,omitempty"`
}

// NewPostProjectsAPIProjectIDStoreOK creates PostProjectsAPIProjectIDStoreOK with default headers values
func NewPostProjectsAPIProjectIDStoreOK() *PostProjectsAPIProjectIDStoreOK {

	return &PostProjectsAPIProjectIDStoreOK{}
}

// WithPayload adds the payload to the post projects Api project Id store o k response
func (o *PostProjectsAPIProjectIDStoreOK) WithPayload(payload *models.ErrorEvent) *PostProjectsAPIProjectIDStoreOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the post projects Api project Id store o k response
func (o *PostProjectsAPIProjectIDStoreOK) SetPayload(payload *models.ErrorEvent) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PostProjectsAPIProjectIDStoreOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PostProjectsAPIProjectIDStoreBadRequestCode is the HTTP code returned for type PostProjectsAPIProjectIDStoreBadRequest
const PostProjectsAPIProjectIDStoreBadRequestCode int = 400

/*
PostProjectsAPIProjectIDStoreBadRequest Bad request

swagger:response postProjectsApiProjectIdStoreBadRequest
*/
type PostProjectsAPIProjectIDStoreBadRequest struct {
}

// NewPostProjectsAPIProjectIDStoreBadRequest creates PostProjectsAPIProjectIDStoreBadRequest with default headers values
func NewPostProjectsAPIProjectIDStoreBadRequest() *PostProjectsAPIProjectIDStoreBadRequest {

	return &PostProjectsAPIProjectIDStoreBadRequest{}
}

// WriteResponse to the client
func (o *PostProjectsAPIProjectIDStoreBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(400)
}

// PostProjectsAPIProjectIDStoreInternalServerErrorCode is the HTTP code returned for type PostProjectsAPIProjectIDStoreInternalServerError
const PostProjectsAPIProjectIDStoreInternalServerErrorCode int = 500

/*
PostProjectsAPIProjectIDStoreInternalServerError Internal error

swagger:response postProjectsApiProjectIdStoreInternalServerError
*/
type PostProjectsAPIProjectIDStoreInternalServerError struct {
}

// NewPostProjectsAPIProjectIDStoreInternalServerError creates PostProjectsAPIProjectIDStoreInternalServerError with default headers values
func NewPostProjectsAPIProjectIDStoreInternalServerError() *PostProjectsAPIProjectIDStoreInternalServerError {

	return &PostProjectsAPIProjectIDStoreInternalServerError{}
}

// WriteResponse to the client
func (o *PostProjectsAPIProjectIDStoreInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(500)
}
