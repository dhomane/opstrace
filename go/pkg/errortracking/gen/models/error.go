// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"encoding/json"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// Error error
//
// swagger:model Error
type Error struct {

	// actor
	// Example: PostsController#edit
	Actor string `json:"actor,omitempty"`

	// approximated user count
	ApproximatedUserCount uint64 `json:"approximated_user_count,omitempty"`

	// description
	// Example: Missing template posts/edit
	Description string `json:"description,omitempty"`

	// event count
	EventCount uint64 `json:"event_count,omitempty"`

	// fingerprint
	Fingerprint uint32 `json:"fingerprint,omitempty"`

	// first seen at
	// Format: date-time
	FirstSeenAt strfmt.DateTime `json:"first_seen_at,omitempty"`

	// last seen at
	// Format: date-time
	LastSeenAt strfmt.DateTime `json:"last_seen_at,omitempty"`

	// name
	// Example: ActionView::MissingTemplate
	Name string `json:"name,omitempty"`

	// project id
	ProjectID uint64 `json:"project_id,omitempty"`

	// Status of the error
	// Enum: [unresolved resolved ignored]
	Status string `json:"status,omitempty"`
}

// Validate validates this error
func (m *Error) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateFirstSeenAt(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLastSeenAt(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateStatus(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Error) validateFirstSeenAt(formats strfmt.Registry) error {
	if swag.IsZero(m.FirstSeenAt) { // not required
		return nil
	}

	if err := validate.FormatOf("first_seen_at", "body", "date-time", m.FirstSeenAt.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *Error) validateLastSeenAt(formats strfmt.Registry) error {
	if swag.IsZero(m.LastSeenAt) { // not required
		return nil
	}

	if err := validate.FormatOf("last_seen_at", "body", "date-time", m.LastSeenAt.String(), formats); err != nil {
		return err
	}

	return nil
}

var errorTypeStatusPropEnum []interface{}

func init() {
	var res []string
	if err := json.Unmarshal([]byte(`["unresolved","resolved","ignored"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		errorTypeStatusPropEnum = append(errorTypeStatusPropEnum, v)
	}
}

const (

	// ErrorStatusUnresolved captures enum value "unresolved"
	ErrorStatusUnresolved string = "unresolved"

	// ErrorStatusResolved captures enum value "resolved"
	ErrorStatusResolved string = "resolved"

	// ErrorStatusIgnored captures enum value "ignored"
	ErrorStatusIgnored string = "ignored"
)

// prop value enum
func (m *Error) validateStatusEnum(path, location string, value string) error {
	if err := validate.EnumCase(path, location, value, errorTypeStatusPropEnum, true); err != nil {
		return err
	}
	return nil
}

func (m *Error) validateStatus(formats strfmt.Registry) error {
	if swag.IsZero(m.Status) { // not required
		return nil
	}

	// value enum
	if err := m.validateStatusEnum("status", "body", m.Status); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this error based on context it is used
func (m *Error) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *Error) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Error) UnmarshalBinary(b []byte) error {
	var res Error
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
