package metrics

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	requestName string = "http_requests_total"
	latencyName string = "http_requests_duration_seconds"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: requestName,
			Help: "number of requests by status code, method and path",
		},
		[]string{"code", "method", "path"},
	)
	latencyHist = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    latencyName,
			Help:    "duration of HTTP requests by status code, method and path",
			Buckets: []float64{0.1, 0.25, 0.5, 1.0, 5.0},
		},
		[]string{"code", "method", "path"},
	)
)

func ConfiguredCollectors() []prometheus.Collector {
	return []prometheus.Collector{
		requestCounter,
		latencyHist,
	}
}

// StripFunc is a helper method to strip path such that it can be used as a label (satisfy low cardinality constraints).
type StripFunc func(str string) string

func PrometheusMiddleware(next http.Handler, strip StripFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// delegate to capture response on the way back
		delegate := &responseWriterDelegator{ResponseWriter: w}
		rw := delegate

		begin := time.Now()
		next.ServeHTTP(rw, r)

		code := sanitizeCode(delegate.status)
		method := sanitizeMethod(r.Method)

		requestCounter.WithLabelValues(code, method, strip(r.URL.Path)).Inc()
		latencyHist.WithLabelValues(
			code,
			method,
			strip(r.URL.Path)).Observe(float64(time.Since(begin)) / float64(time.Second))
	})
}

type responseWriterDelegator struct {
	http.ResponseWriter
	status      int
	written     int64
	wroteHeader bool
}

func (r *responseWriterDelegator) WriteHeader(code int) {
	r.status = code
	r.wroteHeader = true
	r.ResponseWriter.WriteHeader(code)
}

func (r *responseWriterDelegator) Write(b []byte) (int, error) {
	if !r.wroteHeader {
		r.WriteHeader(http.StatusOK)
	}
	n, err := r.ResponseWriter.Write(b)
	if err != nil {
		return n, fmt.Errorf("responseWriteDelegator failed to write with %w", err)
	}
	r.written += int64(n)
	return n, nil
}

func sanitizeMethod(m string) string {
	return strings.ToUpper(m)
}

func sanitizeCode(s int) string {
	return strconv.Itoa(s)
}
