import { check } from 'k6';
import tracing from 'k6/x/tracing';
import { randomIntBetween } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import setup from './setup.js';

const client = new tracing.Client({
    endpoint: setup.urls.otlphttp,
    exporter: "otlphttp",
    headers: setup.headers.default,
    loglevel: "info",
});

export default function () {
    const err = client.push([{
        id: tracing.generateRandomTraceID(),
        random_service_name: false,
        spans: {
            count: randomIntBetween(5,10),
            size: randomIntBetween(300,1000),
            random_name: true,
            fixed_attrs: {
                "test": "test",
            },
        }
    }]);
    check(err, {
        'push err is null': (e) => e === null,
    });
    if (err !== null) {
        console.error(err);
    }

    client.exportMetrics();
}

export function teardown() {
    client.shutdown();
}