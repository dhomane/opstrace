# Benchmark scripts

The benchmark scripts implement different load testing scenarios to benchmark
monitoring APIs, including error tracking and tracing.

The scripts are developed with [Grafana k6](https://k6.io/docs/) to generate
load on API endpoints and check their status and expected functionality.

## Error tracking

Error tracking load test is is implemented in `errortracking/script.js`.
Currently, it has only a basic test for storing error events. It expects that
the POST request to `/projects/api/{PROJECT_ID}/store` endpoint succeed with
`200` status code.

Use the following environment variables to pass parameters to tests:

- `ERROR_TRACKING_API`: Error tracking API endpoint, e.g. `https://example.com/errortracking/api/v1`.
- `PROJECT_ID`: The ID of the project to run tests against it.
- `SENTRY_SECRET`: The authorization secret which can be generated via UI. _This
  parameter is required._
- `PAYLOAD_FILE`: The base payload. Note that test scripts overwrite `event_id`
  and `timestamp` attributes of the payload to generate a unique payload for
  each request. By default it uses `errortrackning/payload/2k.json`.

Note that the default values for some of these parameters are stored in `.env`
file.

The configuration for different load test scenarios are provided in separate
files in `errortracking/config/`, including:

- `baseline.json`: 100 requests per second for 5 minutes;
- `medium.json`: 1,000 requests per second for 5 minutes;
- `high.json`: 10,000 requests per second for 5 minutes.

Use each configuration to run a specific scenario.

### Usage

Here is an example usage:

```shell
source .env # Alternatively use `direnv allow`

# Authorization secret is required
export SENTRY_SECRET='<SECRET>'

# Run baseline scenario
k6 run -c errortracking/config/baseline.json errortracking/script.js
```

## Tracing

The [OTLP HTTP Specification](https://github.com/open-telemetry/opentelemetry-specification/blob/main/specification/protocol/otlp.md#otlphttp)
for the collector is what we're benchmarking, using the Protobuf encoded payloads.
JSON payloads are experimental and not typically implemented with the clients.
We also do not currently support the GRPC endpoint due to path handling limitations.

`k6` does not support encoding Protobuf bodies for HTTP requests, so we're using an extension.
[xk6-client-tracing](https://github.com/grafana/xk6-client-tracing) has been
[forked](https://gitlab.com/gitlab-org/opstrace/tools/xk6-client-tracing) and built with
[xk6](https://github.com/grafana/xk6) so the API is exposed to the js scripts.
See `./tracing/Makefile` for details, run `make` to build a local `k6` for benchmarking.

Tracing requires the following environment variables:

* `TRACING_API`: the API endpoint for otlp http.
* `API_KEY`: the UI API key.
* `PROJECT_ID`: The ID of the project to run tests against it.

Running the example, for example:

```bash
cd benchmark/tracing
make
./bin/k6 run example.js
```
