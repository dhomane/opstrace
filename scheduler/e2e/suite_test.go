package e2e

import (
	"context"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	schedulerapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"

	// support GCP auth for e2e CI environment.
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	//+kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

var (
	k8sClient     client.Client // Configured kubernetes client
	defaultClient client.Client // client for default namespace
	restConfig    *rest.Config  // Config of configured k8s REST interface.
	clientset     *kubernetes.Clientset
	ctx           context.Context    // Context is canceled if Ginkgo times out.
	cancel        context.CancelFunc // Context cancel function.
)

func TestE2E(t *testing.T) {
	RegisterFailHandler(Fail)

	// default Eventually settings for real clusters
	// image download might take a while on fresh installs
	SetDefaultEventuallyTimeout(time.Minute * 5)
	SetDefaultEventuallyPollingInterval(time.Second)

	RunSpecs(t, "Scheduler E2E tests")
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.Background())

	Expect(scheme.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(apiextensionsv1.AddToScheme(scheme.Scheme)).To(Succeed())
	Expect(schedulerapi.AddToScheme(scheme.Scheme)).To(Succeed())

	cfg, err := config.GetConfig()
	Expect(err).NotTo(HaveOccurred())
	restConfig = cfg

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	defaultClient = client.NewNamespacedClient(k8sClient, "default")
	clientset = kubernetes.NewForConfigOrDie(restConfig)

	By("check scheduler CRDs exists in cluster")
	crd := &apiextensionsv1.CustomResourceDefinition{}
	Expect(k8sClient.Get(ctx, client.ObjectKey{
		Name: "clusters." + schedulerv1alpha1.GroupVersion.Group,
	}, crd)).To(Succeed())

	Expect(k8sClient.Get(ctx, client.ObjectKey{
		Name: "gitlabnamespaces." + schedulerv1alpha1.GroupVersion.Group,
	}, crd)).To(Succeed())
})

var _ = AfterSuite(func() {
	By("no teardown required")
	cancel()
})
