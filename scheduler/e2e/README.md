# Scheduler E2E Tests

These tests aim to provide coverage of all the application features through the entrypoint of the scheduler custom resources.

Tests are written using [ginkgo](https://onsi.github.io/ginkgo/).

## Assumptions

The tests do not install the CRDs required for the scheduler, or the scheduler manager itself.
These must be installed into the target cluster first.
The reason for this is to allow the user to decide how the cluster is set up, where the manager runs (could be on host machine, or in CI).

## Running Locally

At the root of the project, run:

```bash
make kind deploy e2e-test
```

Or, when actively working on parts of the system:

```bash
make kind docker-build kind-load-docker-images
make deploy e2e-test
```

## Developing Tests

In principle, try to write tests through external interfaces (e.g. ingress) when testing user facing features.

Use the custom resources of the scheduler (`Cluster` and `GitLabNamespace`) to create necessary cluster/namespace state. This allows us to set up namespace resources without conducting the provisioning step via GitLab OAuth.

As a general rule, we keep a single `Cluster` and dependencies operational for the duration of the tests as it is time consuming to rebuild the cluster for each spec.
There are a few tests that check `Cluster` and `GitLabNamespace` deletion are working as expected.
