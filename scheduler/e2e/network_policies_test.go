package e2e

import (
	"fmt"
	"strings"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var _ = Describe("Tenant network policies", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	namespace := &schedulerv1alpha1.GitLabNamespace{}
	var anotherNamespace *schedulerv1alpha1.GitLabNamespace

	beforeAllLoadCluster(cluster)
	beforeAllLoadNamespace(namespace)

	BeforeAll(func() {
		expectClusterReady(cluster)
		expectGitLabNamespaceReady(namespace)

		By("create a secondary namespace to test policy rules")
		anotherNamespace = &schedulerv1alpha1.GitLabNamespace{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "2",
				Namespace: "default",
			},
			Spec: schedulerv1alpha1.GitLabNamespaceSpec{
				ID:                  2,
				TopLevelNamespaceID: 2,
				Name:                "test",
				Path:                "test",
				FullPath:            "test",
			},
		}

		createOrUpdate(anotherNamespace)
		expectGitLabNamespaceReady(anotherNamespace)
	})

	AfterAll(func() {
		deleteGitLabNamespaceAndVerify(anotherNamespace)
	})

	Context("Argus pods", func() {
		It("have limited network egress", func() {
			By("query the argus pods")
			pods := &corev1.PodList{}
			Expect(k8sClient.List(ctx, pods, client.InNamespace(anotherNamespace.Namespace()), client.MatchingLabels{
				"app": "argus",
			})).To(Succeed())
			Expect(pods.Items).NotTo(BeEmpty())

			// pick a pod to target
			target := pods.Items[len(pods.Items)-1]

			netcatCheck := func(address string, port int, wantConnect bool) string {
				nc := fmt.Sprintf("nc -zv -w3 %s %d", address, port)
				if wantConnect {
					return nc + " || { echo 'expected connection to succeed'; exit 1; }"
				}
				return nc + " && { echo 'expected connection timeout'; exit 1; } || :"
			}

			netcatChecks := []string{
				// cluster nginx-ingress is accessible
				netcatCheck(fmt.Sprintf("%s.%s", constants.NginxIngressServiceName, cluster.Namespace()), 80, true),
				// can access internet
				netcatCheck("google.com", 80, true),
				// cannot access argus in another namespace
				netcatCheck(fmt.Sprintf("%s.%s", constants.ArgusServiceName, namespace.Namespace()), constants.ArgusHTTPPort, false),
				// cannot access gatekeeper pods
				netcatCheck(fmt.Sprintf("%s.%s", constants.GatekeeperName, cluster.Namespace()), 3001, false),
				// can access own argus address when cluster target is KIND
				netcatCheck(fmt.Sprintf("%s.%s", constants.ArgusServiceName, anotherNamespace.Namespace()), constants.ArgusHTTPPort, cluster.Spec.Target == common.KIND),
			}

			By("run netcat in argus ephemeral container to validate egress")
			pod := target.DeepCopy()
			ephemeralContainerName := fmt.Sprintf("test-egress-%d", len(pod.Spec.EphemeralContainers))

			_, err := clientset.CoreV1().Pods(anotherNamespace.Namespace()).UpdateEphemeralContainers(ctx, pod.Name, func() *corev1.Pod {
				pod.Spec.EphemeralContainers = append(pod.Spec.EphemeralContainers, corev1.EphemeralContainer{
					EphemeralContainerCommon: corev1.EphemeralContainerCommon{
						Name:    ephemeralContainerName,
						Image:   "busybox:latest",
						Command: []string{"/bin/sh", "-c", strings.Join(netcatChecks, "\n")},
					},
				})
				return pod
			}(), metav1.UpdateOptions{})

			Expect(err).NotTo(HaveOccurred())

			By("check ephemeral container has succeeded")
			Eventually(func(g Gomega) {
				pod := &corev1.Pod{}
				g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(target.DeepCopy()), pod)).To(Succeed())

				es := corev1.ContainerStatus{}
				for _, s := range pod.Status.EphemeralContainerStatuses {
					if s.Name == ephemeralContainerName {
						es = s
						break
					}
				}

				g.Expect(es.State.Terminated).NotTo(BeNil())
				g.Expect(es.State.Terminated.ExitCode).To(BeNumerically("==", 0), "container exit status")
			}).Should(Succeed())
		})
	})
})
