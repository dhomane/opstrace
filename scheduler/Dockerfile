# Build the manager binary
FROM --platform=${BUILDPLATFORM} golang:1.19 as builder

ENV CGO_ENABLED=0

WORKDIR /workspace/go
# Copy common go packages
COPY go/go.mod ./
COPY go/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY go/pkg/ pkg/

WORKDIR /workspace/tenant-operator
# Copy tenant operator
COPY tenant-operator/go.mod ./
COPY tenant-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY tenant-operator/api/ api/
COPY tenant-operator/controllers/ controllers/
COPY tenant-operator/version/ version/

WORKDIR /workspace/clickhouse-operator
# Copy clickhouse operator
COPY clickhouse-operator/go.mod ./
COPY clickhouse-operator/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

COPY clickhouse-operator/api/ api/
COPY clickhouse-operator/controllers/ controllers/

WORKDIR /workspace/operator
# Copy the Go Modules manifests
COPY scheduler/go.mod ./
COPY scheduler/go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY scheduler/main.go ./main.go
COPY scheduler/api/ api/
COPY scheduler/controllers/ controllers/
COPY scheduler/version/ version/

ARG GO_BUILD_LDFLAGS
ARG TARGETARCH
ARG TARGETOS
# Build. Using `bash -c` format to avoid quoting issues.
RUN ["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -a -o manager main.go"]

FROM --platform=${BUILDPLATFORM} debian:stretch-slim

COPY --from=builder /workspace/operator/manager .

ENTRYPOINT ["./manager"]
