package namespace

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "512Mi"
	CpuLimit      = "300m"
)

var Replicas int32 = 1

func GetTenantOperatorName() string {
	return constants.TenantOperatorName
}

func GetTenantOperatorSelector() map[string]string {
	return map[string]string{
		"app": constants.TenantOperatorName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.TenantOperatorName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.TenantOperatorName,
	}
}

func getContainerEnv(cr *v1alpha1.GitLabNamespace) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  "WATCH_NAMESPACE",
			Value: cr.Namespace(),
		},
		{
			Name: "POD_NAME",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.name"},
			},
		},
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Port:   intstr.FromInt(8081),
				Path:   "/readyz",
				Scheme: corev1.URISchemeHTTP,
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       3,
	}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:   "/healthz",
				Port:   intstr.FromInt(8081),
				Scheme: "HTTP",
			},
		},
		InitialDelaySeconds: 30,
		PeriodSeconds:       3,
		FailureThreshold:    10,
	}
}

func getContainers(clusterConfig *v1alpha1.Cluster, cr *v1alpha1.GitLabNamespace) []corev1.Container {
	return []corev1.Container{{
		Name:    constants.TenantOperatorName,
		Image:   constants.DockerImageFullName(constants.TenantImageName),
		Command: []string{"/manager"},
		Args: []string{
			// don't enable leader election because it adds an extra provisioning delay and probably isn't necessary
			fmt.Sprintf("--gatekeeper=http://%s.%s.svc.cluster.local:3001", constants.GatekeeperName, clusterConfig.Namespace()),
			fmt.Sprintf("--target=%s", clusterConfig.Spec.Target),
		},
		Env: getContainerEnv(cr),
		Ports: []corev1.ContainerPort{
			{
				Name:          "metrics",
				ContainerPort: 8080,
			},
			{
				Name:          "health",
				ContainerPort: 8081,
			}},
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		ReadinessProbe:  getReadinessProbe(),
		LivenessProbe:   getLivenessProbe(),
	}}
}

func getDeploymentSpec(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
	current v1.DeploymentSpec,
) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetTenantOperatorSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetTenantOperatorName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getContainers(clusterConfig, cr),
				ServiceAccountName: GetTenantOperatorName(),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.GitLabNamespace) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetTenantOperatorName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.GitLabNamespace) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetTenantOperatorName(),
	}
}

func DeploymentMutator(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
	current *v1.Deployment,
) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(clusterConfig, cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Operator.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Operator.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.Operator.Components.Deployment.Labels,
	)

	return nil
}
