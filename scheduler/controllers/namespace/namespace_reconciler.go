package namespace

import (
	"fmt"
	"net/url"

	"github.com/go-logr/logr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/group/jaeger"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/controllers/tenant/argus"
)

type GitLabNamespaceReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewGitLabNamespaceReconciler(teardown bool, logger logr.Logger) *GitLabNamespaceReconciler {
	return &GitLabNamespaceReconciler{
		Teardown: teardown,
		Log:      logger.WithName("gitlab-namespace"),
	}
}

func (i *GitLabNamespaceReconciler) Reconcile(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) common.DesiredState {
	desired := common.DesiredState{}
	if i.Teardown {
		// remove group first
		desired = desired.AddAction(i.getGroupDesiredState(state.GetClusterState(), cr))
		// wait for group to be gone before we delete the clickhouse credentials
		// otherwise the tenant-operator has to do some magic to tear the group
		// down without access to these credentials
		desired = desired.AddActions(i.getGroupGone(state))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		// If there are no more active groups in this tenant then we teardown the
		// entire tenant
		if state.TenantEmpty() {
			// remove Tenant. This will cause all groups within the tenant to
			// be torn down too (via the tenant-operator)
			desired = desired.AddAction(i.getTenantDesiredState(state.GetPostgresEndpoint(), state.GetClusterState(), cr))
			// make sure tenant is gone before we remove it's operator
			desired = desired.AddActions(i.getTenantGone(state))
			// remove Tenant operator next
			desired = desired.AddAction(i.getDeploymentDesiredState(state.GetClusterState(), cr))
			// block until tenant-operator is removed
			desired = desired.AddActions(i.getTenantReadiness(state))

			desired = desired.AddActions(i.getRBACDesiredState(cr))
			desired = desired.AddAction(i.getServiceAccountDesiredState(state.GetClusterState(), cr))
			desired = desired.AddActions(i.getPostgresDesiredState(state, cr))
			// remove namespace last
			desired = desired.AddAction(i.getNamespaceDesiredState(cr))
			// block until namespace is removed
			desired = desired.AddActions(i.getNamespaceGone(state))
		}
	} else {
		// create the namespace first
		desired = desired.AddAction(i.getNamespaceDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getServiceAccountDesiredState(state.GetClusterState(), cr))
		desired = desired.AddActions(i.getPostgresDesiredState(state, cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(state.GetClusterState(), cr))
		desired = desired.AddActions(i.getTenantReadiness(state))
		// reconcile tenant/group last once we know the operator is ready
		desired = desired.AddAction(i.getTenantDesiredState(state.GetPostgresEndpoint(), state.GetClusterState(), cr))
		desired = desired.AddAction(i.getGroupDesiredState(state.GetClusterState(), cr))
	}

	return desired
}

func (i *GitLabNamespaceReconciler) getTenantReadiness(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Operator != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Operator,
					Msg: "check tenant-operator is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Operator,
			Msg: "check tenant-operator readiness",
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Group != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Group,
					Msg: "check group is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getTenantGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Tenant != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Tenant,
					Msg: "check tenant is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getNamespaceGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Namespace != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Namespace,
					Msg: "check namespace is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getRBACDesiredState(cr *v1alpha1.GitLabNamespace) []common.Action {
	objects, err := GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize tenant rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
				// Don't want a GitlabNamespace CR to own the whole this because multiple
				// GitlabNamespace CRs map to the same resource
				SkipOwnerRef: true,
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getNamespaceDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	ns := Namespace(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: ns,
			Msg: "tenant namespace",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ns,
		Msg: "tenant namespace",
		// Don't want a GitlabNamespace CR to own the whole namespace because multiple
		// GitlabNamespace CRs map to the same namespace
		SkipOwnerRef: true,
		Mutator: func() error {
			return NamespaceMutator(cr, ns)
		},
	}
}

func (i *GitLabNamespaceReconciler) getPostgresDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	tenant := Tenant(state.GetPostgresEndpoint(), state.GetClusterState(), cr)
	tenantEndpoint := state.GetPostgresEndpoint()
	tenantEndpoint.Path = fmt.Sprintf("/%s", state.GetPostgresDatabaseName(cr))
	postgresSecret := argus.PostgresEndpointSecret(tenant, []byte(tenantEndpoint.String()))

	dbName := state.GetPostgresDatabaseName(cr)
	if i.Teardown {
		actions := []common.Action{}
		if state.PostgresDBExists {
			actions = append(actions, common.PostgresAction{
				Msg: fmt.Sprintf("drop postgres database: %s", dbName),
				SQL: fmt.Sprintf("DROP DATABASE %s", dbName),
				URL: state.GetPostgresEndpoint().String(),
			})
		}
		actions = append(actions, common.GenericDeleteAction{
			Ref: postgresSecret,
			Msg: "tenant credentials",
		})
		return actions
	}

	actions := []common.Action{common.GenericCreateOrUpdateAction{
		Ref: postgresSecret,
		Msg: "tenant credentials",
		Mutator: func() error {
			argus.PostgresEndpointSecretMutator(postgresSecret, []byte(tenantEndpoint.String()))
			return nil
		},
	}}

	if !state.PostgresDBExists {
		actions = append(actions, common.PostgresAction{
			Msg: fmt.Sprintf("create postgres database: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE %s", dbName),
			URL: state.GetPostgresEndpoint().String(),
		})
	} else {
		actions = append(actions, common.LogAction{
			Msg: fmt.Sprintf("postgres database already exists: %s", dbName),
		})
	}
	return actions
}

func (i *GitLabNamespaceReconciler) getClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	desired := []common.Action{}
	desired = append(desired, i.getJaegerClickHouseDesiredState(state, cr)...)
	desired = append(desired, i.getErrorTrackingClickHouseDesiredState(state, cr)...)
	return desired
}

//nolint:funlen
func (i *GitLabNamespaceReconciler) getJaegerClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	group := Group(state.GetClusterState(), cr)
	jaegerCredentialsSecret := jaeger.ClickHouseCredentialsSecret(group, state.GetClickhouseEndpoints().Native)

	actions := []common.Action{}

	if i.Teardown {
		// we do not teardown the credential secret here, we defer that to last
		// after having deleted the corresponding user first.
	} else {
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: jaegerCredentialsSecret,
				Msg: "group jaeger-clickhouse credentials",
				Mutator: func() error {
					jaeger.ClickHouseCredentialsSecretMutator(group, jaegerCredentialsSecret, state.GetClickhouseEndpoints().Native)
					return nil
				},
			},
		)
	}

	// get userinfo created by jaeger.ClickHouseCredentialsSecret
	userInfo, err := state.ClickHouseUserInfo(JaegerSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg:   fmt.Sprintf("clickhouse user credentials for jaeger-clickhouse in group %d not available yet", cr.Spec.ID),
				Error: err,
			},
		)
		return actions
	}

	user := userInfo.Username()
	pass, _ := userInfo.Password()

	// get quotas info created by jaeger.ClickHouseQuotasConfigmap
	quotas, err := state.ClickHouseQuotasInfo(JaegerSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg: fmt.Sprintf("clickhouse user quotas for jaeger in group %d not available yet", cr.Spec.ID),
				// we do not report errors here because quotas are reconciled lazily inside tenant-operator
			},
		)
	}

	databaseName := ""
	useTracingRemoteStorageBackend, err := common.ParseFeatureAsBool(cr.Spec.Features, "TRACING_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <TRACING_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}
	if useTracingRemoteStorageBackend {
		switch state.GetClusterState().Spec.Target {
		case common.GCP:
			databaseName = constants.JaegerGCSDatabaseName
		case common.AWS:
			databaseName = constants.JaegerS3DatabaseName
		}
	} else {
		databaseName = constants.JaegerDatabaseName
	}

	if i.Teardown {
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      state.GetClickhouseEndpoints().Native,
				Database: databaseName,
			},
			common.GenericDeleteAction{
				Ref: jaegerCredentialsSecret,
				Msg: "group jaeger-clickhouse credentials",
			},
		)
	} else {
		// create user first
		actions = append(actions,
			ClickHouseUsersProvision(
				user,
				pass,
				[]string{"SELECT", "INSERT", "ALTER", "CREATE"},
				databaseName,
				state.GetClickhouseEndpoints(),
			)...,
		)
		// if we found quotas configured, create them next
		for quotaName, quotaDef := range quotas { // if quotas == nil, there will be no iterations
			actions = append(actions,
				ClickHouseQuotasProvision(
					quotaName,
					databaseName,
					state.GetClickhouseEndpoints(),
					quotaDef,
					user,
				)...,
			)
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getErrorTrackingClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	group := Group(state.GetClusterState(), cr)
	errortrackingCredentialsSecret := errortracking.ClickHouseCredentialsSecret(group, state.GetClickhouseEndpoints().Native)

	actions := []common.Action{}

	if i.Teardown {
		// we do not teardown the credential secret here, we defer that to last
		// after having deleted the corresponding user first.
	} else {
		actions = append(actions,
			common.GenericCreateOrUpdateAction{
				Ref: errortrackingCredentialsSecret,
				Msg: "group errortracking-clickhouse credentials",
				Mutator: func() error {
					errortracking.ClickHouseCredentialsSecretMutator(
						group,
						errortrackingCredentialsSecret,
						state.GetClickhouseEndpoints().Native,
					)
					return nil
				},
			},
		)
	}

	// get userinfo created by errortracking.ClickHouseCredentialsSecret
	userInfo, err := state.ClickHouseUserInfo(ErrorTrackingSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg:   fmt.Sprintf("clickhouse user credentials for errortracking in group %d not available yet", cr.Spec.ID),
				Error: err,
			},
		)
		return actions
	}

	user := userInfo.Username()
	pass, _ := userInfo.Password()

	// get quotas info created by errortracking.ClickHouseQuotasConfigmap
	quotas, err := state.ClickHouseQuotasInfo(ErrorTrackingSubsystemName)
	if err != nil && !i.Teardown {
		actions = append(actions,
			common.LogAction{
				Msg: fmt.Sprintf("clickhouse user quotas for errortracking in group %d not available yet", cr.Spec.ID),
				// we do not report errors here because quotas are reconciled lazily inside tenant-operator
			},
		)
	}

	if i.Teardown {
		actions = append(actions,
			common.ClickHouseAction{
				Msg:      fmt.Sprintf("clickhouse SQL drop user %s if exists", user),
				SQL:      fmt.Sprintf("DROP USER IF EXISTS %s ON CLUSTER '{cluster}'", user),
				URL:      state.GetClickhouseEndpoints().Native,
				Database: constants.ErrorTrackingDatabaseName,
			},
			common.GenericDeleteAction{
				Ref: errortrackingCredentialsSecret,
				Msg: "group errortracking-clickhouse credentials",
			},
		)
	} else {
		// create user first
		actions = append(actions,
			ClickHouseUsersProvision(
				user,
				pass,
				[]string{"SELECT", "INSERT", "ALTER", "CREATE"},
				constants.ErrorTrackingDatabaseName,
				state.GetClickhouseEndpoints(),
			)...,
		)
		// if we found quotas configured, create them next
		for quotaName, quotaDef := range quotas { // if quotas == nil, there will be no iterations
			actions = append(actions,
				ClickHouseQuotasProvision(
					quotaName,
					constants.ErrorTrackingDatabaseName,
					state.GetClickhouseEndpoints(),
					quotaDef,
					user,
				)...,
			)
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getServiceAccountDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	sa := ServiceAccount(clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "tenant service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "tenant service account",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return ServiceAccountMutator(clusterConfig, cr, sa)
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	group := Group(clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: group,
			Msg: "group cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: group,
		Msg: "group cr",
		Mutator: func() error {
			return GroupMutator(clusterConfig, cr, group)
		},
	}
}

func (i *GitLabNamespaceReconciler) getTenantDesiredState(
	postrgreSQLEndpoint *url.URL,
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	tenant := Tenant(postrgreSQLEndpoint, clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: tenant,
			Msg: "tenant cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: tenant,
		Msg: "tenant cr",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return TenantMutator(postrgreSQLEndpoint, clusterConfig, cr, tenant)
		},
	}
}

func (i *GitLabNamespaceReconciler) getDeploymentDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	deploy := Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "tenant-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "tenant-operator deployment",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return DeploymentMutator(clusterConfig, cr, deploy)
		},
	}
}
