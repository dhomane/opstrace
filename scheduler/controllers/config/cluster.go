package config

import (
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type ClickHouseEndpoints struct {
	Http   url.URL
	Native url.URL
}

func (che *ClickHouseEndpoints) Copy() *ClickHouseEndpoints {
	res := ClickHouseEndpoints{
		Http:   *common.MustParse(che.Http.String()),
		Native: *common.MustParse(che.Native.String()),
	}
	return &res
}
