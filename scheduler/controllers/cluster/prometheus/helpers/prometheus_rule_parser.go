package prometheus

import (
	"github.com/prometheus/common/model"
	yaml "gopkg.in/yaml.v3"
)

// ref: https://github.com/prometheus/prometheus/blob/main/model/rulefmt/rulefmt.go
// RuleGroups is a set of rule groups that are typically exposed in a file.
type RuleGroups struct {
	Groups []RuleGroup `yaml:"groups"`
}

// RuleGroup is a list of sequentially evaluated recording and alerting rules.
type RuleGroup struct {
	Name     string         `yaml:"name"`
	Interval model.Duration `yaml:"interval,omitempty"`
	Limit    int            `yaml:"limit,omitempty"`
	Rules    []Rule         `yaml:"rules"`
}

// Rule describes an alerting or recording rule.
type Rule struct {
	Record      string            `yaml:"record,omitempty"`
	Alert       string            `yaml:"alert,omitempty"`
	Expr        string            `yaml:"expr"`
	For         model.Duration    `yaml:"for,omitempty"`
	Labels      map[string]string `yaml:"labels,omitempty"`
	Annotations map[string]string `yaml:"annotations,omitempty"`
}

func ParseAsRuleGroups(corpus []byte) (RuleGroups, error) {
	var ruleGroups RuleGroups
	if err := yaml.Unmarshal(corpus, &ruleGroups); err != nil {
		return RuleGroups{}, err
	}
	return ruleGroups, nil
}

func ParseAsRules(corpus []byte) ([]Rule, error) {
	rules := make([]Rule, 0)
	if err := yaml.Unmarshal(corpus, &rules); err != nil {
		return nil, err
	}
	return rules, nil
}
