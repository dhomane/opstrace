package prometheus

import (
	"os"
	"testing"
)

func TestParseAsRuleGroup(t *testing.T) {
	corpus, err := os.ReadFile("fixtures/rules.yaml")
	if err != nil {
		t.Fatal(err)
	}
	// t.Logf("%s", corpus)
	if _, err := ParseAsRuleGroups(corpus); err != nil {
		t.Fatal(err)
	}
	// t.Logf("%+v", ruleGroups)
}
