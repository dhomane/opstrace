package prometheus

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
)

func BuildPrometheusRule(cr *v1alpha1.Cluster, name string, corpus []byte) (*monitoring.PrometheusRule, error) {
	ruleGroups, err := ParseAsRuleGroups(corpus)
	if err != nil {
		return &monitoring.PrometheusRule{}, err
	}

	if len(ruleGroups.Groups) == 0 {
		return &monitoring.PrometheusRule{}, fmt.Errorf("no rules groups configured")
	}

	groups := make([]monitoring.RuleGroup, 0)
	for _, group := range ruleGroups.Groups {
		r := make([]monitoring.Rule, 0)
		for _, rule := range group.Rules {
			r = append(r, monitoring.Rule{
				Record:      rule.Record,
				Alert:       rule.Alert,
				Expr:        intstr.FromString(rule.Expr),
				For:         rule.For.String(),
				Labels:      rule.Labels,
				Annotations: rule.Annotations,
			})
		}

		groups = append(groups, monitoring.RuleGroup{
			Name:     group.Name,
			Interval: group.Interval.String(),
			Rules:    r,
		})
	}

	return &monitoring.PrometheusRule{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: cr.Namespace(),
			Labels: map[string]string{
				"tenant": "system",
			},
		},
		Spec: monitoring.PrometheusRuleSpec{
			Groups: groups,
		},
	}, nil
}
