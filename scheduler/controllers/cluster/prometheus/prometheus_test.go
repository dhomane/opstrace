package prometheus

import (
	"os"
	"testing"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/equality"
	k8syaml "k8s.io/apimachinery/pkg/util/yaml"
)

func TestPrometheusMutator(t *testing.T) {
	for _, test := range []struct {
		file        string
		patchObject monitoring.Prometheus
	}{
		{
			file:        "tests/1.yaml",
			patchObject: monitoring.Prometheus{},
		},
		{
			file: "tests/2.yaml",
			patchObject: monitoring.Prometheus{
				Spec: monitoring.PrometheusSpec{
					CommonPrometheusFields: monitoring.CommonPrometheusFields{
						ExternalURL: "https://opstrace.blah/v1",
						RemoteWrite: []monitoring.RemoteWriteSpec{
							{
								URL: "https://blah/v1",
							},
						},
					},
				},
			},
		},
	} {
		t.Logf("---------------------")
		t.Logf("Testing %s", test.file)
		cr, err := parseYAML(test.file)
		if err != nil {
			t.Fatal(err)
		}

		current, err := Prometheus(cr)
		if err != nil {
			t.Fatal(err)
		}
		expected := current.DeepCopy()
		// hand roll the changes we expect to see
		if err := common.PatchObject(expected, test.patchObject); err != nil {
			t.Fatal(err)
		}

		if err := PrometheusMutator(cr, current); err != nil {
			t.Fatal(err)
		}

		if !equality.Semantic.DeepEqual(current, expected) {
			t.Fatalf("Failed. expected: %+v, got: %+v", expected.Spec, current.Spec)
		}
	}
}

func parseYAML(filepath string) (*v1alpha1.Cluster, error) {
	cr := &v1alpha1.Cluster{}

	r, err := os.Open(filepath)
	if err != nil {
		return cr, err
	}
	decoder := k8syaml.NewYAMLOrJSONDecoder(r, 4096)

	if err := decoder.Decode(cr); err != nil {
		return cr, err
	}

	return cr, nil
}
