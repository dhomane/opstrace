package clickhouse

import (
	"net/url"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	ClusterReplicas = int32(3)
	// TODO(joe): this is way too low, we can patch it at runtime.
	// A better solution would be dynamic resizing, eventually.
	DataVolumeSize = "25Gi"
)

func GetClickHouseName(cr *v1alpha1.Cluster) string {
	return constants.ClickHouseClusterServiceName
}

func getClickHouseSpec(user *url.Userinfo) (clickhousev1alpha1.ClickHouseSpec, error) {
	replicas := ClusterReplicas
	dataVolumeSize, err := resource.ParseQuantity(DataVolumeSize)
	if err != nil {
		return clickhousev1alpha1.ClickHouseSpec{}, err
	}

	storageClass := constants.StorageClassName
	return clickhousev1alpha1.ClickHouseSpec{
		Image:        constants.OpstraceImages().ClickHouseImage,
		Replicas:     &replicas,
		StorageSize:  dataVolumeSize,
		StorageClass: &storageClass,
		//TODO: see https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1890
		Tolerations: []corev1.Toleration{
			{
				Key:      "nodepool",
				Operator: "Equal",
				Value:    "clickhouse-nodes",
				Effect:   "NoSchedule",
			},
		},
		Affinity: &corev1.Affinity{
			NodeAffinity: &corev1.NodeAffinity{
				PreferredDuringSchedulingIgnoredDuringExecution: []corev1.PreferredSchedulingTerm{
					{
						Weight: 100,
						Preference: corev1.NodeSelectorTerm{
							MatchExpressions: []corev1.NodeSelectorRequirement{
								{
									Key:      "cloud.google.com/gke-nodepool",
									Operator: "In",
									Values: []string{
										"clickhouse-nodes",
									},
								},
							},
						},
					},
				},
			},
		},
		AdminUsers: []clickhousev1alpha1.ClickHouseAdmin{
			{
				Name: user.Username(),
				SecretKeyRef: corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: GetCredentialsSecretName(),
					},
					Key: constants.ClickHouseCredentialsPasswordKey,
				},
			},
		},
	}, nil
}

func ClickHouse(cr *v1alpha1.Cluster, user *url.Userinfo) *clickhousev1alpha1.ClickHouse {
	spec, err := getClickHouseSpec(user)
	if err != nil {
		return nil
	}
	return &clickhousev1alpha1.ClickHouse{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetClickHouseName(cr),
			Namespace: cr.Namespace(),
		},
		Spec: spec,
	}
}

func ClickHouseSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetClickHouseName(cr),
	}
}

func ClickHouseMutator(cr *v1alpha1.Cluster, current *clickhousev1alpha1.ClickHouse, user *url.Userinfo) error {
	currentSpec := &current.Spec
	spec, err := getClickHouseSpec(user)
	if err != nil {
		return err
	}
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}

	// Apply CR overrides
	overrides := cr.Spec.Overrides.ClickHouse.Components.Deployment.Spec
	if err := common.PatchObject(currentSpec, overrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		map[string]string{},
		current.Annotations,
	)
	current.Annotations = common.MergeMap(
		current.Annotations,
		cr.Spec.Overrides.ClickHouse.Components.Deployment.Annotations,
	)

	current.Labels = common.MergeMap(
		current.Labels,
		cr.Spec.Overrides.ClickHouse.Components.Deployment.Labels)

	return nil
}
