package prometheusOperator

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getServiceMonitorName() string {
	return GetPrometheusOperatorDeploymentName()
}

func getServiceMonitorLabels() map[string]string {
	labels := GetPrometheusOperatorDeploymentSelector()
	// Configure the system-tenant prometheus to scrape this
	labels["tenant"] = "system"

	return labels
}

func getServiceMonitorAnnotations(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:        "http",
			HonorLabels: true,
			Interval:    "30s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Cluster) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: GetPrometheusOperatorDeploymentSelector(),
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace()},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(cr),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	crOverrides := cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Spec
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		&crOverrides,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec

	current.Annotations = utils.MergeMap(
		getServiceMonitorAnnotations(cr),
		cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Annotations,
	)

	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(),
		cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Labels,
	)

	return nil
}
