package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/jaegerOperator"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type JaegerOperatorReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewJaegerOperatorReconciler(teardown bool, logger logr.Logger) *JaegerOperatorReconciler {
	return &JaegerOperatorReconciler{
		Teardown: teardown,
		Log:      logger.WithName("jaeger-operator"),
	}
}

func (i *JaegerOperatorReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getIssuerDesiredState(cr))
	desired = desired.AddAction(i.getCertificateDesiredState(cr))
	desired = desired.AddAction(i.getDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))

	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *JaegerOperatorReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.Jaeger.OperatorDeployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Jaeger.OperatorDeployment,
					Msg: "check jaeger-operator deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Jaeger.OperatorDeployment,
			Msg: "check jaeger-operator deployment readiness",
		},
	}
}

func (i *JaegerOperatorReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := jaegerOperator.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize jaeger-operator rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("jaeger-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("jaeger-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *JaegerOperatorReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := jaegerOperator.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "jaeger-operator service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "jaeger-operator service",
		Mutator: func() error {
			return jaegerOperator.ServiceMutator(cr, svc)
		},
	}
}

func (i *JaegerOperatorReconciler) getCertificateDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := jaegerOperator.Certificate(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "jaeger-operator certificate",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "jaeger-operator certificate",
		Mutator: func() error {
			jaegerOperator.CertificateMutator(cr, s)
			return nil
		},
	}
}

func (i *JaegerOperatorReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := jaegerOperator.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "jaeger-operator service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "jaeger-operator service account",
		Mutator: func() error {
			jaegerOperator.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *JaegerOperatorReconciler) getIssuerDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := jaegerOperator.Issuer(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "jaeger-operator issuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "jaeger-operator issuer",
		Mutator: func() error {
			jaegerOperator.IssuerMutator(cr, s)
			return nil
		},
	}
}

func (i *JaegerOperatorReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := jaegerOperator.Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "jaeger-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "jaeger-operator deployment",
		Mutator: func() error {
			return jaegerOperator.DeploymentMutator(cr, deploy)
		},
	}
}

func (i *JaegerOperatorReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := jaegerOperator.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "jaeger-operator servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "jaeger-operator servicemonitor",
		Mutator: func() error {
			return jaegerOperator.ServiceMonitorMutator(cr, monitor)
		},
	}
}
