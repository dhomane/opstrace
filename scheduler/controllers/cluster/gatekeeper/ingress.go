package gatekeeper

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	netv1 "k8s.io/api/networking/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func getIngressTLS(cr *v1alpha1.Cluster) []netv1.IngressTLS {
	return []netv1.IngressTLS{
		{
			Hosts: []string{cr.Spec.GetHost()},
		},
	}
}

func getIngressLabels(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{}
}

func getIngressAnnotations(existing map[string]string) map[string]string {
	return common.MergeMap(existing, map[string]string{
		"external-dns.alpha.kubernetes.io/ttl":                "30",
		"kubernetes.io/ingress.class":                         "nginx",
		"nginx.ingress.kubernetes.io/client-body-buffer-size": "1m",
		"nginx.ingress.kubernetes.io/mergeable-ingress-type":  "master",
	})
}

func getIngressSpec(cr *v1alpha1.Cluster) netv1.IngressSpec {
	pathType := netv1.PathTypePrefix

	return netv1.IngressSpec{
		TLS: getIngressTLS(cr),
		Rules: []netv1.IngressRule{
			{
				Host: cr.Spec.GetHost(),
				IngressRuleValue: netv1.IngressRuleValue{
					HTTP: &netv1.HTTPIngressRuleValue{
						Paths: []netv1.HTTPIngressPath{
							{
								Path:     "/",
								PathType: &pathType,
								Backend: netv1.IngressBackend{
									Service: &netv1.IngressServiceBackend{
										Name: GetGatekeeperDeploymentName(),
										Port: netv1.ServiceBackendPort{
											Name: "http",
										},
									},
									Resource: nil,
								},
							},
						},
					},
				},
			},
		},
	}
}

func Ingress(cr *v1alpha1.Cluster) *netv1.Ingress {
	return &netv1.Ingress{
		ObjectMeta: v1.ObjectMeta{
			Name:        GetGatekeeperDeploymentName(),
			Namespace:   cr.Namespace(),
			Labels:      getIngressLabels(cr),
			Annotations: getIngressAnnotations(nil),
		},
		Spec: getIngressSpec(cr),
	}
}

func IngressMutator(cr *v1alpha1.Cluster, current *netv1.Ingress) error {
	currentSpec := &current.Spec
	spec := getIngressSpec(cr)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Gatekeeper.Components.Ingress.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getIngressAnnotations(current.Annotations),
		cr.Spec.Overrides.Gatekeeper.Components.Ingress.Annotations,
	)
	current.Labels = common.MergeMap(
		getIngressLabels(cr),
		cr.Spec.Overrides.Gatekeeper.Components.Ingress.Labels,
	)

	return nil
}
func IngressSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetGatekeeperDeploymentName(),
	}
}
