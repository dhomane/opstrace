package gatekeeper

import (
	"fmt"
	"net/url"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "256Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "512Mi"
	CpuLimit      = "300m"
)

var Replicas int32 = 3

func GetGatekeeperDeploymentName() string {
	return constants.GatekeeperName
}

func GetGatekeeperDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.GatekeeperName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	maxUnavailable := intstr.FromString("25%")
	maxSurge := intstr.FromString("25%")

	return v1.DeploymentStrategy{
		Type: v1.RollingUpdateDeploymentStrategyType,
		RollingUpdate: &v1.RollingUpdateDeployment{
			MaxUnavailable: &maxUnavailable,
			MaxSurge:       &maxSurge,
		},
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.GatekeeperName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.GatekeeperName,
	}
}

//nolint:funlen
func getContainerEnv(cr *v1alpha1.Cluster, postgresEndpoint *url.URL) []corev1.EnvVar {
	skipTLSInsecureVerify := "false"
	if cr.Spec.Target == common.KIND {
		// running on localhost domain and the loopback interface is not trusted
		skipTLSInsecureVerify = "true"
	}

	return []corev1.EnvVar{
		{
			Name:  "NAMESPACE",
			Value: cr.Namespace(),
		},
		{
			Name:  "REDIS_ADDRESS",
			Value: fmt.Sprintf("rfs-%s.%s.svc.cluster.local:26379", constants.RedisName, cr.Namespace()),
		},
		{
			Name: "REDIS_PASSWORD",
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.RedisName,
					},
					Key: "password",
				},
			},
		},
		{
			Name:  "POSTGRES_ENDPOINT",
			Value: postgresEndpoint.String(),
		},
		{
			Name:  "USE_SECURE_COOKIE",
			Value: "true",
		},
		{
			Name: "COOKIE_SECRET",
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: constants.SessionCookieSecretName,
					},
					Key: "COOKIE_SECRET",
				},
			},
		},
		{
			Name: "GITLAB_OAUTH_CLIENT_ID",
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.GitLab.AuthSecret.Name,
					},
					Key:      constants.AuthSecretOAuthClientIDKey,
					Optional: pointer.Bool(false),
				},
			},
		},
		{
			Name: "GITLAB_OAUTH_CLIENT_SECRET",
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.GitLab.AuthSecret.Name,
					},
					Key:      constants.AuthSecretOAuthClientSecretKey,
					Optional: pointer.Bool(false),
				},
			},
		},
		{
			Name: "GITLAB_INTERNAL_ENDPOINT_TOKEN",
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.GitLab.AuthSecret.Name,
					},
					Key:      "internal_endpoint_token",
					Optional: pointer.Bool(false),
				},
			},
		},
		{
			Name:  "DOMAIN",
			Value: cr.Spec.GetHostURL(),
		},
		{
			Name:  "TLS_SKIP_INSECURE_VERIFY",
			Value: skipTLSInsecureVerify,
		},
		{
			Name:  "GITLAB_INSTANCE_URL",
			Value: cr.Spec.GitLab.InstanceURL,
		},
		{
			Name:  "GITLAB_GROUP_ALLOWED_ACCESS",
			Value: cr.Spec.GitLab.GroupAllowedAccess,
		},
		{
			Name:  "GITLAB_GROUP_ALLOWED_SYSTEM_NAMESPACE_ACCESS",
			Value: cr.Spec.GitLab.GroupAllowedSystemAccess,
		},
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Port:   intstr.FromInt(3001),
				Path:   "/readyz",
				Scheme: corev1.URISchemeHTTP,
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       3,
		FailureThreshold:    3,
	}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:   "/healthz",
				Port:   intstr.FromInt(8081),
				Scheme: "HTTP",
			},
		},
		InitialDelaySeconds: 30,
		PeriodSeconds:       3,
		FailureThreshold:    10,
	}
}

func getContainers(cr *v1alpha1.Cluster, postgresEndpoint *url.URL) []corev1.Container {
	return []corev1.Container{{
		Name:  constants.GatekeeperName,
		Image: constants.DockerImageFullName(constants.GatekeeperImageName),
		Env:   getContainerEnv(cr, postgresEndpoint),
		Ports: []corev1.ContainerPort{
			{
				Name:          "http",
				ContainerPort: 3001,
			},
			{
				Name:          "metrics",
				ContainerPort: 8080,
			},
		},
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		ReadinessProbe:  getReadinessProbe(),
		LivenessProbe:   getLivenessProbe(),
	}}
}

func getDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec, postgresEndpoint *url.URL) v1.DeploymentSpec {
	var terminationGracePeriod int64 = 10

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetGatekeeperDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetGatekeeperDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:                    getContainers(cr, postgresEndpoint),
				ServiceAccountName:            GetGatekeeperDeploymentName(),
				TerminationGracePeriodSeconds: &terminationGracePeriod,
				Affinity: common.WithPodAntiAffinity(metav1.LabelSelector{
					MatchLabels: GetGatekeeperDeploymentSelector(),
				}, nil),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetGatekeeperDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetGatekeeperDeploymentName(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment, postgresEndpoint *url.URL) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec, postgresEndpoint)
	// Apply default overrides
	if err := common.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := common.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Gatekeeper.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = common.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Gatekeeper.Components.Deployment.Annotations,
	)
	current.Annotations["reloader.stakater.com/auto"] = "true"
	current.Labels = common.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.Gatekeeper.Components.Deployment.Labels,
	)

	return nil
}
