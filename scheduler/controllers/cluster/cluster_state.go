package cluster

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ClusterState struct {
	ClickHouse         ClickHouseState
	Jaeger             JaegerState
	CertMananger       CertManagerState
	PrometheusOperator PrometheusOperatorState
	Prometheus         PrometheusState
	Redis              RedisState
	ExternalDNS        ExternalDNSState
	Gatekeeper         GatekeeperState
	ErrorTrackingAPI   ErrorTrackingAPIState
	PostgresEndpoint   *v1.Secret
	// All Opstrace tenants
	Tenants *v1.NamespaceList
	// Cluster-wide monitoring state
	Monitoring MonitoringState
}

func NewClusterState() *ClusterState {
	return &ClusterState{}
}

func (i *ClusterState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readClickHouseState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readTenants(ctx, client)
	if err != nil {
		return err
	}

	err = i.readPostgresEndpoint(ctx, client)
	if err != nil {
		return err
	}

	err = i.readJaegerState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readCertManagerState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readPrometheusOperatorState(ctx, cr, client)
	if err != nil {
		return err
	}

	if err = i.readPrometheusState(ctx, cr, client); err != nil {
		return err
	}

	err = i.readRedisState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readExternalDNSState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readGatekeeperState(ctx, cr, client)
	if err != nil {
		return err
	}

	err = i.readErrorTrackingAPIState(ctx, cr, client)
	if err != nil {
		return err
	}

	if err := i.readMonitoringState(ctx, cr, client); err != nil {
		return err
	}

	return err
}

// Get ClickHouse endpoints for the scheduler user
func (i *ClusterState) GetPostgresEndpoint() (*url.URL, error) {
	if i.PostgresEndpoint == nil || i.PostgresEndpoint.Data[constants.PostgresEndpointKey] == nil {
		return nil, fmt.Errorf("%s key not set in %s secret in %s namespace. This is needed to connect to the postgres instance",
			constants.PostgresEndpointKey,
			constants.PostgresCredentialName,
			constants.PostgresCredentialNamespace)
	}
	endpoint := string(i.PostgresEndpoint.Data[constants.PostgresEndpointKey])
	return url.Parse(endpoint)
}

func (i *ClusterState) readTenants(ctx context.Context, c client.Client) error {
	currentState := &v1.NamespaceList{}
	selector, err := metav1.LabelSelectorAsSelector(
		&metav1.LabelSelector{
			MatchLabels: map[string]string{
				constants.TenantLabelIdentifier: "true",
			},
		},
	)
	if err != nil {
		return err
	}
	err = c.List(ctx, currentState, &client.ListOptions{
		LabelSelector: selector,
	})
	if err != nil {
		return err
	}
	i.Tenants = currentState.DeepCopy()
	return nil
}

func (i *ClusterState) readPostgresEndpoint(ctx context.Context, c client.Client) error {
	currentState := &v1.Secret{}
	selector := client.ObjectKey{
		Namespace: constants.PostgresCredentialNamespace,
		Name:      constants.PostgresCredentialName,
	}
	err := c.Get(ctx, selector, currentState)
	if err != nil {
		return err
	}
	i.PostgresEndpoint = currentState.DeepCopy()
	return nil
}

func (i *ClusterState) readClickHouseState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	clickHouseState := NewClickHouseState()
	if err := clickHouseState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.ClickHouse = *clickHouseState
	return nil
}

func (i *ClusterState) readJaegerState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	jaegerState := NewJaegerState()
	if err := jaegerState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Jaeger = *jaegerState
	return nil
}

func (i *ClusterState) readCertManagerState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	certManagerState := NewCertManagerState()
	if err := certManagerState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.CertMananger = *certManagerState
	return nil
}

func (i *ClusterState) readPrometheusOperatorState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	prometheusOperatorState := NewPrometheusOperatorState()
	if err := prometheusOperatorState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.PrometheusOperator = *prometheusOperatorState
	return nil
}

func (i *ClusterState) readPrometheusState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	prometheusState := NewPrometheusState()
	if err := prometheusState.Read(ctx, cr, client); err != nil {
		return err
	}
	i.Prometheus = *prometheusState
	return nil
}

func (i *ClusterState) readRedisState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	redisState := NewRedisState()
	if err := redisState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Redis = *redisState
	return nil
}

func (i *ClusterState) readExternalDNSState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	externalDNSState := NewExternalDNSState()
	if err := externalDNSState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.ExternalDNS = *externalDNSState
	return nil
}

func (i *ClusterState) readGatekeeperState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	gatekeeperState := NewGatekeeperState()
	if err := gatekeeperState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Gatekeeper = *gatekeeperState
	return nil
}

func (i *ClusterState) readErrorTrackingAPIState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	errorTrackingAPIState := NewErrorTrackingAPIState()
	if err := errorTrackingAPIState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.ErrorTrackingAPI = *errorTrackingAPIState
	return nil
}

func (i *ClusterState) readMonitoringState(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	monitoringState := NewMonitoringState()
	if err := monitoringState.Read(ctx, cr, client); err != nil {
		return err
	}

	i.Monitoring = *monitoringState
	return nil
}
