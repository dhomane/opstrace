package cluster

import (
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ReloaderReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests []byte
}

func NewReloaderReconciler(
	initialManifests []byte,
	teardown bool,
) *ReloaderReconciler {
	return &ReloaderReconciler{
		Teardown:         teardown,
		Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ReloaderInventoryID)),
		initialManifests: initialManifests,
	}
}

func (i *ReloaderReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, no point in generating manifests as we will use
		// Inventory for that
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: constants.ReloaderInventoryID,
				GetInventory:  func() *v1beta2.ResourceInventory { return cr.Status.Inventory[constants.ReloaderInventoryID] },
				Msg:           "manifests/reloader prune",
				Log:           i.Log,
			},
		}
	}

	overrideKustomization := parseReloaderOverrides(cr)
	manifests, err := ApplyOverrides(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[reloader] failed to apply overrides for reloader manifests",
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      constants.ReloaderInventoryID,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.ReloaderInventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.ReloaderInventoryID] = inv
			},
			Msg: "manifests/reloader ensure",
			Log: i.Log,
		},
	}
}

func parseReloaderOverrides(overrides *v1alpha1.Cluster) kustomize.Kustomization {
	res := kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: overrides.Namespace(),
		Resources: []string{
			tmpResourceData,
		},
		PatchesJson6902: []types.Patch{
			{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s"}]`,
					overrides.Namespace(),
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "RoleBinding",
						},
						Name: "reloader-reloader-role-binding",
					},
				},
			},
		},
	}

	return res
}
