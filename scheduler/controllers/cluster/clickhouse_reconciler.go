package cluster

import (
	"errors"
	"fmt"

	"github.com/go-logr/logr"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewClickHouseReconciler(teardown bool, logger logr.Logger) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse"),
	}
}

func (i *ClickHouseReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	// when tearing down our desired state, it is important we follow a sequence of
	// actions which allows all components to be deleted correctly without creating
	// orphans OR causing a component to be gone before anything that still references
	// and/or needs it. Though not mandatory, a good sequence to follow is the reverse
	// order of how each component got provisioned when building the desired state
	// initially.
	if i.Teardown {
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
	} else {
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	}

	desired = desired.AddActions(i.getReadiness(state, cr))

	return desired
}

func (i *ClickHouseReconciler) getReadiness(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if i.Teardown {
		if state.ClickHouse.Cluster != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ClickHouse.Cluster,
					Msg: "check clickhouse cluster is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}

	endpoints, err := state.ClickHouse.GetEndpoints()
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
			Error: err,
		}}
	}

	useRemoteStorageET, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <ET_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}
	useRemoteStorageTracing, err := common.ParseFeatureAsBool(cr.Spec.Features, "TRACING_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <TRACING_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}

	if state.ClickHouse.isClusterReady() {
		actions := []common.Action{
			common.LogAction{
				Msg: "clickhouse cluster is ready",
			},
			common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.JaegerDatabaseName),
				URL: endpoints.Native,
			},
			common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.ErrorTrackingAPIDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.ErrorTrackingAPIDatabaseName),
				URL: endpoints.Native,
			},
		}
		// add storage backend specific databases as needed
		//
		// This is currently guarded by a feature-specific value while we test & migrate
		// code over to use remote storage backends. Once things have settled down, this
		// can be cleaned up with the corresponding feature definition.
		if useRemoteStorageET {
			if cr.Spec.Target == common.GCP {
				actions = append(actions,
					common.ClickHouseAction{
						Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.ErrorTrackingGCSDatabaseName),
						SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
							constants.ErrorTrackingGCSDatabaseName),
						URL: endpoints.Native,
					})
			}
		}

		if useRemoteStorageTracing {
			switch cr.Spec.Target {
			case common.GCP:
				actions = append(actions, common.ClickHouseAction{
					Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerGCSDatabaseName),
					SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
						constants.JaegerGCSDatabaseName),
					URL: endpoints.Native,
				})
			case common.AWS:
				actions = append(actions, common.ClickHouseAction{
					Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerGCSDatabaseName),
					SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
						constants.JaegerS3DatabaseName),
					URL: endpoints.Native,
				})
			}
		}

		return actions
	}
	return []common.Action{common.LogAction{
		Msg:   "clickhouse cluster not ready",
		Error: errors.New("clickhouse cluster not ready"),
	}}
}

func (i *ClickHouseReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := clickhouse.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "clickhouse credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "clickhouse credentials",
		Mutator: func() error {
			return clickhouse.CredentialsMutator(cr, s)
		},
	}
}

func (i *ClickHouseReconciler) getClickHouseDesiredState(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if state.ClickHouse.Credentials == nil {
		if i.Teardown {
			// if the secret does not exist now AND we're tearing down the CR, we're done here
			// because the CR should have been deleted in a previous reconcile loop. Just make
			// sure that has happened successfully.
			if state.ClickHouse.Cluster != nil {
				return []common.Action{
					common.CheckGoneAction{
						Ref: state.ClickHouse.Cluster,
						Msg: "check clickhouse CR is gone",
					},
				}
			} else {
				return []common.Action{} // nothing to do
			}
		}
		// if the secret does not exist yet BUT we're in a provisioning loop, report transient error
		// and come back again during the next reconcile.
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: fmt.Errorf("haven't read clickhouse scheduler user credentials yet, this should be a transient error that resolves on next reconcile"),
			},
		}
	}

	user, err := state.ClickHouse.GetSchedulerCredentials()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: err,
			},
		}
	}
	current := clickhouse.ClickHouse(cr, user)

	if i.Teardown {
		actions := []common.Action{}
		actions = append(actions, common.GenericDeleteAction{
			Ref: current,
			Msg: "clickhouse CR",
		})
		// make sure the CR is correctly deleted after the operator is done
		// cleaning up all associated resources
		if state.ClickHouse.Cluster != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.ClickHouse.Cluster,
				Msg: "check clickhouse CR is gone",
			})
		}
		return actions
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: current,
			Msg: "Clickhouse CR",
			Mutator: func() error {
				return clickhouse.ClickHouseMutator(cr, current, user)
			},
		},
	}
}

func (i *ClickHouseReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouse.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse cluster servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse cluster servicemonitor",
		Mutator: func() error {
			return clickhouse.ServiceMonitorMutator(cr, monitor)
		},
	}
}
