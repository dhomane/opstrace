package jaegerOperator

import (
	_ "embed"
	"html/template"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"

	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Embed go template files at compile time
//
//go:embed rbac.go.tmpl
var rbacTmpl string

// Get all RBAC objects from the embedded go templates above
func GetRBACObjects(cr *v1alpha1.Cluster) ([]client.Object, error) {
	rbacTemplate := template.New("jaeger-operator-rbac")

	rbac, err := common.RenderTemplate(rbacTemplate, rbacTmpl, map[string]interface{}{
		"Name":      constants.JaegerOperatorName,
		"Namespace": cr.Namespace(),
	})
	if err != nil {
		return []client.Object{}, err
	}

	return common.ParseYaml(rbac)
}
