package jaegerOperator

import (
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "128Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "256Mi"
	CpuLimit      = "200m"
)

var Replicas int32 = 1

func GetJaegerOperatorDeploymentName() string {
	return constants.JaegerOperatorName
}

func GetJaegerOperatorDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.JaegerOperatorName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.JaegerOperatorName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.JaegerOperatorName,
	}
}

func getVolumes() []corev1.Volume {
	var defaultMode int32 = 420
	return []corev1.Volume{{
		Name: "cert",
		VolumeSource: corev1.VolumeSource{
			Secret: &corev1.SecretVolumeSource{
				SecretName:  constants.JaegerOperatorServiceCertName,
				DefaultMode: &defaultMode,
			},
		},
	}}
}

func getOperatorContainerEnv() []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name:  "OPERATOR_NAME",
			Value: GetJaegerOperatorDeploymentName(),
		},
		{
			// Set to empty string: Watch all (per-tenant) namespaces
			Name:  "WATCH_NAMESPACE",
			Value: "",
		},
		{
			Name: "POD_NAME",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.name"},
			},
		},
		{
			Name: "POD_NAMESPACE",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.namespace"},
			},
		},
	}
}

func getVolumeMounts() []corev1.VolumeMount {
	return []corev1.VolumeMount{{
		MountPath: "/tmp/k8s-webhook-server/serving-certs",
		Name:      "cert",
		ReadOnly:  true,
	}}
}

func getPorts() []corev1.ContainerPort {
	return []corev1.ContainerPort{
		{
			ContainerPort: 8383,
			Name:          "http-metrics",
		},
		{
			ContainerPort: 8686,
			Name:          "cr-metrics",
		},
	}
}

func getContainers() []corev1.Container {
	return []corev1.Container{{
		Name:  "jaeger-operator",
		Image: constants.OpstraceImages().JaegerOperatorImage,
		Args: []string{
			"start",
		},
		Env:             getOperatorContainerEnv(),
		Ports:           getPorts(),
		Resources:       getResources(),
		VolumeMounts:    getVolumeMounts(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		LivenessProbe:   getLivenessProbe(),
		ReadinessProbe:  getReadinessProbe(),
		SecurityContext: &corev1.SecurityContext{
			AllowPrivilegeEscalation: pointer.Bool(false),
			RunAsNonRoot:             pointer.Bool(true),
		},
	}}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path: "/healthz",
				Port: intstr.FromInt(8081),
			},
		},
		InitialDelaySeconds: 15,
		PeriodSeconds:       20,
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path: "/readyz",
				Port: intstr.FromInt(8081),
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       10,
	}
}

func getDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetJaegerOperatorDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetJaegerOperatorDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Volumes:            getVolumes(),
				Containers:         getContainers(),
				ServiceAccountName: GetJaegerOperatorDeploymentName(),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetJaegerOperatorDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetJaegerOperatorDeploymentName(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.JaegerOperator.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.JaegerOperator.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.JaegerOperator.Components.Deployment.Labels,
	)

	return nil
}
