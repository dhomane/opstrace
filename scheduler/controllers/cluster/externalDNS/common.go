package externalDNS

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/externalDNS/cloudflare"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/externalDNS/gcp"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	errNoProvider       string = "no supported externaldns provider configured"
	errMultipleProvider string = "multiple externaldns providers configured"
)

func validateClusterConfiguration(cr *v1alpha1.Cluster) {
	var providerCount int = 0

	// gcp
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		providerCount++
	}
	// cloudflare
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		providerCount++
	}

	// assertions
	if providerCount == 0 {
		panic(errNoProvider)
	}
	if providerCount > 1 {
		panic(errMultipleProvider)
	}
}

func ExternalDNSSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.ExternalDNSName,
	}
}

func ExternalDNSServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	validateClusterConfiguration(cr)
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSServiceAccount(cr)
	}
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		return cloudflare.ExternalDNSServiceAccount(cr)
	}
	return nil
}

func ExternalDNSServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	validateClusterConfiguration(cr)
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		gcp.ExternalDNSServiceAccountMutator(cr, current)
	}
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		cloudflare.ExternalDNSServiceAccountMutator(cr, current)
	}
}

func GetRBACObjects(cr *v1alpha1.Cluster) ([]client.Object, error) {
	validateClusterConfiguration(cr)
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.GetRBACObjects(cr)
	}
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		return cloudflare.GetRBACObjects(cr)
	}
	return nil, nil
}

func ExternalDNSDeployment(cr *v1alpha1.Cluster) *appsv1.Deployment {
	validateClusterConfiguration(cr)
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSDeployment(cr)
	}
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		return cloudflare.ExternalDNSDeployment(cr)
	}
	return nil
}

func ExternalDNSDeploymentMutator(cr *v1alpha1.Cluster, current *appsv1.Deployment) error {
	validateClusterConfiguration(cr)
	if cr.Spec.DNS.ExternalDNSProvider.GCP != nil {
		return gcp.ExternalDNSDeploymentMutator(cr, current)
	}
	if cr.Spec.DNS.ExternalDNSProvider.Cloudflare != nil {
		return cloudflare.ExternalDNSDeploymentMutator(cr, current)
	}
	return nil
}
