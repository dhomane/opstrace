package cloudflare

import (
	"fmt"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
)

const (
	MemoryRequest = "100Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "200Mi"
	CpuLimit      = "200m"
)

var (
	replicas int32 = 1
)

func ExternalDNSDeployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetExternalDNSDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func ExternalDNSDeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getExternalDNSDeploymentSpec(cr, current.Spec)
	// apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Spec,
	); err != nil {
		return err
	}

	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getExternalDNSDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getExternalDNSDeploymentLabels(),
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Labels,
	)
	return nil
}

func getExternalDNSDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getExternalDNSDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

func getExternalDNSDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetExternalDNSDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetExternalDNSDeploymentName(),
				Labels:      getExternalDNSPodLabels(),
				Annotations: getExternalDNSPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getExternalDNSContainers(cr),
				ServiceAccountName: GetExternalDNSDeploymentName(),
			},
		},
		Strategy: getExternalDNSDeploymentStrategy(),
	}
}

func GetExternalDNSDeploymentName() string {
	return constants.ExternalDNSName
}

func GetExternalDNSDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

func getExternalDNSPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getExternalDNSPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

// https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/cloudflare.md
func getExternalDNSContainers(cr *v1alpha1.Cluster) []corev1.Container {
	args := []string{
		"--log-level=info",
		"--source=service",
		"--provider=cloudflare",
		"--cloudflare-proxied",
	}

	domain := cr.Spec.DNS.Domain
	if domain != nil {
		args = append(args, fmt.Sprintf("--domain-filter=%s", *domain))
	}

	zone := cr.Spec.DNS.ExternalDNSProvider.Cloudflare.ZoneID
	if zone != nil {
		args = append(args, fmt.Sprintf("--zone-id-filter=%s", *zone))
	}

	return []corev1.Container{{
		Name:            constants.ExternalDNSName,
		Image:           constants.OpstraceImages().ExternalDNSImage,
		Args:            args,
		Env:             getContainerEnv(cr),
		Resources:       getExternalDNSResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}}
}

func getContainerEnv(cr *v1alpha1.Cluster) []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name: constants.CloudflareAPITokenKey,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.DNS.ExternalDNSProvider.Cloudflare.CFAuthSecret.Name,
					},
					Key:      constants.CloudflareAPITokenKey,
					Optional: pointer.Bool(true),
				},
			},
		},
		{
			Name: constants.CloudflareAPIKeyKey,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.DNS.ExternalDNSProvider.Cloudflare.CFAuthSecret.Name,
					},
					Key:      constants.CloudflareAPIKeyKey,
					Optional: pointer.Bool(true),
				},
			},
		},
		{
			Name: constants.CloudflareAPIEmailKey,
			ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: cr.Spec.DNS.ExternalDNSProvider.Cloudflare.CFAuthSecret.Name,
					},
					Key:      constants.CloudflareAPIEmailKey,
					Optional: pointer.Bool(true),
				},
			},
		},
	}
}

func getExternalDNSResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getExternalDNSDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}
