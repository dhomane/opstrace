package gcp

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "100Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "200Mi"
	CpuLimit      = "200m"
)

var Replicas int32 = 1

func GetExternalDNSDeploymentName() string {
	return constants.ExternalDNSName
}

func GetExternalDNSDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

func getExternalDNSResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getExternalDNSDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getExternalDNSDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

func getExternalDNSDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getExternalDNSPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getExternalDNSPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ExternalDNSName,
	}
}

func getExternalDNSContainers(cr *v1alpha1.Cluster) []corev1.Container {
	domain := cr.Spec.DNS.Domain

	return []corev1.Container{{
		Name:  constants.ExternalDNSName,
		Image: constants.OpstraceImages().ExternalDNSImage,
		Args: []string{
			"--source=service",
			"--source=ingress",
			"--provider=google",
			"--registry=txt",
			"--log-level=info",
			fmt.Sprintf("--domain-filter=%s", *domain),
			fmt.Sprintf("--txt-owner-id=opstrace-%s", cr.Name),
		},
		Resources:       getExternalDNSResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}}
}

func getExternalDNSDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetExternalDNSDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetExternalDNSDeploymentName(),
				Labels:      getExternalDNSPodLabels(),
				Annotations: getExternalDNSPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getExternalDNSContainers(cr),
				ServiceAccountName: GetExternalDNSDeploymentName(),
			},
		},
		Strategy: getExternalDNSDeploymentStrategy(),
	}
}

func ExternalDNSDeployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetExternalDNSDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func ExternalDNSDeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getExternalDNSDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getExternalDNSDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getExternalDNSDeploymentLabels(),
		cr.Spec.Overrides.ExternalDNS.Components.Deployment.Labels,
	)

	return nil
}
