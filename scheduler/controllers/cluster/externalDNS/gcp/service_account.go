package gcp

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getExternalDNSServiceAccountName() string {
	return GetExternalDNSDeploymentName()
}

func getExternalDNSServiceAccountLabels() map[string]string {
	return GetExternalDNSDeploymentSelector()
}

func getExternalDNSServiceAccountAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	if cr.Spec.DNS.ExternalDNSProvider.GCP.DNSServiceAccountName == "" {
		panic("must configure valid Spec.DNS.ExternalDNSProvider.GCP.DNSServiceAccountName")
	}
	desired := map[string]string{
		"iam.gke.io/gcp-service-account": cr.Spec.DNS.ExternalDNSProvider.GCP.DNSServiceAccountName,
	}
	return common.MergeMap(existing, desired)
}

func getExternalDNSServiceAccountImagePullSecrets(cr *v1alpha1.Cluster) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func ExternalDNSServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getExternalDNSServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getExternalDNSServiceAccountLabels(),
			Annotations: getExternalDNSServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getExternalDNSServiceAccountImagePullSecrets(cr),
	}
}

func ExternalDNSServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getExternalDNSServiceAccountLabels()
	current.Annotations = getExternalDNSServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getExternalDNSServiceAccountImagePullSecrets(cr)
}
