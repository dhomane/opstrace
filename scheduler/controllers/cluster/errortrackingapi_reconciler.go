package cluster

import (
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	errortrackingapi "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/errorTrackingAPI"
)

type ErrorTrackingAPIReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewErrorTrackingAPIReconciler(teardown bool, logger logr.Logger) *ErrorTrackingAPIReconciler {
	return &ErrorTrackingAPIReconciler{
		Teardown: teardown,
		Log:      logger.WithName("errortracking"),
	}
}

func (r *ErrorTrackingAPIReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(r.getServiceAccountDesiredState(cr))
	// desired = desired.AddActions(r.getRBACDesiredState(cr))
	desired = desired.AddAction(r.getServiceDesiredState(cr))
	desired = desired.AddAction(r.getStatefulSetDesiredState(cr, state))
	desired = desired.AddAction(r.getIngressDesiredState(cr))
	desired = desired.AddAction(r.getServiceMonitorDesiredState(cr))
	desired = desired.AddActions(r.getReadiness(state))

	return desired
}

func (r *ErrorTrackingAPIReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := errortrackingapi.ServiceMonitor(cr)

	if r.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "errortracking-api service monitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "errortracking-api service monitor",
		Mutator: func() error {
			return errortrackingapi.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (r *ErrorTrackingAPIReconciler) getReadiness(state *ClusterState) []common.Action {
	if r.Teardown {
		if state.ErrorTrackingAPI.Statefulset != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ErrorTrackingAPI.Statefulset,
					Msg: "check errortracking-api statefulset is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.ErrorTrackingAPI.Statefulset,
			Msg: "check errortracking-api deployment readiness",
		},
		common.IngressReadyAction{
			Ref: state.ErrorTrackingAPI.Ingress,
			Msg: "check errortracking-api ingress readiness",
		},
	}
}

func (r *ErrorTrackingAPIReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := errortrackingapi.ServiceAccount(cr)

	if r.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "errortracking-api service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "errortracking-api service account",
		Mutator: func() error {
			errortrackingapi.ServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (r *ErrorTrackingAPIReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := errortrackingapi.Service(cr)

	if r.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "errortracking-api service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "errortracking-api service",
		Mutator: func() error {
			return errortrackingapi.ServiceMutator(cr, sa)
		},
	}
}

func (r *ErrorTrackingAPIReconciler) getStatefulSetDesiredState(cr *v1alpha1.Cluster, state *ClusterState) common.Action {
	sts := errortrackingapi.StatefulSet(cr)

	if r.Teardown {
		return common.GenericDeleteAction{
			Ref: sts,
			Msg: "errortracking-api statefulset",
		}
	}

	clickhouseEndpoints, err := state.ClickHouse.GetEndpoints()
	if err != nil {
		return common.LogAction{
			Msg:   "failed to retrieve clickhouse endpoints",
			Error: err,
		}
	}

	// TODO: check how to create a user/pwd for error tracking
	clickhouseDSN := clickhouseEndpoints.Native.String() + "/" + constants.ErrorTrackingAPIDatabaseName

	useRemoteStorage, err := common.ParseFeatureAsBool(cr.Spec.Features, "ET_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return common.LogAction{
			Msg:   "failed to parse value for feature <ET_USE_REMOTE_STORAGE_BACKEND> as a boolean",
			Error: err,
		}
	}

	if useRemoteStorage {
		switch cr.Spec.Target {
		case common.GCP:
			clickhouseDSN = clickhouseEndpoints.Native.String() + "/" + constants.ErrorTrackingGCSDatabaseName
		case common.AWS:
			clickhouseDSN = clickhouseEndpoints.Native.String() + "/" + constants.ErrorTrackingS3DatabaseName
		}
	}

	apiBaseURL := cr.Spec.GetHostURL()
	if cr.Spec.DNS.Domain != nil {
		apiBaseURL = "https://errortracking." + *cr.Spec.DNS.Domain
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sts,
		Msg: "errortracking-api statefulset",
		Mutator: func() error {
			return errortrackingapi.StatefulSetMutator(cr, sts, clickhouseDSN, apiBaseURL)
		},
	}
}

func (i *ErrorTrackingAPIReconciler) getIngressDesiredState(cr *v1alpha1.Cluster) common.Action {
	ing := errortrackingapi.Ingress(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: ing,
			Msg: "errortracking-api ingress",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ing,
		Msg: "errortracking-api ingress",
		Mutator: func() error {
			return errortrackingapi.IngressMutator(cr, ing)
		},
	}
}
