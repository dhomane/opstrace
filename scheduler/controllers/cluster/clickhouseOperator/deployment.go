package clickhouseOperator

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	MemoryRequest = "64Mi"
	CpuRequest    = "10m"
	MemoryLimit   = "128Mi"
	CpuLimit      = "500m"
)

func GetClickHouseOperatorDeploymentName() string {
	return constants.ClickHouseOperatorName
}

func GetClickHouseOperatorDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getPodLabels() map[string]string {
	return map[string]string{
		"app": constants.ClickHouseOperatorName,
	}
}

func getReadinessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Port:   intstr.FromInt(8081),
				Path:   "/readyz",
				Scheme: corev1.URISchemeHTTP,
			},
		},
		InitialDelaySeconds: 5,
		PeriodSeconds:       10,
	}
}

func getLivenessProbe() *corev1.Probe {
	return &corev1.Probe{
		ProbeHandler: corev1.ProbeHandler{
			HTTPGet: &corev1.HTTPGetAction{
				Path:   "/healthz",
				Port:   intstr.FromInt(8081),
				Scheme: "HTTP",
			},
		},
		InitialDelaySeconds: 15,
		PeriodSeconds:       20,
	}
}

func getPodAnnotations(existing map[string]string) map[string]string {
	return existing
}

func getContainers() []corev1.Container {
	return []corev1.Container{{
		Name:    constants.ClickHouseOperatorName,
		Image:   constants.DockerImageFullName(constants.ClickHouseImageName),
		Command: []string{"/manager"},
		Ports: []corev1.ContainerPort{
			{
				Name:          "metrics",
				ContainerPort: 8080,
			},
			{
				Name:          "health",
				ContainerPort: 8081,
			}},
		Resources:       getResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
		ReadinessProbe:  getReadinessProbe(),
		LivenessProbe:   getLivenessProbe(),
	}}
}

func getDeploymentSpec(current v1.DeploymentSpec) v1.DeploymentSpec {
	replicas := int32(1)
	return v1.DeploymentSpec{
		Replicas: &replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetClickHouseOperatorDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetClickHouseOperatorDeploymentName(),
				Labels:      getPodLabels(),
				Annotations: getPodAnnotations(current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getContainers(),
				ServiceAccountName: GetClickHouseOperatorDeploymentName(),
			},
		},
		Strategy: getDeploymentStrategy(),
	}
}

func Deployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetClickHouseOperatorDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func DeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetClickHouseOperatorDeploymentName(),
	}
}

func DeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getDeploymentSpec(current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}

	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getDeploymentLabels(),
		cr.Spec.Overrides.ClickHouseOperator.Components.Deployment.Labels,
	)

	return nil
}
