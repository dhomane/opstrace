package cluster

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"strings"

	"github.com/fluxcd/pkg/ssa"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/krusty"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/filesys"
)

const tmpResourceData = "data.yml"

func embeddedFStoInMemFs(upstream embed.FS) (filesys.FileSystem, error) {
	res := filesys.MakeFsInMemory()

	// embed.FS and filesys.FileSystem interfaces are not compatible, hence we
	// copy everything over as krusty requires filesys.FileSystem

	err := fs.WalkDir(upstream, ".", func(path string, d fs.DirEntry, walkErr error) error {
		if walkErr != nil {
			return fmt.Errorf("encountered a walk error: %w", walkErr)
		}

		if d.IsDir() {
			return res.Mkdir(path)
		}

		if !strings.HasSuffix(d.Name(), ".yml") && !strings.HasSuffix(d.Name(), ".yaml") {
			return nil
		}

		data, err := upstream.ReadFile(path)
		if err != nil {
			// Wut?
			return fmt.Errorf("failed to read path %s from embedded file system: %w", path, err)
		}

		if err := res.WriteFile(path, data); err != nil {
			return fmt.Errorf("failed to write path %s to the in-memory file system: %w", path, err)
		}
		log.Log.Info("manifest was read from embedded filesystem", "path", path)

		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("unable to traverse embedded filesystem: %w", err)
	}

	return res, nil
}

// ParseManifests parses embedded manifests and then applies default
// kustomizations. The result is a map with keys being
// directories in the resulting filesystem, and values being byte blobs
// containing parsed manifests.
func InitialManifests(upstream embed.FS) (map[string][]byte, error) {
	manifestsFS, err := embeddedFStoInMemFs(upstream)
	if err != nil {
		return nil, fmt.Errorf("unable to merge overlay onto embedded manifests: %w", err)
	}
	log.Log.Info("Embedded manifests were uploaded into Memoryfs")

	// NOTE(prozlach): We assume that every directory we find is non-empty and
	// contains correct manifests, if not - krusty will fail. We can perform
	// extra checks during the mergeFSes call to handle it more gracefully
	dirs, err := manifestsFS.ReadDir(".")
	if err != nil {
		return nil, fmt.Errorf("unable to list directories in merged FS: %w", err)
	}

	res := make(map[string][]byte)
	opts := krusty.MakeDefaultOptions()
	opts.DoPrune = true
	kustomizer := krusty.MakeKustomizer(opts)

	for _, dir := range dirs {
		log.Log.Info("kustomizing initial manifests", "subcomponent", dir)
		resourceMap, err := kustomizer.Run(manifestsFS, dir)
		if err != nil {
			return nil, fmt.Errorf("kustomization apply of directory %q failed: %w", dir, err)
		}

		yamlResources, err := resourceMap.AsYaml()
		if err != nil {
			return nil, fmt.Errorf("failed to render resource map as YAML for dir %q: %w", dir, err)
		}

		res[dir] = yamlResources
	}

	return res, nil
}

// ApplyOverrides applies kustomizations to given set of manifests and returns
// a resulting set of manifests parsed into unstructured.Unstructured.
func ApplyOverrides(
	manifests []byte,
	overrideKustomization kustomize.Kustomization,
) ([]*unstructured.Unstructured, error) {
	opts := krusty.MakeDefaultOptions()
	opts.DoPrune = true
	kustomizer := krusty.MakeKustomizer(opts)

	tmpfs := filesys.MakeFsInMemory()

	data, err := json.Marshal(overrideKustomization)
	if err != nil {
		return nil, fmt.Errorf("unable to marshal kustomization to YAML: %w", err)
	}
	err = tmpfs.WriteFile("kustomization.yaml", data)
	if err != nil {
		return nil, fmt.Errorf("unable to write kustomization blob to in-memory FS: %w", err)
	}

	err = tmpfs.WriteFile(tmpResourceData, manifests)
	if err != nil {
		return nil, fmt.Errorf("unable to write manifest blob to in-memory FS: %w", err)
	}

	resourceMap, err := kustomizer.Run(tmpfs, ".")
	if err != nil {
		return nil, fmt.Errorf("kustomization apply failed: %w", err)
	}

	yamlResources, err := resourceMap.AsYaml()
	if err != nil {
		return nil, fmt.Errorf("failed to render resource as YAML: %w", err)
	}

	// Convert the build result into Kubernetes unstructured objects.
	objects, err := ssa.ReadObjects(bytes.NewReader(yamlResources))
	if err != nil {
		return nil, fmt.Errorf("failed to parse overridden objects into Unstructured")
	}

	return objects, nil
}
