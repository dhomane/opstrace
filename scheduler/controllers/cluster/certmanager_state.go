package cluster

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	cm "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/certManager"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type CertManagerState struct {
	// Track deployment readiness
	CertManagerDeployment *appsv1.Deployment
	CaInjectorDeployment  *appsv1.Deployment
}

func NewCertManagerState() *CertManagerState {
	return &CertManagerState{}
}

func (i *CertManagerState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readDeployment(ctx, cr, client)
	if err != nil {
		return err
	}
	err = i.readCainjector(ctx, cr, client)
	if err != nil {
		return err
	}
	return nil
}

func (i *CertManagerState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := cm.CertManagerDeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.CertManagerDeployment = currentState.DeepCopy()
	return nil
}

func (i *CertManagerState) readCainjector(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := cm.CainjectorDeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.CaInjectorDeployment = currentState.DeepCopy()
	return nil
}
