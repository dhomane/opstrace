package storage

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	storage "k8s.io/api/storage/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getProvisioner(cr *v1alpha1.Cluster) string {
	switch cr.Spec.Target {
	case common.AWS:
		return "kubernetes.io/aws-ebs"
	case common.GCP:
		return "pd.csi.storage.gke.io"
	case common.KIND:
		return "rancher.io/local-path"
	default:
		panic("unsupported spec.Target")
	}
}

func getType(cr *v1alpha1.Cluster) string {
	switch cr.Spec.Target {
	case common.AWS:
		return "gp2"
	case common.GCP:
		return "pd-ssd"
	case common.KIND:
		return ""
	default:
		panic("unsupported spec.Target")
	}
}

func StorageClass(cr *v1alpha1.Cluster) *storage.StorageClass {
	bindingMode := storage.VolumeBindingWaitForFirstConsumer
	allowExpansion := true
	params := map[string]string{}
	paramType := getType(cr)
	if paramType != "" {
		params["type"] = paramType
	}

	return &storage.StorageClass{
		ObjectMeta: metav1.ObjectMeta{
			Name:      constants.StorageClassName,
			Namespace: cr.Namespace(),
		},
		Parameters:           params,
		Provisioner:          getProvisioner(cr),
		VolumeBindingMode:    &bindingMode,
		AllowVolumeExpansion: &allowExpansion,
	}
}

func StorageClassMutator(cr *v1alpha1.Cluster, current *storage.StorageClass) {
	class := StorageClass(cr)
	current.ObjectMeta = class.ObjectMeta
	current.Parameters = class.Parameters
	current.Provisioner = class.Provisioner
	current.VolumeBindingMode = class.VolumeBindingMode
	current.AllowVolumeExpansion = class.AllowVolumeExpansion
}
