package certManager

import (
	_ "embed"
	"html/template"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"

	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Embed go template files at compile time
//
//go:embed rbac.go.tmpl
var rbacTmpl string

//go:embed cainjector-rbac.go.tmpl
var caInjectorRbacTmpl string

// Get all RBAC objects from the embedded go templates above
func GetRBACObjects(cr *v1alpha1.Cluster) ([]client.Object, error) {
	rbacTemplate := template.New("certmanager-rbac")
	caInjectorRBACTemplate := template.New("certmanager-cainjector-rbac")

	objects := make([]client.Object, 0)

	rbac, err := common.RenderTemplate(rbacTemplate, rbacTmpl, map[string]interface{}{
		"Name":      constants.CertManagerName,
		"Namespace": cr.Namespace(),
	})
	if err != nil {
		return objects, err
	}
	caInjectorrbac, err := common.RenderTemplate(caInjectorRBACTemplate, caInjectorRbacTmpl, map[string]interface{}{
		"Name":      constants.CainjectorName,
		"Namespace": cr.Namespace(),
	})
	if err != nil {
		return objects, err
	}
	rbacObjects, err := common.ParseYaml(rbac)
	if err != nil {
		return objects, err
	}
	caInjectorRBACObjects, err := common.ParseYaml(caInjectorrbac)
	if err != nil {
		return objects, err
	}

	return append(rbacObjects, caInjectorRBACObjects...), nil
}
