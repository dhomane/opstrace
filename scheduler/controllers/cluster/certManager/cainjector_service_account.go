package certManager

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getCainjectorServiceAccountName() string {
	return GetCainjectorDeploymentName()
}

func getCainjectorServiceAccountLabels() map[string]string {
	return GetCainjectorDeploymentSelector()
}

func getCainjectorServiceAccountAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getCainjectorServiceAccountImagePullSecrets(cr *v1alpha1.Cluster) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func CainjectorServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getCainjectorServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getCainjectorServiceAccountLabels(),
			Annotations: getCainjectorServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getCainjectorServiceAccountImagePullSecrets(cr),
	}
}

func CainjectorServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getCainjectorServiceAccountLabels()
	current.Annotations = getCainjectorServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getCainjectorServiceAccountImagePullSecrets(cr)
}
