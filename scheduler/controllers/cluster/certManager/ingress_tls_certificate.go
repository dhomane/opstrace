package certManager

import (
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Certificate(cr *v1alpha1.Cluster) *certmanager.Certificate {
	res := &certmanager.Certificate{
		ObjectMeta: v1.ObjectMeta{
			Name:      constants.HTTPSCertSecretName,
			Namespace: cr.Namespace(),
		},
		Spec: certmanager.CertificateSpec{
			DNSNames: []string{cr.Spec.GetHost()},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "ClusterIssuer",
			},
			SecretName: constants.HTTPSCertSecretName,
		},
	}

	if cr.Spec.Target != common.KIND {
		res.Spec.IssuerRef.Name = cr.Spec.DNS.CertificateIssuer
	} else {
		res.Spec.IssuerRef.Name = constants.SelfSignedCAIssuerName
	}

	return res
}

func CertificateMutator(cr *v1alpha1.Cluster, current *certmanager.Certificate) {
	cert := Certificate(cr)
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
}
