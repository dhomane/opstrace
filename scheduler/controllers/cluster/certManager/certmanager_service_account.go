package certManager

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getCertManagerServiceAccountName() string {
	return GetCertManagerDeploymentName()
}

func getCertManagerServiceAccountLabels() map[string]string {
	return GetCertManagerDeploymentSelector()
}

func getCertManagerServiceAccountAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	annotations := map[string]string{}

	if cr.Spec.Target == common.GCP {
		if cr.Spec.DNS.GCPCertManagerServiceAccount == nil {
			// For now we can assume that if deploying on GCP, the DNS provider is also GCP.
			// In the future we could support any ExternalDNS provider and at that point we
			// should drop this nil check block.
			panic("target is GCP and must have cr.Spec.DNS.GCPCertManagerServiceAccount set")
		}
		annotations["iam.gke.io/gcp-service-account"] = *cr.Spec.DNS.GCPCertManagerServiceAccount
	}
	return common.MergeMap(existing, annotations)
}

func getCertManagerServiceAccountImagePullSecrets(cr *v1alpha1.Cluster) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func CertManagerServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getCertManagerServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getCertManagerServiceAccountLabels(),
			Annotations: getCertManagerServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getCertManagerServiceAccountImagePullSecrets(cr),
	}
}

func CertManagerServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getCertManagerServiceAccountLabels()
	current.Annotations = getCertManagerServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getCertManagerServiceAccountImagePullSecrets(cr)
}
