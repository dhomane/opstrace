package certManager

import (
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

const (
	MemoryRequest = "128Mi"
	CpuRequest    = "100m"
	MemoryLimit   = "512Mi"
	CpuLimit      = "500m"
)

var Replicas int32 = 1

func GetCertManagerDeploymentName() string {
	return constants.CertManagerName
}

func GetCertManagerDeploymentSelector() map[string]string {
	return map[string]string{
		"app": constants.CertManagerName,
	}
}

func getCertManagerResources() corev1.ResourceRequirements {
	return corev1.ResourceRequirements{
		Requests: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryRequest),
			corev1.ResourceCPU:    resource.MustParse(CpuRequest),
		},
		Limits: corev1.ResourceList{
			corev1.ResourceMemory: resource.MustParse(MemoryLimit),
			corev1.ResourceCPU:    resource.MustParse(CpuLimit),
		},
	}
}

func getCertManagerDeploymentStrategy() v1.DeploymentStrategy {
	return v1.DeploymentStrategy{
		Type: v1.RecreateDeploymentStrategyType,
	}
}

func getCertManagerDeploymentLabels() map[string]string {
	return map[string]string{
		"app": constants.CertManagerName,
	}
}

func getCertManagerDeploymentAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getCertManagerPodAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getCertManagerPodLabels() map[string]string {
	return map[string]string{
		"app": constants.CertManagerName,
	}
}

func getCertManagerContainerEnv() []corev1.EnvVar {
	return []corev1.EnvVar{
		{
			Name: "POD_NAMESPACE",
			ValueFrom: &corev1.EnvVarSource{
				FieldRef: &corev1.ObjectFieldSelector{FieldPath: "metadata.namespace"},
			},
		},
	}
}

func getCertManagerPorts() []corev1.ContainerPort {
	return []corev1.ContainerPort{
		{
			ContainerPort: 9402,
			Name:          "metrics",
			Protocol:      "TCP",
		},
	}
}

func getCertManagerContainers() []corev1.Container {
	return []corev1.Container{{
		Name:  constants.CertManagerName,
		Image: constants.OpstraceImages().CertManagerImage,
		Args: []string{
			"--v=2",
			"--cluster-resource-namespace=$(POD_NAMESPACE)",
			"--leader-elect=false",
		},
		Env:             getCertManagerContainerEnv(),
		Ports:           getCertManagerPorts(),
		Resources:       getCertManagerResources(),
		ImagePullPolicy: corev1.PullIfNotPresent,
	}}
}

func getCertManagerDeploymentSpec(cr *v1alpha1.Cluster, current v1.DeploymentSpec) v1.DeploymentSpec {
	var user int64 = 1001

	return v1.DeploymentSpec{
		Replicas: &Replicas,
		Selector: &metav1.LabelSelector{
			MatchLabels: GetCertManagerDeploymentSelector(),
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Name:        GetCertManagerDeploymentName(),
				Labels:      getCertManagerPodLabels(),
				Annotations: getCertManagerPodAnnotations(cr, current.Template.Annotations),
			},
			Spec: corev1.PodSpec{
				Containers:         getCertManagerContainers(),
				ServiceAccountName: GetCertManagerDeploymentName(),
				SecurityContext: &corev1.PodSecurityContext{
					//
					// for cert-manager to be able to read the the service account
					// token with the iam role. otherwise the challenge will fail
					// with:
					//
					// Warning  PresentError  17s (x4 over 42s)  cert-manager
					// Error presenting challenge: error instantiating route53
					// challenge solver: unable to assume role: WebIdentityErr:
					// failed fetching WebIdentity token: caused by: WebIdentityErr:
					// unable to read file at
					// /var/run/secrets/eks.amazonaws.com/serviceaccount/token
					// caused by: open
					// /var/run/secrets/eks.amazonaws.com/serviceaccount/token:
					// permission denied
					//
					// https://github.com/aws/amazon-eks-pod-identity-webhook/issues/8
					//
					FSGroup:   &user,
					RunAsUser: &user,
				},
			},
		},
		Strategy: getCertManagerDeploymentStrategy(),
	}
}

func CertManagerDeployment(cr *v1alpha1.Cluster) *v1.Deployment {
	return &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      GetCertManagerDeploymentName(),
			Namespace: cr.Namespace(),
		},
	}
}

func CertManagerDeploymentSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      GetCertManagerDeploymentName(),
	}
}

func CertManagerDeploymentMutator(cr *v1alpha1.Cluster, current *v1.Deployment) error {
	currentSpec := &current.Spec
	spec := getCertManagerDeploymentSpec(cr, current.Spec)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.CertManager.Components.Deployment.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getCertManagerDeploymentAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.CertManager.Components.Deployment.Annotations,
	)
	current.Labels = utils.MergeMap(
		getCertManagerDeploymentLabels(),
		cr.Spec.Overrides.CertManager.Components.Deployment.Labels,
	)

	return nil
}
