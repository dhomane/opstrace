package certManager

import (
	cmacme "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	certmanager "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getStagingDNSServer(cr *v1alpha1.Cluster) string {
	s := cr.Spec.DNS.ACMEserver
	if s != nil {
		return *s
	}
	return "https://acme-staging-v02.api.letsencrypt.org/directory"
}

func StagingClusterIssuer(cr *v1alpha1.Cluster) *certmanager.ClusterIssuer {
	return &certmanager.ClusterIssuer{
		ObjectMeta: v1.ObjectMeta{
			Name: constants.LetsEncryptStaging,
		},
		Spec: certmanager.IssuerSpec{
			IssuerConfig: certmanager.IssuerConfig{
				ACME: &cmacme.ACMEIssuer{
					Server: getStagingDNSServer(cr),
					Email:  getACMEEmail(cr),
					PrivateKey: cmmeta.SecretKeySelector{
						LocalObjectReference: cmmeta.LocalObjectReference{
							Name: "letsencrypt-staging",
						},
					},
					Solvers: []cmacme.ACMEChallengeSolver{{
						DNS01: getDNSChallenge(cr),
					}},
				},
			},
		},
	}
}

func StagingClusterIssuerMutator(cr *v1alpha1.Cluster, current *certmanager.ClusterIssuer) {
	issuer := StagingClusterIssuer(cr)
	current.Spec.SelfSigned = issuer.Spec.SelfSigned
}
