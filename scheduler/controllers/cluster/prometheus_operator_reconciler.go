package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/prometheusOperator"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type PrometheusOperatorReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewPrometheusOperatorReconciler(teardown bool, logger logr.Logger) *PrometheusOperatorReconciler {
	return &PrometheusOperatorReconciler{
		Teardown: teardown,
		Log:      logger.WithName("prometheus-operator"),
	}
}

func (i *PrometheusOperatorReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddAction(i.getDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	desired = desired.AddActions(i.getReadiness(state))

	return desired
}

func (i *PrometheusOperatorReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.PrometheusOperator.OperatorDeployment != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.PrometheusOperator.OperatorDeployment,
					Msg: "check prometheus-operator deployment is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.PrometheusOperator.OperatorDeployment,
			Msg: "check prometheus-operator deployment readiness",
		},
	}
}

func (i *PrometheusOperatorReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := prometheusOperator.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize prometheus-operator rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("prometheus-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("prometheus-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *PrometheusOperatorReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := prometheusOperator.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "prometheus-operator service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "prometheus-operator service",
		Mutator: func() error {
			return prometheusOperator.ServiceMutator(cr, svc)
		},
	}
}

func (i *PrometheusOperatorReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := prometheusOperator.PrometheusOperatorServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "prometheus-operator service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "prometheus-operator service account",
		Mutator: func() error {
			prometheusOperator.PrometheusOperatorServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *PrometheusOperatorReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := prometheusOperator.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "prometheus-operator servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "prometheus-operator servicemonitor",
		Mutator: func() error {
			return prometheusOperator.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (i *PrometheusOperatorReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := prometheusOperator.PrometheusOperatorDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "prometheus-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "prometheus-operator deployment",
		Mutator: func() error {
			return prometheusOperator.PrometheusOperatorDeploymentMutator(cr, deploy)
		},
	}
}
