package cluster

import (
	"context"
	"net/http"

	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	finalizerName = "cluster.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=clusters,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=clusters/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=clusters/finalizers,verbs=update
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers;statefulsets;statefulsets/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles;clusterroles;rolebindings;clusterrolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=monitoring.coreos.com,resources=servicemonitors;prometheusrules;prometheuses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=cert-manager.io,resources=certificates;issuers,verbs=get;list;watch;create;update;patch;delete

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileCluster) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&opstracev1alpha1.Cluster{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileCluster{}

// ReconcileCluster reconciles a Cluster object
type ReconcileCluster struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client                  client.Client
	Scheme                  *runtime.Scheme
	Transport               *http.Transport
	Recorder                record.EventRecorder
	Log                     logr.Logger
	InitialManifests        map[string][]byte
	StatusPoller            *polling.StatusPoller
	DriftPreventionInterval time.Duration
}

// Reconcile , The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileCluster) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result ctrl.Result, err error) {
	cluster := &opstracev1alpha1.Cluster{}
	err = r.Client.Get(ctx, request.NamespacedName, cluster)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info("Cluster has been removed from API", "name", request.Name)

			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := cluster.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.ClusterFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()

	// Read current state
	currentState := NewClusterState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	desiredState := r.getDesiredState(teardown, currentState, cr)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.ClusterFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr)
	}

	return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
}

//nolint:funlen // TODO(prozlach) - needs to be refactored out smaller functions
func (r *ReconcileCluster) getDesiredState(teardown bool, currentState *ClusterState, cr *opstracev1alpha1.Cluster) common.DesiredState {
	desiredState := common.DesiredState{}
	if teardown {
		// Get the actions required to reach the desired state
		// The order of process matters here. Since we're tearing down,
		// we want to do so in the opposite order (in general) than we
		// do when we're creating/updating.
		reloader := NewReloaderReconciler(r.InitialManifests["reloader"], teardown)
		desiredState = append(desiredState, reloader.Reconcile(cr)...)

		prometheus := NewPrometheusReconciler(teardown, r.Log)
		desiredState = append(desiredState, prometheus.Reconcile(currentState, cr)...)

		monitoring := NewMonitoringReconciler(teardown, r.Log)
		desiredState = append(desiredState, monitoring.Reconcile(currentState, cr)...)

		nginxIngress := NewNginxIngressReconciler(r.InitialManifests["nginxIngress"], teardown)
		desiredState = append(desiredState, nginxIngress.Reconcile(cr)...)

		certManager := NewCertManagerReconciler(teardown, r.Log)
		desiredState = append(desiredState, certManager.Reconcile(currentState, cr)...)

		errorTracking := NewErrorTrackingAPIReconciler(teardown, r.Log)
		desiredState = append(desiredState, errorTracking.Reconcile(currentState, cr)...)

		gatekeeper := NewGatekeeperReconciler(teardown, r.Log)
		desiredState = append(desiredState, gatekeeper.Reconcile(currentState, cr)...)

		redis := NewRedisReconciler(teardown, r.Log)
		desiredState = append(desiredState, redis.Reconcile(currentState, cr)...)

		ch := NewClickHouseReconciler(teardown, r.Log)
		desiredState = append(desiredState, ch.Reconcile(currentState, cr)...)

		chOperator := NewClickHouseOperatorReconciler(teardown, r.Log)
		desiredState = append(desiredState, chOperator.Reconcile(currentState, cr)...)

		jaegerOperator := NewJaegerOperatorReconciler(teardown, r.Log)
		desiredState = append(desiredState, jaegerOperator.Reconcile(currentState, cr)...)

		prometheusOperator := NewPrometheusOperatorReconciler(teardown, r.Log)
		desiredState = append(desiredState, prometheusOperator.Reconcile(currentState, cr)...)

		storage := NewStorageReconciler(teardown, r.Log)
		desiredState = append(desiredState, storage.Reconcile(cr)...)

		externalDNS := NewExternalDNSReconciler(teardown, r.Log)
		desiredState = append(desiredState, externalDNS.Reconcile(currentState, cr)...)
	} else {
		// Get the actions required to reach the desired state
		// The order of process matters here. In general, we should first update
		// the lowest order dependencies and then work our way up the stack.
		//
		// Reconciliation will block (and retry) until each action returns successfully.
		// This means that readiness check actions will return errors
		// until each readiness check passes, which blocks downstream actions until the readiness
		// check passes. So in the following case, ClickHouse cluster
		// won't we upgraded/reconciled until the ClickHouse operator has been reconciled
		// and passes the readiness check.
		chOperator := NewClickHouseOperatorReconciler(teardown, r.Log)
		desiredState = chOperator.Reconcile(currentState, cr)

		storage := NewStorageReconciler(teardown, r.Log)
		desiredState = append(desiredState, storage.Reconcile(cr)...)

		ch := NewClickHouseReconciler(teardown, r.Log)
		desiredState = append(desiredState, ch.Reconcile(currentState, cr)...)

		redis := NewRedisReconciler(teardown, r.Log)
		desiredState = append(desiredState, redis.Reconcile(currentState, cr)...)

		// Update other system dependencies
		certManager := NewCertManagerReconciler(teardown, r.Log)
		desiredState = append(desiredState, certManager.Reconcile(currentState, cr)...)

		nginxIngress := NewNginxIngressReconciler(r.InitialManifests["nginxIngress"], teardown)
		desiredState = append(desiredState, nginxIngress.Reconcile(cr)...)

		jaegerOperator := NewJaegerOperatorReconciler(teardown, r.Log)
		desiredState = append(desiredState, jaegerOperator.Reconcile(currentState, cr)...)

		prometheusOperator := NewPrometheusOperatorReconciler(teardown, r.Log)
		desiredState = append(desiredState, prometheusOperator.Reconcile(currentState, cr)...)

		externalDNS := NewExternalDNSReconciler(teardown, r.Log)
		desiredState = append(desiredState, externalDNS.Reconcile(currentState, cr)...)

		gatekeeper := NewGatekeeperReconciler(teardown, r.Log)
		desiredState = append(desiredState, gatekeeper.Reconcile(currentState, cr)...)

		errorTracking := NewErrorTrackingAPIReconciler(teardown, r.Log)
		desiredState = append(desiredState, errorTracking.Reconcile(currentState, cr)...)

		monitoring := NewMonitoringReconciler(teardown, r.Log)
		desiredState = append(desiredState, monitoring.Reconcile(currentState, cr)...)

		prometheus := NewPrometheusReconciler(teardown, r.Log)
		desiredState = append(desiredState, prometheus.Reconcile(currentState, cr)...)

		reloader := NewReloaderReconciler(r.InitialManifests["reloader"], teardown)
		desiredState = append(desiredState, reloader.Reconcile(cr)...)
	}

	return desiredState
}

// Handle success case
func (r *ReconcileCluster) manageSuccess(cluster *opstracev1alpha1.Cluster) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cluster.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cluster.Status.Conditions, condition)

	r.Log.Info("cluster successfully reconciled", "cluster", cluster.Name)
}

// Handle error case: update cluster with error message and status
func (r *ReconcileCluster) manageError(cluster *opstracev1alpha1.Cluster, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cluster.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cluster.Status.Conditions, condition)
}
