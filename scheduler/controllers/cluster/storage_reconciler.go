package cluster

import (
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/storage"
)

type StorageReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewStorageReconciler(teardown bool, logger logr.Logger) *StorageReconciler {
	return &StorageReconciler{
		Teardown: teardown,
		Log:      logger.WithName("storage"),
	}
}

func (i *StorageReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}
	desired = desired.AddAction(i.getStorageDesiredState(cr))

	return desired
}

func (i *StorageReconciler) getStorageDesiredState(cr *v1alpha1.Cluster) common.Action {
	storageClass := storage.StorageClass(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: storageClass,
			Msg: "storageClass",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: storageClass,
		Msg: "storageClass",
		Mutator: func() error {
			storage.StorageClassMutator(cr, storageClass)
			return nil
		},
	}
}
