package node

import (
	"embed"

	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	prometheus "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/prometheus/helpers"
)

//go:embed node_rules.yaml
var f embed.FS

func PrometheusRule(cr *v1alpha1.Cluster) (*monitoring.PrometheusRule, error) {
	corpus, err := f.ReadFile("node_rules.yaml")
	if err != nil {
		return &monitoring.PrometheusRule{}, err
	}
	return prometheus.BuildPrometheusRule(cr, monitors.Node, corpus)
}

func PrometheusRuleMutator(cr *v1alpha1.Cluster, current *monitoring.PrometheusRule) error {
	rule, err := PrometheusRule(cr)
	if err != nil {
		return err
	}
	current.Spec = rule.Spec
	return nil
}
