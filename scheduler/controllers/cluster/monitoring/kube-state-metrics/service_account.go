package kubestatemetrics

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(),
		},
	}
}

//nolint:unparam
func ServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations()
}

func getServiceAccountName() string {
	return monitors.KubeStateMetrics
}

func getServiceAccountLabels() map[string]string {
	return map[string]string{}
}

func getServiceAccountAnnotations() map[string]string {
	return map[string]string{}
}
