package kubestatemetrics

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec()
	// Apply default overrides
	if err := utils.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// Apply CR overrides
	crOverrides := cr.Spec.Overrides.Monitoring.KubeStateMetrics.Components.ServiceMonitor.Spec
	if err := utils.PatchObject(currentSpec, &crOverrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	return nil
}

func getServiceMonitorName() string {
	return monitors.KubeStateMetrics
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"k8s-app": monitors.KubeStateMetrics,
		"tenant":  "system",
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec() monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"k8s-app": monitors.KubeStateMetrics,
			},
		},
		JobLabel: "k8s-app",
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			HonorLabels:   true,
			Interval:      "30s",
			Port:          "http-metrics",
			Scheme:        "http",
			ScrapeTimeout: "30s",
		},
		{
			HonorLabels:   true,
			Interval:      "30s",
			Port:          "telemetry",
			Scheme:        "http",
			ScrapeTimeout: "30s",
		},
	}
}
