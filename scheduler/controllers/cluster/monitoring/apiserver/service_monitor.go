package apiserver

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// Apply CR overrides
	crOverrides := cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Spec
	if err := utils.PatchObject(currentSpec, &crOverrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	return nil
}

func getServiceMonitorName() string {
	return monitors.APIServer
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"k8s-app": monitors.APIServer,
		"tenant":  "system",
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec(cr *v1alpha1.Cluster) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"component": "apiserver",
				"provider":  "kubernetes",
			},
		},
		JobLabel: "component",
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace()},
		},
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			BearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
			Port:            "https",
			Scheme:          "https",
			TLSConfig: &monitoring.TLSConfig{
				CAFile: "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt",
				SafeTLSConfig: monitoring.SafeTLSConfig{
					ServerName: "kubernetes",
				},
			},
			HonorLabels: true,
			Interval:    "30s",
			MetricRelabelConfigs: []*monitoring.RelabelConfig{
				{
					Action: "drop",
					Regex:  "etcd_(debugging|disk|request|server).*",
					SourceLabels: []monitoring.LabelName{
						"__name__",
					},
				},
				{
					Action: "drop",
					Regex:  "apiserver_admission_controller_admission_latencies_seconds_.*",
					SourceLabels: []monitoring.LabelName{
						"__name__",
					},
				},
				{
					Action: "drop",
					Regex:  "apiserver_admission_step_admission_latencies_seconds_.*",
					SourceLabels: []monitoring.LabelName{
						"__name__",
					},
				},
			},
		},
	}
}
