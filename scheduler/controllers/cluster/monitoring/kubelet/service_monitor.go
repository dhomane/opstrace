package kubelet

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(),
			Annotations: getServiceMonitorAnnotations(),
		},
		Spec: getServiceMonitorSpec(),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec()
	// Apply default overrides
	if err := utils.PatchObject(currentSpec, &spec); err != nil {
		return err
	}
	// Apply CR overrides
	crOverrides := cr.Spec.Overrides.PrometheusOperator.Components.ServiceMonitor.Spec
	if err := utils.PatchObject(currentSpec, &crOverrides); err != nil {
		return err
	}
	current.Spec = *currentSpec
	return nil
}

func getServiceMonitorName() string {
	return monitors.Kubelet
}

func getServiceMonitorLabels() map[string]string {
	return map[string]string{
		"tenant":  "system",
		"k8s-app": monitors.Kubelet,
	}
}

func getServiceMonitorAnnotations() map[string]string {
	return map[string]string{}
}

func getServiceMonitorSpec() monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: map[string]string{
				"k8s-app": "kubelet",
			},
		},
		JobLabel: "k8s-app",
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{"kube-system"},
		},
	}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			BearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
			HonorLabels:     true,
			Interval:        "30s",
			Port:            "https-metrics",
			Scheme:          "https",
			TLSConfig: &monitoring.TLSConfig{
				SafeTLSConfig: monitoring.SafeTLSConfig{
					InsecureSkipVerify: true,
				},
			},
		},
		{
			BearerTokenFile: "/var/run/secrets/kubernetes.io/serviceaccount/token",
			HonorLabels:     true,
			Interval:        "30s",
			MetricRelabelConfigs: []*monitoring.RelabelConfig{
				{
					Action: "drop",
					Regex:  "container_(network_tcp_usage_total|network_udp_usage_total|tasks_state|cpu_load_average_10s)",
					SourceLabels: []monitoring.LabelName{
						"__name__",
					},
				},
			},
			Path:   "/metrics/cadvisor",
			Port:   "https-metrics",
			Scheme: "https",
			TLSConfig: &monitoring.TLSConfig{
				SafeTLSConfig: monitoring.SafeTLSConfig{
					InsecureSkipVerify: true,
				},
			},
		},
	}
}
