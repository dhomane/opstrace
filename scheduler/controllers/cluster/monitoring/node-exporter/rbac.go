package nodeexporter

import (
	_ "embed"
	"html/template"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

//go:embed rbac.go.tmpl
var rbacTmpl string

func GetRBACObjects(cr *v1alpha1.Cluster) ([]client.Object, error) {
	rbacTemplate := template.New("node-exporter-rbac")

	rbac, err := common.RenderTemplate(rbacTemplate, rbacTmpl, map[string]interface{}{
		"Name":      monitors.NodeExporter,
		"Namespace": cr.Namespace(),
	})
	if err != nil {
		return []client.Object{}, err
	}
	return common.ParseYaml(rbac)
}
