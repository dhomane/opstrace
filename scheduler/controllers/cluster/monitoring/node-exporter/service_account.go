package nodeexporter

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func ServiceAccount(cr *v1alpha1.Cluster) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: getServiceAccountImagePullSecrets(cr),
	}
}

func getServiceAccountName() string {
	return monitors.NodeExporter
}

func getServiceAccountLabels() map[string]string {
	return map[string]string{
		"app": monitors.NodeExporter,
	}
}

func getServiceAccountAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getServiceAccountImagePullSecrets(cr *v1alpha1.Cluster) []v1.LocalObjectReference {
	return cr.Spec.ImagePullSecrets
}

func ServiceAccountMutator(cr *v1alpha1.Cluster, current *v1.ServiceAccount) {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = getServiceAccountImagePullSecrets(cr)
}
