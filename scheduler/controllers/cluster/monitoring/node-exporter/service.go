package nodeexporter

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func Service(cr *v1alpha1.Cluster) *v1.Service {
	return &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceLabels(),
			Annotations: getServiceAnnotations(cr, nil),
		},
		Spec: getServiceSpec(),
	}
}

func ServiceMutator(cr *v1alpha1.Cluster, current *v1.Service) error {
	current.Name = getServiceName()
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceSpec()
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		cr.Spec.Overrides.Gatekeeper.Components.Service.Spec,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec
	current.Annotations = utils.MergeMap(
		getServiceAnnotations(cr, current.Annotations),
		cr.Spec.Overrides.Gatekeeper.Components.Service.Annotations,
	)
	current.Labels = utils.MergeMap(
		getServiceLabels(),
		cr.Spec.Overrides.Gatekeeper.Components.Service.Labels,
	)

	return nil
}

func getServiceName() string {
	return monitors.NodeExporter
}

func getServiceLabels() map[string]string {
	return map[string]string{
		"app": monitors.NodeExporter,
	}
}

func getServiceAnnotations(cr *v1alpha1.Cluster, existing map[string]string) map[string]string {
	return existing
}

func getServiceSpec() v1.ServiceSpec {
	return v1.ServiceSpec{
		ClusterIP: v1.ClusterIPNone,
		Ports:     getServicePorts(),
		Selector:  getServiceLabels(),
		Type:      v1.ServiceTypeClusterIP,
	}
}

func getServicePorts() []v1.ServicePort {
	return []v1.ServicePort{
		{
			Name:       "https",
			Port:       9100,
			TargetPort: intstr.FromString("https"),
		},
	}
}
