package nodeexporter

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func Daemonset(cr *v1alpha1.Cluster) *appsv1.DaemonSet {
	return &appsv1.DaemonSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      getDaemonsetName(),
			Namespace: cr.Namespace(),
			Labels:    getDaemonsetLabels(),
		},
		Spec: getDaemonsetSpec(),
	}
}

func DaemonsetMutator(current *appsv1.DaemonSet) {
	current.Labels = getDaemonsetLabels()
	spec := getDaemonsetSpec()
	current.Spec.Template = spec.Template
}

func getDaemonsetName() string {
	return monitors.NodeExporter
}

func getDaemonsetLabels() map[string]string {
	return map[string]string{
		"app": monitors.NodeExporter,
	}
}

var (
	mountPropagation       = corev1.MountPropagationHostToContainer
	runAsNonRoot     bool  = true
	runAsUser        int64 = 65534
)

//nolint:funlen
func getDaemonsetSpec() appsv1.DaemonSetSpec {
	return appsv1.DaemonSetSpec{
		Selector: &metav1.LabelSelector{
			MatchLabels: map[string]string{
				"app": "node-exporter",
			},
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: getDaemonsetLabels(),
			},
			Spec: corev1.PodSpec{
				ImagePullSecrets: []corev1.LocalObjectReference{},
				Containers: []corev1.Container{
					{
						Name:  monitors.NodeExporter,
						Image: constants.OpstraceImages().NodeExporterImage,
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceCPU:    resource.MustParse("50m"),
								corev1.ResourceMemory: resource.MustParse("100Mi"),
							},
							Limits: corev1.ResourceList{
								corev1.ResourceCPU:    resource.MustParse("1700m"),
								corev1.ResourceMemory: resource.MustParse("180Mi"),
							},
						},
						Args: []string{
							"--web.listen-address=127.0.0.1:9100",
							"--path.procfs=/host/proc",
							"--path.sysfs=/host/sys",
							"--path.rootfs=/host/root",
							"--collector.filesystem.ignored-mount-points=^/(dev|proc|sys|var/lib/docker/.+)($|/)",
							"--collector.filesystem.ignored-fs-types=^(autofs|binfmt_misc|cgroup|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|mqueue|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|sysfs|tracefs)$",
						},
						VolumeMounts: []corev1.VolumeMount{
							{
								MountPath: "/host/proc",
								Name:      "proc",
								ReadOnly:  false,
							},
							{
								MountPath: "/host/sys",
								Name:      "sys",
								ReadOnly:  false,
							},
							{
								MountPath:        "/host/root",
								MountPropagation: &mountPropagation,
								Name:             "root",
								ReadOnly:         true,
							},
						},
					},
					{
						Name:  "kube-rbac-proxy",
						Image: constants.OpstraceImages().KubeRBACProxyImage,
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceCPU:    resource.MustParse("50m"),
								corev1.ResourceMemory: resource.MustParse("20Mi"),
							},
							Limits: corev1.ResourceList{
								corev1.ResourceCPU:    resource.MustParse("1000m"),
								corev1.ResourceMemory: resource.MustParse("60Mi"),
							},
						},
						Args: []string{
							"--logtostderr",
							"--secure-listen-address=$(IP):9100",
							"--tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
							"--upstream=http://127.0.0.1:9100/",
						},
						Env: []corev1.EnvVar{
							{
								Name: "IP",
								ValueFrom: &corev1.EnvVarSource{
									FieldRef: &corev1.ObjectFieldSelector{
										FieldPath: "status.podIP",
									},
								},
							},
						},
						Ports: []corev1.ContainerPort{
							{
								ContainerPort: 9100,
								HostPort:      9100,
								Name:          "https",
							},
						},
					},
				},
				HostNetwork: true,
				HostPID:     true,
				NodeSelector: map[string]string{
					"kubernetes.io/os": "linux",
				},
				SecurityContext: &corev1.PodSecurityContext{
					RunAsNonRoot: &runAsNonRoot,
					RunAsUser:    &runAsUser,
				},
				ServiceAccountName: monitors.NodeExporter,
				Tolerations: []corev1.Toleration{
					{
						Operator: corev1.TolerationOpExists,
					},
				},
				Volumes: []corev1.Volume{
					{
						Name: "proc",
						VolumeSource: corev1.VolumeSource{
							HostPath: &corev1.HostPathVolumeSource{
								Path: "/proc",
							},
						},
					},
					{
						Name: "sys",
						VolumeSource: corev1.VolumeSource{
							HostPath: &corev1.HostPathVolumeSource{
								Path: "/sys",
							},
						},
					},
					{
						Name: "root",
						VolumeSource: corev1.VolumeSource{
							HostPath: &corev1.HostPathVolumeSource{
								Path: "/",
							},
						},
					},
				},
			},
		},
	}
}
