package redis

import (
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getServiceMonitorName(cr *v1alpha1.Cluster) string {
	return constants.RedisName
}

func getServiceMonitorSelector(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{
		"redisfailovers.databases.spotahome.com/name": getServiceMonitorName(cr),
	}
}

func getServiceMonitorLabels(cr *v1alpha1.Cluster) map[string]string {
	labels := getServiceMonitorSelector(cr)
	labels["app"] = getServiceMonitorName(cr)
	// Configure the system-tenant prometheus to scrape this
	labels["tenant"] = "system"

	return labels
}

func getServiceMonitorAnnotations(cr *v1alpha1.Cluster) map[string]string {
	return map[string]string{}
}

func getServiceMonitorEndpoints() []monitoring.Endpoint {
	return []monitoring.Endpoint{
		{
			Port:     "http-metrics",
			Interval: "30s",
		},
	}
}

func getServiceMonitorSpec(cr *v1alpha1.Cluster) monitoring.ServiceMonitorSpec {
	return monitoring.ServiceMonitorSpec{
		Endpoints: getServiceMonitorEndpoints(),
		Selector: metav1.LabelSelector{
			MatchLabels: getServiceMonitorSelector(cr),
		},
		JobLabel: constants.SelectorLabelName,
		NamespaceSelector: monitoring.NamespaceSelector{
			MatchNames: []string{cr.Namespace()},
		},
	}
}

func ServiceMonitor(cr *v1alpha1.Cluster) *monitoring.ServiceMonitor {
	return &monitoring.ServiceMonitor{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceMonitorName(cr),
			Namespace:   cr.Namespace(),
			Labels:      getServiceMonitorLabels(cr),
			Annotations: getServiceMonitorAnnotations(cr),
		},
		Spec: getServiceMonitorSpec(cr),
	}
}

func ServiceMonitorMutator(cr *v1alpha1.Cluster, current *monitoring.ServiceMonitor) error {
	current.Name = getServiceMonitorName(cr)
	currentSpec := current.Spec.DeepCopy()
	spec := getServiceMonitorSpec(cr)
	// Apply default overrides
	if err := utils.PatchObject(
		currentSpec,
		&spec,
	); err != nil {
		return err
	}
	crOverrides := cr.Spec.Overrides.Redis.Components.ServiceMonitor.Spec
	// Apply CR overrides
	if err := utils.PatchObject(
		currentSpec,
		&crOverrides,
	); err != nil {
		return err
	}
	current.Spec = *currentSpec

	current.Annotations = utils.MergeMap(
		getServiceMonitorAnnotations(cr),
		cr.Spec.Overrides.Redis.Components.ServiceMonitor.Annotations,
	)

	current.Labels = utils.MergeMap(
		getServiceMonitorLabels(cr),
		cr.Spec.Overrides.Redis.Components.ServiceMonitor.Labels,
	)

	return nil
}
