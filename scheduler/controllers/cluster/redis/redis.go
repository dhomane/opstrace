package redis

import (
	redis "github.com/spotahome/redis-operator/api/redisfailover/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var RedisSentinalReplicas int32 = 3
var StorageSize = "8Gi"

func Redis(cr *v1alpha1.Cluster) *redis.RedisFailover {
	return &redis.RedisFailover{
		ObjectMeta: v1.ObjectMeta{
			Name:      constants.RedisName,
			Namespace: cr.Namespace(),
		},
		Spec: redis.RedisFailoverSpec{
			Sentinel: redis.SentinelSettings{
				Image:            constants.OpstraceImages().RedisImage,
				Replicas:         RedisSentinalReplicas,
				ImagePullSecrets: cr.Spec.ImagePullSecrets,
			},
			Redis: redis.RedisSettings{
				Image:    constants.OpstraceImages().RedisImage,
				Replicas: RedisSentinalReplicas,
				Storage: redis.RedisStorage{
					PersistentVolumeClaim: &redis.EmbeddedPersistentVolumeClaim{
						EmbeddedObjectMetadata: redis.EmbeddedObjectMetadata{
							Name: constants.RedisName,
						},
						Spec: corev1.PersistentVolumeClaimSpec{
							AccessModes: []corev1.PersistentVolumeAccessMode{
								corev1.ReadWriteOnce,
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									corev1.ResourceStorage: resource.MustParse(StorageSize),
								},
							},
						},
					},
				},
				Exporter: redis.Exporter{
					Enabled: true,
					Image:   constants.OpstraceImages().RedisMetricsExporterImage,
					Env: []corev1.EnvVar{
						{
							Name: "REDIS_PASSWORD",
							ValueFrom: &corev1.EnvVarSource{
								SecretKeyRef: &corev1.SecretKeySelector{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: constants.RedisName,
									},
									Key: "password",
								},
							},
						},
					},
				},
			},
			Auth: redis.AuthSettings{
				SecretPath: constants.RedisName,
			},
		},
	}
}

func RedisMutator(cr *v1alpha1.Cluster, current *redis.RedisFailover) {
	redis := Redis(cr)
	current.Spec.Sentinel = redis.Spec.Sentinel
	current.Spec.Auth = redis.Spec.Auth
	current.Spec.Redis = redis.Spec.Redis
}

func RedisSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.RedisName,
	}
}
