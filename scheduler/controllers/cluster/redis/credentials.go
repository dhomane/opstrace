package redis

import (
	utils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Generate random password and don't change it once created
func getCredentialsData() map[string][]byte {
	return map[string][]byte{
		"password": []byte(utils.RandStringRunes(10)),
	}
}

func Credentials(cr *v1alpha1.Cluster) *v1.Secret {
	secret := &v1.Secret{}
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      constants.RedisName,
		Namespace: cr.Namespace(),
	}

	secret.Data = getCredentialsData()

	return secret
}

// Don't mutate to ensure we don't change the password after
// it's first created
func CredentialsMutator(cr *v1alpha1.Cluster, current *v1.Secret) error {
	return nil
}

func CredentialsSelector(cr *v1alpha1.Cluster) client.ObjectKey {
	return client.ObjectKey{
		Namespace: cr.Namespace(),
		Name:      constants.RedisName,
	}
}
