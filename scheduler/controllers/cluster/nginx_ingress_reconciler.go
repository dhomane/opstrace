package cluster

import (
	"fmt"
	"strings"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type NginxIngressReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests []byte
}

func NewNginxIngressReconciler(
	initialManifests []byte,
	teardown bool,
) *NginxIngressReconciler {
	return &NginxIngressReconciler{
		Teardown:         teardown,
		Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.NginxIngressInventoryID)),
		initialManifests: initialManifests,
	}
}

func (i *NginxIngressReconciler) Reconcile(cr *v1alpha1.Cluster) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, no point in generating manifests as we will use
		// Inventory for that
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: constants.NginxIngressInventoryID,
				GetInventory:  func() *v1beta2.ResourceInventory { return cr.Status.Inventory[constants.NginxIngressInventoryID] },
				Msg:           "manifests/nginx-ingress prune",
				Log:           i.Log,
			},
		}
	}

	overrideKustomization := ParseNginxIngressOverrides(cr)
	manifests, err := ApplyOverrides(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[nginxIngress] failed to apply overrides for nginx ingress manifests",
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      constants.NginxIngressInventoryID,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.NginxIngressInventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.NginxIngressInventoryID] = inv
			},
			Msg: "manifests/nginx-ingress ensure",
			Log: i.Log,
		},
	}
}

//nolint:funlen // This is just a long object definition with few ifs, not a complex function
func ParseNginxIngressOverrides(cr *v1alpha1.Cluster) kustomize.Kustomization {
	res := kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			tmpResourceData,
		},
		PatchesJson6902: []types.Patch{
			{
				Patch: fmt.Sprintf(
					`[
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1hostname", "value": "%s" },
						{"op": "add", "path": "/metadata/annotations/external-dns.alpha.kubernetes.io~1ttl", "value": "30" }
					]`,
					cr.Spec.GetHost(),
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
			{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
					cr.Namespace(),
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "ServiceMonitor",
						},
						Name: constants.NginxIngressServiceMonitorName,
					},
				},
			},
		},
	}

	if cr.Spec.Target == common.AWS {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// * Use an NLB type loadbalancer
			// * Ensure the ELB idle timeout is less than nginx keep-alive timeout. Because we're
			//   using WebSockets, the value will need to be
			//   increased to '3600' to avoid any potential issues. We set the NGINX timeout to 3700
			//   so that it's longer than the LB timeout.
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-type", "value": "nlb"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-backend-protocol", "value": "tcp"},
					{"op": "add", "path": "/metadata/annotations/service.beta.kubernetes.io~1aws-load-balancer-connection-idle-timeout", "value": "3600"}
				]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
		)
	}

	if cr.Spec.Target == common.KIND {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use a NodePort type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeNodePort,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
			types.Patch{
				Patch: `[
					{"op": "add", "path": "/spec/template/spec/nodeSelector", "value": {"ingress-ready": "true" }},
					{"op": "replace", "path": "/spec/replicas", "value": 1},
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--publish-status-address=localhost"}
					]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.NginxIngressDeploymentName,
					},
				},
			},
		)
	} else {
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "replace", "path": "/spec/type", "value": "%s"}]`,
					v1.ServiceTypeLoadBalancer,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
			types.Patch{
				Patch: `[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--publish-service=$(POD_NAMESPACE)/ingress-nginx-controller"}]`,
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Deployment",
						},
						Name: constants.NginxIngressDeploymentName,
					},
				},
			},
		)
	}

	if len(cr.Spec.DNS.FirewallSourceIPsAllowed) > 0 {
		tmp := make([]string, len(cr.Spec.DNS.FirewallSourceIPsAllowed))
		for idx, s := range cr.Spec.DNS.FirewallSourceIPsAllowed {
			tmp[idx] = fmt.Sprintf(`"%s"`, s)
		}
		tmpJoined := strings.Join(tmp, ",")
		res.PatchesJson6902 = append(res.PatchesJson6902,
			// Use an NLB type loadbalancer
			types.Patch{
				Patch: fmt.Sprintf(
					`[{"op": "add", "path": "/spec/loadBalancerSourceRanges", "value": [%s]}]`,
					tmpJoined,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "Service",
						},
						Name: constants.NginxIngressServiceName,
					},
				},
			},
		)
	}

	return res
}
