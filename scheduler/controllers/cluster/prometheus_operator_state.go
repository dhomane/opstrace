package cluster

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/prometheusOperator"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type PrometheusOperatorState struct {
	// Track deployment readiness
	OperatorDeployment *appsv1.Deployment
}

func NewPrometheusOperatorState() *PrometheusOperatorState {
	return &PrometheusOperatorState{}
}

func (i *PrometheusOperatorState) Read(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	err := i.readDeployment(ctx, cr, client)

	return err
}

func (i *PrometheusOperatorState) readDeployment(ctx context.Context, cr *v1alpha1.Cluster, client client.Client) error {
	currentState := &appsv1.Deployment{}
	selector := prometheusOperator.PrometheusOperatorDeploymentSelector(cr)
	err := client.Get(ctx, selector, currentState)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	i.OperatorDeployment = currentState.DeepCopy()
	return nil
}
