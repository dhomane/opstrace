package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	monitoring "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	monitors "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/apiserver"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/coredns"
	kubecontrollermanager "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/kube-controller-manager"
	kubescheduler "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/kube-scheduler"
	kubestatemetrics "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/kube-state-metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/kubelet"
	node "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/node"
	nodeexporter "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/monitoring/node-exporter"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type MonitoringReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewMonitoringReconciler(teardown bool, logger logr.Logger) *MonitoringReconciler {
	return &MonitoringReconciler{
		Teardown: teardown,
		Log:      logger.WithName("monitoring"),
	}
}

//nolint:unparam
func (m *MonitoringReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.APIServer))
	desired = desired.AddAction(m.getPrometheusRuleDesiredState(cr, monitors.APIServer))
	desired = desired.AddAction(m.getPrometheusRuleDesiredState(cr, monitors.Kubernetes))

	desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.CoreDNS))
	desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.KubeControllerManager))
	desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.Kubelet))

	desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.KubeScheduler))
	desired = desired.AddAction(m.getPrometheusRuleDesiredState(cr, monitors.KubeScheduler))

	{
		// kube-state-metrics
		desired = desired.AddAction(m.getServiceAccountDesiredState(cr, monitors.KubeStateMetrics))
		desired = desired.AddAction(m.getServiceDesiredState(cr, monitors.KubeStateMetrics))
		desired = desired.AddAction(m.getDeploymentDesiredState(cr, monitors.KubeStateMetrics))
		desired = desired.AddActions(m.getRBACDesiredState(cr, monitors.KubeStateMetrics))
		desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.KubeStateMetrics))
	}

	{
		// node-exporter
		desired = desired.AddAction(m.getServiceAccountDesiredState(cr, monitors.NodeExporter))
		desired = desired.AddActions(m.getRBACDesiredState(cr, monitors.NodeExporter))
		desired = desired.AddAction(m.getServiceDesiredState(cr, monitors.NodeExporter))
		desired = desired.AddAction(m.getDaemonsetDesiredState(cr, monitors.NodeExporter))
		desired = desired.AddAction(m.getServiceMonitorDesiredState(cr, monitors.NodeExporter))
		desired = desired.AddAction(m.getPrometheusRuleDesiredState(cr, monitors.NodeExporter))
	}

	desired = desired.AddAction(m.getPrometheusRuleDesiredState(cr, monitors.Node))

	return desired
}

func (m *MonitoringReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		sa      *v1.ServiceAccount
		msg     string
		mutator func() error
		err     error
	)

	msg = fmt.Sprintf("%s service account", monitorType)
	err = nil

	switch monitorType {
	case monitors.KubeStateMetrics:
		sa = kubestatemetrics.ServiceAccount(cr)
		mutator = func() error {
			kubestatemetrics.ServiceAccountMutator(cr, sa)
			return nil
		}
	case monitors.NodeExporter:
		sa = nodeexporter.ServiceAccount(cr)
		mutator = func() error {
			nodeexporter.ServiceAccountMutator(cr, sa)
			return nil
		}
	default:
		err = fmt.Errorf("service account definition unknown")
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested service account", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     sa,
		Msg:     msg,
		Mutator: mutator,
	}
}

func (m *MonitoringReconciler) getRBACDesiredState(cr *v1alpha1.Cluster, monitorType string) []common.Action {
	var (
		objects []client.Object
		err     error
	)

	switch monitorType {
	case monitors.KubeStateMetrics:
		objects, err = kubestatemetrics.GetRBACObjects(cr)
	case monitors.NodeExporter:
		objects, err = nodeexporter.GetRBACObjects(cr)
	default:
		err = fmt.Errorf("rbac definition unknown")
	}

	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   fmt.Sprintf("failed to serialize %s rbac resources", monitorType),
				Error: err,
			},
		}
	}

	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)
		if m.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("%s %s", monitorType, obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("%s %s", monitorType, obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (m *MonitoringReconciler) getServiceDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		service *v1.Service
		msg     string
		mutator func() error
		err     error
	)

	msg = fmt.Sprintf("%s service", monitorType)
	err = nil

	switch monitorType {
	case monitors.KubeStateMetrics:
		service = kubestatemetrics.Service(cr)
		mutator = func() error {
			return kubestatemetrics.ServiceMutator(cr, service)
		}
	case monitors.NodeExporter:
		service = nodeexporter.Service(cr)
		mutator = func() error {
			return nodeexporter.ServiceMutator(cr, service)
		}
	default:
		err = fmt.Errorf("service definition unknown")
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested service", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: service,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     service,
		Msg:     msg,
		Mutator: mutator,
	}
}

func (m *MonitoringReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		deployment *appsv1.Deployment
		msg        string
		mutator    func() error
		err        error
	)

	msg = fmt.Sprintf("%s deployment", monitorType)
	err = nil

	switch monitorType {
	case monitors.KubeStateMetrics:
		deployment = kubestatemetrics.Deployment(cr)
		mutator = func() error {
			return kubestatemetrics.DeploymentMutator(cr, deployment)
		}
	default:
		err = fmt.Errorf("deployment definition unknown")
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested deployment", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: deployment,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     deployment,
		Msg:     msg,
		Mutator: mutator,
	}
}

func (m *MonitoringReconciler) getDaemonsetDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		daemonset *appsv1.DaemonSet
		msg       string
		mutator   func() error
		err       error
	)

	msg = fmt.Sprintf("%s daemonset", monitorType)
	err = nil

	switch monitorType {
	case monitors.NodeExporter:
		daemonset = nodeexporter.Daemonset(cr)
		mutator = func() error {
			nodeexporter.DaemonsetMutator(daemonset)
			return nil
		}
	default:
		err = fmt.Errorf("daemonset definition unknown")
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested daemonset", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: daemonset,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     daemonset,
		Msg:     msg,
		Mutator: mutator,
	}
}

func (m *MonitoringReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		sm      *monitoring.ServiceMonitor
		msg     string
		mutator func() error
		err     error
	)

	msg = fmt.Sprintf("%s servicemonitor", monitorType)

	switch monitorType {
	case monitors.APIServer:
		sm = apiserver.ServiceMonitor(cr)
		mutator = func() error {
			return apiserver.ServiceMonitorMutator(cr, sm)
		}
	case monitors.CoreDNS:
		sm = coredns.ServiceMonitor(cr)
		mutator = func() error {
			return coredns.ServiceMonitorMutator(cr, sm)
		}
	case monitors.KubeControllerManager:
		sm = kubecontrollermanager.ServiceMonitor(cr)
		mutator = func() error {
			kubecontrollermanager.ServiceMonitorMutator(sm)
			return nil
		}
	case monitors.KubeStateMetrics:
		sm = kubestatemetrics.ServiceMonitor(cr)
		mutator = func() error {
			return kubestatemetrics.ServiceMonitorMutator(cr, sm)
		}
	case monitors.NodeExporter:
		sm = nodeexporter.ServiceMonitor(cr)
		mutator = func() error {
			return nodeexporter.ServiceMonitorMutator(cr, sm)
		}
	case monitors.Kubelet:
		sm = kubelet.ServiceMonitor(cr)
		mutator = func() error {
			return kubelet.ServiceMonitorMutator(cr, sm)
		}
	case monitors.KubeScheduler:
		sm = kubescheduler.ServiceMonitor(cr)
		mutator = func() error {
			return kubescheduler.ServiceMonitorMutator(cr, sm)
		}
	default:
		err = fmt.Errorf("service monitor definition unknown")
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested service monitor", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     sm,
		Msg:     msg,
		Mutator: mutator,
	}
}

func (m *MonitoringReconciler) getPrometheusRuleDesiredState(cr *v1alpha1.Cluster, monitorType string) common.Action {
	var (
		pr      *monitoring.PrometheusRule
		msg     string
		mutator func() error
		err     error
	)

	msg = fmt.Sprintf("%s prometheusrule", monitorType)

	switch monitorType {
	case monitors.APIServer:
		pr, err = apiserver.ApiserverPrometheusRule(cr)
		mutator = func() error {
			return apiserver.ApiserverPrometheusRuleMutator(cr, pr)
		}
	case monitors.Kubernetes:
		pr, err = apiserver.KubernetesPrometheusRule(cr)
		mutator = func() error {
			return apiserver.KubernetesPrometheusRuleMutator(cr, pr)
		}
	case monitors.KubeScheduler:
		pr, err = kubescheduler.PrometheusRule(cr)
		mutator = func() error {
			return kubescheduler.PrometheusRuleMutator(cr, pr)
		}
	case monitors.NodeExporter:
		pr, err = nodeexporter.PrometheusRule(cr)
		mutator = func() error {
			return nodeexporter.PrometheusRuleMutator(cr, pr)
		}
	case monitors.Node:
		pr, err = node.PrometheusRule(cr)
		mutator = func() error {
			return node.PrometheusRuleMutator(cr, pr)
		}
	default:
		err = fmt.Errorf("prometheus rule definition unknown for monitor:%s", monitorType)
	}

	if err != nil {
		return common.LogAction{
			Msg:   fmt.Sprintf("[%s] failed to get requested prometheus rule", monitorType),
			Error: err,
		}
	}

	if m.Teardown {
		return common.GenericDeleteAction{
			Ref: pr,
			Msg: msg,
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref:     pr,
		Msg:     msg,
		Mutator: mutator,
	}
}
