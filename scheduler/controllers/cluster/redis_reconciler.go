package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/redis"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/redisOperator"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type RedisReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewRedisReconciler(teardown bool, logger logr.Logger) *RedisReconciler {
	return &RedisReconciler{
		Teardown: teardown,
		Log:      logger.WithName("redis"),
	}
}

func (i *RedisReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	// when tearing down our desired state, it is important we follow a sequence of
	// actions which allows all components to be deleted correctly without creating
	// orphans OR causing a component to be gone before anything that still references
	// and/or needs it. Though not mandatory, a good sequence to follow is the reverse
	// order of how each component got provisioned when building the desired state
	// initially.
	if i.Teardown {
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
		desired = desired.AddActions(i.getCRDesiredState(state, cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
	} else {
		desired = desired.AddAction(i.getServiceAccountDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(cr))
		desired = desired.AddActions(i.getCRDesiredState(state, cr))
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	}

	desired = desired.AddActions(i.getReadiness(state))
	return desired
}

func (i *RedisReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		actions := []common.Action{}
		if state.Redis.Sentinel != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.Redis.Sentinel,
					Msg: "check redis sentinel is gone",
				},
			)
		}
		if state.Redis.Replicas != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.Redis.Replicas,
					Msg: "check redis replicas are gone",
				},
			)
		}
		if state.Redis.Redis != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.Redis.Redis,
					Msg: "check redis CR is gone",
				},
			)
		}
		if state.Redis.Operator != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.Redis.Operator,
					Msg: "check redis operator is gone",
				},
			)
		}
		return actions
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Redis.Operator,
			Msg: "check redis operator readiness",
		},
		common.DeploymentReadyAction{
			Ref: state.Redis.Sentinel,
			Msg: "check redis sentinel readiness",
		},
		// The operator uses a non-configurable UpdateStrategy set to OnDelete, and
		// this strategy is incompatible with this StatefulSetReadyAction.
		// TODO: find a way to monitor the status of an upgrade on this statefulset.
		// The redis operator handles the deletion/upgrade of pods directly:
		// https://github.com/spotahome/redis-operator/blob/1ca79551e11ec62a23c7debb4419e32256a7368d/operator/redisfailover/checker.go#L14
		//
		// Perhaps we need to track the pods and check that the revision of each pod matches the
		// revision of the Statefulset
		//
		// common.StatefulSetReadyAction{
		// 	Ref: state.Redis.Replicas,
		// 	Msg: "check redis replicas readiness",
		// },
	}
}

func (i *RedisReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := redis.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "redis credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "redis credentials",
		Mutator: func() error {
			return redis.CredentialsMutator(cr, s)
		},
	}
}

func (i *RedisReconciler) getServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := redisOperator.ServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "redis operator service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "redis operator service account",
		Mutator: func() error {
			redisOperator.ServiceAccountMutator(cr, s)
			return nil
		},
	}
}

func (i *RedisReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := redisOperator.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize redis-operator rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("redis-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("redis-operator %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *RedisReconciler) getCRDesiredState(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	s := redis.Redis(cr)

	if i.Teardown {
		actions := []common.Action{}
		actions = append(actions, common.GenericDeleteAction{
			Ref: s,
			Msg: "redis CR",
		})
		// make sure the CR is correctly deleted after the operator is done
		// cleaning up all associated resources
		if state.Redis.Redis != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.Redis.Redis,
				Msg: "check redis CR is gone",
			})
		}
		return actions
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: s,
			Msg: "redis CR",
			Mutator: func() error {
				redis.RedisMutator(cr, s)
				return nil
			},
		},
	}
}

func (i *RedisReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := redisOperator.Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "redis operator",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "redis operator",
		Mutator: func() error {
			return redisOperator.DeploymentMutator(cr, deploy)
		},
	}
}

func (i *RedisReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := redis.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "redis servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "redis servicemonitor",
		Mutator: func() error {
			return redis.ServiceMonitorMutator(cr, monitor)
		},
	}
}
