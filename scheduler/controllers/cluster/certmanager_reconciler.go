package cluster

import (
	"fmt"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/certManager"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type CertManagerReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewCertManagerReconciler(teardown bool, logger logr.Logger) *CertManagerReconciler {
	return &CertManagerReconciler{
		Teardown: teardown,
		Log:      logger.WithName("certmanager"),
	}
}

func (i *CertManagerReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	desired = desired.AddAction(i.getCertManagerServiceAccountDesiredState(cr))
	desired = desired.AddAction(i.getCainjectorServiceAccountDesiredState(cr))
	desired = desired.AddActions(i.getRBACDesiredState(cr))
	desired = desired.AddAction(i.getServiceDesiredState(cr))
	desired = desired.AddAction(i.getSelfSignedIssuerDesiredState())
	desired = desired.AddAction(i.getSelfSignedCACertDesiredState(cr))
	desired = desired.AddAction(i.getSelfSignedCAIssuerDesiredState())
	desired = desired.AddAction(i.getDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getCainjectorDeploymentDesiredState(cr))
	desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	desired = desired.AddActions(i.getReadiness(state))

	if cr.Spec.Target != common.KIND {
		// TODO(prozlach): Make letsencrypt issuers provisioned basing on
		// cluster configuration instead of hardcoding them basing on cluster
		// type.
		i.Log.Info("Provisioning letsencrypt prod and staging issuers")
		desired = desired.AddAction(i.getProdIssuerDesiredState(cr))
		desired = desired.AddAction(i.getStagingIssuerDesiredState(cr))
	} else {
		i.Log.Info("Letsencrypt issuers are not available on KIND cluster, using self-signed CA")
	}
	desired = desired.AddAction(i.getTLSCertDesiredState(cr))

	return desired
}

func (i *CertManagerReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		actions := []common.Action{}
		if state.CertMananger.CertManagerDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.CertMananger.CertManagerDeployment,
					Msg: "check certmanager deployment is gone",
				},
			)
		}
		if state.CertMananger.CaInjectorDeployment != nil {
			actions = append(actions,
				common.CheckGoneAction{
					Ref: state.CertMananger.CaInjectorDeployment,
					Msg: "check cainjector deployment is gone",
				},
			)
		}
		return actions
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.CertMananger.CertManagerDeployment,
			Msg: "check certmanager deployment readiness",
		},
		common.DeploymentReadyAction{
			Ref: state.CertMananger.CaInjectorDeployment,
			Msg: "check cainjector deployment readiness",
		},
	}
}

func (i *CertManagerReconciler) getRBACDesiredState(cr *v1alpha1.Cluster) []common.Action {
	objects, err := certManager.GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize certmanager rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("certmanager %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("certmanager %s", obj.GetObjectKind().GroupVersionKind().Kind),
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *CertManagerReconciler) getSelfSignedIssuerDesiredState() common.Action {
	issuer := certManager.SelfSignedClusterIssuer()

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager self-signed clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager self-signed clusterIssuer",
		Mutator: func() error {
			certManager.SelfSignedClusterIssuerMutator(issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getSelfSignedCAIssuerDesiredState() common.Action {
	issuer := certManager.SelfSignedCAIssuer()

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager letsencrypt-prod clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager self-signed CA clusterIssuer",
		Mutator: func() error {
			certManager.SelfSignedCAIssuerMutator(issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getProdIssuerDesiredState(cr *v1alpha1.Cluster) common.Action {
	issuer := certManager.ProdClusterIssuer(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager letsencrypt-prod clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager letsencrypt-prod clusterIssuer",
		Mutator: func() error {
			certManager.ProdClusterIssuerMutator(cr, issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getStagingIssuerDesiredState(cr *v1alpha1.Cluster) common.Action {
	issuer := certManager.StagingClusterIssuer(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: issuer,
			Msg: "certmanager letsencrypt-staging clusterIssuer",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: issuer,
		Msg: "certmanager letsencrypt-staging clusterIssuer",
		Mutator: func() error {
			certManager.StagingClusterIssuerMutator(cr, issuer)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getSelfSignedCACertDesiredState(cr *v1alpha1.Cluster) common.Action {
	cert := certManager.SelfSignedCACertificate(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: cert,
			Msg: "self-signed CA certificate",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: cert,
		Msg: "self-signed CA certificate",
		Mutator: func() error {
			certManager.SelfSignedCACertificateMutator(cr, cert)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getTLSCertDesiredState(cr *v1alpha1.Cluster) common.Action {
	cert := certManager.Certificate(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: cert,
			Msg: "tls certificate",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: cert,
		Msg: "tls certificate",
		Mutator: func() error {
			certManager.CertificateMutator(cr, cert)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getServiceDesiredState(cr *v1alpha1.Cluster) common.Action {
	svc := certManager.Service(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: svc,
			Msg: "certmanager service",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: svc,
		Msg: "certmanager service",
		Mutator: func() error {
			return certManager.ServiceMutator(cr, svc)
		},
	}
}

func (i *CertManagerReconciler) getCertManagerServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := certManager.CertManagerServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "certmanager service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "certmanager service account",
		Mutator: func() error {
			certManager.CertManagerServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getCainjectorServiceAccountDesiredState(cr *v1alpha1.Cluster) common.Action {
	sa := certManager.CainjectorServiceAccount(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "cainjector service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "cainjector service account",
		Mutator: func() error {
			certManager.CainjectorServiceAccountMutator(cr, sa)
			return nil
		},
	}
}

func (i *CertManagerReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	sm := certManager.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sm,
			Msg: "certmanager servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sm,
		Msg: "certmanager servicemonitor",
		Mutator: func() error {
			return certManager.ServiceMonitorMutator(cr, sm)
		},
	}
}

func (i *CertManagerReconciler) getDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := certManager.CertManagerDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "certmanager deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "certmanager deployment",
		Mutator: func() error {
			return certManager.CertManagerDeploymentMutator(cr, deploy)
		},
	}
}

func (i *CertManagerReconciler) getCainjectorDeploymentDesiredState(cr *v1alpha1.Cluster) common.Action {
	deploy := certManager.CainjectorDeployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "cainjector deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "cainjector deployment",
		Mutator: func() error {
			return certManager.CainjectorDeploymentMutator(cr, deploy)
		},
	}
}
