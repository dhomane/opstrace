package v1alpha1

import (
	"fmt"

	cmacme "github.com/cert-manager/cert-manager/pkg/apis/acme/v1"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ClusterSpec defines the desired state of Cluster.
type ClusterSpec struct {
	// Optional namespace for deploying cluster resources (tenants will still have separate namespaces).
	// Defaults to the default namespace
	// +optional
	Namespace string `json:"namespace"`
	// Target environment (aws, gcp, kind)
	Target common.EnvironmentTarget `json:"target"`
	// GitLab instance integration config
	GitLab GitLabSpec `json:"gitlab"`
	// Cluster-wide image pull secrets (NOTE: Not plumbed to all components yet)
	ImagePullSecrets []v1.LocalObjectReference `json:"imagePullSecrets,omitempty"`
	// DNS configuration
	// +optional
	DNS DNSSpec `json:"dns"`
	// Gitlab Observability UI configuration
	GOUI GOUISpec `json:"goui"`
	// Set overrides for components and config
	// +optional
	Overrides ClusterOverridesSpec `json:"overrides"`
	// Setup features for cluster and its resources
	// +optional
	Features map[string]string `json:"features,omitempty"`
}

type GOUISpec struct {
	// Image path for which image to install for Observability UI.
	// This value eventually gets plumbed down into each Tenant object when we
	// create one using which we spin up a corresponding instance of the UI.
	// Should there be need to override the image specific to a tenant, it
	// should be done as an override on the corresponding deployment instead.
	Image *string `json:"image,omitempty"`
}

type DNSSpec struct {
	// Certificate Issuer (letsencrypt-prod or letsencrypt-staging)
	CertificateIssuer string `json:"certificateIssuer,omitempty"`
	// Cluster domain e.g. observe.gitlab.com
	Domain *string `json:"domain,omitempty"`
	// Used to configure a DNS01 challenge provider to be used when solving DNS01
	// challenges.
	// Only one DNS provider may be configured per solver.
	DNS01Challenge cmacme.ACMEChallengeSolverDNS01 `json:"dns01Challenge"`
	// ACMEEmail is the email address to be associated with the ACME account.
	// It will be used to contact you in case of issues with your account or certificates,
	// including expiry notification emails. This field may be updated after the account is
	// initially registered
	ACMEEmail string `json:"acmeEmail"`
	// ACMEServer is the URL used to access the ACME server's 'directory' endpoint.
	// For example, for Let's Encrypt's staging endpoint,
	// you would use: "https://acme-staging-v02.api.letsencrypt.org/directory".
	// Only ACME v2 endpoints (i.e. RFC 8555) are supported.
	ACMEserver *string `json:"acmeServer,omitempty"`
	// ExternalDNS provider config
	// +optional
	ExternalDNSProvider ExternalDNSProviderSpec `json:"externalDNSProvider"`
	// GCPCertManagerServiceAccount is the service account for managing DNS on GCP.
	// This field is only relevant if the Domain specified is hosted in GCP.
	// +optional
	GCPCertManagerServiceAccount *string `json:"gcpCertManagerServiceAccount,omitempty"`
	// If specified and supported by the platform, this will restrict traffic through
	// the cloud-provider load-balancer will be restricted to the specified client IPs.
	// This field will be ignored if the cloud-provider does not support the feature."
	// More info: https://kubernetes.io/docs/tasks/access-application-cluster/configure-cloud-provider-firewall/
	// +optional
	FirewallSourceIPsAllowed []string `json:"firewallSourceIPsAllowed"`
}

type ExternalDNSProviderSpec struct {
	// +optional
	GCP *ExternalDNSGCPSpec `json:"gcp,omitempty"`
	// +optional
	Cloudflare *ExternalDNSCloudflareSpec `json:"cloudflare,omitempty"`
	// We can support more here... (see https://github.com/kubernetes-sigs/external-dns#the-latest-release)
}

type ExternalDNSGCPSpec struct {
	// Service account name in GCP IAM to use for managing dns zone
	DNSServiceAccountName string `json:"dnsServiceAccountName"`
}

type ExternalDNSCloudflareSpec struct {
	// Reference to a Secret object containing auth credentials for your Cloudflare DNS setup
	// (see: https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/cloudflare.md#creating-cloudflare-credentials)
	CFAuthSecret v1.LocalObjectReference `json:"cloudflareSecret"`
	// Zone ID to restrict management to a specific zone
	// +optional
	ZoneID *string `json:"zoneId,omitempty"`
}

type GitLabSpec struct {
	InstanceURL              string                  `json:"instanceUrl"`
	GroupAllowedAccess       string                  `json:"groupAllowedAccess"`
	GroupAllowedSystemAccess string                  `json:"groupAllowedSystemAccess"`
	AuthSecret               v1.LocalObjectReference `json:"authSecret"`
}

type ClusterOverridesSpec struct {
	// +optional
	ClickHouseOperator GeneralOverridesSpec `json:"clickhouseOperator"`
	// +optional
	ClickHouse ClickHouseOverridesSpec `json:"clickhouse"`
	// +optional
	JaegerOperator GeneralOverridesSpec `json:"jaegerOperator"`
	// +optional
	CertManager GeneralOverridesSpec `json:"certManager"`
	// +optional
	Cainjector GeneralOverridesSpec `json:"caInjector"`
	// +optional
	PrometheusOperator GeneralOverridesSpec `json:"prometheusOperator"`
	// +optional
	Prometheus PrometheusOverridesSpec `json:"prometheus"`
	// +optional
	RedisOperator GeneralOverridesSpec `json:"redisOperator"`
	// +optional
	Redis RedisOverridesSpec `json:"redis"`
	// +optional
	ExternalDNS GeneralOverridesSpec `json:"externalDNS"`
	// +optional
	Gatekeeper GeneralOverridesSpec `json:"gatekeeper"`
	// +optional
	NginxIngress GeneralOverridesSpec `json:"nginxIngress"`
	// +optional
	ErrorTrackingAPI GeneralOverridesSpec `json:"errorTrackingAPI"`
	// +optional
	Monitoring MonitoringOverrideSpec `json:"monitoring"`
}

type GeneralOverridesSpec struct {
	// +optional
	Components ComponentsSpec `json:"components"`
}

type PrometheusOverridesSpec struct {
	// +optional
	// +kubebuilder:pruning:PreserveUnknownFields
	PrometheusSpec runtime.RawExtension `json:"spec"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor"`
}

type MonitoringOverrideSpec struct {
	// +optional
	KubeStateMetrics KubeStateMetricsSpec `json:"kubeStateMetrics"`
}

type KubeStateMetricsSpec struct {
	// +optional
	Components ComponentsSpec `json:"components"`
}

type RedisOverridesSpec struct {
	// +optional
	Components RedisComponentsSpec `json:"components"`
}

type RedisComponentsSpec struct {
	// +optional
	Deployment RedisCRSpec `json:"deployment"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor"`
}

type RedisCRSpec struct {
	// +optional
	Labels map[string]string `json:"labels"`
	// +optional
	Annotations map[string]string `json:"annotations"`
	// +optional
	Spec *[]byte `json:"-"`
}

type ClickHouseOverridesSpec struct {
	// +optional
	Components ClickHouseComponentsSpec `json:"components,omitempty"`
}

type ClickHouseComponentsSpec struct {
	// +optional
	Service Service `json:"service,omitempty"`
	// +optional
	Deployment ClickHouseCRSpec `json:"deployment,omitempty"`
	// +optional
	ServiceMonitor ServiceMonitor `json:"serviceMonitor,omitempty"`
}

type ClickHouseCRSpec struct {
	// +optional
	Labels map[string]string `json:"labels,omitempty"`
	// +optional
	Annotations map[string]string `json:"annotations,omitempty"`
	// This spec is actually a copy of the one provided in Clickhouse operator with changes made for optional fields.
	// +optional
	Spec *ClickHouseSpec `json:"spec,omitempty"`
}

// ClusterStatus defines the observed state of Cluster.
type ClusterStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope="Cluster"
// Cluster is the Schema for the Clusters API.
type Cluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterSpec   `json:"spec,omitempty"`
	Status ClusterStatus `json:"status,omitempty"`
}

// Cluster is a cluster-scoped resource so this method returns
// the namespace for cluster resources to be deployed to.
func (c *Cluster) Namespace() string {
	n := c.Spec.Namespace
	if n == "" {
		return "default"
	}
	return n
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// ClusterList contains a list of Cluster.
type ClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Cluster `json:"items"`
}

func (t *ClusterSpec) GetHost() string {
	if t.DNS.Domain != nil {
		return *t.DNS.Domain
	}
	return "localhost"
}

func (t *ClusterSpec) GetHostURL() string {
	if t.DNS.Domain != nil {
		return fmt.Sprintf("https://%s", *t.DNS.Domain)
	}
	return "http://localhost"
}

// check if we should configure externalDNS.
func (t *ClusterSpec) UseExternalDNS() bool {
	return t.Target != common.KIND
}

// get GOUI-specific configuration.
func (t *ClusterSpec) GetGOUISpec() GOUISpec {
	return t.GOUI
}

func init() {
	SchemeBuilder.Register(&Cluster{}, &ClusterList{})
}
