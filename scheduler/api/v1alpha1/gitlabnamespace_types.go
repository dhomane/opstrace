/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"fmt"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// GitLabNamespaceSpec defines the desired state of GitLabNamespace.
type GitLabNamespaceSpec struct {
	ID int64 `json:"id"`
	// The top level namespace in GitLab
	TopLevelNamespaceID int64  `json:"top_level_namespace_id"`
	Name                string `json:"name"`
	Path                string `json:"path"`
	FullPath            string `json:"full_path"`
	AvatarURL           string `json:"avatar_url"`
	WebURL              string `json:"web_url"`
	// Set overrides for components and config
	// +optional
	Overrides TenantOverridesSpec `json:"overrides"`
	// Setup features for group and its resources
	// +optional
	Features map[string]string `json:"features,omitempty"`
}

type TenantOverridesSpec struct {
	// +optional
	Operator GeneralOverridesSpec `json:"operator"`
}

// GitLabNamespaceStatus defines the observed state of GitLabNamespace.
type GitLabNamespaceStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`
	ArgusURL   *string            `json:"argusUrl,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:scope="Cluster"

// GitLabNamespace is the Schema for the gitlabnamespaces API.
type GitLabNamespace struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GitLabNamespaceSpec   `json:"spec,omitempty"`
	Status GitLabNamespaceStatus `json:"status,omitempty"`
}

// We create a Kubernetes namespace that maps to the top level GitLab namespace.
func (c *GitLabNamespace) Namespace() string {
	return fmt.Sprint(c.Spec.TopLevelNamespaceID)
}

// Returns true if this namespace represents the toplevel/root namespace,
// i.e. this group is equivalent to the tenant.
func (c *GitLabNamespace) IsTopLevel() bool {
	return c.Spec.ID == c.Spec.TopLevelNamespaceID
}

//+kubebuilder:object:root=true

// GitLabNamespaceList contains a list of GitLabNamespace.
type GitLabNamespaceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitLabNamespace `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GitLabNamespace{}, &GitLabNamespaceList{})
}
