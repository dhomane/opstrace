# kubernetes-postgresql

Kubernetes StatefulSet manifests for PostgreSQL.

These are applied when running GOB on a KIND cluster (for local development).

Do not use this configuration for production as the credentials are hardcoded.

See more https://devopscube.com/deploy-postgresql-statefulset/.